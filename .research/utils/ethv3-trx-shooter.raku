#!/usr/bin/env raku
use v6;

use Pheix::Model::Database::Access;
use Pheix::Model::Database::Blockchain;
use Pheix::Model::Database::Blockchain::SendTx;

use URI;
use JSON::Fast;
use HTTP::Tiny;
use Node::Ethereum::KZG;
use Net::Ethereum;

constant transaction_hash = '0xd4cf74fb8d09bbe0b0e364407d0ceba2008daf4be553f1f5c269ee5ba4ee99d5';
constant storagename      = 'alchemy/holesky/v0.6.19';
constant beacon_endpoint  = 'https://eth-holesky-beacon.public.blastapi.io';
#constant storagename     = 'holesky_storage';
#constant beacon_endpoint = 'https://eth-beacon-chain-holesky.drpc.org/rest/';

multi MAIN (
    Str:D :$blobpath!,        #= Path to local file to be sent as blob
    Bool  :$annotate = False, #= Add endpoint to transaction input data as annotation
) returns UInt
{
    die sprintf("invalid blob path <%s>", $blobpath) unless $blobpath.chars && $blobpath.IO && $blobpath.IO.f;

    my $holesky = Pheix::Model::Database::Access.new(
        :table(storagename),
        :fields([]),
        :debug(True)
    );

    die '**PANIC: can not continue without signer' unless
        $holesky.chainobj.sgnobj && $holesky.chainobj.sgnobj ~~ Pheix::Model::Database::Blockchain;

    $holesky.chainobj.sgnobj.config = {
        keystore => sprintf("%s/Documents/auth/EthPheixTest-acc/%s", $*HOME, 'UTC--2023-05-28T10-41-36.248Z--224b48cafcb5d1c73e4e4494cf546dd3c37f1019');
    };

    $holesky.chainobj.ethobj.keepalive = False;

    my $uri               = URI.new($holesky.chainobj.apiurl);
    my $blob_from_captcha = $blobpath.IO.slurp(:bin);

    die '**FAILURE: blob transaction failed' unless
        $holesky.chainobj.set_blob_data(
            :blobs([$blob_from_captcha]),
            :data($annotate ?? {endpoint => sprintf("%s://%s:%d", $uri.scheme, $uri.host, $uri.port)} !! {})
        )<status>;

    return 1;
}

multi MAIN (
    Str:D :$trxhash!,      #= Transaction hash to get blob from on Holešky test network
    UInt  :$hashindex = 0, #= Versioned hash index in EIP-4844 transaction, default <0>
    Str   :$extension      #= Extension for file to save blob to, default <bin>
) returns UInt
{
    die sprintf("invalid transaction hash <%s>", $trxhash) unless $trxhash ~~ m:i/^ 0x<xdigit>**64 $/;

    my $holesky = Pheix::Model::Database::Access.new(
        :table(storagename),
        :fields([]),
        :debug(True)
    );

    my $blob = $holesky.chainobj.get_blob_data(:$trxhash, :$hashindex);

    sprintf("./blob-%s-%s.%s", $trxhash, DateTime.now(formatter => {
        sprintf(
            "%04d%02d%02d-%02d:%02d:%02u.%04u",
            .year,
            .month,
            .day,
            .hour,
            .minute,
            .second.Int,
            (.second % (.second.Int || .second)) * 10_000
        )
    }), $extension && $extension ~~ m:i/^ <[a..z]>+ $/ ?? $extension !! 'bin').IO.spurt($blob, :bin);

    return 1;
}
