#/usr/bin/env raku

use JSON::Fast;

my $cumulative_content;
my $cumulative_file = sprintf("%s/%s-sgn-%s.sources", $*CWD, $*PROGRAM.basename, DateTime.now(formatter => {
    sprintf(
        "%04d%02d%02d-%02d:%02d:%02u.%04u",
        .year,
        .month,
        .day,
        .hour,
        .minute,
        .second.Int,
        (.second % (.second.Int || .second)) * 10_000
    )
}));

my @base_dirs  = <bin lib sol t utils>;
my @extensions = <raku rakumod t js sh bash sol>;

my $projects = [
    '/home/kostas/git/pheix-research/smart-contracts',
    '/home/kostas/git/pheix-research/raku-dossier-prototype',
    '/home/kostas/git/dcms-raku',
    '/home/kostas/git/raku-ethelia'
];

my $stats = {
    total => {
        files => 0,
        size  => 0
    },
};

for $projects.values -> $project {
    my $meta6 = sprintf("%s/META6.json", $project);

    if $meta6.IO.e && $meta6.IO.f {
        my $decoded = from-json($meta6.IO.slurp);

        $stats{$project}<version> = $decoded<version>;
    }

    for $project {
        my $filename = .Str;

        (my $reproj = $project) ~~ s:g/\//\\\//;
        $reproj ~~ s:g/\-/\\\-/;

        collect(:$project, :$filename) when !.IO.d && .IO.extension ~~ any @extensions;
        .IO.dir()».&?BLOCK when .IO.d &&
            (
                .IO.path eq $project ||
                .IO.path ~~ /^<{sprintf("%s\\\/bin", $reproj)}>/ ||
                .IO.path ~~ /^<{sprintf("%s\\\/lib", $reproj)}>/ ||
                .IO.path ~~ /^<{sprintf("%s\\\/sol", $reproj)}>/ ||
                .IO.path ~~ /^<{sprintf("%s\\\/t", $reproj)}>/   ||
                .IO.path ~~ /^<{sprintf("%s\\\/utils", $reproj)}>/
            )
    }
}

sprintf("collect sources from projects:\n\t%s\nstatistics:\n%s", $projects.join("\n\t"), to-json($stats)).say;

sub collect(Str :$project, Str :$filename!) returns Bool {
    return False unless $filename.IO.e && $filename.IO.f;
    return False if $filename eq '/home/kostas/git/pheix-research/smart-contracts/t/PheixDatabase/txt/datasets.js';

    my $size = ($filename.IO.s / 1000);

    sprintf("%.3f KB : %s", $size, $filename).say;

    $stats<total><size>  += $size;
    $stats<total><files> += 1;

    $stats{$project}<size>  += $size;
    $stats{$project}<files> += 1;

    $cumulative_content ~= sprintf("%s", $filename.IO.slurp);

    return True;
}

$cumulative_file.IO.spurt($cumulative_content) if $cumulative_content && $cumulative_content.chars;
