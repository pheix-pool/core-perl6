#!/usr/bin/env raku
use v6;

use Pheix::Datepack;

constant rootpath   = './';
constant outputname = sprintf("./source-compilcation-%s.txt", Pheix::Datepack.new.get_date_logging);
constant nocomments = @*ARGS[0].defined && @*ARGS[0] == 1 ?? True !! False;;

my @files;
my @root = rootpath.IO;

while @root {
    for @root.pop.dir -> $path {
        if $path.f && $path.Str ~~ /\.(raku|rakumod|t)$/ {
            @files.push($path.Str);
        }

        @root.push: $path if $path.d;
    }
}

for @files.sort -> $file {
    my $content;

    if nocomments {
        $content = $file.IO.slurp.lines.map({ $_ if $_ !~~ /^(<[\s]>+'#')|('#')/}).join("\n") ~ "\n";
    }
    else {
        $content = $file.IO.slurp;
    }

    if outputname.IO.e {
        my $fh = open outputname, :a;

        next unless $fh;

        $fh.print($content);
        $fh.close;
    }
    else {
        outputname.IO.spurt($content)
    }
}

sprintf("Source is saved to %s", outputname).say;
