unit class Pheix::Utils;

use MIME::Base64;
use OpenSSL::CryptTools;
use OpenSSL::Digest;
use experimental :pack;
use Pheix::Model::JSON;

use NativeCall;
use MagickWand;
use MagickWand::Enums;
use MagickWand::NativeCall;
use MagickWand::NativeCall::Wand;
use MagickWand::NativeCall::Image;
use MagickWand::NativeCall::DrawingWand;
use MagickWand::NativeCall::PixelWand;

has Pheix::Model::JSON $.jsonobj = Pheix::Model::JSON.new;

has Bool $.test  = False;
has Str  $!path  = $!test ?? './www/' !! './';
has Str  $.smfn  = $!path ~ 'sitemap_static.xml';
has Str  $.font  = $!path ~ 'resources/skins/akin/css/fonts/georgia.ttf';
has Int  $.lzwsz = 97000;
has @!profile;

has MIME::Base64 $!base = MIME::Base64.new;

my sub calloc(size_t, size_t) returns Pointer is native { * }

method stringify(:$data!) {
    my $structure = $data.clone;

    X::AdHoc.new(:payload(sprintf("Unable to stringify structure %s", $structure.^name))).throw unless
        $structure ~~ Hash || $structure ~~ List || $structure ~~ Array;

    if $structure ~~ Hash {
        for $structure.kv -> $key, $value {
            if $value ~~ Hash || $value ~~ List || $value ~~ Array {
                $structure{$key} = self.stringify(:data($value));
            }
            else {
                $structure{$key} = $value.Str;
            }
        }
    }
    else {
        for $structure.kv -> $key, $value {
            if $value ~~ Hash || $value ~~ List || $value ~~ Array {
                $structure[$key] = self.stringify(:data($value));
            }
            else {
                $structure[$key] = $value.Str;
            }
        }
    }

    return $structure;
}

method get_token_by_now returns Str {
    my Blob[uint8] $token = now.to-posix.head.Str.encode;

    return sprintf("0x%s", sha384($token).map(*.base(16)).join.lc);
}

method set_smfn(Str :$fname!, Bool :$force) returns Bool {
    my Bool $ret = False;

    if $fname.IO.e || $force {
        $!smfn = $fname;
        $ret   = True;
    }

    $ret;
}

method get_hex( Str $s ) returns Str {
    my Str $ret = Nil;
    if $s {
        for $s.split(q{},:skip-empty) -> $char {
            if $char {
                my $hex = q{%} ~ sprintf "%02x", ord($char);
                $ret ~= $hex;
            }
        }
    }
    $ret;
}

method get_str( Str $hex ) returns Str {
    my Str $ret = Nil;
    if $hex {
        if $hex ~~ m:i/ [ '%' <[\da..f]> ** 1..2 ]+ / {
            for $hex.split(q{%},:skip-empty) -> $char {
                if $char {
                    my $c = chr( :16($char) );
                    $ret ~= $c;
                }
            }
        }
        else {
            $ret = $hex;
        }
    }
    $ret;
}

method show_captcha(Str $value is copy, Bool :$darkmode = False) returns Str {
    $value ~~ s:g/^ '?' // if $value ~~ /^ \? /;

    my $image = self.get_captcha($value, :$darkmode);

    return q{} unless $image;

    my @list;
    my $len = calloc(1, 1);

    MagickResetIterator($image.handle);
    MagickSetImageFormat($image.handle, 'png');

    my $ptr = MagickGetImageBlob($image.handle, $len);

    my $data_length = nativecast(Int, $len);
    my $data = nativecast(CArray[byte], $ptr);

    for ^($data_length) {
        my UInt $byte;
        if $data[$_] >= 0 {
            $byte = $data[$_];
        }
        else {
            $byte = 256 + $data[$_];
        }
        @list.push($byte);
    };

    my $imgbuf = Buf[uint8].new(@list);

    ('data:image/png;base64,' ~ $!base.encode($imgbuf, :oneline) // q{});
}

method get_captcha(Str $val, Bool :$darkmode = False) returns MagickWand {
    my Str  $d_cv;
    my MagickWand $ret;
    my UInt $k = 5;
    my UInt $i = $!test ?? 1 !! 2;
    my %p =
        densty => 72 * $k,
        font   => $!font,
        bgclr  => 'white',
        fntclr => 'grey',
        fntsz  => 28,
        width  => 88 * $k,
        height => 31 * $k,
        x_crd  => 1 * $k,
        y_crd  => 34 * $k,
        angle  => -15,
        swirl  => 25,
        capt   => sprintf("%06d", (rand * 10**6).floor),
    ;
    try {
        $d_cv = self.do_decrypt($val);
        CATCH {
            default {
                die 'do_decrypt error: ' ~ (.Str) ~  ', val: ' ~ $val ~ "\n" ~
                    'Source: ' ~ &?ROUTINE.name ~ ' at line '  ~
                    .backtrace[1].line ~ "\n";
            }
        }
    }
    if $d_cv {
        if $d_cv ~~ m:i/^ <[\d\:]>+ $/ {
            %p<capt> = $d_cv.split(q{:}, :skip-empty).head.substr(0, 6);
        }
    }

    try {
        $ret = MagickWand.new(:d_handle(NewDrawingWand));
        $ret.create(%p<width>, %p<height>, %p<bgclr>);

        DrawSetDensity($ret.d_handle, %p<densty> ~ q{,} ~ %p<densty>);
        DrawSetFontSize($ret.d_handle, %p<fntsz>.Num);
        DrawSetFont($ret.d_handle, %p<font>);
        PixelSetColor($ret.p_handle, %p<fntclr>);
        DrawSetFillColor($ret.d_handle, $ret.p_handle);

        for ^$i {
            $ret.annotate(%p<x_crd>, %p<y_crd>, %p<angle>, %p<capt>);
            $ret.swirl(%p<swirl>);
        }

        my Str $composite =
            $!jsonobj.get_setting('Pheix', 'captcha_composite_op', sprintf("%s%s", $darkmode ?? 'dark' !! 'light', 'mode'));

        if $composite && (my $operator = CompositeOperator.pick(*).grep({$_.Str eq $composite}).head) {
            my Str $distortion_image =
                $!jsonobj.get_setting('Pheix', 'captcha_composite_op', 'distortionimage');

            if $distortion_image && $distortion_image.IO.e && $distortion_image.IO.f {
                my $distortion = MagickWand.new;

                $distortion.read($distortion_image);
                $distortion.resize((%p<width>/$distortion.width));
                $distortion.crop(0, 70, %p<width>, %p<height>);

                MagickSetImageOpacity($distortion.handle, (0.5).Num);
                MagickCompositeImage($ret.handle, $distortion.handle, $operator, 0, 0);

                $distortion.cleanup;
            }
        }

        CATCH {
            default {
                die 'MagickWand error: ' ~ (.Str) ~ "\n" ~
                    'Source: ' ~ &?ROUTINE.name ~ ' at line '  ~
                    .backtrace[1].line ~ "\n";
            }
        }
    }

    $ret;
}

method gen_cipher_param(Str $pname!) returns Str {
    my Str $ret = Nil;

    X::AdHoc.new(:payload('undefined JSON object on private key fetch')).throw unless $!jsonobj.defined;

    my $key = $!jsonobj.get_setting('Pheix', 'privatekey', 'value');

    X::AdHoc.new(:payload('no generic-purpose private key')).throw unless $key && $key ne q{};

    if $pname eq 'iv' {
        my @f0 = $key.split(q{},:skip-empty);
        my @f1 = @f0.map({ $_ if $_ ~~ m:i/<[\d]>/ });

        for @f1 {
            $ret ~= @f0[$_];
        }
    }
    else {
        $ret = $key;
    }

    $ret;
}

method get_files(Str $d) returns Seq {
    my Seq $ret = Nil;

    if $d {
        if $d.IO.e {
            $ret = $d.IO.dir.map({ $_ if $_ ~~ :f });
        }
    }

    $ret;
}

method do_encrypt(Str $s) returns Str {
    my Str $ret = Nil;

    if $s {
        my Str $iv  = self.gen_cipher_param('iv');
        my Str $key = self.gen_cipher_param('key');
        my Buf $crt = encrypt(
            $s.encode, :aes256, :iv($iv.encode), :key($key.encode)
        );

        if $crt {
            my Str $c = $crt
                .unpack("A*")
                .split(q{},:skip-empty)
                .map({ q{%} ~ sprintf "%02x", ord($_) if $_ })
                .join(q{});
            if $c ~~ m:i/ <[\da..f\%]>+ / {
                $ret = $c;
            }
        }
    }

    $ret;
}

method do_decrypt(Str $c) returns Str {
    my Str $ret = Nil;

    if $c {
        if $c ~~ m:i/^ <[\da..f%]>+ $/ {
            my Str $iv  = self.gen_cipher_param('iv');
            my Str $key = self.gen_cipher_param('key');
            my @l = $c
                .split(q{%}, :skip-empty)
                .map({
                    :16($_) if $_ ~~ m:i/^ <[\da..f]> ** 1..2 $/
                });
            if @l {
                my Buf $b = buf8.new(@l);
                my Buf $plain =
                    decrypt(
                        $b,
                        :aes256,
                        :iv($iv.encode),
                        :key($key.encode),
                    );
                if $plain {
                    $ret = $plain.decode;
                }
            }
        }
    }

    $ret;
}

method add_profile_data(Str :$msg!, UInt :$initial) {
    if $!test {
        my $prev = (@!profile.tail)<time>:exists ??
            (@!profile.tail)<time> !!
                0;

        @!profile.push(
            {
                elapsed => $initial // now.Rat - $prev,
                msg  => $msg,
                time => now.Rat,
            }
        );
    }
}

method print_profile_stats {
    if $!test {
        my $total = 0;
        for @!profile -> $p {
            $total += $p<elapsed>;
            say sprintf("%32s: %f %f", $p<msg>, $p<elapsed>, $total);
        }
    }
}
