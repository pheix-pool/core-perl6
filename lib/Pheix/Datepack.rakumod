unit class Pheix::Datepack;

use Pheix::Utils;
use Pheix::Model::Resources;

has DateTime $.date;
has UInt     $.unixtime;

method format_update returns Str {
    my Str $ret;

    if $!date {
        my $_robj = Pheix::Model::Resources.new.init;
        $ret =
            $!date.day ~ q{ } ~
            $_robj.months{$!date.month} ~
            ', ' ~ $!date.year;
    }

    return $ret;
}

method year_update returns Str {
    my Str $ret;

    $ret = $!date.year.Str if $!date;

    return $ret;
}

method hex_unixtime returns Str {
    my Str $ret;

    $ret = Pheix::Utils.new.get_hex($!unixtime.Str) if $!unixtime && $!unixtime > 0;

    return $ret;
}

method get_http_response_date returns Str {
    my Str $ret;

    if $!unixtime and $!date {
        my $_robj = Pheix::Model::Resources.new.init;
        my $_dobj = DateTime.new($!unixtime);

        $ret = sprintf(
            "%s, %02d %s %04d %02d:%02d:%02d GMT",
            $_robj.weekd_reduced{$!date.day-of-week},
            $!date.day,
            $_robj.months_reduced{$!date.month},
            $!date.year,
            $_dobj.hour,
            $_dobj.minute,
            $_dobj.second,
        );
    }

    return $ret;
}

method convert_http_response_date_to_unixtime(Str :$date) returns UInt {
    if $date &&
       $date ~~ /^<:alpha> ** 3 ',' \s (<[\d]>+) \s (<:alpha> ** 3) \s (<[\d]> ** 4) \s (<[\d]> ** 2 ':' <[\d]> ** 2 ':' <[\d]> ** 2)  \s 'GMT'$/
    {
        my $day       = $0.UInt;
        my $monthname = $1.Str;
        my $year      = $2.UInt;
        my $time      = $3.Str;

        my $month = <Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec>.first($monthname, :k).UInt;

        X::AdHoc.new(:payload(sprintf("wrong month name %s", $monthname))).throw unless $month.defined;

        my $isodate = sprintf("%04d-%02d-%02dT%sZ", $year, $month + 1, $day, $time);

        return DateTime.new($isodate).posix;
    }

    return 0;
}

method get_date_filename(Str :$prefix, Str :$extension) returns Str {
    my $dobj = $!date // DateTime.new($!unixtime // now);
    my $robj = Pheix::Model::Resources.new.init;

    sprintf(
        "%s-%s-%02d-%s-%04d-%02d-%02d-%02d.%s",
        $prefix || 'dump',
        $robj.weekd_reduced{$dobj.day-of-week},
        $dobj.day,
        $robj.months_reduced{$dobj.month},
        $dobj.year,
        $dobj.hour,
        $dobj.minute,
        $dobj.second,
        $extension || 'log'
    );
}

method get_date_logging returns Str {
    my $dobj = $!date // DateTime.new($!unixtime // now);
    my $robj = Pheix::Model::Resources.new.init;

    return sprintf(
        "%04d-%02d-%02d %02d:%02d:%02u.%04u",
        $dobj.year,
        $dobj.month,
        $dobj.day,
        $dobj.hour,
        $dobj.minute,
        $dobj.second.Int,
        ($dobj.second % ($dobj.second.Int || $dobj.second)) * 10_000
    );
}
