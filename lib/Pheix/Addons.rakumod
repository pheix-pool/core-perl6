unit class Pheix::Addons;

use Pheix::Model::JSON;

has Bool             $.test = False;
has Pheix::Model::JSON $!jo = Pheix::Model::JSON.new;

method get_addons returns Hash {
    my %ret;

    my %addonscnf = $!jo.get_all_settings_for_group_member(
        'Pheix',
        'addons',
        'installed'
    );

    for %addonscnf.values -> $v {
        next unless $v && $v<addon>;

        my Str $addon = $v<addon>;

        try {
            require ::($addon);

            CATCH {
                default {
                    my $e = .message;
                    my $b = .backtrace;

                    X::AdHoc.new(:payload(
                        sprintf(
                            "Required addon %s is failed at %s\:\:%s\n***ERROR:\n%s**BACKTRACE:\n%s%s",
                            $addon,
                            self.^name,
                            &?ROUTINE.name,
                            $e,
                            $b,
                            ('-=' xx 20).join
                        );
                    )).throw;
                }
            }
        }

        require ::($addon);

        my %addondets;
        my $jsonobj;

        if $v<config> && $v<config>.IO.e {
            %addondets<objct> = ::($addon).new(:confpath($v<config>));
            $jsonobj          = %addondets<objct>.jsonobj;
        }
        else {
            $jsonobj          = $!jo;
            %addondets<objct> = ::($addon).new;
        }

        %addondets<aname> = %addondets<objct>.get_name;
        %addondets<exten> = $v<extension> && +$v<extension> ?? True !! False;
        %addondets<confr> =
            $jsonobj.get_all_settings_for_group_member(
                %addondets<objct>.get_name,
                'routing',
                'routes'
            );

        if (%addondets<confr>:exists) && (%addondets<objct>:exists) {
            %ret{$addon} = %addondets;
        }
    }

    %ret;
}
