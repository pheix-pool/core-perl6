unit class Pheix::Model::Resources::En;

has Bool $.test = False;
has Str  $.parent is rw;
has Str  $.locale is rw;
has Str  $.render_mess_prefix  = 'render time: ';
has Str  $.render_mess_postfix = ' seconds';
has Str  $.welcome       = 'Welcome to Pheix!';
has Str  $.helloworld    = 'Hello, World! It&apos;s works ;)';
has Str  $.error         = 'Something went wrong :(';

has %.vocabulary =
    error => 'Error',
    usrcnterror => 'User content is missed',
    storeok     => 'Data is successfully stored',
;

has %.months =
    1  => 'January',
    2  => 'February',
    3  => 'March',
    4  => 'April',
    5  => 'May',
    6  => 'June',
    7  => 'July',
    8  => 'August',
    9  => 'September',
    10 => 'October',
    11 => 'November',
    12 => 'December'
;

has %.months_reduced =
    1  => 'Jan',
    2  => 'Feb',
    3  => 'Mar',
    4  => 'Apr',
    5  => 'May',
    6  => 'Jun',
    7  => 'Jul',
    8  => 'Aug',
    9  => 'Sep',
    10 => 'Oct',
    11 => 'Nov',
    12 => 'Dec'
;

has %.weekd_reduced =
    1 => 'Mon',
    2 => 'Tue',
    3 => 'Wed',
    4 => 'Thu',
    5 => 'Fri',
    6 => 'Sat',
    7 => 'Sun'
;

has %.http_codes =
    400 => 'Bad Request',
    401 => 'Unauthorized',
    402 => 'Payment Required',
    403 => 'Forbidden',
    404 => 'Not Found',
    405 => 'Method Not Allowed',
    413 => 'Payload Too Large',
    429 => 'Too Many Requests'
;
