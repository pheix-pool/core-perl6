unit class Pheix::Model::Database::Blockchain::Signatures;

use Bitcoin::Core::Secp256k1;
use Node::Ethereum::Keccak256::Native;

has Str  $.privatekey is default(q{});
has Bool $.crasherr  is default(True);
has Any  $.netethobj is default(Nil);
has Any  $!signerobj = Bitcoin::Core::Secp256k1.new;

method sign(Str :$message!) {
    return q{} unless $!privatekey ne q{} && $!netethobj && $message ne q{};

    my $messtosign = Node::Ethereum::Keccak256::Native.new.keccak256(:msg($message));

    (my $msg = $!netethobj.buf2hex($messtosign)) ~~ s/^'0x'//;

    my $publickey = $!signerobj.create_public_key(:privkey($!privatekey));
    my $signature = $!signerobj.ecdsa_sign(:privkey($!privatekey), :msg($msg), :recover(True));
    my $converted = $!signerobj.recoverable_signature_convert(:sig($signature));

    if $!crasherr && !$!signerobj.ecdsa_recover(:pubkey($publickey), :msg($msg), :sig($signature)) {
        X::AdHoc.new(:payload(sprintf("cannot recover signature:\n\t0x%s\n\t%s (%d bytes)\n\t%s (%s bytes)", $msg, $!netethobj.buf2hex($signature), $signature.bytes, $!netethobj.buf2hex($converted), $converted.bytes))).throw;
    }

    return $!netethobj.buf2hex($signature);
}

method validate(Str :$message!, Str :$signature!) returns Bool {
    return False unless $!privatekey ne q{} && $!netethobj && $signature ~~ m/^'0x'/ && $message ne q{};

    my $publickey = $!signerobj.create_public_key(:privkey($!privatekey));
    my $sigbuffer = $!netethobj.hex2buf($signature);

    return False unless $sigbuffer.bytes;

    (my $msg = $!netethobj.buf2hex(Node::Ethereum::Keccak256::Native.new.keccak256(:msg($message)))) ~~ s/^'0x'//;


    if $!signerobj.verify_ecdsa_sign(:pubkey($publickey), :msg($msg), :sig($sigbuffer)) {
        return True;
    }

    return False;
}
