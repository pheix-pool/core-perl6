unit class Pheix::Model::Database::Blockchain::SendTx;

use Bitcoin::Core::Secp256k1;
use Node::Ethereum::Keccak256::Native;
use Node::Ethereum::KeyStore::V3;
use Node::Ethereum::RLP;
use Node::Ethereum::KZG;
use Pheix::Datepack;
use Net::Ethereum;

use Pheix::Test::Blockchain;
use Pheix::Test::BlockchainComp::Helpers;
use Pheix::Model::Database::Blockchain::EIP4844;

has Rat  $.gasprmult is default(1.33);
has Any  $.targetobj is default(Nil);
has Any  $.signerobj is default(Nil);
has Bool $.debug is default(False);
has      $.diag  is default(sub diag(Str:$m){$m.say});
has Str  $.trusted_setup_filepath = sprintf("%s/git/raku-node-ethereum-kzg/c-kzg-4844/src/trusted_setup.txt", $*HOME);

has $!eip4844    = Pheix::Model::Database::Blockchain::EIP4844.new(:$!trusted_setup_filepath, :config($!targetobj.config), :$!debug);
has $!keystorev3 = Node::Ethereum::KeyStore::V3.new;
has $!keccak     = Node::Ethereum::Keccak256::Native.new;

method eip4844 returns Pheix::Model::Database::Blockchain::EIP4844 {
    return $!eip4844;
}

method send_signed_tx(
    Str  :$method!,
    Hash :$data,
    UInt :$txgas,
    Bool :$waittx,
         :@blobs,
) returns Str {
    my Str $tx;

    my $type  = $!signerobj ?? 'signed' !! 'native';
    my $mdata = @blobs && @blobs.elems && $data && $data.pairs ??
        $!targetobj.ethobj.string2hex($data.gist) !!
            $!targetobj.ethobj.marshal($method, $data);

    if $!signerobj {
        $!diag(:m(sprintf("**SIGN: marshalled data:\n%s", $mdata))) if $!debug;

        my UInt $local_nonce = $!targetobj.nonce;

        my $rawtx = self.sign(
            :marshalledtx($mdata),
            :gasqty($txgas),
            :$waittx,
            :t(($data<tabname>:exists) ?? $data<tabname> !! Str),
            :@blobs
        );

        try {
            $tx = self.send(:$rawtx);

            CATCH {
                default {
                    $!targetobj.nonce = $local_nonce;
                    $!targetobj.sgnlog.pop;

                    Pheix::Test::BlockchainComp::Helpers
                        .new(:tstobj(Pheix::Test::Blockchain.new))
                        .flush_signing_session(
                            :sgnlog($!targetobj.sgnlog),
                            :filepostfix('-exception-report')
                        );

                    .throw;
                }
            }
        }
    }
    else {
        my $gas   = $txgas || $!targetobj.gasqty;
        my $nonce = $!targetobj.ethobj.eth_getTransactionCount(:data($!targetobj.ethacc), :tag('pending'));

        $tx = $!targetobj.ethobj.sendTransaction(
            :account($!targetobj.ethacc),
            :scid($!targetobj.scaddr),
            :fname($method),
            :fparams($data),
            :$gas,
        );

        self.push2log(:$nonce, :$gas, :data($mdata));
    }

    $!targetobj.sgnlog.tail ~= sprintf("|%s|%12s|%7s", $tx, $method, $type);

    if $!debug {
        $!diag(:m(sprintf("**TLOG: %s", $!targetobj.sgnlog.tail)));
        $!diag(:m(sprintf("**SIGN: sent %s tx %s", $type, $tx)));
    }

    return $tx;
}

method selfcheck returns Bool {
    $!targetobj.dbswitch == 1 && $!signerobj.dbswitch == 1 ??
        True !!
            False;
}

method sign(
    Str  :$marshalledtx!,
    Str  :$blocktag = 'pending',
    UInt :$gasqty,
    Bool :$waittx = False,
    Str  :$t,
         :@blobs,
) returns Str {
    my %sign;
    my %kzgdata;
    my Str $noncedetails;

    $!diag(:m(sprintf("**SIGN: try to sign tx for %s", $t // $!targetobj.table))) if $!debug;

    # get nonce
    my UInt $targetnonce = $!targetobj.nonce;
    my UInt $nc = $!targetobj.ethobj.eth_getTransactionCount(
        :data($!targetobj.ethacc),
        :tag($blocktag)
    ) if $waittx || ($targetnonce.defined && $targetnonce == 0);

    if $nc.defined && $nc > $targetnonce {
        $!diag(:m(sprintf("**SIGN: got nonce %u (local=%u), block=%s", $nc, $targetnonce, ~$blocktag))) if $!debug;

        $targetnonce = $nc;
    }
    else {
        $!diag(:m(sprintf("**SIGN: gen nonce %u (%s)", $targetnonce, $waittx.Str))) if $!debug;

        $nc = $nc.defined && $nc == 0 ?? 0 !! (++$targetnonce);
    }

    $!diag(:m(sprintf("**SIGN: nonce local=%u, nonce remote=%u", $targetnonce, $nc))) if $!debug;

    my %trx =
        from     => $!targetobj.ethacc,
        to       => $!targetobj.scaddr,
        gas      => $gasqty // $!targetobj.gasqty,
        nonce    => $nc,
        data     => $marshalledtx;

    my $fees = $!targetobj.ethobj.get_fee_data;

    X::AdHoc.new(:payload('***ERR: no EIP-1559 fees are calculated')).throw unless $fees<maxFeePerGas> && $fees<maxPriorityFeePerGas>;

    %trx<maxfeepergas>         = $fees<maxFeePerGas>;
    %trx<maxpriorityfeepergas> = $fees<maxPriorityFeePerGas>;

    $!diag(:m(sprintf("**SIGN: EIP-1559 maxfeepergas=%d, maxpriorityfeepergas=%d", %trx<maxfeepergas>, %trx<maxpriorityfeepergas>))) if $!debug;

    if @blobs && @blobs.elems {
        %kzgdata = $!eip4844.kzg_commitment_data(:@blobs, :$fees);

        %trx<maxfeeperblobgas>   = %kzgdata<maxfeeperblobgas>;
        %trx<kzgversionedhashes> = %kzgdata<kzgversionedhashes>;
    }

    if $!signerobj.config<keystore> && $!signerobj.config<keystore>.IO.f {
        X::AdHoc.new(:payload('***ERR: no keystore password')).throw unless $!signerobj.ethobj.unlockpwd;

        $!keystorev3.keystorepath = $!signerobj.config<keystore>;

        %sign = self.sign_transaction(
            :trx(%trx),
            :privatekey($!keystorev3.decrypt_key(:password($!signerobj.ethobj.unlockpwd))),
            :%kzgdata
        );

        $!diag(:m('**SIGN: transaction is manually signed')) if $!debug;
    }
    else {
        $!diag(:m(sprintf("**SIGN: try to unlock %s addr at %s", $!signerobj.ethacc, $!signerobj.apiurl))) if $!debug;

        $!signerobj.ethobj.personal_unlockAccount(
            :account($!signerobj.ethacc),
            :password($!signerobj.ethobj.unlockpwd),
        );

        %sign = $!signerobj.ethobj.eth_signTransaction(|%trx);
    }

    # logging
    self.push2log(:nonce(%trx<nonce>), :gas(%trx<gas>), :data(%trx<data>));

    $!targetobj.nonce = $targetnonce;

    (%sign<raw>:exists && %sign<raw> ~~ m:i/^ 0x<xdigit>+ $/) ??
        %sign<raw> !!
            Str;
}

method send(Str :$rawtx!) returns Str {
    if $!debug {
        $!diag(:m(sprintf("**SIGN: try to send signed transaction with %d bytes to %s", $rawtx.encode.bytes, $!targetobj.apiurl)));
        # $!diag(:m(sprintf("**SIGN: raw transaction data\n%s", $rawtx)));
    }

    $!targetobj.ethobj.eth_sendRawTransaction(:data($rawtx));
}

method sign_transaction(:%trx!, Hash :$privatekey!, :%kzgdata) returns Hash {
    return {} unless $privatekey.keys && $privatekey<str> && $privatekey<buf8>;

    my $transaction = Hash.new(%trx);
    my @trx_fields  = <chain_id nonce max_priority_fee_per_gas max_fee_per_gas gas_limit destination amount data access_list max_fee_per_blob_gas blob_versioned_hashes>;

    my $rlp = Node::Ethereum::RLP.new;

    $transaction<value>                = sprintf("0x%x", $transaction<value> // 0);
    $transaction<nonce>                = sprintf("0x%x", $transaction<nonce> // 0);
    $transaction<maxpriorityfeepergas> = sprintf("0x%x", $transaction<maxpriorityfeepergas>);
    $transaction<maxfeepergas>         = sprintf("0x%x", $transaction<maxfeepergas>);
    $transaction<gas>                  = sprintf("0x%x", $transaction<gas>);

    my $signdata;

    $signdata<chain_id>    = $rlp.int_to_hex(:x($!targetobj.ethobj.net_version));
    $signdata<destination> = $transaction<to>;
    $signdata<data>        = $transaction<data>;
    $signdata<nonce>                    = %trx<nonce> ?? $rlp.int_to_hex(:x(%trx<nonce>)) !! 0;
    $signdata<max_priority_fee_per_gas> = $rlp.int_to_hex(:x(%trx<maxpriorityfeepergas>));
    $signdata<max_fee_per_gas>          = $rlp.int_to_hex(:x(%trx<maxfeepergas>));
    $signdata<gas_limit>                = $rlp.int_to_hex(:x(%trx<gas>));

    $signdata<amount>               = $rlp.int_to_hex(:x(%trx<value>)) if %trx<value>;
    $signdata<max_fee_per_blob_gas> = $rlp.int_to_hex(:x(%trx<maxfeeperblobgas>)) if %trx<maxfeeperblobgas>;

    # $signdata.gist.say;

    my @raw;

    for @trx_fields -> $field {
        if $field === 'max_fee_per_blob_gas' || $field === 'blob_versioned_hashes' {
            next unless %kzgdata && %kzgdata.keys;
        }

        if $field === 'access_list' {
            @raw.push(self.access_list(:$transaction));

            next;
        }

        if $field === 'blob_versioned_hashes' {
            X::AdHoc.new(:payload('***ERR: no versioned hashes in KZG data for blob transaction')).throw unless
                %kzgdata<kzgversionedhashes> && %kzgdata<kzgversionedhashes>.elems;

            @raw.push(%kzgdata<kzgversionedhashes>);

            next;
        }

        my $data = $signdata{$field} ??
            buf8.new(($signdata{$field}.Str ~~ m:g/../).map({ :16($_.Str) if $_ ne '0x' })) !!
                buf8.new();

        @raw.push($data);
    }

    my $rlptx;
    my $txtype = 2;

    if %kzgdata && %kzgdata.keys {
        X::AdHoc.new(:payload('***ERR: no blobs, commitments or proofs in KZG data for blob transaction')).throw unless
            %kzgdata<blobs>          && %kzgdata<blobs>.elems          &&
            %kzgdata<kzgcommitments> && %kzgdata<kzgcommitments>.elems &&
            %kzgdata<kzgproofs>      && %kzgdata<kzgproofs>.elems;

        my $versioned_hashes = %kzgdata<kzgversionedhashes>.map({$!targetobj.ethobj.buf2hex($_)}).join(', ');

        $!diag(:m(sprintf("**SIGN: EIP-4844 transaction with %d blobs, versioned hashes: %s", %kzgdata<blobs>.elems, $versioned_hashes))) if $!debug;

        $txtype = 3;
    }

    $rlptx = $rlp.rlp_encode(:input(@raw));

    $!diag(:m(sprintf("**SIGN: transaction type is %s", $txtype == 2 ?? 'EIP-1559' !! 'EIP-2718/EIP-4844'))) if $!debug;

    (my $hash = $!targetobj.ethobj.buf2hex($!keccak.keccak256(:msg($rlptx.unshift($txtype)))).lc) ~~ s:g/ '0x' //;

    my $secp256k1  = Bitcoin::Core::Secp256k1.new;
    my $signature  = $secp256k1.ecdsa_sign(:privkey($privatekey<str>), :msg($hash), :recover(True));
    my $serialized = $secp256k1.recoverable_signature_serialize(:sig($signature));

    if $!debug {
        my $pubkey    = buf8.allocate(64, 0xff);
        my $parsedsig = $secp256k1.recoverable_signature_parse_compact(:sig($serialized<signature>), :recovery($serialized<recovery>));

        X::AdHoc.new(:payload('***ERR: signatures mismatch')).throw unless $parsedsig == $signature;

        $secp256k1.ecdsa_recover(:$pubkey, :sig($parsedsig), :msg($hash));

        my $cmppubkey  = $secp256k1.compressed_public_key(:$pubkey, :cmp(False));
        my $senderaddr = $!targetobj.ethobj.buf2hex($!keccak.keccak256(:msg($cmppubkey.subbuf(1, *))).subbuf(*-20));

        $!diag(:m(sprintf("**SIGN: recovered sender address %s", $senderaddr.lc)));
    }

    my $y = $serialized<recovery> ?? $rlp.int_to_hex(:x($serialized<recovery>)) !! 0,

    my $r = self.skip_lead_nulls(:input($serialized<signature>.subbuf(0,32)));
    my $s = self.skip_lead_nulls(:input($serialized<signature>.subbuf(32,32)));

    @raw.push(buf8.new($y ?? ($y.Str ~~ m:g/..?/).map({ :16($_.Str) if $_ && $_ ne '0x' }) !! Empty), $r, $s);

    return {
        raw => $txtype == 2 ??
            $!targetobj.ethobj.buf2hex($rlp.rlp_encode(:input(@raw)).unshift($txtype)) !!
                $!targetobj.ethobj.buf2hex($rlp.rlp_encode(:input([@raw, %kzgdata<blobs>, %kzgdata<kzgcommitments>, %kzgdata<kzgproofs>])).unshift($txtype)),
    };
}

method skip_lead_nulls(buf8 :$input) returns buf8 {
    my $buf = $input;

    for $buf.list.kv -> $index, $byte {
        if !$byte {
            $buf = $buf.subbuf($index + 1,*);
        }
        else {
            last;
        }
    }

    return $buf;
}

method push2log(UInt :$nonce!, UInt :$gas!, Str :$data) returns Bool {
    $!targetobj.sgnlog.push([
        sprintf("%s", Pheix::Datepack.new.get_date_logging),
        sprintf("%06d", $nonce),
        sprintf("%10d", $!targetobj.cmpobj.lzw.get_bytes(:s($data // q{}))),
        sprintf("%10d", $gas),
    ].join(q{|}));

    return True;
}

method access_list(Hash :$transaction!) returns Array {
    my $access_list = []; # $!targetobj.ethobj.eth_createAccessList(:transaction($transaction))<accessList>;
    my $ret;

    for $access_list.List -> $accessdata {
        for $accessdata.keys.sort -> $key {
            my $value = $accessdata{$key};

            if $value ~~ Array {
                $ret.push($value.map({buf8.new(($_.Str ~~ m:g/../).map({ :16($_.Str) if $_ ne '0x' }))}).Array);
            }
            else {
                $ret.push(buf8.new(($value.Str ~~ m:g/../).map({ :16($_.Str) if $_ ne '0x' })));
            }
        }
    }

    return $ret // [];
}
