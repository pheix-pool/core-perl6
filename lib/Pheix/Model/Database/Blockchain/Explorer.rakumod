unit class Pheix::Model::Database::Blockchain::Explorer;

use Net::Ethereum;

has $.blockchainobj;

method unmarshal_trx_input(Str :$fname!, Str :$txhash!) returns Hash {
    return {} unless $!blockchainobj && $txhash ~~ m:i/^ 0x<xdigit>**64 $/;

    my $trx = $!blockchainobj.ethobj.eth_getTransactionByHash($txhash);

    return {} unless $trx && $trx ~~ Hash && $trx<input> ~~ m:i/^ 0x<xdigit>+ $/;

    return {} unless $trx<input> ~~ s:i/^ 0x<xdigit>**8 //;

    return {} unless $!blockchainobj.config && $!blockchainobj.config<explorer>;

    return {} unless $!blockchainobj.config<explorer><abi> && $!blockchainobj.config<explorer><abi>.IO.f;

    my $ethereum = Net::Ethereum.new(abi => $!blockchainobj.config<explorer><abi>.IO.slurp);
    my $trxinput = $ethereum.unmarshal($fname, $trx<input>);

    return {} unless $trxinput && $trxinput ~~ Hash && $trxinput.keys;

    return $trxinput;
}

method distributed_select_all(Str :$fname!, :@hashes!) returns List {
    return unless @hashes.elems;

    my @ret;

    for @hashes -> $txhash {
        next unless $txhash ~~ m:i/^ 0x<xdigit>**64 $/;

        my $data = self.unmarshal_trx_input(:$fname, :$txhash);

        next unless $data ~~ Hash && $data<rowdata> ~~ Buf;

        @ret.push($data<rowdata>.decode);
    }

    return @ret;
}
