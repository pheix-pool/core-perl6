unit class Pheix::Model::Database::Blockchain::EIP4844;

use JSON::Fast;
use HTTP::Tiny;
use Net::Ethereum;
use Node::Ethereum::KZG;

has UInt $.max_slots_to_check = 10;
has Str  $.trusted_setup_filepath;
has Bool $.debug is default(False);
has      $.diag  is default(sub diag(Str:$m){$m.say});
has      $.config;

has $!kzg    = Node::Ethereum::KZG.new(:$!trusted_setup_filepath);
has $!neteth = Net::Ethereum.new;
has $!ua     = HTTP::Tiny.new;

method kzg returns Node::Ethereum::KZG {
    return $!kzg;
}

method kzg_commitment_data(:@blobs!, Hash :$fees!) returns Hash {
    my %kzg;

    X::AdHoc.new(:payload('***ERR: exceed maximum blobs per transaction')).throw unless @blobs.elems <= 6;

    X::AdHoc.new(:payload('***ERR: no EIP-4844 fees are calculated')).throw unless $fees<baseFeePerBlobGas> && $fees<maxFeePerBlobGas>;

    %kzg<blobs>              = [];
    %kzg<kzgcommitments>     = [];
    %kzg<kzgproofs>          = [];
    %kzg<kzgversionedhashes> = [];
    %kzg<maxfeeperblobgas>    = $fees<maxFeePerBlobGas>;
    %kzg<blobmaxsize>         = $!kzg.constants<FIELD_ELEMENTS_PER_BLOB> * $!kzg.constants<BYTES_PER_FIELD_ELEMENT>;

    $!kzg.load_trusted_setup_from_file;

    for @blobs.values -> $data {
        my $blob = $!kzg.blob.encode_blob_data(:$data);

        X::AdHoc.new(:payload('***ERR: EIP-4844 blob size')).throw unless $blob.bytes <= %kzg<blobmaxsize>;

        %kzg<blobs>.push(buf8.new(0 xx (%kzg<blobmaxsize> - $blob.bytes)).append($blob));
        %kzg<kzgcommitments>.push($!kzg.blob_to_kzg_commitment(:blob(%kzg<blobs>.tail)));
        %kzg<kzgproofs>.push($!kzg.compute_blob_kzg_proof(:blob(%kzg<blobs>.tail), :commitment(%kzg<kzgcommitments>.tail)));

        X::AdHoc.new(:payload('***ERR: cannot prove EIP-4844 commitment')).throw unless $!kzg.verify_blob_kzg_proof(:blob(%kzg<blobs>.tail), :commitment(%kzg<kzgcommitments>.tail), :proof(%kzg<kzgproofs>.tail));

        %kzg<kzgversionedhashes>.push($!kzg.kzg_to_versioned_hash(:commitment(%kzg<kzgcommitments>.tail)));
    }

    $!kzg.free_trusted_setup;

    return %kzg;
}

method get_blob(Hash :$block!, Str :$versioned_hash!) returns buf8 {
    my buf8 $blob;

    X::AdHoc.new(:payload('***ERR: no beacon endpoint in config')).throw unless
        $!config &&
        $!config.keys &&
        $!config<explorer> &&
        $!config<explorer><beacon_endpoint>;

    X::AdHoc.new(:payload('***ERR: invalid block data')).throw unless $block && $block.keys;

    my $parent_beacon_block_root = $block<parentBeaconBlockRoot>;

    X::AdHoc.new(:payload(sprintf("***ERR: can not get parentBeaconBlockRoot for block %d", $block<number>))).throw unless
        $parent_beacon_block_root && $parent_beacon_block_root ~~ m:i/^ 0x<xdigit>+ $/;

    my $beacon_endpoint = $!config<explorer><beacon_endpoint>;
    my $block_response  = $!ua.get(sprintf("%s/eth/v2/beacon/blocks/%s", $beacon_endpoint, $parent_beacon_block_root));

    X::AdHoc.new(:payload(sprintf("***ERR: can not get slot for block root %s: status=%d reason=%s", $parent_beacon_block_root, $block_response<status>, $block_response<reason>))).throw
        unless $block_response<success> && $block_response<content>;

    my $decoded_block_response = from-json($block_response<content>.decode);
    my $slot = $decoded_block_response<data><message><slot>.Int;

    X::AdHoc.new(:payload('***ERR: no positive slot value in beacon endpoint response')).throw unless $slot;

    my $blob_response;
    my $tested_slot;

    for 1..$!max_slots_to_check -> $shift {
        $tested_slot   = $slot + $shift;
        $blob_response = $!ua.get(sprintf("%s/eth/v1/beacon/blob_sidecars/%d", $beacon_endpoint, $tested_slot));

        last if $blob_response<success> && $blob_response<content>;

        $!diag(:m(sprintf("***WARN: can not get blob for slot %d: status=%d reason=%s", $tested_slot, $blob_response<status>, $blob_response<reason>))) if $!debug;
    }

    X::AdHoc.new(:payload(sprintf("***ERR: no blob in slots %d..%d: status=%d reason=%s", $slot, $tested_slot, $blob_response<status>, $blob_response<reason>))).throw
        unless $blob_response<success> && $blob_response<content>;

    my $decoded_blob_response = from-json($blob_response<content>.decode);

    X::AdHoc.new(:payload('***ERR: no blob sidecars are found')).throw unless
        $decoded_blob_response &&
        $decoded_blob_response.keys &&
        $decoded_blob_response<data> &&
        $decoded_blob_response<data>.elems;

    $!diag(:m(sprintf("***INF: got blob sidecars for slot=%d", $tested_slot))) if $!debug;

    X::AdHoc.new(:payload('***ERR: can not load trusted setup')).throw unless $!kzg.load_trusted_setup_from_file;

    for $decoded_blob_response<data>.values -> $sidecar {
        my $kzg_commitment       = $sidecar<kzg_commitment>;
        my $bytes_per_commitment = $!kzg.constants<BYTES_PER_COMMITMENT>;

        next unless
            $kzg_commitment &&
            $kzg_commitment ~~ m:i/^ 0x<xdigit>**{$bytes_per_commitment * 2} $/;

        my $commitment = $!neteth.hex2buf($kzg_commitment);

        if $versioned_hash.lc eq $!neteth.buf2hex($!kzg.kzg_to_versioned_hash(:$commitment)).lc {
            $blob = $!kzg.blob.decode_blob_data(:data($!neteth.hex2buf($sidecar<blob>)));

            $!diag(:m(sprintf("***INF: blob for block %d with versioned hash %s is found (blob index at sidecar %d)", $block<number>, $versioned_hash, $sidecar<index>))) if $!debug;

            last;
        }
    }

    X::AdHoc.new(:payload('***ERR: can not free trusted setup')).throw unless $!kzg.free_trusted_setup;

    return $blob;
}
