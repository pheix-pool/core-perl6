unit class Pheix::Model::Database::Compression;

use Pheix::Datepack;
use LZW::Revolunet;
use Compress::Bzip2;

has Bool $.test          = False;
has uint $.dictsize      = 97000;
has LZW::Revolunet $.lzw = LZW::Revolunet.new(:dictsize($!dictsize));
has Int $!ratio is default(0);
has Str $!algo is default(q{});
has $.config is default({}) is rw;;

method compress(Str :$data!) returns Buf[uint8] {
    my $ret = Buf[uint8].new;

    return $ret unless $data && $data.chars;

    if $!config && ($!config<compression>:exists) && $!config<compression> eq 'bzip2' {
        my $strbuf  = Buf[uint8].new($data.encode);
        my $compbuf = compressToBlob($strbuf);
        $!ratio     = floor(100 - $compbuf.bytes/$strbuf.bytes*100);
        $!algo      = 'bzip2';

        # sprintf("Bizp2 compression with ratio %d", $!ratio).say;

        return $compbuf;
    }

    if $!lzw {
        my $encoded = $!lzw.encode_utf8(:s($data));

        try {
            if $!test {
                X::AdHoc.new(:payload('crash test')).throw;
            }
            else {
                $ret    = Buf[uint8].new($!lzw.compress(:s($encoded)).encode);
                $!ratio = $!lzw.get_ratio;
                $!algo  = 'lzw';
            }

            CATCH {
                default {
                    if not $!test {
                        $*ERR.say: .message;

                        for .backtrace.reverse {
                            $*ERR.say: "  in block {.subname} at {.file} line {.line}";
                        }
                    }

                    my $logf = self.dumper(:unixtime(397556573), :data($data));
                    my $logb = self.dumper(:unixtime(397556573), :data($data), :bin);

                    my $dumpmsg = sprintf("compress failure, dumps at\n\t%s\n\t%s", $logf, $logb);

                    X::AdHoc.new(:payload($dumpmsg)).throw;
                }
            }
        }
    }

    return $ret;
}

method decompress(Buf[uint8] :$data!) returns Str {
    my Str $ret = q{};

    return $ret unless $data && $data.bytes;

    if $!config && ($!config<compression>:exists) && $!config<compression> eq 'bzip2' {
        $!algo = 'bzip2';

        return decompressToBlob($data).decode;
    }

    if $!lzw {
        $!algo    = 'lzw';
        my $decmp = $!lzw.decompress(:s($data.decode));

        $ret = $!lzw.decode_utf8(:s($decmp));
    }

    return $ret;
}

method get_bytes(Str :$data) returns uint {
    my uint $ret = 0;

    return 0 unless $data && $data.chars;

    if $!lzw {
        $ret = $!lzw.get_bytes(:s($data));
    }

    return $ret;
}

method get_ratio returns int {
    return $!ratio;
}

method get_algo returns int {
    return $!algo;
}

method dumper(int :$unixtime, Str :$data, Bool :$bin, Bool :$dry) returns Str {
    my Str $fname;
    my $dateobj = Pheix::Datepack.new(:unixtime($unixtime));

    if $bin {
        $fname = $dateobj.get_date_filename(:extension('bin'));

        if !$dry {
            my $databuf = Buf[uint8].new($data.encode);
            my $fhandle = $fname.IO.open(:w, :bin);

            $fhandle.write(compressToBlob($databuf));
            $fhandle.close;
        }
    }
    else {
        $fname = $dateobj.get_date_filename;

        spurt $fname, $data if !$dry;
    }

    return $fname;
}
