unit class Pheix::Model::Database::Access;

use Pheix::Model::JSON;
use Pheix::Model::Database::Filechain;
use Pheix::Model::Database::Blockchain;
use Pheix::Test::Blockchain;

has Int  $.dbswitch;
has Str  $.table;
has Bool $.test  is default(False);
has Bool $.debug is default(False);

has      $.fields     = Nil;
has Any  $.chainobj   = Nil;
has Hash $.constrpars = Nil;

submethod BUILD(
    Str  :$table!,
         :$fields!,
         :$utils,
    Bool :$test = False,
    Bool :$debug = False,
         :$constrpars  = {},
    UInt :$txwaitsec   = 15,
    UInt :$txwaititers = 20,
    Pheix::Model::JSON :$jsonobj = Pheix::Model::JSON.new
) {
    $!fields     = $fields.elems ?? $fields !! <id data compression>;
    $!constrpars = $constrpars;
    $!test       = $test;
    $!debug      = $debug;
    my %tabsets  = $jsonobj.get_all_settings_for_group_member(
        'Pheix',
        'storage',
        $table,
    ) || type => 0;

    $!table = %tabsets<mock>:!exists ?? $table !! %tabsets<mock>;

    given $jsonobj.check_int(~%tabsets<type>) {
        when 1 {
            my Str $abi;
            my Str $fabi;
            if (
                (%tabsets<path>:exists) &&
                (%tabsets<strg>:exists) &&
                (%tabsets<extn>:exists)
            ) {
                my Str $fpath = sprintf("%s/%s.%s",
                    %tabsets<path>,
                    %tabsets<strg>,
                    %tabsets<extn>
                );

                if ( $fpath.IO.e ) {
                    $abi  = $fpath.IO.slurp;
                    $fabi = $fpath;
                }
            }

            my $bcobj = self!create_bchain_obj(
                :abi($abi),
                :fabi($fabi),
                :utils($utils),
                :sets(%tabsets)
            );

            proceed unless $bcobj.apiurl.defined;

            if $bcobj.is_node_active {
                $bcobj.ethobj.tx_wait_sec   = $txwaitsec;
                $bcobj.ethobj.tx_wait_iters = $txwaititers;

                if (%tabsets<depl>:exists) && $jsonobj.check_int(~%tabsets<depl>) == 1 {
                    $bcobj.nounlk = True if (%tabsets<lock>:exists) && $jsonobj.check_int(~%tabsets<lock>) == 1;

                    my $res = $bcobj.deploy_precompiled_smartcontract(
                        :constructor_params($!constrpars)
                    );

                    if ($res<status>:exists) && $jsonobj.check_int(~$res<status>) == 1 {
                        $bcobj.sctx   = $res<transactionHash>;
                        $bcobj.scaddr = $res<contractAddress>;

                        $bcobj.ethobj.contract_id = $bcobj.scaddr;
                    }
                    else {
                        proceed;
                    }
                }
                else {
                    if (%tabsets<hash>:exists) && %tabsets<hash> ~~ m:i/^ 0x<xdigit>**64 $/ {
                        if $bcobj.set_contract_addr {
                            if $bcobj.scaddr !~~ m:i/^ 0x<[0]>**40 $/ {
                                $bcobj.ethobj.contract_id = $bcobj.scaddr;
                            }
                        }
                    }

                    if %tabsets<sign>:exists {
                        my %ssets =
                            $jsonobj.get_all_settings_for_group_member(
                                'Pheix',
                                'storage',
                                %tabsets<sign>,
                            ) || type => q{0};

                        if (%ssets<type>:exists) && (%ssets<type> eq q{1}) {
                            my $signer = self!create_bchain_obj(
                                :abi($abi),
                                :tab(%tabsets<sign>),
                                :fields([]),
                                :sets(%ssets),
                                :sctx(q{})
                            );

                            if $signer.is_node_active {
                                $bcobj.sgnobj = $signer;
                            }

                            sprintf("DEBUG: signer %s", $bcobj.sgnobj ?? 'ok' !! 'fails').say if $!debug;
                        }
                    }
                }

                $!dbswitch = 1;
                $!chainobj = $bcobj;
            } else {
                proceed;
            }
        }
        default {
            $!dbswitch = 0;
            $!chainobj = Pheix::Model::Database::Filechain.new(
                :fcpath(%tabsets<path> // Str),
                :file($!table ~ '.tnk'),
                :fields($!fields),
                :test($!test),
            );
        }
    }

    # ( "Set DB type " ~ $!dbswitch ~ " for tab <" ~ $!table ~ ">").say;
}

method !create_bchain_obj(
    Str :$abi!,
    Str :$fabi,
    Str :$tab,
    Str :$sctx,
        :$fields,
        :%sets!,
        :$utils
) returns Pheix::Model::Database::Blockchain {
    my Str $qstr   = (%sets<qstr>:exists) ?? %sets<qstr> !! q{};
    my Str $apiurl = sprintf(
        "%s%s%s%s", %sets<prtl>, %sets<host>, %sets<port> ?? q{:} ~ %sets<port> !! q{}, $qstr
    );

    return Pheix::Model::Database::Blockchain.new(
        :abi($abi),
        :apiurl($apiurl),
        :sctx($sctx // %sets<hash> // q{}),
        :table($tab // $!table),
        :fields($fields // $!fields),
        :account(%sets<user>:exists ?? %sets<user> !! q{}),
        :unlockpwd(%sets<pass>:exists ?? %sets<pass> !! q{}),
        :fabi($fabi // q{}),
        :test($!test),
        :utls($utils // Nil),
        :debug($!debug),
        :config(((%sets<conf>:exists) && %sets<conf> ~~ Hash) ?? %sets<conf> !! {})
    );
}

method get_chain_obj returns Any {
    $!table ?? $!chainobj !! Nil;
}

method modified returns Instant {
    $!chainobj.get_modify_time;
}

method dbpath returns Str {
    $!chainobj.get_path;
}

method exists returns Bool {
    my Bool $ret = False;

    given $!dbswitch {
        default {
            my $obj = self.get_chain_obj;
            if $obj {
                $ret = $obj.table_exists;
            }
        }
    }

    $ret;
}

method insert(Hash $data is rw) returns Bool {
    # %arg = field1 => value1, field2 => value2, ...;
    my Bool $ret = False;

    given $!dbswitch {
        default {
            my $_obj = self.get_chain_obj;
            my $id   = $data<id> // 0;

            if $data<id> ~~ Str && $data<id> ~~ m/^<[0..9]>+$/ {
                $id = $data<id>.UInt;
            }

            if $_obj {
                my $cmp = $data<compression> // False;

                my $res = $_obj.row_insert(
                    :data($data),
                    :waittx($data<waittx> // True),
                    :comp($cmp ~~ Bool ?? $cmp !! $cmp.UInt.Bool)
                    :id($id)
                );

                $ret = $res<status>;

                if $!dbswitch == 1 {
                    $data<txhash> = $res<txhash>;
                }
            }
        }
    }

    return $ret;
}

method set(Hash $set_ref is rw, Hash $where_ref, Bool :$waittx = True) returns Int {
    # %set = k1 => v1, k2 => v2, ...;
    # %where = cond1 => condval1, cond2 => condval2, ...;
    # sql: UPDATE table SET k1=v1, k2=v2
    # WHERE cond1=condval1 AND cond2=condval2;
    my %ret = status => False;
    given $!dbswitch {
        default {
            my $_obj = self.get_chain_obj;
            if $_obj {
                %ret = $_obj.row_set(:data($set_ref), :clause($where_ref), :$waittx);

                if $!dbswitch == 1 {
                    $set_ref<txhash> = %ret<txhash>;
                }
            }
        }
    }

    %ret<status>.Int;
}

method get(Hash $clause) returns List {
    # %arg = k1 => v1, k2 => v2, ...;
    # sql: SELECT * FROM table WHERE k1=v1 AND k2=v2,...;
    my @rows;

    given $!dbswitch {
        default {
            my $_obj = self.get_chain_obj;
            if $_obj {
                @rows = $_obj.row_get(:clause($clause));
            }
        }
    }

    @rows;
}

method get_count(Hash $clause) returns Int {
    # %arg = k1 => v1, k2 => v2, ...;
    # sql: SELECT COUNT(1) FROM table WHERE k1=v1 AND k2=v2,...;
    my Int $ret = 0;

    my $chainobj = self.get_chain_obj;

    if $chainobj {
        given $!dbswitch {
            when 1 {
                $ret = $chainobj.count_rows;
            }
            default {
                $ret = $chainobj.row_get(:clause($clause)).elems;
            }
        }
    }

    $ret;
}

method get_all(Bool :$fast, Bool :$withcomp) returns List {
    # get all records from table
    # e.g.: SELECT * FROM table WHERE 1=1;
    my @rows;

    my $chainobj = self.get_chain_obj;

    if $chainobj {
        given $!dbswitch {
            when 1 {
                @rows =
                    Pheix::Test::Blockchain
                        .new
                        .fast_select_all(:dbobj(self), :t($!table), :$withcomp)
                        .map({ %(data => $_) });
            }
            default {
                @rows =
                    ($fast && $!dbswitch == 0) ??
                        $chainobj.table_raw !!
                            $chainobj.row_get(:clause(Nil.Hash));
            }
        }
    }

    @rows;
}

method remove( Hash $arg_ref ) returns Int {
    # %arg = k1 => v1, k2 => v2;
    # e.g.: DELETE FROM table WHERE k1=v1 AND k2=v2,...;
    my Int $ret = 0;
    given $!dbswitch {
        default {
            my $_obj = self.get_chain_obj;
            if $_obj {
                $ret = $_obj.row_remove(:clause($arg_ref));
            }
        }
    }
    $ret;
}

method remove_all returns Bool {
    # e.g.: DROP TABLE table;
    my Bool $ret = False;

    my $chainobj = self.get_chain_obj;

    if $chainobj {
        given $!dbswitch {
            when 1 {
                $ret =
                    Pheix::Test::Blockchain
                        .new
                        .drop_table(:dbobj(self), :t($!table));
            }
            default {
                $ret = $chainobj.table_drop;
            }
        }
    }

    $ret;
}
