unit class Pheix::Model::Database::Filechain;

has Str $!lf   = "\x[0A]";

has Str  $.file;
has Str  $.fcpath;
has Bool $.test;
has      $.fields;
has      $.config is default({}) is rw;

submethod BUILD(
    Str  :$file,
    Str  :$fcpath,
    Bool :$test   = False,
         :$fields = Nil,
) {
    $!file   = $file;
    $!fcpath = ($fcpath && $fcpath.IO.e) ?? ~$fcpath.IO.cleanup !! 'conf/system';
    $!fields = $fields;
    $!test   = $test;
}

method get_path(Str :$tab) returns Str {
    my Str $tabfile  = $tab ?? ($tab ~ '.tnk') !! $!file;
    my Str $pathprfx = $!test ?? './t' !! ($!fcpath ~~ /^\// ?? q{} !! q{.});

    return sprintf("%s/%s/%s", $pathprfx, $!fcpath, $tabfile);
}

method get_modify_time(Str :$tab) returns Instant {
    my Instant $rs = now;
    my $_path  = self.get_path(:tab($tab));
    if $_path.IO.e {
        $rs = $_path.IO.modified;
    }
    $rs;
}

method get_fields(Str :$tab, Bool :$force = False) returns List {
    my @ret = <anonymous>;

    if !$!fields || $force {
        my $path = self.get_path(:tab($tab));

        if $path.IO.e {
            my $fh     = $path.IO.open(:r);
            my $agenda = $fh.lines[0];

            if substr($agenda, 0, 1) eq q{#} {
                @ret = substr($agenda, 2).split(q{;}, :skip-empty);
            }

            $fh.close;
        }
    }

    return $force ?? @ret !! $!fields // @ret;
}

method write_filechain(Str $path, Str $data) returns Bool {
    my Bool $ret = False;

    my $fh = $path.IO.open(:a);

    if $fh {
        if $fh.lock {
            if $fh.spurt($data ~ $!lf) {
                $ret = $fh.unlock.Bool;
            }
        }

        $fh.close;
    }

    $ret;
}

method table_exists(Str :$t) returns Bool {
    self.get_path(:tab($t)).IO.e;
}

method table_create(Str :$t) returns Hash {
    my %ret = status => False;

    my $flds = self.get_fields(:tab($t));

    if $flds {
        my $_path = self.get_path(:tab($t));
        if !$_path.IO.e {
            %ret<status> = self.write_filechain(
                $_path,
                '# ' ~ $flds.join(q{;}),
            );
        }
    }

    %ret;
}

method table_drop(Str :$t) returns Bool {
    my Bool $ret = False;
    my Str $path = self.get_path(:tab($t));

    if $path.IO.e {
        $ret = $path.IO.unlink.Bool;
    }

    $ret;
}

method table_raw returns List {
    my @rows;
    my Str  $path = self.get_path;
    my List $flds = self.get_fields;

    if $path.IO.e {
        my $_fh = $path.IO.open;
        if $_fh {
            if $_fh.lock(:shared) {
                my @data = $_fh.lines;
                @data.shift if @data.elems && @data[0] ~~ m/^ '#' /;

                for @data -> $r {
                    my %row;
                    my UInt $i = 0;

                    $r.chomp
                      #.split(q{|}, :skip-empty)
                      .split(q{|})
                      .map({%row{$flds[$i++]} = $_});

                    @rows.push(%row);
                }
            }
            $_fh.close;
        }
    }
    @rows;
}

method row_insert(
    Str  :$t,
    UInt :$id,
    Hash :$data!,
) returns Hash {
    my %ret = status => False;

    my Str  $path = self.get_path(:tab($t));
    my List $flds = self.get_fields(:tab($t));

    my @cols = $flds.map({
        $data{$_} ?? $data{$_}.Str !! q{}
    });

    if $path.IO.e {
        %ret<status> = self.write_filechain($path, @cols.join(q{|}));
    }
    else {
        if self.table_create {
            %ret<status> = self.write_filechain($path, @cols.join(q{|}));
        }
    }

    %ret;
}

method row_set(
    Str  :$t,
    Hash :$data!,
    Hash :$clause!
) returns Hash {
    my Bool $ret  = False;
    my Str  $path = self.get_path(:tab($t));

    if $path.IO.e {
        my @_lines;
        my Int $_upd = 0;
        my $_fh = $path.IO.open(:r);

        if $_fh {
            if $_fh.lock(:shared) {
                @_lines = self!set_in_array(
                    $data,
                    $clause,
                    $_fh.lines,
                );
            }
            $_fh.close;
        }

        if @_lines {
            with $path.IO.open(:w) {
                if .lock {
                    $ret = .spurt(@_lines.join($!lf) ~ $!lf).Bool;
                }
                .close;
            }
        }
    }
    else {
        $ret =
            self.row_insert(
                :data($data.Hash.append($clause.Hash))
            )<status>;
    }

    %(status => $ret);
}

method row_get(Str :$t, Hash :$clause!) returns List {
    my @rows;

    my Str $path = self.get_path(:tab($t));

    if $path.IO.e {
        my $_fh = $path.IO.open;
        if $_fh {
            if $_fh.lock(:shared) {
                @rows = self.get_from_array(
                    $clause,
                    $_fh.lines,
                );
            }
            $_fh.close;
        }
    }

    @rows;
}

method row_remove(Str :$t, Hash :$clause!) returns Bool {
    my Bool $ret = False;
    my Str $path = self.get_path(:tab($t));

    if $path.IO.e {
        my @_lines;
        my @_lines_upd;
        my $_fh = $path.IO.open(:r);
        if $_fh {
            if $_fh.lock(:shared) {
                @_lines = $_fh.lines;
                @_lines_upd = self!remove_from_array(
                    $clause,
                    @_lines,
                );
            }
            $_fh.close;
        }
        if @_lines ne @_lines_upd && @_lines_upd {
            if !(@_lines_upd.elems == 1 && substr(@_lines_upd[0], 0, 1) eq "#" ) {
                with $path.IO.open(:w) {
                    if .lock {
                        $ret = .spurt(@_lines_upd.join($!lf) ~ $!lf).Bool;
                    }
                    .close;
                }
            }
            else {
                $ret = $path.IO.unlink;
            }
        }
    }

    $ret;
}

method get_from_array(Hash $arg_ref, @lines, Bool $bchain?) returns List {
    my @rows;
    my List $flds = self.get_fields;
    for @lines -> $row {
        next unless substr($row, 0, 1) ne q{#} || $bchain;

        my %row;
        my Int $i = 0;

        if $bchain && $flds.elems == 1 {
            %row = $flds[0] => $row;
        } else {
            $row.chomp.split(q{|}).map({%row{$flds[$i++]} = $_});
        }

        if !$arg_ref || !(%row{$arg_ref.keys}:exists).map({ $_ if !$_ }) {
            if !$arg_ref || %row{$arg_ref.keys}.values eq $arg_ref.values {
                @rows.push(%row);
            }
        }
    }

    return @rows;
}

method !set_in_array(
    Hash $set_ref,
    Hash $where_ref,
    @lines
) returns List {
    my UInt $upd = 0;
    my @_lines = @lines;
    my $flds = self.get_fields;
    for @_lines {
        if substr($_, 0, 1) ne q{#} {
            my %row;
            my Int $i = 0;

            $_
              .chomp
              .split(q{|}, :skip-empty)
              .map({%row{$flds[$i++]} = $_});

            if !(%row{$set_ref.keys}:exists).map({ $_ if !$_ }) &&
               !(%row{$where_ref.keys}:exists).map({ $_ if !$_ }) {
                if %row{$where_ref.keys}.values eq $where_ref.values {
                    %row{$set_ref.keys} = $set_ref.values;
                    $_ = $flds
                        .map({ %row{$_} ?? %row{$_}.Str !! q{} })
                        .join(q{|});
                    $upd++;
                }
            }
        }
    }
    ( $upd ?? @_lines !! () );
}

method !remove_from_array(Hash $arg_ref, @lines) returns List {
    my @lines_upd;
    my $flds = self.get_fields;
    for @lines {
        if substr($_, 0, 1) ne q{#} {
            my %row;
            my Int $i = 0;

            $_.chomp
              .split(q{|}, :skip-empty)
              .map({%row{$flds[$i++]} = $_});

            if !(%row{$arg_ref.keys}:exists).map({ $_ if !$_ }) {
                if %row{$arg_ref.keys}.values ne $arg_ref.values {
                    @lines_upd.push($_);
                }
            }
        }
        else {
            @lines_upd.push($_);
        }
    }
    @lines_upd;
}
