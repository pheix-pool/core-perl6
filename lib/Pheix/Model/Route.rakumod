unit class Pheix::Model::Route;

use Router::Right;

has Bool $.test = False;

method import_addon_routes(
    Str           :$prefix!,
    Router::Right :$router!,
                  :%addons!
) returns Router::Right {
    my Str $pfx = $prefix // q{};

    for %addons.kv -> $class, %a {
        for %a<confr>.keys -> $r {
            if %a<confr>{$r}<route>:exists {
                my %route = %a<confr>{$r}<route>;

                if (%route<path>:exists) && (%route<hdlr>:exists) {
                    (my $postfix = $pfx) ~~ s:g/\W//;

                    $router.add(
                        :name(sprintf("%s-%s-%s-%s", now.Rat.Str, %a<objct>.get_name, $r, $postfix || 'default')),
                        :path(sprintf("GET %s%s", $pfx, %route<path>)),
                        :payload(sprintf('%s#%s', $class, %route<hdlr>{$pfx || 'default'})),
                    ) if %route<hdlr>{$pfx || 'default'}:exists;
                }
            }
        }
    }

    $router;
}

method basic_routes_with_handler(
    Str           :$handler_class!,
    Str           :$prefix,
    Router::Right :$extrouter
) returns Router::Right {
    die 'No prefix for external router is given' if
        $extrouter.defined && !$prefix.defined;

    my Router::Right $router = $extrouter // Router::Right.new;
    my Str $pfx              = $prefix // q{};

    # Static API routes
    if (
        !$extrouter.defined ||
        (
            $extrouter.defined &&
            !($extrouter.route('api-debug')<route>:exists) &&
            !($extrouter.route('api-error')<route>:exists) &&
            !($extrouter.route('api')<route>:exists)
        )
    ) {
        $router.add(
            :name('api-debug'),
            :path('GET /api-debug'),
            :payload('Pheix::Controller::Basic#api_debug'),
        );

        $router.add(
            :name('api-error'),
            :path('GET|POST /api-error/{code:<[0..9]>**3..3}'),
            :payload('Pheix::Controller::Basic#api-error'),
        );

        $router.add(
            :name('api'),
            :path('POST /api'),
            :payload('Pheix::Controller::Basic#api'),
        );
    }

    # Dynamic routes
    $router.add(
        :name(sprintf("%s-%s", now.Rat.Str, 'index')),
        :path(sprintf("GET %s%s", $pfx, '/{index:(\'index\')?}')),
        :payload(sprintf('%s#index', $handler_class)),
    );

    $router.add(
        :name(sprintf("%s-%s", now.Rat.Str, 'error')),
        :path(sprintf("GET %s%s", $pfx, '/error/{code:<[0..9]>**3..3}')),
        :payload(sprintf('%s#error', $handler_class)),
    );

    $router.add(
        :name(sprintf("%s-%s", now.Rat.Str, 'bigbro')),
        :path(sprintf("GET %s%s", $pfx, '/bigbrother/{query:.*}')),
        :payload(sprintf("%s#bigbrother", $handler_class)),
    );

    $router.add(
        :name(sprintf("%s-%s", now.Rat.Str, 'sitemap')),
        :path(sprintf("GET %s%s", $pfx, '/sitemap{.format:\'xml\'}')),
        :payload(sprintf("%s#sitemap", $handler_class)),
    );

    $router.add(
        :name(sprintf("%s-%s", now.Rat.Str, 'redirect')),
        :path(sprintf("GET %s%s", $pfx, '/redirect/{query:.*}')),
        :payload(sprintf("%s#redirect", $handler_class)),
    );

    $router.add(
        :name(sprintf("%s-%s", now.Rat.Str, 'captcha')),
        :path(sprintf("GET %s%s", $pfx, '/captcha/{query:.*}')),
        :payload(sprintf("%s#captcha", $handler_class)),
    );

    $router.add(
        :name(sprintf("%s-%s", now.Rat.Str, 'presentation')),
        :path(sprintf("GET %s%s", $pfx, '/presentation')),
        :payload(
            %(
                controller  => $handler_class,
                action      => 'userdefined',
                params      => %(
                    db_name => 'presentation',
                    db_flds => <id data compression>
                )
            )
        )
    );

    $router.add(
        :name(sprintf("%s-%s", now.Rat.Str, 'tpc20cic')),
        :path(sprintf("GET %s%s", $pfx, '/tpc20cic')),
        :payload(
            %(
                controller  => $handler_class,
                action      => 'userdefined',
                params      => %(
                    db_name => 'tpc20cic_presentation',
                    db_flds => <id data compression>
                )
            )
        )
    );

    $router.add(
        :name(sprintf("%s-%s", now.Rat.Str, 'logger')),
        :path(sprintf("POST %s%s", $pfx, '/logger')),
        :payload(sprintf("%s#logger", $handler_class)),
    );

    $router;
}
