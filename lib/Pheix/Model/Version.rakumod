unit class Pheix::Model::Version;

constant ver_maj = 0;
constant ver_min = 15;
constant ver_rel = 52;

method get_version returns Str {
    ( ver_maj.Str ~ q{.} ~ ver_min.Str ~ q{.} ~ ver_rel.Str );
}

method get_major returns UInt {
    ver_maj;
}

method get_minor returns UInt {
    ver_min;
}

method get_release returns UInt {
    ver_rel;
}
