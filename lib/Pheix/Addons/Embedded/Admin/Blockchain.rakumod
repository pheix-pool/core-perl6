unit class Pheix::Addons::Embedded::Admin::Blockchain;

use Pheix::Addons::Embedded::Admin::Blockchain::Balance;
use Pheix::Model::Database::Access;
use Pheix::Model::JSON;

use Pheix::Test::Blockchain;
use Pheix::Test::BlockchainComp::Helpers;

use Crypt::LibGcrypt;
use Crypt::LibGcrypt::Random;

has Pheix::Model::JSON $.jsonobj;
has Str  $.authpolicy  = 'basic';
has Str  $.rootaddress = q{};
has UInt $.txwaitsec   = 1;
has UInt $.txwaititers = 90;
has UInt $.sealperiod  = 5;
has UInt $.sesstime    = 300;
has UInt $.expiredelta = 20;
has UInt $.sessseed    = (1_000_000..1_000_000_000).rand.Int;
has Bool $.tokensync   = True;

has      %!gateways  = {asc => 'auth-smart-contract', agw => 'auth-node'};
has      $!balance   = Pheix::Addons::Embedded::Admin::Blockchain::Balance.new;

method get_authgateway(Str :$token) returns Pheix::Model::Database::Access {
    return self!configure_auth_gateway(:$token);
}

method auth_on_blockchain(
    Str :$addr!,
    Str :$pwd!,
        :$sharedobj
) returns Hash {
    my %ret =
        status  => False,
        pkey    => q{},
        addr    => '0x' ~ q{0} x 40,
        tx      => '0x' ~ q{0} x 64;

    return %ret unless $!authpolicy ~~ m:i/^ (basic|extended) $/;

    my $agw = self!configure_auth_gateway;

    if $agw && $agw.dbswitch == 1 && $addr ~~ m:i/^ 0x<xdigit>**40 $/ {
        try {
            $agw.chainobj.ethobj.personal_sign(
                :message($agw.chainobj.ethobj.string2hex(self.^name)),
                :account($addr),
                :password($pwd)
            );
        }

        if $! {
            %ret<message> = $!.message;
        }
        else {
            X::AdHoc.new(:payload(sprintf("unlock account %s failure", $addr))).throw unless
                $agw.chainobj.ethobj.personal_unlockAccount(:account($addr), :password($pwd), :duration(0));

            my $catch_details = self!catch_contract(:$agw, :$addr, :$sharedobj);

            if $catch_details && $catch_details<scaddr> && $catch_details<txhash> {
                %ret = self.validate_on_blockchain(:token($catch_details<txhash>));
            }

            if !%ret<status> {
                X::AdHoc.new(:payload('Auth on blockchain failure: configuration is missed')).throw unless $!jsonobj;

                $!jsonobj.set_group_setting('Pheix', 'storage', %!gateways<asc>, 'user', $addr, :temporary(True));
                $!jsonobj.set_group_setting('Pheix', 'storage', %!gateways<asc>, 'pass', $pwd, :temporary(True));

                my $asc = Pheix::Model::Database::Access.new(
                    :table(%!gateways<asc>),
                    :fields([]),
                    :constrpars({_sealperiod => $!sealperiod, _delta => $!sesstime, _seedmod => $!sessseed, _pkey => :10[random(32).list]}),
                    :$!txwaitsec,
                    :$!txwaititers,
                    :$!jsonobj
                );

                $!jsonobj.set_group_setting('Pheix', 'storage', %!gateways<asc>, 'user', q{}, :temporary(True));
                $!jsonobj.set_group_setting('Pheix', 'storage', %!gateways<asc>, 'pass', q{}, :temporary(True));

                if $asc && $asc.dbswitch == 1 {
                    my $tx;
                    my %h = $asc.
                            chainobj.
                            ethobj.
                            contract_method_call('getPkey', {}, $addr);

                    if $!authpolicy eq 'extended' {
                        $tx = $!balance.update(:agw(self!configure_auth_gateway(:token($asc.chainobj.sctx))), :token($asc.chainobj.sctx), :$sharedobj);
                    }
                    else {
                        $tx = $asc.chainobj.sctx;
                    }

                    if %h<publickey> && %h<publickey> ~~ m:i/^ 0x<xdigit>**64 $/ {
                        %ret<status>  = True;
                        %ret<addr>    = $addr.lc;
                        %ret<pkey>    = %h<publickey>;
                        %ret<tx>      = $tx;
                        %ret<session> = $!sesstime;
                        %ret<scope>   = $addr.lc eq $!rootaddress.lc ?? 'full' !! 'address';
                    }
                }
            }
        }
    }

    return %ret;
}

method validate_on_blockchain(Str :$token!, :$sharedobj, :$update = True, :$payload) returns Hash {
    my %pkey;
    my %validate = status => False;

    if (my $agw = self!configure_auth_gateway(:token($token))) {
        if $payload && $payload.keys && $payload<recovery> {
            return %validate unless
                $payload<recovery><address>   ~~ m:i/^ 0x<xdigit>**40 $/ &&
                $payload<recovery><publickey> ~~ m:i/^ 0x<xdigit>**64 $/ &&
                $payload<recovery><signature> ~~ m:i/^ 0x<xdigit>+ $/    &&
                $agw.chainobj.ethacc eq $payload<recovery><address>;

            %pkey = $agw.chainobj.ethobj.contract_method_call('getPkey', {}, $agw.chainobj.ethacc);

            return %validate unless %pkey<publickey> && %pkey<publickey> ~~ m:i/^ 0x<xdigit>**64 $/ && %pkey<publickey> eq $payload<recovery><publickey>;

            my $addr = $agw.chainobj.ethobj.personal_ecRecover(:message($payload<recovery><publickey>), :signature($payload<recovery><signature>));

            return %validate unless $addr eq $agw.chainobj.ethacc;
        }

        my $upd = self!session_details(:$agw);

        if $!balance.validate(:$agw, :$token, :$!authpolicy, :%pkey) && $upd<delta> > 0 {
            my $pkey = :10[random(32).list];

            %validate =
                status   => True,
                addr     => $agw.chainobj.ethacc.lc,
                expired  => $upd<expired>,
                session  => $upd<delta>,
                pkey     => $agw.chainobj.ethobj.contract_method_call('getPkey', {}, $agw.chainobj.ethacc)<publickey>,
                scope    => $agw.chainobj.ethacc.lc eq $!rootaddress.lc ?? 'full' !! 'address',
                tx       => ($upd<expired> || !$update || $!authpolicy ne 'extended') ?? $token !! $!balance.update(:$agw, :$token, :waittx($!tokensync), :$sharedobj),
                contract => $agw.chainobj.scaddr.lc,
                expiredpkey  => $agw.chainobj.ethobj.web3_sha3(sprintf("0x%064x", $pkey)),
                expiredinput => $agw.chainobj.ethobj.marshal('updateState', { publickey => $pkey }),
                exitinput    => $agw.chainobj.ethobj.marshal('doExit', Hash.new),
            ;
        }
    }

    return %validate;
}

method extend_on_blockchain(Str :$token!, :$sharedobj) returns Hash {
    my %extend = status => False;

    if (my $agw = self!configure_auth_gateway(:token($token))) {
        my $pkey      = :10[random(32).list];
        my %updatetrx = $agw.chainobj.write_blockchain(:method('updateState'), :data({ publickey => $pkey }), :waittx(True));

        if %updatetrx<txhash>:exists {
            $!balance.push2log(:transaction(%updatetrx<txhash>));
        }
        else {
            %updatetrx<txhash> = $token;
        }

        my $updetails = self!session_details(:trx(%updatetrx<txhash>), :agw($agw));

        my %h = $agw.chainobj.ethobj.contract_method_call('getPkey', {}, $agw.chainobj.ethacc);

        %h<publickey>:delete if %h<publickey> ~~ m:i/^ 0x<[0]>**64 $/;

        if %h<publickey> && %h<publickey> ~~ m:i/^ 0x<xdigit>**64 $/ && $updetails<delta> > 0 {
            my $tx;

            if $!authpolicy eq 'extended' {
                $tx = $!balance.update(:agw(self!configure_auth_gateway(:token(%updatetrx<txhash>))), :token(%updatetrx<txhash>), :waittx($!tokensync), :$sharedobj)
            }
            else {
                $tx = %updatetrx<txhash>;
            }

            %extend =
                status  => True,
                addr    => $agw.chainobj.ethacc.lc,
                expired => $updetails<expired>,
                session => $updetails<delta>,
                pkey    => %h<publickey>,
                tx      => $tx,
                scope   => $agw.chainobj.ethacc.lc eq $!rootaddress.lc ?? 'full' !! 'address',
            ;
        }
    }

    return %extend;
}

method close_on_blockchain(Str :$token!, Bool :$waittx = True) returns Bool {
    my %extend = status => False;

    if (my $agw = self!configure_auth_gateway(:token($token))) {
        my %updatetrx = $agw.chainobj.write_blockchain(
            :method('doExit'),
            :$waittx
        );

        if %updatetrx<txhash>:exists && %updatetrx<txhash> !~~ m:i/^ 0x<[0]>**64 $/ {
            Pheix::Test::BlockchainComp::Helpers
                .new(:tstobj(Pheix::Test::Blockchain.new))
                .flush_signing_session(
                    :sgnlog($!balance.transactonlog),
                    :filepostfix('-close-blockchain-session')
                );

            return $agw.chainobj.lock_account;
        }
    }

    return False;
}

method auth_smartcontact_abi(Str :$table!, Int :$pkey = 0) returns Str {
    return unless $table ~~ /login/;

    my Str $abi;

    $!jsonobj.set_group_setting('Pheix', 'storage', %!gateways<asc>, 'depl', 0, :temporary(True));

    my $asc = Pheix::Model::Database::Access.new(
        :table(%!gateways<asc>),
        :fields([]),
        :constrpars({_sealperiod => $!sealperiod, _delta => $!sesstime, _seedmod => $!sessseed, _pkey => $pkey}),
        :$!txwaitsec,
        :$!txwaititers,
        :$!jsonobj
    );

    $!jsonobj.set_group_setting('Pheix', 'storage', %!gateways<asc>, 'depl', 1, :temporary(True));

    if $asc && $asc.dbswitch == 1 {
        if $asc.chainobj.fabi && $asc.chainobj.fabi.IO.e && $asc.chainobj.fabi.IO.f {
            my Str $asc_name = ($asc.chainobj.fabi.IO.basename.IO.extension: '').Str;
            my Str $bin_path = sprintf("%s/%s.bin", $asc.chainobj.fabi.IO.dirname, $asc_name);

            if $bin_path.IO.e && $bin_path.IO.f {
                $abi = sprintf("%s%064x%064x%064x%064x", $bin_path.IO.lines.join, $!sealperiod, $!sesstime, $!sessseed, $pkey);
            }
        }
    }

    return $abi;
}

method !session_details(
    Str :$trx,
    Pheix::Model::Database::Access :$agw!
) returns Hash {
    return {} unless $agw && $agw.dbswitch == 1;

    my %trx  = blockNumber => 0;
    my $ret  = {expired => False};
    my $logs = $agw.chainobj.get_logs(:address($agw.chainobj.scaddr));

    %trx = $agw.chainobj.ethobj.eth_getTransactionByHash($trx) if $trx && $trx ~~ m:i/^ 0x<xdigit>**64 $/;

    if $logs && $logs.elems > 0 {
        if (($logs.tail)<transactionHash>:exists) && (%trx<blockNumber>:exists) {
            my $gentrx  = $agw.chainobj.ethobj.eth_getTransactionReceipt(($logs.tail)<transactionHash>);

            $ret<genbn> = $gentrx<blockNumber>.Int;
            $ret<trxbn> = %trx<blockNumber>.Int;
            $ret<block> = $ret<trxbn> > $ret<genbn> ?? $ret<trxbn> !! $ret<genbn>;

            my $prevblck = $agw.chainobj.ethobj.eth_getBlockByNumber($ret<block>);
            my $lastblck = $agw.chainobj.ethobj.eth_getBlockByNumber('latest');

            $ret<delta> = $!sesstime - ($lastblck<timestamp>.Int - $prevblck<timestamp>.Int);

            if $ret<delta> > 0 && $ret<delta> <= $!expiredelta {
                $ret<expired> = True;
            }
        }
    }

    return $ret;
}

method !configure_auth_gateway(Str :$token) returns Pheix::Model::Database::Access {
    X::AdHoc.new(:payload('Auth gateway failure: configuration is missed')).throw unless $!jsonobj;

    my $agw = Pheix::Model::Database::Access.new(
        :table(%!gateways<agw>),
        :fields([]),
        :$!txwaitsec,
        :$!txwaititers,
        :$!jsonobj,
    );

    return unless $agw && $agw.dbswitch == 1;

    $agw.chainobj.evsign = 'PheixAuthCode(uint256)';

    if $token && $token ~~ m:i/^ 0x<xdigit>**64 $/ {
        my %trx = $agw.chainobj.ethobj.eth_getTransactionByHash($token);

        return unless %trx<from> && %trx<from> ~~ m:i/^ 0x<xdigit>**40 $/;

        my Str $scadr = %trx<to> // q{};

        if $scadr !~~ m:i/^ 0x<xdigit>**40 $/ {
            my %receipt = $agw.chainobj.ethobj.eth_getTransactionReceipt($token);

            if %receipt<contractAddress> && %receipt<contractAddress> ~~ m:i/^ 0x<xdigit>**40 $/ {
                $scadr = %receipt<contractAddress>;
            }
            else {
                $scadr = '0x' ~ q{0} x 40;
            }
        }

        return unless $scadr !~~ m:i/^ 0x<[0]>**40 $/;

        $agw.chainobj.ethacc = %trx<from>;
        $agw.chainobj.scaddr = $scadr;
        $agw.chainobj.ethobj.contract_id = $scadr;
    }

    return $agw;
}

method !catch_contract(
    Pheix::Model::Database::Access :$agw!,
                                   :$addr!,
                                   :$sharedobj

) returns Hash {
    my $ret = {};
    my $logs;

    return $ret unless $agw && $agw.dbswitch == 1;

    my $lastblock  = $agw.chainobj.ethobj.eth_getBlockByNumber('latest');

    my $startblock = (($lastblock<number> // 0) - $!sesstime/$!sealperiod).Int;

    $startblock = 0 unless $startblock > 0;

    $logs = $agw.chainobj.get_logs(:from($startblock), :address($addr), :topics([sprintf("0x%064x", 2)]));
    $logs = $agw.chainobj.get_logs(:from($startblock), :address($addr), :topics([sprintf("0x%064x", 1)])) unless $logs && $logs.elems > 0;

    $sharedobj<dbugobj>.log(
        :entry(sprintf("catch contract feature on logon for %s: %s", $addr, $logs.gist)),
        :sharedobj($sharedobj)
    ) if $sharedobj;

    if $logs && $logs.elems > 0 {
        my $latestlog = $logs.tail;

        if ($latestlog<transactionHash>:exists) &&
           ($latestlog<blockNumber>:exists) &&
           ($latestlog<address>:exists) {
            $ret = {
                scaddr => $latestlog<address>,
                txhash => $latestlog<transactionHash>
            };

        }
    }

    return $ret;
}
