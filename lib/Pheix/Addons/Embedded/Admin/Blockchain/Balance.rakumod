unit class Pheix::Addons::Embedded::Admin::Blockchain::Balance;

use Pheix::Datepack;
use Pheix::Model::Database::Access;
use JSON::Fast;

has $.transationminingdelay = 10;
has $.transactonlog = [];

method push2log(Str :$transaction!, UInt :$nonce = 0, UInt :$bytes = 0, UInt :$gas = 0, Str :$method = 'balance') returns Bool {
    $!transactonlog.push([
        sprintf("%s", Pheix::Datepack.new.get_date_logging),
        sprintf("%06d", $nonce),
        sprintf("%10d", $bytes),
        sprintf("%10d", $gas),
        sprintf("%s|%12s|%7s", $transaction, $method, 'native'),
    ].join(q{|}));

    return True;
}

method !precheck(Pheix::Model::Database::Access :$agw!, Str :$token!, :%pkey) returns Hash {
    return {} unless $agw.chainobj.signaturefactory;

    return {} unless $agw.dbswitch == 1 && $token ~~ m:i/^ 0x<xdigit>**64 $/ && $token !~~ m:i/^ 0x<[0]>**64 $/;

    my $access_trx = $agw.chainobj.ethobj.eth_getTransactionByHash($token);

    return {} unless $access_trx && $access_trx.keys.elems;

    my %publickey = %pkey && %pkey<publickey> ?? %pkey !!
        $agw.chainobj.ethobj.contract_method_call('getPkey', {}, $agw.chainobj.ethacc);

    %publickey<publickey>:delete if %publickey<publickey> ~~ m:i/^ 0x<[0]>**64 $/;

    return {} unless %publickey<publickey> && %publickey<publickey> ~~ m:i/^ 0x<xdigit>**64 $/;

    return $access_trx;
}

method update(Pheix::Model::Database::Access :$agw!, Str :$token!, Bool :$waittx = True, :$sharedobj) returns Str {
    my $access_token;
    my $access_trx = self!precheck(:$agw, :$token);

    return q{} unless $access_trx.keys;

    my $sender     = $access_trx<from>;
    my $receipient = $agw.chainobj.scaddr;

    my $balance = $agw.chainobj.ethobj.eth_getBalance($receipient, 'latest');
    my $amount  = (1_000_000_000 .. 10_000_000_000).rand.UInt;

    my $update_balance_trx = {
        from  => $sender,
        to    => $receipient,
        value => '0x' ~ $amount.base(16),
        data  => $agw.chainobj.signaturefactory.sign(:message(sprintf("%d", $balance + $amount))),
    };

    try {
        $access_token = $agw.chainobj.ethobj.eth_sendTransaction($update_balance_trx);

        self.push2log(:transaction($access_token));

        $agw.chainobj.ethobj.wait_for_transaction(:hashes([$access_token])) if $waittx;

        CATCH {
            default {
                my $e = .message;

                if $sharedobj {
                    $sharedobj<dbugobj>.log(
                        :entry(sprintf("failure while updating auth balance: %s", $e)),
                        :$sharedobj
                    );
                }

                $access_token = $token;
            }
        };
    }

    return $access_token;
}

method validate(Pheix::Model::Database::Access :$agw!, Str :$token!, Str :$authpolicy = 'basic', :%pkey) returns Bool {
    my $access_trx = self!precheck(:$agw, :$token, :%pkey);

    return False unless $access_trx && $access_trx.keys && $access_trx<input>;

    return True unless $authpolicy eq 'extended';

    if !$access_trx<blockNumber> {
        # "/tmp/validate_transactonlog.dump".IO.spurt(sprintf("token: %s\n%s", $token, to-json(self.transactonlog)));

        my @logrecord = self.transactonlog.tail.split(q{|}, :skip-empty);

        (my $created = @logrecord[0]) ~~ s/\s/T/;
        my $trxhash  = @logrecord[4];

        if $token eq $trxhash {
            return True if DateTime.now.Instant.UInt - DateTime.new($created).Instant.UInt <= $!transationminingdelay;
        }
    }

    my $receipient = $agw.chainobj.scaddr;
    my $balance    = $agw.chainobj.ethobj.eth_getBalance($receipient, 'latest');

    return $agw.chainobj.signaturefactory.validate(:message(sprintf("%s", $balance)), :signature($access_trx<input>));
}
