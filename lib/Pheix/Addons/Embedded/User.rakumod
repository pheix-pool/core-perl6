unit class Pheix::Addons::Embedded::User;

use JSON::Fast;

use Pheix::Controller::API;
use Pheix::Model::Database::Access;
use Pheix::Model::JSON;

has Str  $.confpath = 'conf/addons';
has Str  $!name     = 'EmbeddedUser';
has Bool $.nocache  = False;

has @!pflds = <id data compression>;
has @!tflds = <id timestamp seouri title metadescr metakeywords header footer content allowpage>;
has $!dbobj = Pheix::Model::Database::Access.new(:table($!name.lc), :fields(@!tflds));

has Pheix::Model::JSON $!jobjsys = Pheix::Model::JSON.new.set_entire_config(:addon('Pheix'));
has Pheix::Model::JSON $.jsonobj = self.init_json_config(:path($!confpath));

has Bool $.allowid = $!jsonobj.get_setting($!name, 'accessbyid', 'value').Bool;
has UInt $!defpid  = $!jsonobj.get_conf_value($!name, 'defaultid').UInt;
has Str  $!tabname = $!name.lc ~ q{/} ~ $!defpid;

has $!builtinapi = Pheix::Controller::API.new;
has $.ctrl;

method init_json_config(Str :$path) returns Pheix::Model::JSON {
    my Pheix::Model::JSON $jsonobj =
       Pheix::Model::JSON.new(:addonpath($path)).set_entire_config(:addon($!name));

    my %setup = $jsonobj.get_entire_config;

    my %database;
    my @rows = $!dbobj.get_all(:fast(True));

    for @rows -> %r {
        %database{%r{@!tflds[0]}} = %r;
    }

    %setup<database> = %database;

    $jsonobj.set_entire_config(:setup(%setup))
}

method get(:$ctrl!) returns Pheix::Addons::Embedded::User {
    $!ctrl = $ctrl.clone;

    self;
}

method get_class returns Str {
    self.^name;
}

method get_defpid returns UInt {
    $!defpid;
}

method get_name returns Str {
    $!name;
}

method get_sm returns List {
    my @sitemap;
    my %setup = $!jsonobj.get_entire_config;

    for %setup<database>.keys -> $record {
        my Str $pid    = %setup<database>{$record}<id>;
        my Str $seouri = %setup<database>{$record}<seouri>;
        my Str $path   =
            %setup<database>{$record}<allowpage> == 1 ??
                $!name.lc ~ '/page/' !!
                    $!name.lc ~ q{/};


        my Str $page =
            $seouri && $seouri.chars ??
                $seouri !!
                    ($!allowid ?? $pid !! Str);

        if $page {
            my $t = $!name.lc ~ q{/} ~ $pid;
            my %s = $!jobjsys.get_all_settings_for_group_member('Pheix', 'storage', $t);

            next unless !(%s<type>:exists) || %s<type> == 0;

            my $dtobj = DateTime.new($!dbobj.chainobj.get_modify_time(:tab($t)));
            my $rtup  = sprintf(
                "%04d-%02d-%02d",
                $dtobj.year,
                $dtobj.month,
                $dtobj.day
            );

            @sitemap.push(
                {
                    loc     => $path ~ $page,
                    lastmod => $rtup,
                }
            );
        }
    }

    @sitemap;
}

multi method get_record(UInt :$id!) returns Hash {
    my %ret;
    my @rows;

    if $!nocache {
        @rows = $!dbobj.get({id => $id});

        if @rows.elems == 1 {
            %ret = @rows[0];
        }
    }
    else {
        my %setup = $!jsonobj.get_entire_config;
        %ret      = %setup<database>{$id} if %setup<database>{$id}:exists;
    }

    %ret;
}

multi method get_record(Str :$seouri!) returns Hash {
    my %ret;
    my @rows;

    if $!nocache {
        @rows = $!dbobj.get({seouri => $seouri});

        if @rows.elems == 1 {
            %ret = @rows[0];
        }
    }
    else {
        my %setup = $!jsonobj.get_entire_config;
        for %setup<database>.keys -> $id {
            if %setup<database>{$id}<seouri>:exists {
                (my $seo = $seouri) ~~ s/ \.html? $//;
                %ret = %setup<database>{$id} if %setup<database>{$id}<seouri> eq $seo;
            }
        }
    }

    %ret;
}

method get_content(:%match!, Bool :$hdr, Bool :$ftr) returns Str {
    my %setup    = $!jsonobj.get_entire_config;
    my Str  $sel = $hdr ?? 'header' !! ($ftr ?? 'footer' !! 'content');
    my Str  $ret = sprintf("The default %s", $sel);
    my UInt $pid = $!defpid;

    if (%match<id>:exists) && self.check_page(:match(%match)) {
        $pid = %match<id>.UInt;
        $ret = sprintf("The fallback %s for %d", $sel, $pid);
    }

    my %row = self.get_record(:id($pid));

    if %row && (%row{$sel}:exists) {
        $ret = %row{$sel};
    }

    $ret;
}

method check_page(:%match!) returns Bool {
    my UInt $pageid = %match<id>:exists ?? %match<id>.UInt !! 0;
    my Str  $table  = $pageid > 0 ?? $!name.lc ~ q{/} ~ $pageid !! q{};
    my Bool $ret    = False;

    if $!nocache {
        $ret = Pheix::Model::Database::Access
            .new(:table($table),:fields(@!pflds))
            .exists;
    }
    else {
        my %setup = $!jsonobj.get_entire_config;
        $ret = True if (%setup<database>:exists) && (%setup<database>{$pageid}:exists);
    }

    $ret;
}

method allow_as_page(:%match!) returns Bool {
    my UInt $pid = %match<id>:exists ?? %match<id>.UInt !! 0;
    my Bool $ret = False;

    if $pid > 0 {
        my %row = self.get_record(:id($pid));

        if %row && (%row<allowpage>:exists) {
            $ret = %row<allowpage>.UInt.Bool;
        }
    }

    $ret;
}

method fill_seodata(:%match!) returns Hash {
    my %ret;

    my @keys   = <title metadescr metakeywords header>;
    my Bool $e = self.check_page(:match(%match));

    if $e {
        my UInt $pageid =
            %match<id>:exists ??
                %match<id>.UInt !!
                    0;

        %ret = self.get_record(:id($pageid));
    }

    if !%ret {
        for @keys -> $key {
            %ret{$key} = $!jsonobj.get_group_setting(
                $!name,
                'indexseotags',
                $key,
                'value',
            ) // q{};
        }
    }

    %ret;
}

method browse(UInt :$tick, :%match!) {
    my %m = %match;

    %m<details> = {path => '/api' ~ %match<details><path>};

    if %m<seouri>:exists {
        my %record = self.get_record(:seouri(~%m<seouri>));

        if ~%m<seouri> ~~ /^^ <[0..9]>+ $$/ && $!allowid {
            %record = self.get_record(:id(+%m<seouri>));
        }

        %m<id> = %record<id> if %record<id>:exists;
    }

    if (!self.check_page(:match(%m)) && (%m<id>:exists)) ||
       ((%m<seouri>:exists) && ~%m<seouri> ~~ /^^ <[0..9]>+ $$/ && !($!allowid)) {
        $!ctrl.error(
            :tick($tick),
            :code(404),
            :req(%m<details><path>)
        );
    }
    else {
        if %m<page>:exists {
            if self.allow_as_page(:match(%m)) {
                %m<params> = %(
                    db_name => $!name.lc ~ q{/} ~ %m<id>,
                    db_flds => <id data compression>
                );

                $!ctrl.userdefined(:tick($tick), :match(%m));
            }
            else {
                $!ctrl.error(
                    :tick($tick),
                    :code(403),
                    #:msg('this is self.allow_as_page(:match(%m))'),
                    :req(%m<details><path>)
                );
            }
        }
        else {
            $!ctrl.index(:tick($tick), :match(%m), :addon($!name));
        }
    }
}

method browse_seo_api(
    :%match!,
    Str  :$route,
    UInt :$tick!,
    Hash :$sharedobj!,
    Hash :$credentials = {},
    Hash :$payload = {},
) returns Hash {
    my %rc;

    if %match<seouri>:exists {
        my %record = self.get_record(:seouri(~%match<seouri>));

        if ~%match<seouri> ~~ /^^ <[0..9]>+ $$/ && $!allowid {
            %record = self.get_record(:id(+%match<seouri>));
        }

        if %record<id>:exists {
            my %m  = %match;
            %m<id> = %record<id>;

            %rc = self.browse_api(
                :match(%m),
                :route($route),
                :tick($tick),
                :sharedobj($sharedobj),
                :credentials($credentials),
                :payload($payload)
            );
        }
    }

    %rc = self.error(
        :tick($tick),
        :match(%match),
        :sharedobj($sharedobj)
    ) if !%rc;

    %rc;
}

method browse_api(
         :%match!,
    Str  :$route,
    UInt :$tick!,
    Hash :$sharedobj!,
    Hash :$credentials = {},
    Hash :$payload = {},
) returns Hash {
    my %rc;
    my $start = now;

    if self.allow_as_page(:match(%match)) {
        %rc = self.error(
            :tick($tick),
            :match(%match),
            :code('403'),
            :message(
                sprintf(
                    "page <%d> is forbidden via API, use direct page route instead",
                    %match<id>
                )
            ),
            :sharedobj($sharedobj)
        );
    }
    else {
        my Str $accesstoken;
        my Str $sesstoken = $sharedobj<pageobj>.get_tparams<tmpl_sesstoken>;
        my Str $addon     = 'Ethelia';

        if $!ctrl && $!ctrl.addons.keys && $!ctrl.addons{$addon} {
            my $ethelia = $!ctrl.addons{$addon};

            my %accesstokens = $ethelia<objct>.jsonobj.get_setting($addon, 'accesstokens', 'group') // Hash.new;

            # return self.error(:%match, :$tick, :$sharedobj, :code('401'), :message(%accesstokens.gist));

            if %accesstokens.keys {
                $accesstoken = %accesstokens.keys.grep({ %accesstokens{$_}<active>.Bool && %accesstokens{$_}<scope> eq 'full' }).tail;
            }
        }

        my %t =
            content => self.get_content(:match(%match)),
            header  => self.get_content(:match(%match), :hdr(True)),
            footer  => self.get_content(:match(%match), :ftr(True)),
            id      => %match<id>:exists ?? %match<id>.UInt !! 0,
            tmpl_sesstoken   => $sesstoken,
            tmpl_accesstoken => $accesstoken // $sesstoken,
            tmpl_encaptcha   => $sharedobj<pageobj>.get_tparams<tmpl_encaptcha>,
        ;

        my Str $t =
            %t<id> > 0 ??
                $!name.lc ~ q{/} ~ %t<id> !!
                    $!tabname;

        my Str $component_raw = $sharedobj<pageobj>.raw_pg(
            :table($t),
            :fields(@!pflds)
        );

        if $component_raw {
            %rc =
                component => $sharedobj<mb64obj>.encode-str(
                    $component_raw,
                    :oneline
                ),
                tparams => %t,
            ;
        }
        else {
            %rc = self.error(
                :tick($tick),
                :match(%match),
                :code('400'),
                :message(
                    sprintf(
                        "raw component is missed, t=%s, fields=%s",
                        $t,
                        @!pflds.join(q{,})
                    )
                ),
                :sharedobj($sharedobj)
            );
        }
    }

    %rc<component_render> = (now - $start).Str;

    %rc;
}

method error(
         :%match!,
    UInt :$tick!,
    Hash :$sharedobj!,
    Str  :$code,
    Str  :$message
) returns Hash {
    $!builtinapi.error(
        :route(%match<details><path>),
        :code($code // '404'),
#       :message($message // (self.^name ~ ': page is not found')),
        :message($message // q{}),
        :tick($tick),
        :sharedobj($sharedobj),
    );
}
