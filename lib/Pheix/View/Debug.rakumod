unit class Pheix::View::Debug;

use Pheix::View::HTML::Markup;

has Str $!lf  = "\x[0A]";

method env_vars_list returns List {
    my @env;
    for (%*ENV.keys.sort) -> $key {
        @env.push('%*ENV{' ~ $key ~ '} = ' ~ %*ENV{$key});
    }
    @env;
}

method show_env_vars returns Str {
    my Str $env;
    my $obj = Pheix::View::HTML::Markup.new;
    for (%*ENV.keys.sort) -> $key {
        $env ~= '%*ENV{' ~ $key ~ '} = ' ~ %*ENV{$key} ~ $obj.br ~ $!lf;
    }
    $env;
}

method api_debug_form(Str :$url) returns Str {
    "<link rel=\"icon\" href=\"data:;base64,iVBORw0KGgo=\">" ~
    "<link rel=\"stylesheet\" href=\"https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css\" crossorigin=\"anonymous\">" ~
    "<div id=\"api-debug-container\" class=\"p-2\"></div>" ~
    "<script src=\"https://code.jquery.com/jquery-3.2.1.min.js\"></script>" ~
    "<script src=\"resources/skins/akin/js/api-debug.js\"></script>";
}

method log(Str :$entry, :$sharedobj) returns Bool {
    my $time = time;

    return False unless $sharedobj &&
        ($sharedobj<logrobj>:exists) &&
            ($sharedobj<mb64obj>:exists);

    my $insert_params = {
        id  => ++$time,
        log => $sharedobj<mb64obj>.encode-str($entry, :oneline)
    };

    $sharedobj<logrobj>.insert($insert_params);

    return True;
}
