unit class Pheix::View::HTML::Markup;

has $.br = '<br>';

method uni_tag(
    Str   $tag!,
    Str   $content,
    Hash :$attrs
) returns Str {
    my @attributes;

    for $attrs.kv -> $attr_name, $attr_value {
        next unless $attr_name && $attr_value && $attr_value ne q{};

        @attributes.push(sprintf(" %s=\"%s\"", $attr_name, $attr_value));
    }

    return sprintf(
        "\<%s%s\>%s\</%s\>", $tag,
            (@attributes && @attributes.elems ?? @attributes.join(q{}) !! q{}),
                $content, $tag);
}
