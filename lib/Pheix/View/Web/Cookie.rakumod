unit class Pheix::View::Web::Cookie;

use Pheix::Datepack;

has Str  $.name;
has Str  $.value;
has UInt $.created is default(time);
has Str  $.expires_s is default('+1h');
has UInt $.expires_t = $!created + _expire_calc($!expires_s).UInt;

has %!cookie_fields =
    Expires  => Pheix::Datepack
        .new(:date(DateTime.new($!expires_t)), :unixtime($!expires_t))
        .get_http_response_date,
    Max-Age  => _expire_calc($!expires_s),
    Domain   => %*ENV<HTTP_HOST>,
    Path     => q{/},
    SameSite => 'Strict',
    Secure   => False,
    HttpOnly => True,
;

method field(Str :$key) returns Cool {
    my Cool $value;

    return $value unless $key && $key ne q{};

    if %!cookie_fields{$key}:exists {
        $value = %!cookie_fields{$key} // Str;
    }

    return $value;
}

method cookie(%fields? = {}) returns Str {
    my Str $cookie;
    if $!name {
        my %f;

        $cookie = $!name ~ q{=} ~ $!value ~ q{;};

        %!cookie_fields.keys.map({ %f{$_} = %fields{$_}:exists ?? %fields{$_} !! %!cookie_fields{$_} });

        for %f.sort -> (:$key, :$value) {
            next if !$key or !$value;

            if $value ne 'True' and $value ne 'False' {
                my $v = $value;
                if lc($key) eq 'max-age' {
                    $v = _expire_calc($value.Str);
                }
                $cookie ~= $key ~ q{=} ~ $v ~ q{;};
            }
            else {
                $cookie ~= $key ~ q{;} if $value eq 'True';
            }
        }
    }

    return $cookie;
}

method expire_calc(Str $time) returns Str {
    return _expire_calc($time);
}

method get_value(Str :$name!, Str :$cookies!) returns Str {
    return q{} unless $cookies && $cookies ne q{} && $name && $name ne q{};

    my %parsed;

    $cookies.split(/<[;\s]>+/, :skip-empty).map(
        {
            my @c = $_.split(q{=}, :skip-empty);
            %parsed{@c[0]} = @c[1];
        }
    );

    if %parsed && %parsed.keys && (%parsed{$name}:exists) {
        return %parsed{$name};
    }

    return q{};
}

# This internal routine is based on Perl5
# CGI::Utils.pm expire_calc() routine.
# It incorporates modifications from Mark Fisher.
sub _expire_calc(Str $time) returns Str {
    my Str $offset;

    my ( %mult ) = (
        's'=>1,
        'm'=>60,
        'h'=>60*60,
        'd'=>60*60*24,
        'M'=>60*60*24*30,
        'y'=>60*60*24*365,
    );

    if !$time || $time.lc eq 'now' {
        $offset = q{0};
    }
    elsif $time ~~ m/^ '-'? \d+ $/ {
        $offset = $time;
    }
    elsif $time ~~ m/^ (<[+-]>? [\d+|\d*\.\d*]) (<[smhdMy]>) $/ {
        $offset = ((%mult{$1} || 1)*$0).Str;
    }
    else {
        $offset = q{0};
    }

    return $offset ~~ /<[\d]>/ ?? $offset !! q{0};
}
