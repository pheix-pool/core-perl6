unit class Pheix::Controller::Basic;

use Pheix::View::HTML::Markup;
use Pheix::Model::Database::Access;
use Pheix::Model::JSON;
use Pheix::Model::Resources;
use Pheix::Controller::API;
use Pheix::Controller::Stats;
use Pheix::View::Debug;
use Pheix::View::Web::Headers;
use Pheix::View::Template;
use Pheix::View::Pages;
use Pheix::Addons::November::CGI;
use Pheix::Datepack;
use Pheix::Utils;
use Pheix::Test::FastCGI;

use MIME::Base64;
use OpenSSL::Digest;
use JSON::Fast;
use URI::Encode;
use FastCGI::NativeCall;
use JSON::Schema;

has Bool $.test = False;
has      %.sharedobj;
has      %.addons;
has      $.apirobj;
has      $!glbrobj;
has UInt $!maxreq = 65536;

submethod BUILD(:$apirobj!, :%addons, Bool :$test, :$mockedutils, Pheix::Model::JSON :$jsonobj) {
    my %hash  =
        apirobj => $apirobj,
        statobj => Pheix::Controller::Stats.new(:test($test // False)),
        mb64obj => MIME::Base64.new,
        dbugobj => Pheix::View::Debug.new,
        jsonobj => $jsonobj // Pheix::Model::JSON.new.set_entire_config(:addon('Pheix')),
        headobj => Pheix::View::Web::Headers.new,
        fastcgi => $test ?? Pheix::Test::FastCGI.new !! Nil,
        tmplobj => Pheix::View::Template.new,
        rsrcobj => Pheix::Model::Resources.new.init,
        mkupobj => Pheix::View::HTML::Markup.new,
        ctrlapi => Pheix::Controller::API.new,
        logrobj => Pheix::Model::Database::Access.new(
            :table('logs'), :fields(<id log>), :test($test // False)
        ),
    ;

    %hash<utilobj> = $mockedutils // Pheix::Utils.new(
        :test($test // False),
        :jsonobj(%hash<jsonobj>)
    );

    %hash<pageobj> = Pheix::View::Pages.new(
        :test($test // False),
        :tobj(%hash<tmplobj>),
        :utilobj(%hash<utilobj>),
        :headobj(%hash<headobj>),
        :jsonobj(%hash<jsonobj>),
        :rsrcobj(%hash<rsrcobj>),
        :mkupobj(%hash<mkupobj>),
        :addons(%addons)
    );

    my $schemapath = %hash<jsonobj>.get_conf_value('Pheix', 'jsonschemapath');

    if $schemapath && $schemapath.IO.f {
        %hash<validatorobj> = JSON::Schema.new(
            :schema(from-json($schemapath.IO.slurp))
        );
    }

    %!sharedobj = %hash;
    %!addons    = %addons;
    $!apirobj   = $apirobj;
    $!test      = $test // False;

}

method set_fastcgi(FastCGI::NativeCall :$fastcgi!) {
    self.sharedobj<fastcgi> = $fastcgi;
}

method userdefined(UInt :$tick, :%match!) {
    my Str $usr = %!sharedobj<pageobj>.raw_pg(
        :table(%match<params><db_name>),
        :fields(%match<params><db_flds>)
    );

    my %hdr = %!sharedobj<headobj>.header(
        %(Status => $usr ?? 200 !! 404, X-Request-ID => $tick // 1),
        List.new,
    );

    my Str $payload =
        %!sharedobj<pageobj>.show_pg(
            :pg_type($usr ?? 'debug' !! '404'),
            :pg_route(%match<details><path>),
            :pg_env(%!sharedobj<fastcgi>.env)
            :pg_content($usr // %!sharedobj<rsrcobj>.vocabulary<usrcnterror>)
    );

    %!sharedobj<fastcgi>.header(%hdr);
    %!sharedobj<fastcgi>.Print($payload);
}

method index(
    UInt :$tick,
         :%match!,
    Str  :$addon,
    Bool :$admin   = False,
         :%header  = Hash.new,
         :@cookies = List.new
) {
    my $start = now;
    my %hdr = %!sharedobj<headobj>.header(
        %(
            X-Request-ID => $tick // 1,
            %header
        ),
        @cookies,
    );

    %!sharedobj<pageobj>.set_pparams('tmpl_contentcols', $admin ?? ~12 !! ~8);
    %!sharedobj<pageobj>.set_pparams('tmpl_forcehide', $admin ?? '_phx_dnone_force' !! q{});

    srand(time);

    my $upd_captcha = sprintf "%010d", (rand * 10000000000);

    %!sharedobj<pageobj>.set_tparams('tmpl_decaptcha', $upd_captcha);
    %!sharedobj<pageobj>.set_tparams('tmpl_encaptcha', %!sharedobj<utilobj>.do_encrypt(sprintf("%s:%s", $upd_captcha, time)));

    my Str $payload =
        %!sharedobj<pageobj>.show_pg(
            :pg_addon($addon // 'Pheix'),
            :pg_type('index'),
            :pg_route(%match<details><path>),
            :pg_env(%!sharedobj<fastcgi>.env),
            :pg_match(%match)
        ) ~ %!sharedobj<pageobj>.show_rtm(:s(True), :time($start));

    %!sharedobj<fastcgi>.header(%hdr);
    %!sharedobj<fastcgi>.Print($payload);
}

method error(
    UInt :$tick,
         :%match,
    UInt :$code = 404,
    Str  :$req,
    Str  :$msg
) {
    my $start = now;
    my Str $error_code =  %match && (%match<code>:exists) ?? ~%match<code> !! ~$code;

    $error_code = '404' unless $error_code ~~ /^(400|401|402|403|404|405|413|429)$/;

    my %hdr = %!sharedobj<headobj>.header(
        %(Status => $error_code, X-Request-ID => $tick // 1),
        List.new,
    );

    my Str $payload =
        %!sharedobj<pageobj>.show_pg(
            :pg_type($error_code),
            :pg_route($req // q{/} ~ $error_code),
            :pg_content($msg // q{}),
            :pg_env(%!sharedobj<fastcgi>.env)
        ) ~ %!sharedobj<pageobj>.show_rtm(:s(True), :time($start));

    %!sharedobj<fastcgi>.header(%hdr);
    %!sharedobj<fastcgi>.Print($payload);
}

method redirect(UInt :$tick, :%match!) {
    my %envp =
        Pheix::Addons::November::CGI.new(
            :nounesc(True),
            :test($!test)
        ).params;
    if %envp<redirectto> {
        my %hdr = %!sharedobj<headobj>.header(
            %(
                Status => 302,
                Location => %envp<redirectto>,
                X-Request-ID => $tick // 1,
            ),
            List.new,
        );
        %!sharedobj<fastcgi>.header(%hdr);
    } else {
        self.error(
            :tick($tick),
            :code(404),
            :req(%match<details><path>)
        );
    }
}

method bigbrother(UInt :$tick, :%match!) {
    my %hdr = %!sharedobj<headobj>.header(
        %(X-Request-ID => $tick // 1),
        List.new,
    );

    %!sharedobj<statobj>
        .do_log(
            Pheix::Addons::November::CGI.new(
                :nounesc(True),
                :test($!test),
            )
        );

    %!sharedobj<fastcgi>.header(%hdr);
}

method logger(UInt :$tick, :%match!) {
    my %hdr = %!sharedobj<headobj>.header(
        %(X-Request-ID => $tick // 1),
        List.new,
    );

    my %res         = status => 0, msg => q{};
    my $content_len = %!sharedobj<fastcgi>.env<CONTENT_LENGTH>.UInt;

    if $content_len < $!maxreq {
        my $r   = %!sharedobj<fastcgi>.Read($content_len);
        my %req = from-json($r) // {};

        if (%req<log>:exists && %req<log>.defined) {
            my $insert_params = {
                id  => time,
                log => %!sharedobj<mb64obj>.encode-str(%req<log>, :oneline)
            };

            %!sharedobj<logrobj>.insert($insert_params);

            %res = status => 1, msg => 'data is logged';
        }
        else {
            # bad request
            %hdr<Status> = 400;
            %res<msg>    = 'bad request';
        }
    }
    else {
        # payload too large
        %hdr<Status> = 413;
        %res<msg>    = 'payload too large';
    }

    %!sharedobj<fastcgi>.header(%hdr);
    %!sharedobj<fastcgi>.Print(to-json(%res, :sorted-keys));
}

method sitemap(UInt :$tick, :%match!, Bool :$forcestatic) {
    my UInt $dynasm = %!sharedobj<jsonobj>.get_setting(
        'Pheix', 'dynasitemap', 'value', :nocache(True)
    ) || 0;

    if ($dynasm == 1 && !$forcestatic) {
        my %hdr = %!sharedobj<headobj>.header(
            %(
                Content-Type => 'text/xml; charset=UTF-8',
                X-Request-ID => $tick // 1,
            ),
            List.new,
        );

        my Str $payload = %!sharedobj<pageobj>.show_sm;

        %!sharedobj<fastcgi>.header(%hdr);
        %!sharedobj<fastcgi>.Print($payload);
    }
    else {
        my $smfn = %!sharedobj<utilobj>.smfn;
        if $smfn.defined && $smfn.IO.e {
            my $mdtime  = $smfn.IO.modified;
            my $dateobj =
                Pheix::Datepack.new(
                    :date(Date.new($mdtime).DateTime),
                    :unixtime($mdtime.UInt),
            );
            my $fstats = $dateobj.get_http_response_date;
            my %params =
                Content-Type   => 'text/xml; charset=UTF-8',
                Last-Modified  => $fstats,
                Expires        => $fstats,
                Cache-Control  => 'no-cache,no-store,max-age=0,must-revalidate',
                X-Request-ID   => $tick // 1,
            ;
            my %hdr = %!sharedobj<headobj>.header(%params, List.new);

            my Str $payload = $smfn.IO.slurp;

            %!sharedobj<fastcgi>.header(%hdr);
            %!sharedobj<fastcgi>.Print($payload);
        }
        else {
            self.error(
                :tick($tick),
                :code(404),
                :req(%match<details><path>)
            );
        }
    }
}

method api_debug(UInt :$tick, :%match!) {
    my %hdr = %!sharedobj<headobj>.header(
        %(X-Request-ID => $tick // 1),
        List.new,
    );

    my Str $payload = %!sharedobj<dbugobj>.api_debug_form(:url('/api'));

    %!sharedobj<fastcgi>.header(%hdr);
    %!sharedobj<fastcgi>.Print($payload);
}

method api(UInt :$tick, :%match!) {
    my %hdr = %!sharedobj<headobj>.header(
        %(X-Request-ID => $tick // 1),
        List.new,
    );

    my %res =
        status => 0,
        msg => 'bad API request',
        content => '<p>Bad API request</p>',
    ;

    my $content_len = %!sharedobj<fastcgi>.env<CONTENT_LENGTH>.UInt;

    if $content_len < $!maxreq {
        my $r = %!sharedobj<fastcgi>.Read($content_len);

        my %req;

        try {
            %req = self.validate_request(:$r);

            CATCH {
                default {
                    %res<content> = .message;
                }
            }
        }

        %req<route> = uri_decode(%req<route>) if %req<route>:exists;

        %res<request> = %req.gist;
        # spurt "/tmp/request.txt", %req.gist;

        if (
            (%req<credentials>:exists && %req<credentials>.defined && %req<credentials>.keys) &&
            (%req<route>:exists && %req<route>.defined) &&
            (%req<method>:exists && %req<method>.defined)
        ) {
            if $!apirobj.defined && self!tokenized_route(
                :credentials(%req<credentials>),
                :route(%req<route>),
                :method(%req<method>)
            ) {
                my $start = now;

                if (
                    (
                        %req<httpstat>:!exists ||
                        %req<httpstat> !~~ /^(400|401|402|403|404|405|413|429)$/
                    ) &&
                    my %m = %!sharedobj<apirobj>.match(%req<route>, %req<method>)
                ) {
                    my Hash $pload =
                        (%req<payload>:exists) && %req<payload>.keys ??
                            %req<payload> !!
                                {};

                    if %m<controller> ne %!sharedobj<ctrlapi>.^name {
                        my %addon_data = %!addons{%m<controller>}<objct>.get(:ctrl(self))."%m<action>"(
                            :match(%m),
                            :route(%req<route>),
                            :tick($tick),
                            :sharedobj(%!sharedobj),
                            :credentials(%req<credentials>),
                            :payload($pload),
                            :header(%hdr)
                        );

                        if (%addon_data<header>:exists) && %addon_data<header>.keys.elems > 0 {
                            %hdr = %addon_data<header>:delete;
                        }

                        %res =
                            status => 1,
                            msg => %req<route> ~ ' fetch is successful',
                            content => %addon_data
                        ;
                    }
                    else {
                        %res =
                            status => 1,
                            msg => sprintf("%s is fetched via %s.%s: %s\(%s\)", %req<route>, %m<controller>, %m<action>, %!sharedobj<pageobj>.sesstoken, %req<credentials><token>),
                            content => ::(%m<controller>).new(:ctrl(self))."%m<action>"(
                                :match(%m),
                                :route(%req<route>),
                                :tick($tick),
                                :sharedobj(%!sharedobj),
                                :credentials(%req<credentials>),
                                :payload($pload)
                            )
                        ;
                    }
                }
                else {
                    my $start = now;
                    %res =
                        status => 1,
                        msg => %req<route> ~ ' bad request',
                        content => %!sharedobj<ctrlapi>.error(
                            :route(%req<route>),
                            :code(%req<httpstat> // '400'),
                            :message(sprintf("API controller error: reqmsg=%s, reqcode=%s", %req<message> // q{}, %req<httpstat> // q{})),
                            :tick($tick),
                            :sharedobj(%!sharedobj),
                        )
                    ;
                }

                %res<render> = (now - $start).Str;

                CATCH {
                    default {
                        my $error = .message ~ .backtrace;
                        $error ~~ s:g/'{'/(/;
                        $error ~~ s:g/'}'/)/;

                        %res =
                            status => 0,
                            msg => sprintf("type: %s, path: %s, method: %s", $!apirobj.^name, %req<route> // q{}, %req<method> // q{}),
                            content => %!sharedobj<ctrlapi>.error(
                                :route(%req<route> // q{}),
                                :code('400'),
                                :message($error),
                                :tick($tick // 1),
                                :sharedobj(%!sharedobj),
                            )
                        ;
                    }
                };
            }
        }
    }
    else {
        %hdr<Status> = 400;
    }

    %!sharedobj<fastcgi>.header(%hdr);
    %!sharedobj<fastcgi>.Print(to-json(%res, :sorted-keys));
}

method api-error(UInt :$tick, :%match!) {
    my %hdr = %!sharedobj<headobj>.header(
        %(X-Request-ID => $tick // 1),
        List.new,
    );

    my %res;
    my $error_code = ~%match<code> // q{};

    if $error_code ~~ / '4' <[0..9]> ** 2..2 / {
        %res =
            status => 1,
            msg => 'request is successful',
            content => Pheix::Controller::API.new(:ctrl(self)).error(
                :%match,
                :route(sprintf("/%s", $error_code)),
                :tick($tick),
                :sharedobj(%!sharedobj),
                :message(q{})
            )
        ;
    }
    else {
        %res  =
            status => 0,
            msg => 'wrong error code',
            content => '<p>Wrong error code</p>',
        ;
    }

    %!sharedobj<fastcgi>.header(%hdr);
    %!sharedobj<fastcgi>.Print(to-json(%res, :sorted-keys));
}

method validate_request(
    Str :$r!
) returns Hash {
    my $validation =
        { status => True, error => q{}, payload => from-json($r) // {} };

    return $validation<payload> unless %!sharedobj<validatorobj>;

    try {
        %!sharedobj<validatorobj>.validate($validation<payload>);

        CATCH {
            default {
                $validation<status> = False;
                $validation<error>  = .message;
            }
        }
    }

    X::AdHoc.new(:payload($validation<error>)).throw unless $validation<status>;

    return $validation<payload>;
}

method !tokenized_route(
    Hash :$credentials!,
    Str :$route!,
    Str :$method!
) returns Bool {
    True;
}
