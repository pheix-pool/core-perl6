unit class Pheix::Controller::API;

use Pheix::View::Pages;
use Pheix::View::Debug;
use JSON::Fast;

has $.ctrl;

method error(
         :%match,
    Str  :$route!,
    Str  :$code = '404',
    Str  :$message!,
    UInt :$tick!,
    Hash :$sharedobj!
) returns Hash {
    my Str $error_code = %match && (%match<code>:exists) ?? ~%match<code> !! $code;

    $error_code = '404' unless $error_code ~~ /^(400|401|402|403|404|405|413|429)$/;

    my %t =
        tmpl_vocabulary_error => $sharedobj<rsrcobj>.vocabulary<error>,
        tmpl_exception_msg    => $message // q{},
        tmpl_httperr_code     => $error_code,
        tmpl_httperr_text     => $sharedobj<rsrcobj>.http_codes{$error_code}
    ;

    my $logdata = Hash.new({route => $route, match => %match}).push(%t);
    my $entry   = sprintf("API HTTP error %d for %s: %s", $error_code, %*ENV<REMOTE_ADDR> // q{}, to-json($sharedobj<utilobj>.stringify(:data($logdata)), :pretty));

    $sharedobj<dbugobj>.log(:$entry, :$sharedobj);

    return {
        component => $sharedobj<mb64obj>.encode-str(
            $sharedobj<pageobj>.raw_pg(
                :table($sharedobj<pageobj>.httperr),
                :fields(<id data compression>),
            ),
            :oneline
        ),
        tparams => %t,
    }
}

method index(
         :%match!,
    Str  :$route!,
    UInt :$tick!,
    Hash :$sharedobj!,
    Hash :$credentials = {},
    Hash :$payload = {},
) returns Hash {
    my Str $rakudo =
        'This is Rakudo compiler v' ~ $*PERL.compiler.version ~
        ' built on MoarVM v' ~ $*VM.version ~
        ' implementing ' ~ $*PERL ~ ' (' ~ $*PERL.version ~ ').';

    # force header update, see #161
    $sharedobj<pageobj>.fill_seodata('index', 'Pheix');

    my %t =
        $sharedobj<pageobj>.get_tparams,
        tmpl_rakudo_ver => $rakudo,
        tmpl_env_vars   => $sharedobj<dbugobj>.env_vars_list,
        tmpl_paragraph  => $sharedobj<jsonobj>.get_group_setting(
            'Pheix',
            'indexseotags',
            'paragraph',
            'value',
        )
    ;

    my $start = now;

    my %rc =
        component => $sharedobj<mb64obj>.encode-str(
            $sharedobj<pageobj>.raw_pg(
                :table($sharedobj<pageobj>.indxcnt),
                :fields(<id data compression>),
            ),
            :oneline
        ),
        tparams => %t,
    ;

    %rc<component_render> = (now - $start).Str;

    return %rc;
}

method captcha(
         :%match!,
    Str  :$route!,
    UInt :$tick!,
    Hash :$sharedobj!,
    Hash :$credentials = {},
    Hash :$payload = {},
) returns Str {
    die 'No utilites object is initialized' unless $sharedobj<utilobj>;

    return unless $credentials &&
        ($credentials<token>:exists) &&
            $credentials<token> eq $sharedobj<pageobj>.sesstoken;

    my Str $captcha;
    my Str $data = %match<query>:exists ?? %match<query>.Str !! q{};

    if ($data ~~ m:i/^ \? <[%\da..f]>+ $/) {
        my Bool $darkmode = $sharedobj<pageobj>.get_cparams<tmpl_modeclass> ~~ /night/ ?? True !! False;
        $captcha = $sharedobj<utilobj>.show_captcha($data, :$darkmode);
    }
    else {
        die sprintf("No seed data for captcha: <%s>", $data);
    }

    die 'Captcha generation error' unless $captcha;

    return $captcha;
}
