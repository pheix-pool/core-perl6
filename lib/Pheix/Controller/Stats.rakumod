unit class Pheix::Controller::Stats;

use GeoIP2;
use Pheix::Model::Database::Access;
use Pheix::Utils;
use Pheix::View::HTML::Markup;
use Pheix::View::Web::Headers;

has Bool $.test      = False;
has Any  $!headobj   = Pheix::View::Web::Headers.new;
has Any  $!mkupobj   = Pheix::View::HTML::Markup.new;
has UInt $!cropd     = 7;
has Str  $!mmdbpath  = 'ipdb/GeoLite2-Country.mmdb';
has Str  $.geoippath is default('/usr/share/GeoIP/') is rw;
has Any  $!dbobj = Pheix::Model::Database::Access.new(
    table   => 'bigbro',
    fields  => <id referer ip useragent resolution page country>,
    test    => $!test
);

method dbpath returns Str {
    $!dbobj.dbpath;
}

method get_mmdb_localpath returns Str {
    my Str $path = ( $!test ?? './www/' !! './' ) ~ $!mmdbpath;
    ( $path if $path.IO.e ) // Nil;
}

method get_mmdb_systempath returns Str {
    my Str $path = $!geoippath ~ $!mmdbpath.IO.basename;
    ( $path if $path.IO.e ) // Nil;
}

method get_mmdbpath returns Str {
    self.get_mmdb_localpath // self.get_mmdb_systempath;
}

method do_log( Any $cgi ) returns UInt {
    my UInt $rc = 0;
    my $_sn     = $!headobj.proto_sn;
    my $_ipaddr = %*ENV<REMOTE_ADDR> || '*.*.*.*';
    my $_cntry  = self.get_country($_ipaddr);
    ( my $_ref = $cgi.params<ref> || 'undef' ) ~~ s:i/^ $_sn //;

    my $insert_params = {
        id => time,
        referer => $_ref,
        ip => $_ipaddr,
        useragent => %*ENV<HTTP_USER_AGENT> || 'undef',
        resolution => $cgi.params<resolut> || 'undef',
        page => $cgi.params<page> || 'undef',
        country => $_cntry
    };

    $rc = $!dbobj.insert($insert_params);
    self.crop_log($!cropd);
    $rc;
}

multi method get_country( Str:U $ip ) returns Str { 'undef'; }

multi method get_country( Str:D $ip ) returns Str {
    my Str $path    = self.get_mmdbpath;
    state $geoobj //= GeoIP2.new( path => $path ) if $path.defined;
    return try { $geoobj.locate(ip => $ip)<country><iso_code> } // 'undef';
}

method crop_log(UInt $days) returns UInt {
    my UInt $min = DateTime.new(now).earlier(days => $days).posix;
    my @database = $!dbobj.get_all.map({
        $_ if (
            ($_.keys.elems == $!dbobj.fields.elems) &&
            ($_<id>:exists) &&
            ($_<id> ~~ /^ <[\d]>+ $/) &&
            ($_<id> > 0) &&
            ($_<id> < $min)
        )
    });

    my $cnt = @database.elems;

    for @database {
        if $!dbobj.remove(%(id => $_<id>)) {
            $cnt--;
        }
    }

    $cnt == 0 ?? 1 !! 0;
}

method get_traffic returns Hash {
    my %traffic = dates => [], hosts => [], visits => [];

    my @database = $!dbobj.get_all.map({
        $_ if (
            ($_.keys.elems == $!dbobj.fields.elems) &&
            ($_<id>:exists) &&
            ($_<id> ~~ /^ <[\d]>+ $/) &&
            ($_<id> > 0)
        )
    });

    my @days = @database.map({~DateTime.new($_<id>.UInt).clone(hour => 0, minute => 0, second => 0)}).unique;

    for @days -> $datestr {
        my $date = DateTime.new($datestr);
        my $dmin = $date.posix;
        my $dmax = $dmin + 86_400;

        my @daydb    = @database.map({$_ if ($_<id> >= $dmin && $_<id> < $dmax)});
        my @dayips   = @daydb.map({$_<ip>}).unique;
        my @daycntry = @daydb.map({$_<country> if $_<country> ne 'undef'}).unique.sort;

        %traffic<dates>.push(sprintf("%01d-%01d", $date.day, $date.month));
        %traffic<visits>.push(@daydb.elems);
        %traffic<hosts>.push(@dayips.elems);
        %traffic<countries>.push(@daycntry);
    }

    return %traffic;
}
