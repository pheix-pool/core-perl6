unit class Pheix::Test::Blockchain;

use Test;

has UInt $.logdbg     is default(0) is built(False);
has UInt $.loginf     is default(1) is built(False);
has UInt $.logwrn     is default(2) is built(False);
has UInt $.logerr     is default(3) is built(False);
has UInt $.logdpl     is default(4) is built(False);
has UInt $.logmsg     is default(5) is built(False);

has      @!teststat;
has      $!start_time  = now;
has      @.localnets is built(False) = <sepolia holesky>;
has      @.testnets is built(False)  = <sepolia holesky>;
has      @.levels is built(False)    = <0 1 2 3 4 5>;

has UInt $.public_wait = 30;
has UInt $.local_wait  = 3;
has UInt $.genwords    = 500;
has UInt $.waittxiters = 7_200;

has UInt $.debuglevel  = (%*ENV<PHEIXDEBUGLEVEL>.defined && (%*ENV<PHEIXDEBUGLEVEL> (elem) @!levels)) ?? %*ENV<PHEIXDEBUGLEVEL> !! $!logerr;
has Bool $.debug       = (%*ENV<PHEIXDEBUG>.defined && %*ENV<PHEIXDEBUG> == 1) ?? True !! False;
has Bool $.statictest  = (%*ENV<PHEIXTESTSTATIC>.defined && %*ENV<PHEIXTESTSTATIC> == 1) ?? True !! False;
has Bool $.pte         = (%*ENV<PHEIXTESTENGINE>.defined && %*ENV<PHEIXTESTENGINE> == 1) ?? True !! False;
has Bool $.fulltest    = (%*ENV<PHEIXFULLTEST>.defined && %*ENV<PHEIXFULLTEST> == 1) ?? True !! False;
has Str  $.etherlocal  = (%*ENV<ETHEREUMLOCALNODE>.defined && (%*ENV<ETHEREUMLOCALNODE> (elem) @!localnets)) ?? sprintf("%s_local", %*ENV<ETHEREUMLOCALNODE>) !! Str;
has Bool $.usekeystore = (%*ENV<USEGETHKEYSTORE>.defined && %*ENV<USEGETHKEYSTORE> == 1) ?? True !! False;

has Str  $.locstorage is default(Str) is rw;

has Str  $.testnet =
    (%*ENV<PUBLICTESTNET>.defined && (%*ENV<PUBLICTESTNET> (elem) @!testnets) && $!pte) ??
        %*ENV<PUBLICTESTNET> !! (($!etherlocal.defined && $!pte) ?? $!etherlocal !! Str);

method domains returns List {
    [
        "google.com",
        "facebook.com",
        "doubleclick.net",
        "google-analytics.com",
        "akamaihd.net",
        "googlesyndication.com",
        "googleapis.com",
        "googleadservices.com",
        "facebook.net",
        "youtube.com",
        "twitter.com",
        "scorecardresearch.com",
        "microsoft.com",
        "ytimg.com",
        "googleusercontent.com",
        "apple.com",
        "msftncsi.com",
        "2mdn.net",
        "googletagservices.com",
        "adnxs.com",
        "yahoo.com",
        "serving-sys.com",
        "akadns.net",
        "bluekai.com",
        "ggpht.com",
        "rubiconproject.com",
        "verisign.com",
        "addthis.com",
        "crashlytics.com",
        "amazonaws.com"
    ];
}

method ip_addrs returns List {
    [
        "5.135.164.72",
        "212.112.124.163",
        "147.135.206.233",
        "177.234.19.50",
        "31.145.50.34",
        "185.136.151.20",
        "191.239.243.156",
        "202.93.128.98",
        "77.95.132.41",
        "193.117.138.126",
        "182.52.51.10",
        "89.249.65.140",
        "190.214.12.82",
        "91.215.195.143",
        "178.128.88.35",
        "118.172.211.155",
        "87.244.182.181",
        "84.52.69.94",
        "1.2.169.4",
        "43.245.131.205",
        "173.249.51.179",
        "180.180.218.153",
        "91.143.54.254",
        "158.69.243.155",
        "35.185.201.225",
        "82.99.228.90",
        "185.81.99.205",
        "186.215.133.170",
        "31.130.91.37",
        "178.128.21.47"
    ];
}

method browsers returns List {
    [
        "Mozilla/5.0 (Amiga; U; AmigaOS 1.3; en; rv:1.8.1.19) Gecko/20081204 SeaMonkey/1.1.14",
        "Mozilla/5.0 (AmigaOS; U; AmigaOS 1.3; en-US; rv:1.8.1.21) Gecko/20090303 SeaMonkey/1.1.15",
        "Mozilla/5.0 (AmigaOS; U; AmigaOS 1.3; en; rv:1.8.1.19) Gecko/20081204 SeaMonkey/1.1.14",
        "Mozilla/5.0 (Android 2.2; Windows; U; Windows NT 6.1; en-US) AppleWebKit/533.19.4 (KHTML, like Gecko) Version/5.0.3 Safari/533.19.4",
        "Mozilla/5.0 (BeOS; U; BeOS BeBox; fr; rv:1.9) Gecko/2008052906 BonEcho/2.0",
        "Mozilla/5.0 (BeOS; U; BeOS BePC; en-US; rv:1.8.1.1) Gecko/20061220 BonEcho/2.0.0.1",
        "Mozilla/5.0 (BeOS; U; BeOS BePC; en-US; rv:1.8.1.10) Gecko/20071128 BonEcho/2.0.0.10",
        "Mozilla/5.0 (BeOS; U; BeOS BePC; en-US; rv:1.8.1.17) Gecko/20080831 BonEcho/2.0.0.17",
        "Mozilla/5.0 (BeOS; U; BeOS BePC; en-US; rv:1.8.1.6) Gecko/20070731 BonEcho/2.0.0.6",
        "Mozilla/5.0 (BeOS; U; BeOS BePC; en-US; rv:1.8.1.7) Gecko/20070917 BonEcho/2.0.0.7",
        "Mozilla/5.0 (BeOS; U; BeOS BePC; en-US; rv:1.8.1b2) Gecko/20060901 Firefox/2.0b2",
        "Mozilla/5.0 (BeOS; U; BeOS BePC; en-US; rv:1.9a1) Gecko/20051002 Firefox/1.6a1",
        "Mozilla/5.0 (BeOS; U; BeOS BePC; en-US; rv:1.9a1) Gecko/20060702 SeaMonkey/1.5a",
        "Mozilla/5.0 (BeOS; U; Haiku BePC; en-US; rv:1.8.1.10pre) Gecko/20080112 SeaMonkey/1.1.7pre",
        "Mozilla/5.0 (BeOS; U; Haiku BePC; en-US; rv:1.8.1.14) Gecko/20080429 BonEcho/2.0.0.14",
        "Mozilla/5.0 (BeOS; U; Haiku BePC; en-US; rv:1.8.1.17) Gecko/20080831 BonEcho/2.0.0.17",
        "Mozilla/5.0 (BeOS; U; Haiku BePC; en-US; rv:1.8.1.18) Gecko/20081114 BonEcho/2.0.0.18",
        "Mozilla/5.0 (BeOS; U; Haiku BePC; en-US; rv:1.8.1.21pre) Gecko/20090218 BonEcho/2.0.0.21pre",
        "Mozilla/5.0 (Darwin; FreeBSD 5.6; en-GB; rv:1.8.1.17pre) Gecko/20080716 K-Meleon/1.5.0",
        "Mozilla/5.0 (Darwin; FreeBSD 5.6; en-GB; rv:1.9.1b3pre)Gecko/20081211 K-Meleon/1.5.2",
        "Mozilla/5.0 (Future Star Technologies Corp.; Star-Blade OS; x86_64; U; en-US) iNet Browser 4.7",
        "Mozilla/5.0 (Linux 2.4.18-18.7.x i686; U) Opera 6.03 [en]",
        "Mozilla/5.0 (Linux 2.4.18-ltsp-1 i686; U) Opera 6.1 [en]",
        "Mozilla/5.0 (Linux 2.4.19-16mdk i686; U) Opera 6.11 [en]",
        "Mozilla/5.0 (Linux 2.4.21-0.13mdk i686; U) Opera 7.11 [en]",
        "Mozilla/5.0 (Linux X86; U; Debian SID; it; rv:1.9.0.1) Gecko/2008070208 Debian IceWeasel/3.0.1",
        "Mozilla/5.0 (Linux i686 ; U; en; rv:1.8.1) Gecko/20061208 Firefox/2.0.0 Opera 9.70",
        "Mozilla/5.0 (Linux i686; U; en; rv:1.8.1) Gecko/20061208 Firefox/2.0.0",
        "Mozilla/5.0 (Linux i686; U; en; rv:1.9.1.6) Gecko/20091201 Firefox/3.5.6 Opera 10.51",
        "Mozilla/5.0 (Linux) Gecko Iceweasel (Debian) Mnenhy"
    ];
}

method resolut returns List {
    [
        "960x640",   "800x480",   "1152x768",  "1024x800",  "1024x640",
        "1280x1024", "1280x960",  "1440x1024", "1600x1024", "1600x1200",
        "1920x1080", "1920x1200", "2048x1152", "2048x1536"
    ];
}

method pages returns List {
    [
        "/index.php",
        "/license.txt",
        "/readme.html",
        "/wp-activate.php",
        "/wp-admin/about.php",
        "/wp-admin/admin-ajax.php",
        "/wp-admin/admin-footer.php",
        "/wp-admin/admin-functions.php",
        "/wp-admin/admin-header.php",
        "/wp-admin/admin-post.php",
        "/wp-admin/admin.php",
        "/wp-admin/async-upload.php",
        "/wp-admin/comment.php",
        "/wp-admin/credits.php",
        "/wp-admin/custom-background.php",
        "/wp-admin/custom-header.php",
        "/wp-admin/edit-comments.php",
        "/wp-admin/edit-form-advanced.php",
        "/wp-admin/edit-form-comment.php",
        "/wp-admin/edit-link-form.php",
        "/wp-admin/edit-tag-form.php",
        "/wp-admin/edit-tags.php",
        "/wp-admin/edit.php",
        "/wp-admin/export.php",
        "/wp-admin/freedoms.php",
        "/wp-admin/gears-manifest.php"
    ];
}

method countries returns List {
    [
        "ES", "ET", "FI", "FJ", "FK", "FM", "FO", "FR", "FX", "GA",
        "GB", "GD", "GE", "GF", "GG", "GH", "GI", "GL", "GM", "GN",
        "GP", "GQ", "GR", "GS", "GT", "GU", "GW", "GY", "HK", "HN",
        "HR", "HT", "HU"
    ];
}

method drop_table(
    #Pheix::Model::Database::Access :$dbobj!,
        :$dbobj!,
    Str :$t
) returns Bool {
    my $co      = $dbobj.chainobj;
    my Bool $rc = True;

    if ($co.table_exists(:t($t)) && $co.unlock_account) {
        my @txs;
        my @ids;
        my %rhash;
        my UInt $rows = $co.count_rows(:t($t));

        if $rows {
            for ^$rows -> $rinx {
                my $rowid = $co.get_id_byindex(
                    :t($t),
                    :index($rinx)
                );
                if $rowid > 0 {
                    @ids.push($rowid);
                }
            }
        }

        if @ids.elems > 0 {
            for @ids -> $id {
                %rhash = $co.delete(
                    :t($t),
                    :id($id),
                    :waittx(False),
                );
                @txs.push(%rhash<txhash>);
            }
        }
        else {
            %rhash = $co.delete(
                :t($t),
                :id(0),
                :waittx(False),
            );
            @txs.push(%rhash<txhash>);
            diag('***INF: drop blank tab ' ~ $t ~ ': ' ~ %rhash<txhash>) if $!debug;
        }

        $co.wait_for_transactions(:hashes(@txs), :attempts($!waittxiters));

        if self.prove_existence(:$dbobj, :$t) {
            diag(sprintf("***ERR: failed drop table %s, failed txs: %s",  $t, @txs.join(q{,}) // q{-}));

            $rc = False;
        }
    }

    return $rc;
}

method drop_all(
    #Pheix::Model::Database::Access :$dbobj!
    :$dbobj!
) returns Bool {
    my $co        = $dbobj.chainobj;
    my Bool $rc   = False;
    my @txs;
    my @existed_tables;
    for (0..($co.count_tables - 1)) -> $index {
        my Str $t = $co.get_tabname_byindex(:index($index));
        if ($t && $co.table_exists(:t($t))) {
            @existed_tables.push(%(name => $t, deleted => False));
        }
    }

    for @existed_tables.kv -> $index,%record {
        my Str $t = %record<name>;
        %record<deleted> = self.drop_table(:dbobj($dbobj), :t($t));
    }

    my @fail = @existed_tables.map({ $_ if $_<deleted> == False });
    if (
        @existed_tables &&
        @fail.elems == 0
    ) {
        $rc = True;
    }
    else {
        diag('***ERR: failed to delete tabs: ' ~ @fail.map({ $_<name> }).join(q{,}));
    }
    $rc
}

method fast_select_all(
         :$dbobj!,
    Str  :$t!,
    Bool :$withcomp = False
) returns List {
    my @ret;
    my $co   = $dbobj.chainobj;
    my $rows = $co.count_rows(:t($t));
    for (0..($rows-1)) -> $index {
        my $cmp = False;

        if $withcomp {
            my Int $rowid = $co.get_id_byindex(:t($t), :index($index));
            $cmp = $co.is_row_compressed(:t($t), :id($rowid));
        }

        my Str $ts = $co.get_data_byindex(
            :t($t),
            :index($index),
            :comp($cmp)
        );
        if $ts {
            @ret.push($ts);
        }
    }
    if $rows != @ret.elems {
        diag(
            '***FAILURE: get rows from tab <' ~ $t ~ '> no equal ' ~
            'to chainobj.count_rows() return value at fast_select_all()'
        );
    }
    @ret;
}

method bm(
    Str  :$ts!,
    Str  :$t,
    UInt :$r,
    UInt :$b,
    Bool :$return = False,
) returns Str {
    my Str $rs;

    @!teststat.push(
        %(
            tst   => $ts,
            time  => now,
            tab   => $t // q{-},
            rows  => $r // 0,
            bytes => $b // 0,
        )
    );

    my ($prelast, $last) = @!teststat.tail(2);

    my Str $timestamp = sprintf(
        '(%.3f sec)',
        abs( ( $last<time> // $!start_time ) - $prelast<time> ),
    );

    my Str $msg = sprintf(
        "%s%s%s %s",
        (@!teststat.tail)<tst>,
        ((@!teststat.tail)<tab> ne q{-}) ?? sprintf(": tab <%s>", (@!teststat.tail)<tab>) !! q{},
        ((@!teststat.tail)<rows> != 0) ?? sprintf(" with %d rows", (@!teststat.tail)<rows>) !! q{},
        $timestamp
    );

    return $return ?? $msg !! q{}
}

method print_statistics {
    my $test_duration = ( now - $!start_time );
    printf(
        '%-3s | %-32s | %-16s | %-3s | %-5s | Time (sec)' ~ "\n",
        'N',
        'Test description',
        'Table',
        'Row',
        'Bytes',
    );
    say S:g/<blank>/\-/ with sprintf(
        '%3s + %32s + %16s + %3s + %5s + %10s', ' ', ' ', ' ', ' ', ' ', ' '
    );
    my UInt $testnum = 0;
    for @!teststat -> %st {
        if !( %st<tst> ~~ s/ ^ '#' // ) {
            $testnum++;
        }
        printf(
            '%03d | %-32s | %-16s | %03d | %05d | %.3f' ~ "\n",
            $testnum, %st<tst>,
            %st<tab>:exists   ?? %st<tab>       !! q{-},
            %st<rows>:exists  ?? %st<rows>.Int  !! q{-},
            %st<bytes>:exists ?? %st<bytes>.Int !! q{-},
            ( %st<time> - $!start_time ),
        );
        $!start_time = %st<time>;
    }
    say ( S:g/<blank>/\-/ with
        sprintf(
            '%3s + %32s + %16s + %3s + %5s + %10s', ' ', ' ', ' ', ' ', ' ', ' '
        )
    ) ~ "\n"
    ~ sprintf('%71s | %.3f' ~ "\n", 'Total time:', $test_duration );
}

method is_public(Str :$storage) returns Bool {
    $storage (elem) @!testnets ?? True !! False;
}

method is_local(Str :$storage) returns Bool {
    die 'local storage name is not initialized' unless $!locstorage;

    my @publics = .append(@!testnets.Slip, $!etherlocal);

    $storage &&
        $storage eq $!locstorage &&
            !($storage (elem) @publics) ?? True !! False;
}

method prove_existence(:$dbobj, Str :$t, UInt :$iters = 60) returns Bool {
    my $co = $dbobj.chainobj;

    for ^$iters -> $iter {
        return False unless $co.table_exists(:$t);

        sleep($!local_wait);
    }

    return True;
}
