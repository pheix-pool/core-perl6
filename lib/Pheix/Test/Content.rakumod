unit class Pheix::Test::Content;

use P5quotemeta;
use Pheix::Datepack;

has Str $.cntpath = 'conf/_pages';
has @.chkpars = <
    tmpl_pagetitle
    tmpl_metadesc
    tmpl_metakeys
    tmpl_srvname
    tmpl_timestamp
    tmpl_content
    tmpl_update_year
    tmpl_version
    tmpl_bigbrojs
>;
has Str $!httpdate =
    Pheix::Datepack
        .new(:date(DateTime.now), :unixtime(time))
        .get_http_response_date;

method checkheader(
    Str  :$header!,
    UInt :$status,
    Str  :$ctype,
    Str  :$f,
         :@cookies,
) returns Bool {
    my Bool $ret  = False;

    my UInt $stat = $status // 200;
    my Str $ct =
        $ctype.defined ?? $ctype !! 'text/html; charset=UTF-8';

    my Str $expr1 = 'Last\-Modified\:\s?:' ~ self!dateregexpr(:f($f));
    my Str $expr2 = 'Expires\:\s?:' ~ self!dateregexpr(:f($f));
    my Str $expr3 = '.';

    if @cookies {
        $expr3 = 'Set\-Cookie:\:\s?:' ~ @cookies.map({quotemeta($_)}).join('\s');
    }

    if (
        $header ~~ / <$expr1> / &&
        $header ~~ / <$expr2> / &&
        $header ~~ / <$expr3> / &&
        $header ~~ /
            'Cache-Control:' \s?
            'no-cache,no-store,max-age=0,must-revalidate' / &&
        $header ~~ / 'Content-Type:' \s?: $ct / &&
        $header ~~ / 'P3P: CP="Pheix does not have a P3P policy."' / &&
        $header ~~ / 'Status:' \s? $stat / &&
        $header ~~ / 'X-Request-ID:' \s?: <[\d]>+ / &&
        $header ~~ / 'Content-Length:' \s?: <[\d]>+ /
    ) {
        $ret = True;
    }

    $ret;
}

method checkcontent(Str :$cnt!, :@keys, Hash :$params!) returns Bool {
    my Bool $rc = True;

    my @k = @keys.elems ?? @keys !! @!chkpars;

    for @k -> $key {
        my Str $v = $params{$key};

        if $cnt !~~ / :r $v / {
            $rc = False;
            last;
        }
    }

    $rc;
}

method checkdate(Instant $d) returns Str {
    my Str $date = q{};

    if $d {
        my $dateobj = DateTime.new($d);
        $date  = sprintf(
            "%04d-%02d-%02d",
            $dateobj.year,
            $dateobj.month,
            $dateobj.day
        );
    }

    $date;
}

method !dateregexpr(Str :$f) returns Str {
    my Str $htd = $f.defined ?? self!getfiledate(:f($f)) !! $!httpdate;

    $htd ~~ s/(\d\d)\:(\d\d)\:(\d\d)/\\d\\d\\:\\d\\d\\:\\d\\d/;
    $htd ~~ s:g/\,/\\\,/;
    $htd ~~ s:g/\s/\\s/;

    $htd
}

method !getfiledate(Str :$f) returns Str {
    my Str $ret;

    if $f.defined && $f.IO.e {
        my $mdtime = $f.IO.modified;
        if ($mdtime) {
            $ret = Pheix::Datepack.new(
                :date(Date.new($mdtime).DateTime),
                :unixtime($mdtime.UInt),
            ).get_http_response_date;
        }
    }

    $ret;
}
