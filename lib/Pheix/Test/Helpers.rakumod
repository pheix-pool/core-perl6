unit class Pheix::Test::Helpers;

use Pheix::Model::JSON;

has Str  $!dbpath       = 'conf/system';
has UInt @.ethereumpids = (1693480781, 1725901939);
has UInt @.nomodpids    = (1628445000, 1644497998, 1696983823, 1717176196, 1717367286);
has Str  @.submodules   = <conf/config.json www/resources/skins/akin/img/captcha-distortion.png>;
has @.storages;
has @!stats;

method get_addon_sitemap_from_fs(:$addon!) returns List {
    my @sitemap;

    my %database = ($addon.jsonobj.get_entire_config)<database>;

    for %database.keys -> $record {
        my $pid    = %database{$record}<id>;
        my $seouri = %database{$record}<seouri>;

        my $d = DateTime.new(
            sprintf("%s/%s/%s.tnk", $!dbpath, $addon.get_name.lc, $pid)
                .IO
                .modified || now
        );

        my $lastmod = sprintf("%04d-%02d-%02d", $d.year, $d.month, $d.day);

        my $path =
            %database{$record}<allowpage> == 1 ??
                $addon.get_name.lc ~ '/page/' !!
                    $addon.get_name.lc ~ q{/};

        my $page =
            $seouri && $seouri.chars ??
                $seouri !!
                    ($addon.allowid ?? $pid !! Str);

        if $page {
            next if $pid.UInt ∈ @!ethereumpids;

            @sitemap.push(
                {
                    loc     => $path ~ $page,
                    lastmod => $lastmod,
                }
            );
        }
    }

    @sitemap;
}

method time_stats(Str :$module!) returns UInt {
    my $stime;

    "***INFO: subtests:".say;

    for @!stats -> %h {
        printf("%16s : %10f secs\n", %h<descr>, %h<time>);
        $stime += %h<time>;
    }

    printf(
        "***INFO: total for %s unit tests <%.3f> secs\n",
        $module,
        $stime
    );

    1;
}

method get_code_time(Str :$descr!, :$coderef!) returns Duration {
    my $mtime = { :descr($descr), :start(now), :time(0) };

    {
        $coderef();
    }

    $mtime<time> = now - $mtime<start>;
    push(@!stats, $mtime);

    $mtime<time>;
}

method patch_storage_config(
    Str  :$addon!,
    Str  :$addonpath!,
    Hash :$pairs!
) returns Pheix::Model::JSON {
    my Bool @patched;

    my $jsonobj = Pheix::Model::JSON.new(:$addonpath).set_entire_config(:$addon);

    return $jsonobj unless $pairs.keys;

    for $pairs.kv -> $setting, $value {
        for @!storages -> $storage {
            @patched.push(
                $jsonobj.set_group_setting(
                    $addon, 'storage', $storage, $setting, $value,
                    :temporary(True)
                )
            );
        }
    }

    X::AdHoc.new(:payload('Inconsistent patch')).throw unless
        @patched.elems == @!storages.elems * $pairs.keys;

    for @patched -> $status {
        X::AdHoc.new(:payload('Patch failure')).throw unless $status;
    }

    return $jsonobj;
}

method check_submodules returns Bool {
    for @!submodules -> $filepath {
        return False unless $filepath.IO.e && $filepath.IO.f;
    }

    return True;
}
