unit class Pheix::Test::BlockchainComp::Helpers;

use Test;
use Data::Dump;

use Pheix::Model::Database::Compression;
use Pheix::Datepack;

has      $.tstobj;
has UInt $.tab_size     is default(10);
has Str  $.testnet      is default('tst_table');
has Str  $.localtab     is default('tst_table');
has Str  $.filename     is default('./t/data/utf8demo.txt');
has Str  $.keystorepath is default('./t/data/keystore/geth');
has Bool $.payload      is default(True);
has Bool $.static       is default(False);

class X is Exception {
    has $.payload is default('smth went wrong') is rw;

    method message {
        sprintf("%s: %s", self.^name, $!payload);
    }
};

method dbobj_tweak(:$dbobj, Str :$testnet) {
    if $dbobj && $dbobj.dbswitch == 1 {
        # $dbobj.chainobj.comp  = True;
        $dbobj.chainobj.ethobj.tx_wait_iters =
            $!tstobj.is_local(:storage($testnet)) ?? $!tstobj.waittxiters !! 60;

        $dbobj.chainobj.ethobj.tx_wait_sec =
            $!tstobj.is_local(:storage($testnet)) ??
                $!tstobj.local_wait !!
                    $!tstobj.public_wait;

        $dbobj.chainobj.nounlk = True if $!tstobj.is_public(:storage($testnet));
        $dbobj.chainobj.diag   = sub _diag(Str:$m){self.diag(:l($!tstobj.logdbg),:m($m))};

        if $!tstobj.usekeystore {
            (my $ethacc  = $dbobj.chainobj.ethacc) ~~ s:i/^0x//;
            my $keystore = $!keystorepath.IO.dir.grep({$_.f && $_.basename ~~ /<{$ethacc}>/}).head;

            if $keystore {
                $dbobj.chainobj.sgnobj           = $dbobj.chainobj;
                $dbobj.chainobj.config<keystore> = ~$keystore;

                self.diag(
                    :l($!tstobj.loginf),
                    :m(sprintf("keystore file: %s", $dbobj.chainobj.config<keystore>))
                );
            }
        }
    }

    return $dbobj;
}

method check_test_obj {
    die Pheix::Test::BlockchainComp::Helpers::X.new(:payload('no test obj defined'))
        if !$!tstobj.defined;
}

method debug_database(:%database) {
    self.check_test_obj;

    self.diag(:l($!tstobj.loginf), :m(sprintf("current databases: %d", %database<db>.elems)));

    for %database<db>.List.kv -> $index, $rows {
        self.diag(:l($!tstobj.loginf), :m(sprintf("table #%d rows: %d", $index, $rows.elems)));
    }
}

method create_database(
    Any  :$dbobj!,
    UInt :$fixedrows,
         :@tabnames!,
    Str  :$comptype = 'active'
) returns Hash {
    my %ret = status => False, db => [];

    my @existed_tables;

    self.check_test_obj;

    for @tabnames -> $tabname {
        if ($dbobj && $tabname) {
            my UInt $sz = $fixedrows //
                ($!static ?? $!tab_size !! (1..($!tab_size/2 + 1)).rand.Int);

            my %rtab = self.make_etalon_tab(
                :dbobj($dbobj),
                :t($tabname),
                :tsz($sz),
                :comptype($comptype)
            );

            %ret<db>.push(%rtab<db>) if %rtab<db>.elems;

            @existed_tables.push(
                {
                    name    => $tabname,
                    size    => %rtab<db>.elems,
                    created => %rtab<status>
                }
            );
        }
    }

    if (
        @existed_tables &&
        @existed_tables.map({ $_ if $_<created> == False }).elems == 0
    ) {
        %ret<status> = True;
    }
    else {
        self.diag(:l($!tstobj.logerr), :m('db failure, no existed tabs')) unless @existed_tables && @existed_tables.elems;

        for @existed_tables -> $tab {
            if $tab<created> == False {
                self.diag(:l($!tstobj.logerr), :m(sprintf("db failure, tab %s, size=%d, status=%d", $tab<name>, $tab<size>, $tab<created>)));
            }
        }
    }

    return %ret;
}

method make_etalon_tab(
    Any  :$dbobj!,
    Str  :$t,
    UInt :$tsz,
    Str  :$comptype= 'active'
) returns Hash {
    my %ret = status => False, db => [];

    self.check_test_obj;

    if $dbobj {
        my @txs;
        $dbobj.chainobj.unlock_account;
        $dbobj.chainobj.table_create(:t($t), :waittx(True));
        %ret<db> = self.generate_table(:tsz($tsz // $!tab_size));

        for @(%ret<db>).kv -> $index, $record {
            my %rhash;
            my $comp;

            my Int $rtio = 0;
            my Str $rec  = $record.chomp;

            if $!testnet eq $!localtab {
                $dbobj.chainobj.cmpobj.compress(:data($rec));
                $rtio = $dbobj.chainobj.cmpobj.get_ratio;
            }

            if $comptype eq 'compressed' {
                $comp = True;
            }
            elsif $comptype eq 'plain' {
                $comp = False;
            }
            else {
                $comp = ($rtio > 0 && $!testnet eq $!localtab) ?? True !! False;
            }

            %rhash = $dbobj.chainobj.row_insert(
                :t($t),
                :data(self.make_etalon_rec(:array($rec.split(q{|})))),
                :comp($comp),
                :waittx(False),
            );

            if %rhash<status> && %rhash<error> eq q{} {
                @txs.push(%rhash<txhash>);

                self.diag(:l($!tstobj.loginf), :m(sprintf("%2d. insert to %s, ratio=%3d%%, comp=%5s, tx=%s", $index, ($t // $dbobj.chainobj.table), $rtio, ($comp ?? $dbobj.chainobj.cmpobj.get_algo !! $comp), %rhash<txhash>)));
            }
            else {
                my UInt $olen = %ret<db>.elems;
                my @rebase    = %ret<db>.map({ $_ if $_ ne $record });
                %ret<db>      = @rebase;

                $dbobj.chainobj.nonce = Nil;

                self.diag(:l($!tstobj.logwrn), :m(sprintf("tab %s reorg rows %d -> %d, reason: %s", ($t // $dbobj.chainobj.table), $olen, %ret<db>.elems, %rhash<error>)));
                self.diag(:l($!tstobj.logwrn), :m(sprintf("skip insert to %s comp=%s", ($t // $dbobj.chainobj.table), $comp)));
            }
        }

        if $dbobj.chainobj.wait_for_transactions(:hashes(@txs), :attempts($!tstobj.waittxiters)) {
            %ret<status> = True;
        }
        else {
            if @txs && @txs.kv.elems {
                for @txs.kv -> $index, $txhash {
                    my %h =
                        $dbobj.chainobj.ethobj.
                            eth_getTransactionReceipt($txhash);

                    if %h<status>:!exists || %h<status> == 0 {
                        self.diag(:l($!tstobj.logerr), :m(sprintf("%s: tab %s, row %d, tx hash %s", &?ROUTINE.name, $t, $index, $txhash)));

                        self.trace_transaction(:dbobj($dbobj), :trx($txhash));
                    }
                    else {
                        self.diag(:l($!tstobj.logerr), :m(sprintf("%s: trx %s for tab %s reciept failure %s", &?ROUTINE.name, $txhash, $t, %h.gist)));
                    }
                }
            }
            else {
                self.diag(:l($!tstobj.logwrn), :m('no transactions to be waited'));

                %ret = status => True, db => [];
            }
        }
    }

    return %ret;
}

method generate_table(UInt :$tsz!) returns List {
    my @db_in_mem;

    self.check_test_obj;

    for (1..$tsz) {
        my Str $payload_record;

        my UInt $d_index  = $!static ?? 0 !! $!tstobj.domains.elems.rand.Int;
        my UInt $ip_index = $!static ?? 0 !! $!tstobj.ip_addrs.elems.rand.Int;
        my UInt $ua_index = $!static ?? 0 !! $!tstobj.browsers.elems.rand.Int;
        my UInt $r_index  = $!static ?? 0 !! $!tstobj.resolut.elems.rand.Int;
        my UInt $p_index  = $!static ?? 0 !! $!tstobj.pages.elems.rand.Int;
        my UInt $c_index  = $!static ?? 0 !! $!tstobj.countries.elems.rand.Int;

        my @dataset = [
            $!tstobj.domains[$d_index],
            $!tstobj.ip_addrs[$ip_index],
            $!tstobj.browsers[$ua_index],
            $!tstobj.resolut[$r_index],
            $!tstobj.pages[$p_index],
            $!tstobj.countries[$c_index],
        ];

        my $words_num = $!static ??
            (($!tstobj.genwords * 2).rand.Int || $!tstobj.genwords) !!
                $!tstobj.genwords;

        if $!payload {
            @dataset.push(self.generate_content(:words($words_num)));
        }

        @db_in_mem.push(@dataset.join(q{|}));
    }

    self.diag(:l($!tstobj.loginf), :m('table with ' ~ $tsz ~ ' rows is generated'));

    return @db_in_mem
}

method make_etalon_rec(:@array) returns Hash {
    my %payload;

    self.check_test_obj;

    my @arr = (@array && @array.elems) ?? @array !!
            self.generate_table(:tsz(1))[0].chomp.split(q{|});

    if $!payload {
        %payload = payload => @arr[6];
    }

    return {
        domains   => @arr[0],
        ip_addrs  => @arr[1],
        browsers  => @arr[2],
        resolut   => @arr[3],
        pages     => @arr[4],
        countries => @arr[5],
        %payload
    };
}


method generate_content(UInt :$words!) returns Str {
    my UInt $tbck = 256;
    my UInt $blen = 3;
    my UInt $bwrd = 5;

    self.check_test_obj;

    my @content;
    my @bricks;
    my Str $fcntnt = $!filename.IO.slurp // q{};

    if $fcntnt.chars > 0 {
        my @chars = $fcntnt.split(q{});

        for ^$tbck {
            my @brick;
            for ^$blen {
                my UInt $pos = $!static ?? @chars.elems !! @chars.elems.rand.Int;
                @brick.push(@chars[$pos]);
            }
            @bricks.push(@brick.join(q{}))
        }

        for ^$words {
            my @word;
            my UInt $wlen = $!static ?? $bwrd !! $bwrd.rand.Int + 1;
            for ^$wlen {
                my UInt $pos = $!static ?? @bricks.elems !! @bricks.elems.rand.Int;
                @word.push(@bricks[$pos])
            }
            @content.push(@word.join(q{}));
        }
    }

    my Str $rs = @content.join(q{});

    $rs ~~ s:g/ '|' //;
    $rs ~~ s:g/ <[\r\n]>+ //;

    return $rs;
}

method validate_database(:$dbobj!, Str :$t) returns List {
    my @bcdb;

    self.check_test_obj;

    my UInt $tabs = ($t && $t ne q{}) ?? 1 !! $dbobj.chainobj.count_tables;

    for ^$tabs -> $tinx {
        my @tabrows;

        my Str $tnam = ($tabs == 1 && $t && $t ne q{}) ?? $t !!
                $dbobj.chainobj.get_tabname_byindex(:index($tinx));

        if $dbobj.chainobj.table_exists(:t($tnam)) {
            if !$dbobj.chainobj.is_tab_compressed(:t($tnam)) {
                my UInt $rows = $dbobj.chainobj.count_rows(:t($tnam));

                for ^$rows -> $rinx {
                    my $rid = $dbobj.chainobj.get_id_byindex(:t($tnam), :index($rinx));
                    my $cmp = $dbobj.chainobj.is_row_compressed(:t($tnam), :id($rid));
                    my %row = $dbobj.chainobj.select(:t($tnam), :id($rid), :comp($cmp));
                    my $rec = $dbobj.fields.map({ %row{$_} }).join(q{|}) if %row;

                    if $rec {
                        @tabrows.push($rec);
                    }
                    else {
                        self.diag(:l($!tstobj.logerr), :m(sprintf("select(%s, %d, %d) failed", $tnam, $rid, $cmp)));
                    }
                }
            }
            else {
                self.diag(:l($!tstobj.logerr), :m(sprintf("table %s is compressed", $tnam)));
            }
        }
        else {
            self.diag(:l($!tstobj.logerr), :m(sprintf("table %s is not existed", $tnam)));
        }

        if @tabrows {
            @bcdb.push(@tabrows);
        }
        else {
            self.diag(:l($!tstobj.logerr), :m(sprintf("no rows for table %s", $tnam)));
        }
    }

    return @bcdb;
}

method diag(UInt :$l, Str :$m) {
    return unless $!tstobj.debug;

    my Str $prefix;

    self.check_test_obj;

    given $l {
        when 5  {$prefix = 'MSG: '}
        when 4  {$prefix = 'DPL: '}
        when 3  {$prefix = 'ERR: '}
        when 2  {$prefix = 'WRN: '}
        when 1  {$prefix = 'INF: '}
        when 0  {$prefix = 'DBG: '}
        default {$prefix = q{}}
    }

    diag(sprintf("[%s] %s%s", Pheix::Datepack.new.get_date_logging, $prefix, $m))
        if ($l // $!tstobj.logerr // 0) >= $!tstobj.debuglevel && $m ne q{};
}

method trace_transaction(:$dbobj, Str :$trx, Bool :$diag = True) returns Hash {
    return {error => sprintf("invalid trx hash: %s", $trx // q{})} unless $trx && $trx ~~ m:i/^ 0x<xdigit>**64 $/;

    my $trx_details = {
        trx     => $dbobj.chainobj.ethobj.eth_getTransactionByHash($trx),
        receipt => $dbobj.chainobj.ethobj.eth_getTransactionReceipt($trx),
    };

    try {
        $trx_details<trace> =
            $dbobj.chainobj.ethobj.debug_traceTransaction(:trx($trx)) //
                {status => 'null trace'};

        CATCH {
            default {
                my Str $error = .message;

                $trx_details<trace> = {error => $error};
            }
        };
    }

    self.diag(:m(Dump($trx_details))) if $diag;

    return $trx_details;
}

method flush_signing_session(:$sgnlog!, Str :$filename, Str :$filepostfix = q{}) returns Bool {
    my $fname = $filename;

    $fname = sprintf("%s-sgn-%s%s.log", $*PROGRAM.basename, DateTime.now(formatter => {
        sprintf(
            "%04d%02d%02d-%02d:%02d:%02u.%04u",
            .year,
            .month,
            .day,
            .hour,
            .minute,
            .second.Int,
            (.second % (.second.Int || .second)) * 10_000
        )
    }), $filepostfix) unless $fname && $fname ne q{};

    if $sgnlog && $sgnlog.elems {
        try {
            if $fname.IO.e && $fname.IO.f {
                my $fh = $fname.IO.open(:a);

                $fh.print: sprintf("%s\n", $sgnlog.join("\n"));
                $fh.close;
            }
            else {
                $fname.IO.spurt(sprintf("%s\n", $sgnlog.join("\n")));
            }
        }
        CATCH {
            default {
                my Str $error = .message;

                Pheix::Test::BlockchainComp::Helpers::X.new(
                    :payload(sprintf("Signing log failure: %s", $error))
                ).throw;
            }
        };

        self.diag(:l($!tstobj.loginf), :m(sprintf("save signing log to %s", $fname)));

        return True;
    }
    else {
        self.diag(:l($!tstobj.logwrn), :m(sprintf("attempt to flush null signing log to %s", $fname)));
    }

    return False;
}

method text_to_frames(Str :$text!, UInt :$framelen = 1024) returns List {
    my @frames;

    if $text.defined && $text.chars {
        my $ff  = ($text.chars / $framelen).ceiling;
        my $hf  = $text.chars % $framelen;

        for ^$ff -> $index {
            if $index < $ff {
                @frames.push($text.substr($index * $framelen, $framelen));
            } else {
                @frames.push($text.substr($index * $framelen, $hf));
            }
        }
    }

    return @frames;
}

method buffer_to_frames(Buf[uint8] :$buf!, UInt :$framelen = 1024) returns List {
    my @frames;

    if $buf.defined && $buf.bytes {
        my $ff  = ($buf.bytes / $framelen).ceiling;
        my $hf  = $buf.bytes % $framelen;

        for ^$ff -> $index {
            if $index < $ff {
                @frames.push($buf.subbuf($index * $framelen, $framelen));
            } else {
                @frames.push($buf.subbuf($index * $framelen, $hf));
            }
        }
    }

    return @frames;
}

method get_skip_indexes(Str:D :$mode, UInt:D :$blocks) returns Array {
    return [] if $mode eq 'full';

    srand(((now % 1) * 10000000).Int);

    my UInt $rb = $mode eq 'skip' ?? $blocks !! ($blocks + 1).rand.Int || 1;
    my @indexes;

    self.diag(:l($!tstobj.loginf), :m(sprintf("total frames: %d, uncompressed frames: %d", $blocks, $rb)));

    while (@indexes.elems != $rb) {
        my $index = $blocks.rand.Int;
        if @indexes.map({$_ if $_ == $index}).elems == 0 {
            @indexes.push($index);
        }
    }

    self.diag(:l($!tstobj.loginf), :m(sprintf("uncompressed indexes: %s", @indexes.sort.Array.gist)));

    @indexes.sort.Array;
}
