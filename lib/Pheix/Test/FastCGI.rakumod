unit class Pheix::Test::FastCGI;

use experimental :pack;

use JSON::Fast;
use OpenSSL::Digest;

has @!output;
has UInt $.clen is default(782) is rw;
has Str  $.rmth is default('POST') is rw;
has Str  $.ruri is default('/api') is rw;
has Str  $.uniq = '0x' ~ md5(now.Rat.Str.encode('UTF8')).unpack("H*");
has %!env =
    CONTENT_LENGTH => $!clen,
    CONTENT_TYPE => 'application/x-www-form-urlencoded; charset=UTF-8',
    CONTEXT_DOCUMENT_ROOT => '/home/apache/root/perl6-pheix/www',
    CONTEXT_PREFIX => q{},
    DOCUMENT_ROOT => '/home/apache/root/perl6-pheix/www',
    FCGI_ROLE => 'RESPONDER',
    GATEWAY_INTERFACE => 'CGI/1.1',
    HTTP_ACCEPT => '*/*',
    HTTP_ACCEPT_ENCODING => 'gzip, deflate',
    HTTP_ACCEPT_LANGUAGE => 'ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3',
    HTTP_CONNECTION => 'close',
    HTTP_HOST => 'perl6.mac-webtech',
    HTTP_ORIGIN => 'http://perl6.mac-webtech',
    HTTP_REFERER => 'http://perl6.mac-webtech/',
    HTTP_USER_AGENT => 'Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:82.0) Gecko/20100101 Firefox/82.0',
    HTTP_X_REQUESTED_WITH => 'XMLHttpRequest',
    LD_LIBRARY_PATH => '/usr/local/apache2/lib',
    PATH => '/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/var/lib/snapd/snap/bin',
    QUERY_STRING => q{},
    REDIRECT_STATUS => 200,
    REDIRECT_UNIQUE_ID => 'X5anHapSOYvsofGUdOfjxQAAAMU',
    REDIRECT_URL => '/api',
    REMOTE_ADDR => '127.0.0.1',
    REMOTE_PORT => 40712,
    REQUEST_METHOD => $!rmth,
    REQUEST_SCHEME => 'http',
    REQUEST_URI => $!ruri,
    SCRIPT_FILENAME => '/home/apache/root/perl6-pheix/www/user.raku',
    SCRIPT_NAME => '/user.raku',
    SERVER_ADDR => '127.0.0.100',
    SERVER_ADMIN => 'kostas@perl6.mac-webtech',
    SERVER_NAME => 'perl6.mac-webtech',
    SERVER_PORT => 80,
    SERVER_PROTOCOL => 'HTTP/1.1',
    SERVER_SIGNATURE => q{},
    SERVER_SOFTWARE => 'Apache/2.4.41 (Unix) mod_fcgid/2.3.9',
    UNIQUE_ID => $!uniq
;

method set_env(Str :$key!, Str :$value!) returns Bool {
    %!env{$key} = $value;

    return True;
}

method env(UInt :$clen, Str :$rmth, Str :$ruri) returns Hash {
    my %e = %!env;

    %e<CONTENT_LENGTH> = $clen // $!clen;
    %e<REQUEST_METHOD> = $rmth // $!rmth;
    %e<REQUEST_URI>    = $ruri // $!ruri;

    %e;
}

method reset returns UInt {
    @!output = ();
    $!clen   = 782;
    @!output.elems;
}

method fetch returns Str {
    @!output.join(q{});
}

method accept returns Bool {
    True;
}

method header(%header) returns UInt {
    my %h = Content-Length => %header.keys.elems;

    %h.append(%header) if %header.keys;

    for %h.kv -> $k, $v {
        @!output.push($k ~ ': ' ~ $v ~ "\n");
    }

    @!output.push("\n");

    %h.keys.elems;
}

method Print(Str $content) returns List {
    @!output.push($content);
}

method Read(UInt $length) returns Str {
    my %ret;

    given $length {
        when 1 {
            %ret =
                credentials => {token => $!uniq},
                method      => 'GET',
                route       => '%2Findex',
                httpstat    => '200'
            ;
        }
        when 2 {
            %ret =
                credentials => {token => $!uniq},
                method      => 'GET',
                route       => sprintf("%%2F%s%%2Ffoo%%2Fbar", $!uniq),
                httpstat    => '404'
            ;
        }
        when 3 {
            %ret =
                credentials => {token => $!uniq},
                method      => 'GET',
                route       => sprintf("%%2F%s%%2Ffoo%%2Fbar%%2Fexception", $!uniq),
                httpstat    => 404
            ;
        }
        when 4 {
            %ret =
                log => 'Sep 01 08:38:41 webtech-omen kernel: usb usb2: We don\'t know the algorithms for LPM for this host, disabling LPM.'
            ;
        }
        when 5 {
            %ret =
                credentials => {
                    login => $!uniq,
                    password => md5('pwd'.encode('UTF8')).unpack("H*");
                },
                method      => 'GET',
                route       => sprintf("%%2F%s%%2Ffoo%%2Fbar", $!uniq),
                httpstat    => '200',
                payload     => {
                    payload_key1 => 'value1',
                    payload_key2 => 'value2',
                    payload_key3 => 'value3',
                    payload_keyN => 'valueN'
                }
            ;
        }
        when 6 {
            %ret =
                log => '[12043.613425] nouveau 0000:02:00.0: [drm] *ERROR* crtc 50: Can not calculate constants, dotclock = 0!'
            ;
        }
        default {
            ;
        }
    }

    to-json(%ret, :!pretty);
}
