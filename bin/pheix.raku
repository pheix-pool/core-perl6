#!/usr/bin/env raku

use v6.d;

BEGIN {};

use Pheix::App;

my Bool $test = @*ARGS[0] // q{} eq 'test' ?? True !! False;

my $pheix = Pheix::App.new(:test($test));

$pheix.start;

END {};
