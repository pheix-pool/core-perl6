#!/usr/bin/env raku

# raku -Ilib ./conf/_pages/convert-to-filechain-v2.raku --path=./conf/_pages/index.txt --mode=rand --outpath=. --algo=bzip2

use v6;

use MIME::Base64;
use LZW::Revolunet;
use Pheix::Model::Database::Access;
use Pheix::Model::Database::Blockchain;
use Pheix::Model::JSON;
use Pheix::Test::BlockchainComp::Helpers;
use Pheix::Test::Blockchain;
use Net::Ethereum;

enum Mode <full rand skip>;
enum Algo <lzw bzip2>;

constant framelen = 4096;
constant netether = Net::Ethereum.new;
constant helpobj  = Pheix::Test::BlockchainComp::Helpers.new(:tstobj(Pheix::Test::Blockchain.new(:debuglevel(0), :debug(True))));

sub MAIN (Str:D :$path!, Str:D :$outpath!, Mode:D :$mode = (full), Algo:D :$algo = (lzw))
{
    die 'invalid file is given' unless $path ne q{} && $path.IO.e && $path.IO.f;
    die 'invalid output is given' unless $outpath ne q{} && $outpath.IO.e && $outpath.IO.d;

    my @legend = <id data compression>;
    @legend.tail ~= sprintf("[%s:%s]", $mode, $algo);

    (my $tab = $path.IO.basename) ~~ s:i/\.<{$path.IO.extension}>//;

    my $jsonobj = Pheix::Model::JSON.new.set_entire_config(:addon('Pheix'), :setup(
        {
            module => {
                configuration => {
                    settings => {
                        storage => {
                            group => {
                                $tab => {
                                    type => "0",
                                    path => $outpath,
                                    config => {
                                        compression => $algo
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    ));

    my $dbobj = Pheix::Model::Database::Access.new(
        :table($tab),
        :fields(@legend),
        :jsonobj($jsonobj)
    );

    my $bcobj = Pheix::Model::Database::Blockchain.new(
        :abi('{}'),
        :apiurl('http://localhost'),
        :sctx('0x0'),
        :fields(@legend),
        :table($tab),
        :config($jsonobj.get_entire_config<module><configuration><settings><storage><group>{$tab}<config>)
    );

    $dbobj.remove_all;

    # my $filebuffer = Buf[uint8].new($path.IO.slurp.encode);
    # my @framebuffs = buffer_to_frames(:buf($filebuffer, :framelen(framelen)));
    my $rawdata = $path.IO.slurp;

    if $mode eq 'full' {
        $rawdata = netether.buf2hex($bcobj.cmpobj.compress(:data($rawdata)));
    }
    else {
        $rawdata = prepare_plain(:data($rawdata));
    }

    my @framebuffs = helpobj.text_to_frames(:text($rawdata), :framelen(framelen));
    my @skipindxs  = helpobj.get_skip_indexes(:mode(~$mode), :blocks(@framebuffs.elems));

    # note 'File ', $path, ', tab=', $tab, ', bytes=', $filebuffer.bytes, ' (', @framebuffs.elems, ' frames)';
    note 'File ', $path, ', tab=', $tab, ', chars=', @framebuffs.join.chars, ' (', @framebuffs.elems, ' frames)';
    note 'Skipping encoding on indexes: ', join(q{,}, @skipindxs) if @skipindxs;

    for @framebuffs.kv -> $index, $frame {
        # my $data = netether.buf2hex($frame);
        my $data = $frame;
        my $comp = 0;

        if @skipindxs && @skipindxs.map({$_ if $index == $_}).elems == 0 {
            $data = netether.buf2hex($bcobj.cmpobj.compress(:data($data)));

            note 'index ', $index + 1, ' ratio ', $bcobj.cmpobj.get_ratio, '% by ', $bcobj.cmpobj.get_algo;

            $comp = 1;
        }

        my $insert_data = {
            id => $index + 1,
            data => $data,
            @legend.tail => ~$comp,
        };

        $dbobj.insert($insert_data);
    }
}

sub prepare_plain(Str:D :$data!) {
    my %subst =
        nl => q{},
        vl => '&VerticalLine;'
    ;

    my $prepared = $data;

    if $data ne q{} {
        $prepared ~~ s:g/^^<[\s]>+//;
        $prepared ~~ s:g/^^<[\s]>+$$//;
        $prepared ~~ s:g/<[\r\n]>+//;
        $prepared ~~ s:g/<[|]>+/%subst<vl>/;
    }

    return $prepared;
}
