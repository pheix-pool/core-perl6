#!/usr/bin/env raku

sub MAIN (Str:D :$file, Str :$divider = "\n")
{
    die sprintf("wrong file %s", $file) unless $file.IO.e && $file.IO.f;

    my @deplist;
    my @dependencies = $file.IO.lines;

    die "no content" unless @dependencies && @dependencies.elems;

    for @dependencies.values -> $d {
        my $dependency = $d.chomp.trim;

        next unless $dependency && $dependency.chars && $dependency ~~ m/ ';' $/;

        next unless $dependency !~~ m/ 'experimental' /;

        if $dependency ~~ m/ 'use ' (<[a..zA..Z0..9\_\:\s]>+) ';' $/ {
            my $module = ~$0;

            @deplist.push($module) unless @deplist.grep({$_ eq $module}).head;
        }
    }

    @deplist.sort.join($divider).say;
}
