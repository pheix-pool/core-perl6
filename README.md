# Pheix dCMS implemented in Raku

**Pheix dCMS** is the [Raku](https://raku.org) driven decentralized Content Management System. It is extremely lightweight, flexible and customizable.

## Repository submodules

There are two external submodules in this repository.

1. `dcms-configuration`: https://gitlab.com/pheix/dcms-configuration
2. `dcms-resources`: https://gitlab.com/pheix/dcms-resources

These are mandatory for production-ready deployments and extended testing. Once you have cloned Pheix dCMS, run the command:

```bash
git submodule init && git submodule update --recursive --remote
```

## Installation guide

Check [Installation guide](https://gitlab.com/pheix/dcms-raku/-/wikis/installation-guide) section at our wiki for installation details, tips and tricks.

## API reference

Check [API reference — Pheix API](https://gitlab.com/pheix/dcms-raku/-/wikis/api-reference) section at our wiki for full API reference.

## Dependencies

Check the strict list of the [Pheix dCMS dependencies](https://gitlab.com/pheix/dcms-raku/wikis/module-dependencies) at our wiki.

## Borrowings

Pheix uses [CGI](https://github.com/viklund/november/blob/master/lib/November/CGI.pm) module from [November Wiki engine](https://github.com/viklund/november). Also the generic CGI tests are added to Pheix test bundle.

## License information

Pheix is free and open source software, so you can redistribute it and/or modify it under the terms of the [The Artistic License 2.0](https://opensource.org/licenses/Artistic-2.0).

## Changelog

All latest changes to this project are documented [at Pheix official website](https://pheix.org/changelog.txt).

## Author

Please contact me via [Matrix](https://matrix.to/#/@k.narkhov:matrix.org) or [LinkedIn](https://www.linkedin.com/in/knarkhov/). Your feedback is welcome at [narkhov.pro](https://narkhov.pro/contact-information.html).

## Donate

Pheix dCMS is an independent and self-funded project. All contributions and donations are greatly appreciated. Your support is important to me and inspires me to create new releases. Transfers in USDT, Ethereum, Bitcoin and TON are accepted. Please check the official [Donate](https://pheix.org/embedded/donate) page for details.
