use v6.d;
use Test;
use lib 'lib';

plan 11;

use Pheix::Addons::November::CGI;
use Pheix::Test::Helpers;
use Pheix::Model::JSON;
use Pheix::Utils;

use MagickWand;
use JSON::Fast;

my Pheix::Model::JSON $jsonobj = Pheix::Model::JSON.new;
my Pheix::Utils $obj = Pheix::Utils.new(:test(True), :jsonobj($jsonobj));

# Check stringify_hash method
subtest {
    plan 2;

    my %match =
        code       => 505,
        details    => %(path => '/default/route/to'.match(/.*/)),
        controller => 'A',
        action     => 'B',
        index      => '123'.match(/\d+/),
        nested     => {
            values => {
                1 => '1'.match(/<[0..9]>+/),
                2 => '2'.match(/<[0..9]>+/),
            },
            match => Match.new,
            list  => [
                1,
                '3'.match(/<[0..9]>+/),
                {
                    4 => '4'.match(/<[0..9]>+/),
                    5 => '5'.match(/<[0..9]>+/),
                },
                [ 1, 2, 3, Match.new, { 6 => '6'.match(/<[0..9]>+/)}]
            ]
        }
    ;

    lives-ok { to-json($obj.stringify(:data(%match))) }, 'lives stringified hash to JSON';

    throws-like {
        to-json($obj.stringify(:data(buf8.new('buf8'.encode))));
    },
    Exception,
    message => /'Unable to stringify structure'/,
    'exception on stringified hash with unknown structure to JSON ';
}, 'Check stringify_hash method';

# Check get_token_by_now method
subtest {
    my UInt $plan = 100;

    plan $plan;

    for ^$plan {
        my $token = $obj.get_token_by_now;

        ok $token && $token ~~ /^ 0x<xdigit>**32..* /,
            sprintf("step %03d token %s", $_ + 1, $token);
    }
}, 'Check get_token_by_now method';

# Check get_str method
subtest {
    plan 6;
    is(
        $obj.get_str('%30%31%32%33%34%35%36%37%38%39'),
        '0123456789',
        'get_str() on decoded digits',
    );
    is(
        $obj.get_str('%61%62%63%64%65%66%67%68%69%6a%6b' ~
            '%6c%6d%6e%6f%70%71%72%73%74%75%76%77%78%79%7a'),
        'abcdefghijklmnopqrstuvwxyz',
        'get_str() on decoded uppercase letters',
    );
    is(
        $obj.get_str('%41%42%43%44%45%46%47%48%49%4a%4b' ~
            '%4c%4d%4e%4f%50%51%52%53%54%55%56%57%58%59%5a'),
        'abcdefghijklmnopqrstuvwxyz'.uc,
        'get_str() on decoded lowercase letters',
    );
    is(
        $obj.get_str('%21%22%23%24%25%26%28%29%2a%2b%2c%2d' ~
            '%2e%2f%3a%3b%3c%3d%3e%3f%40%5b%5c%5d%5e%5f%60%7b%7d%7e'),
        '!"#$%&()*+,-./:;<=>?@[\]^_`{}~',
        'get_str() on decoded special characters',
    );
    is(
        $obj.get_str('%27'),
        q{'},
        'get_str() on decoded apostrophe',
    );
    is(
        $obj.get_str(q{}),
        Str,
        'get_str() on empty value',
    );
}, 'Check get_str method';

# Check get_hex method
subtest {
    plan 6;
    is(
        $obj.get_hex('0123456789'),
        '%30%31%32%33%34%35%36%37%38%39',
        'get_hex() on digits',
    );
    is(
        $obj.get_hex('abcdefghijklmnopqrstuvwxyz'),
        '%61%62%63%64%65%66%67%68%69%6a%6b%6c%6d%6e' ~
            '%6f%70%71%72%73%74%75%76%77%78%79%7a',
        'get_hex() on uppercase letters',
    );
    is(
        $obj.get_hex('abcdefghijklmnopqrstuvwxyz'.uc),
        '%41%42%43%44%45%46%47%48%49%4a%4b%4c%4d%4e' ~
            '%4f%50%51%52%53%54%55%56%57%58%59%5a',
        'get_hex() on lowercase letters',
    );
    is(
        $obj.get_hex('!"#$%&()*+,-./:;<=>?@[\]^_`{}~'),
        '%21%22%23%24%25%26%28%29%2a%2b%2c%2d%2e%2f%3a' ~
            '%3b%3c%3d%3e%3f%40%5b%5c%5d%5e%5f%60%7b%7d%7e',
        'get_hex() on special characters',
    );
    is(
        $obj.get_hex(q{'}),
        '%27',
        'get_hex() on apostrophe',
    );
    is(
        $obj.get_hex(q{}),
        Str,
        'get_hex() on empty value',
    );
}, 'Check get_hex method';

# Check get_files method
subtest {
    plan 5;
    ok(
        ( $obj.get_files('./t/').elems > 0 ),
        'get_files returns not empty files array',
    );
    ok(
        ( $obj.get_files('./t/').Str ~~ /$*PROGRAM-NAME/ ),
        'get_files returns self: ' ~ $*PROGRAM-NAME,
    );
    nok(
        ( $obj.get_files('./t/').map({$_ if $_ ~~ :d}) ),
        'get_files returns no dirs',
    );
    is(
        $obj.get_files('./!t/'),
        Seq,
        'get_files returns Seq on unexisted dir',
    );
    is(
        $obj.get_files(q{}),
        Seq,
        'get_files returns Seq on empty string',
    );
}, 'Check get_files method';

if !Pheix::Test::Helpers.new.check_submodules {
    skip-rest('No Pheix submodules are pulled');
    exit;
}

# Check set_smfn method
subtest {
    plan 4;

    my Str $fakefpath = q{/} ~ now.Rat ~ '/super/fake/path/file.xml';

    nok $obj.set_smfn(:fname($fakefpath)), 'set fake';
    ok $obj.set_smfn(:fname($fakefpath), :force(True)), 'force set';
    is $obj.smfn, $fakefpath, 'get forced path';
    ok $obj.set_smfn(:fname('./www/sitemap_static.xml')), 'set real';
}, 'Check set_smfn method';

# Check smfn method
subtest {
    plan 1;
    is( $obj.smfn, "./www/sitemap_static.xml", 'smfn returns ' ~ $obj.smfn );
}, 'Check smfn method';

# Check gen_cipher_param method
subtest {
    plan 3;
    is(
        $obj.gen_cipher_param('key'),
        '72<84d5e~98#&a61^!(5324f7#0a5!@r',
        'proper cipher key, ' ~
            $obj.gen_cipher_param('key').encode.bytes ~ ' bytes',
    );
    is(
        $obj.gen_cipher_param('iv'),
        'e<~4d9~52d8<4e7d', 'proper cipher iv,' ~
        $obj.gen_cipher_param('iv').encode.bytes ~ ' bytes',
    );
    is(
        $obj.gen_cipher_param( q{} ),
        '72<84d5e~98#&a61^!(5324f7#0a5!@r',
        'proper cipher key with empty string arg, ' ~
            $obj.gen_cipher_param('key').encode.bytes ~ ' bytes',
    );
}, 'Check gen_cipher_param method';

# Check do_decrypt method
subtest {
    plan 9;
    is(
        $obj.do_decrypt(
            '%29%d5%5f%fd%80%99%cd%5a%a1%ff%2c%98%20%99%b6%ab'
        ),
        'hello, world!',
        'do_decrypt() with arg',
    );
    is(
        $obj.do_decrypt( q{} ),
        Str,
        'do_decrypt() with blank arg',
    );
    is(
        $obj.do_decrypt('(SD(2288c&@%!'),
        Str,
        'do_decrypt() with not url encoded arg',
    );
    is(
        $obj.do_decrypt('%29%d4%123%ddd%y1'),
        Str,
        'do_decrypt() with corrupted url encoded arg no.1',
    );
    is(
        $obj.do_decrypt('%0%1%a'),
        Str,
        'do_decrypt() with corrupted url encoded arg no.2',
    );
    is(
        $obj.do_decrypt('%00%1%a%fa'),
        Str,
        'do_decrypt() with partially corrupted url encoded arg',
    );
    is(
        $obj.do_encrypt('hello, world!'),
        '%29%d5%5f%fd%80%99%cd%5a%a1%ff%2c%98%20%99%b6%ab',
        'do_encrypt() with arg 1',
    );
    is(
        $obj.do_encrypt('4be81cffe9c6841dc2eab88460a60571'),
        '%0f%5e%b0%14%ad%38%41%66%a4%3b%ac%26%7c%3d%42%b8%f0%a2%e5'  ~
            '%29%23%8c%7d%22%a5%cf%c5%da%b4%5f%9a%5e%07%0e%59%ab%e2' ~
            '%3d%05%0d%69%6d%b9%49%1e%ba%eb%c6',
        'do_encrypt() with arg 2',
    );
    is(
        $obj.do_encrypt( q{} ),
        Str,
        'do_encrypt() with blank arg',
    );
}, 'Check do_decrypt method';

# Check gen_captcha
subtest {
    plan 1;

    todo 'requires ImageMagick 6.9.12-44 installed';
    ok gen_captcha, 'get_captcha generation test';
}, 'Check gen_captcha';

# Check do_encrypt with Pheix::Addons::November::CGI params
subtest {
    plan 200;
    for (0..199) {
        my Str $rnd = sprintf "%010d", (rand*10000000000);
        my Str $d = $obj.do_encrypt($rnd);
        my Any $co = Pheix::Addons::November::CGI.new(:nounesc(True));
        my Str $query =
            'action=showcaptcha&value=' ~ $d ~ '&ts='
            ~ $obj.get_hex(time.Str);
        $co.parse_params( $query );
        my %envp = $co.params;
        is(
            %envp<value>,
            $d,
            'check uri-encoded data transpass through CGI params no.' ~
            ( $_+1 ),
        );
    }
}, 'Check do_encrypt with Pheix::Addons::November::CGI params';

done-testing;

sub gen_captcha returns Bool {
    my Bool $ret = False;
    my UInt $md  = 64;
    my Str $path = './t/captchas/';
    my Str $svfn = $path ~ time ~ '.png';
    my Str $etfn = $path ~ 'test-captcha-2.png';
    my $img = $obj.get_captcha(
        '%fe%6e%10%42%81%6d%77%65%f0%5d%ef%a3%18%79%99%73'
    );
    if $img ~~ MagickWand {
        if $img.write($svfn) {
            if $svfn.IO.e {
                if $svfn.IO.s == $etfn.IO.s {
                    unlink $svfn;
                    $ret = True;
                }
                else {
                    my $d =
                        max($svfn.IO.s, $etfn.IO.s) -
                        min($svfn.IO.s, $etfn.IO.s);
                    if $d < $md {
                        $ret = True;
                    }
                    else {
                        diag(
                            '***ERR: sizes not equal gen[' ~
                            $svfn.IO.s ~
                            '] != etalon[' ~ $etfn.IO.s ~
                            '] AND delta=' ~
                            $d ~ ' (>=' ~ $md ~ q{)}
                        );
                    }
                }
            }
            else {
                diag(
                    '***ERR: could not find ' ~ $svfn ~
                    ' after \$img.write()'
                )
            }
        }
        else {
            diag('***ERR: could not \$img.write() to ' ~ $svfn);
        }
    }
    else {
        diag('***ERR: type of \$img object is not MagickWand: ' ~
        $img.Str);
    }
    $ret;
}
