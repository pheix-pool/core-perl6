use v6.d;
use Test;
use lib 'lib';

use Pheix::Model::Database::Access;
use Pheix::Controller::Blockchain::Signer;
use Pheix::Test::Blockchain;
use Pheix::Test::Helpers;

constant testport = (@*ARGS[0].defined && @*ARGS[0].Int ~~ 2049..9999) ?? @*ARGS[0] !! 8541;
constant tstobj   = Pheix::Test::Blockchain.new;
constant tsthlp   = Pheix::Test::Helpers.new(:storages(<auth-smart-contract>));

plan 2;

if !tstobj.pte {
    diag('PHEIXTESTENGINE was not set');
    skip-rest('Blockchain test should be run via Pheix test engine');
    exit;
}

my $jsonobj;

lives-ok {
    $jsonobj = tsthlp.patch_storage_config(
        :addon('EmbeddedAdmin'),
        :addonpath('conf/addons/custom_path'),
        :pairs({port => testport})
    );
}, 'patch ports config for auth gateway';

my Pheix::Model::Database::Access $auth =
    Pheix::Model::Database::Access.new(
        :table('auth-smart-contract'),
        :fields([]),
        :constrpars({_sealperiod => 5, _delta => 60, _seedmod => 999_000_999, _pkey => 111_999_111}),
        :test(True),
        :$jsonobj,
        :txwaititers(120)
    );

if $auth {
    if $auth.dbswitch == 1 {
        subtest {
            plan 4;

            ok $auth.chainobj.scaddr !~~ m:i/^ 0x<[0]>**40 $/, 'scaddr is ok';
            ok $auth.chainobj.sctx !~~ m:i/^ 0x<[0]>**64 $/, 'sctx is ok';

            my %h = $auth.chainobj.ethobj.contract_method_call('getPkey', {});

            ok %h<publickey>, 'Pkey is defined';
            ok %h<publickey> ~~  m:i/^ 0x<[0..9a..f]> ** 64 $/, 'Pkey is hex';
        }, 'check PheixAuth contract deployment and communication';
    }
    else {
        skip-rest('ethereum node is not available');
    }
}
else {
    skip-rest('database is not available');
}

done-testing;
