use v6.d;
use Test;
use lib 'lib';

plan 1;

use Pheix::View::Web::Headers;

my Any $obj = Pheix::View::Web::Headers.new;

# Check proto method
subtest {
    plan 1;
    my Str $m = "proto";
    my Str $p = $obj.proto;
    if %*ENV<HTTP_REFERER> {
        if %*ENV<HTTP_REFERER> ~~ m/https\:\/\// {
            is(
                $p,
                'https://',
                $m ~ ' with referer=' ~ %*ENV<HTTP_REFERER> ~
                    ' should be https://, got=' ~ $p,
            );
        } else {
            is(
                $p,
                'http://',
                $m ~ ' with referer=' ~ %*ENV<HTTP_REFERER> ~
                    ' should be http://, got=' ~ $p,
            );
        }
    }
    else {
        is(
            $p,
            'http://',
            $m ~ ' with defaults should be http://, got=' ~ $p,
        );
    }
}, "Check proto method";

done-testing;
