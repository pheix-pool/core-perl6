use v6.d;
use Test;
use lib 'lib';

use Pheix::Test::Content;
use Pheix::Test::FastCGI;
use Pheix::Test::Helpers;
use Pheix::Controller::Basic;
use Pheix::Addons::Embedded::Admin;
use Pheix::Test::Blockchain;

use Crypt::LibGcrypt;
use Crypt::LibGcrypt::Random;

constant testport = (@*ARGS[0].defined && @*ARGS[0].Int ~~ 2049..9999) ?? @*ARGS[0] !! 8541;
constant localtab = 'auth-node';
constant tstobj   = Pheix::Test::Blockchain.new(:locstorage(localtab), :genwords(200));
constant txwaititers = 120;

my $jsonobj;

my $tcnt = Pheix::Test::Content.new;
my $fcgi = Pheix::Test::FastCGI.new;
my $thlp = Pheix::Test::Helpers.new(:storages(<auth-smart-contract auth-node>));

plan 14;

use-ok 'Pheix::Addons::Embedded::Admin';

if !$thlp.check_submodules {
    skip-rest('No Pheix submodules are pulled');
    exit;
}

my $ctrl = Pheix::Controller::Basic.new(
    :apirobj(Nil),
    :mockedfcgi($fcgi),
    :test(True),
);

lives-ok {
    $jsonobj = $thlp.patch_storage_config(
        :addon('EmbeddedAdmin'),
        :addonpath('conf/addons/custom_path'),
        :pairs({port => testport})
    );
}, 'patch ports config for auth gateways';

ok $jsonobj.set_conf_value('EmbeddedAdmin', 'authpolicy',  'basic', :temporary(True)), 'set basic authentication policy';
ok $jsonobj.set_conf_value('EmbeddedAdmin', 'sesstimeout', '+1m',   :temporary(True)), 'set session timeout to 1 minute';
ok $jsonobj.set_conf_value('EmbeddedAdmin', 'expiredelta', '20',    :temporary(True)), 'set extend threshold to 20 seconds';

# Check browse method for login
subtest {
    plan 4;

    my $addon = Pheix::Addons::Embedded::Admin.new(:$ctrl, :$jsonobj, :txwaititers(txwaititers));

    my %match =
        details    => %(path => '/admin'),
        controller => $addon.get_class,
        action     => 'browse',
    ;

    is $ctrl.sharedobj<fastcgi>.reset, 0, 'reset content array';

    $addon.browse(:tick(1), :match(%match));

    my ($hdr, $cnt) = $ctrl.sharedobj<fastcgi>.fetch.split("\n\n", 2);

    ok $cnt !~~ / '<TMPL_VAR tmpl_' <[a..z_]>+ '>' /, 'no tmpl vars';

    ok $tcnt.checkheader(
        :header($hdr),
        :status(200),
        :cookies([
            'fcgitick=' ~ 1,
            'pheixaddon=' ~ $addon.get_name,
            'pheixadmin=' ~ True.Str
        ])
    ), 'HTTP header';

    ok $tcnt.checkcontent(
        :cnt($cnt),
        :params(
            %(
                $ctrl.sharedobj<pageobj>.get_pparams,
                $ctrl.sharedobj<pageobj>.get_tparams
            )
        )
    ), 'check content for login page';
}, 'Check browse method for login';

# Check trivial auth failure - no node available
subtest {
    plan 2;

    my $addon = Pheix::Addons::Embedded::Admin.new(:$ctrl, :$jsonobj, :txwaititers(txwaititers));

    my %auth_details =
        $addon.auth_api(:match({}), :tick(1), :sharedobj($ctrl.sharedobj));

    nok %auth_details<tparams><pheixauth>, 'pheixauth is undefined';
    is %auth_details<tparams><table>, 'embeddedadmin/login', 'login table';
}, 'Check trivial auth failure - no node available';

# Ethereum node is required for the next tests
if !tstobj.pte {
    diag('PHEIXTESTENGINE was not set');
    skip-rest('Blockchain test should be run via Pheix test engine');
    done-testing;

    exit;
}

# Check auth smart contract binary data
subtest {
    plan 3;

    my $addon    = Pheix::Addons::Embedded::Admin.new(:$ctrl, :$jsonobj, :txwaititers(txwaititers));
    my $response = $addon.browse_api(:match({}), :route('/api/admin'), :tick(1), :sharedobj($ctrl.sharedobj));

    my $dbobj = Pheix::Model::Database::Access.new(
        :table(localtab),
        :fields([]),
        :test(True),
        :jsonobj($addon.jsonobj),
        :txwaititers(txwaititers)
    );

    ok $response<tparams>.keys, 'tparams found';
    ok $response<tparams><table> ~~ /login/, 'login table';

    if $dbobj {
        if $dbobj.dbswitch {
            ok $response<tparams><tmpl_authsmartcontract> && $response<tparams><tmpl_authsmartcontract> ~~ m:i/^ <xdigit>+ $/, 'auth smart contract binary data';
        }
        else {
            skip-rest('ethereum node is not available');
        }
    }
    else {
        skip-rest('database object is not available');
    }
}, 'Check auth smart contract binary data';

# Check MetaMask auth, refresh, validation and exit
subtest {
    plan 8;

    my $contract_path = 'conf/system/eth/PheixAuth.abi';

    my $addon = Pheix::Addons::Embedded::Admin.new(:$ctrl, :$jsonobj, :txwaititers(txwaititers));
    my $dbobj = Pheix::Model::Database::Access.new(
        :table(localtab),
        :fields([]),
        :test(True),
        :jsonobj($addon.jsonobj),
        :txwaititers(txwaititers)
    );

    if $dbobj {
        if $dbobj.dbswitch {
            my %validate_details;

            ok $dbobj.dbswitch, 'blockchain tab connected';

            my @accounts = $dbobj.chainobj.ethobj.eth_accounts;

            ok @accounts.elems, 'ethereum accounts';

            my %hdr = $ctrl.sharedobj<headobj>.header(
                %(Status => 200, X-Request-ID => 1),
                List.new,
            );

            my $pkey   = :10[random(32).list];
            my $result = $dbobj.chainobj.ethobj.compile_and_deploy_contract(
                :$contract_path,
                :account(@accounts[0]),
                :compile_output_path($contract_path.IO.dirname),
                :skipcompile(True),
                :constructor_params({
                    _sealperiod => $addon.sealperiod,
                    _delta => Pheix::View::Web::Cookie.new.expire_calc($addon.sesstimeout).Int,
                    _seedmod => (1_000_000..1_000_000_000).rand.Int,
                    _pkey => $pkey
                }),
            );

            $ctrl.sharedobj<fastcgi>.set_env(
                :key('HTTP_COOKIE'),
                :value(sprintf("pheixmetamask=%s; pheixauth=%s; pheixsender=%s", ~True, $result<transactionHash>, @accounts.head)),
            );

            ok $result<transactionHash> ~~ m:i/^ 0x<xdigit>**64 $/, 'MetaMask session is opened';

            my $pkey_hashed = $dbobj.chainobj.ethobj.web3_sha3(sprintf("0x%064x", $pkey));

            my $refresh_payload = {
                recovery => {
                    address => @accounts[0],
                    publickey => $pkey_hashed,
                    signature => $dbobj.chainobj.ethobj.personal_sign(:message($pkey_hashed)),
                }
            };

            my %refresh_details = $addon.manage_session_api(
                :match({}),
                :route('/api/session/refresh'),
                :tick(1),
                :sharedobj($ctrl.sharedobj),
                :payload($refresh_payload),
            );

            ok %refresh_details<sesstatus> && %refresh_details<sesstatus> == True, 'MetaMask session is authenticated on refresh';

            for ^3 -> $case {
                my %case =
                    publickey => $pkey_hashed,
                    signature => $dbobj.chainobj.ethobj.personal_sign(:message($pkey_hashed));

                if    $case == 1 {
                    %case<signature> = $dbobj.chainobj.ethobj.personal_sign(:message($dbobj.chainobj.ethobj.string2hex('dummy-message')));
                }
                elsif $case == 2 {
                    %case<publickey> = $dbobj.chainobj.ethobj.web3_sha3(sprintf("0x%064x", 0xdeadbeef));
                    %case<signature> = $dbobj.chainobj.ethobj.personal_sign(:message(%case<publickey>));
                }

                my $payload = {
                    recovery => {
                        address => @accounts[0],
                        |%case
                    }
                };

                %validate_details = $addon.manage_session_api(
                    :match({}),
                    :route('/api/session/validate'),
                    :tick(1),
                    :sharedobj($ctrl.sharedobj),
                    :$payload,
                );

                if    $case == 0 {
                    ok %validate_details<sesstatus> && %validate_details<sesstatus> == True, 'MetaMask session is authenticated on validate';
                }
                elsif $case == 1 {
                    nok %validate_details<sesstatus> && %validate_details<sesstatus> == True, 'MetaMask session is not authenticated with false signature on validate';
                }
                elsif $case == 2 {
                    nok %validate_details<sesstatus> && %validate_details<sesstatus> == True, 'MetaMask session is not authenticated with false pkey and its sinature on validate';
                }
            }

            my %exit_details = $addon.manage_session_api(
                :match({}),
                :route('/api/session/close'),
                :tick(1),
                :sharedobj($ctrl.sharedobj),
                :payload({recovery => {address => @accounts[0]}}),
            );

            ok %exit_details.keys.elems, 'MetaMask session is closed';
        }
        else {
            skip-rest('ethereum node is not available');
        }
    }
    else {
        skip-rest('database object is not available');
    }
}, 'Check MetaMask auth, refresh, validation and exit';

ok $jsonobj.set_conf_value('EmbeddedAdmin', 'authpolicy', 'extended', :temporary(True)), 'set extended authentication policy';

# Check trivial auth, exit and validation
subtest {
    plan 14;

    my $addon = Pheix::Addons::Embedded::Admin.new(:$ctrl, :$jsonobj, :txwaititers(txwaititers));
    my $dbobj = Pheix::Model::Database::Access.new(
        :table(localtab),
        :fields([]),
        :test(True),
        :jsonobj($addon.jsonobj),
        :txwaititers(txwaititers)
    );

    if $dbobj {
        if $dbobj.dbswitch {
            ok $dbobj.dbswitch, 'blockchain tab connected';

            my @accounts = $dbobj.chainobj.ethobj.eth_accounts;

            ok @accounts.elems, 'ethereum accounts';

            my %hdr = $ctrl.sharedobj<headobj>.header(
                %(Status => 200, X-Request-ID => 1),
                List.new,
            );

            my %auth_details = $addon.auth_api(
                :match({}),
                :tick(1),
                :sharedobj($ctrl.sharedobj),
                :credentials({login => @accounts.head, password => $dbobj.chainobj.ethobj.unlockpwd}),
                :header(%hdr),
            );

            ok %auth_details<tparams><pkey> ~~ m:i/^ 0x<xdigit>**64 $/ , 'pkey found';
            ok %auth_details<tparams><tx> ~~ m:i/^ 0x<xdigit>**64 $/ , 'tx found';
            is %auth_details<tparams><table>, 'embeddedadmin/area', 'area table';
            is %auth_details<tparams><session>, 60, 'one minute per session';
            is %auth_details<header><Set-Cookie>.elems, 3, 'cookies found';
            ok %auth_details<header><Set-Cookie>.head ~~ m:i/ <{%auth_details<tparams><tx>}> /, 'pkey cookie';
            ok %auth_details<header><Set-Cookie>.tail ~~ m:i/ <{@accounts.head}> /, 'sender cookie';
            ok $ctrl.sharedobj<logrobj>.chainobj.table_drop, 'clear log';
            ok %auth_details<tparams><status>, 'session is authenticated';

            $ctrl.sharedobj<fastcgi>.set_env(
                :key('HTTP_COOKIE'),
                :value(sprintf("pheixauth=%s; pheixsender=%s", %auth_details<tparams><tx>, @accounts.head)),
            );

            my %exit_details = $addon.manage_session_api(
                :match({}),
                :route('/api/session/close'),
                :tick(1),
                :sharedobj($ctrl.sharedobj),
            );

            ok %exit_details.keys.elems, 'exit response';

            my %validate_details = $addon.manage_session_api(
                :match({}),
                :route('/api/session/validate'),
                :tick(1),
                :sharedobj($ctrl.sharedobj),
            );

            ok %validate_details<sesstatus>:exists, 'sesstatus presented';
            ok %validate_details<sesstatus> == False, 'sesstatus is false';

            # diag(%validate_details.gist);
        }
        else {
            skip-rest('ethereum node is not available');
        }
    }
    else {
        skip-rest('database object is not available');
    }
}, 'Check trivial auth, exit and validation';

# Check auth, validation, extending and exit
subtest {
    plan 16;

    my $addon = Pheix::Addons::Embedded::Admin.new(:$ctrl, :$jsonobj, :txwaititers(txwaititers));
    my $dbobj = Pheix::Model::Database::Access.new(
        :table(localtab),
        :fields([]),
        :test(True),
        :jsonobj($addon.jsonobj),
        :txwaititers(txwaititers)
    );

    if $dbobj {
        if $dbobj.dbswitch {
            my %details;

            ok $dbobj.dbswitch, 'blockchain tab connected';

            my @accounts = $dbobj.chainobj.ethobj.eth_accounts;

            ok @accounts.elems, 'ethereum accounts';

            %details<auth> = $addon.auth_api(
                :match({}),
                :tick(1),
                :sharedobj($ctrl.sharedobj),
                :credentials({login => @accounts.head, password => $dbobj.chainobj.ethobj.unlockpwd}),
            );

            ok %details<auth><tparams><pkey> ~~ m:i/^ 0x<xdigit>**64 $/ , 'pkey found';
            ok %details<auth><tparams><tx> ~~ m:i/^ 0x<xdigit>**64 $/ , 'tx found';
            ok %details<auth><tparams><status>, 'session is authenticated';

            $ctrl.sharedobj<fastcgi>.set_env(
                :key('HTTP_COOKIE'),
                :value(sprintf("pheixauth=%s; pheixsender=%s", %details<auth><tparams><tx>, @accounts.head)),
            );

            react {
                whenever Supply.interval(10) -> $step {
                    %details<validate>.push: $addon.manage_session_api(
                        :match({}),
                        :route('/api/session/validate'),
                        :tick(1),
                        :sharedobj($ctrl.sharedobj),
                    );

                    diag(sprintf("validate session at step %d: %s (expires in %s sec)", $step, (%details<validate>.tail)<sesstatus>, (%details<validate>.tail)<tparams><session>))
                        if tstobj.debug;

                    $ctrl.sharedobj<fastcgi>.set_env(
                        :key('HTTP_COOKIE'),
                        :value(sprintf("pheixauth=%s; pheixsender=%s", (%details<validate>.tail)<tparams><tx>, @accounts.head)),
                    );

                    if (%details<validate>.tail)<tryextend> {
                        %details<extend> = $addon.manage_session_api(
                            :match({}),
                            :route('/api/session/extend'),
                            :tick(1),
                            :sharedobj($ctrl.sharedobj),
                        );

                        $ctrl.sharedobj<fastcgi>.set_env(
                            :key('HTTP_COOKIE'),
                            :value(sprintf("pheixauth=%s; pheixsender=%s", %details<extend><tparams><tx>, @accounts.head)),
                        );

                        diag(sprintf("extend session at step %d: %s ", $step, %details<extend><sesstatus>))
                            if tstobj.debug;
                    }

                    done() if $step == 8;
                }
            }

            %details<close> = $addon.manage_session_api(
                :match({}),
                :route('/api/session/close'),
                :tick(1),
                :sharedobj($ctrl.sharedobj),
            );

            %details<validate>.push: $addon.manage_session_api(
                :match({}),
                :route('/api/session/validate'),
                :tick(1),
                :sharedobj($ctrl.sharedobj),
            );

            for %details<validate>.kv -> $index, $data {
                last if $index == %details<validate>.elems - 1;

                ok $data<sesstatus>, 'session is valid for step ' ~ $index;
            }

            ok ((%details<validate>.tail)<sesstatus>:exists) && (%details<validate>.tail)<sesstatus> == False, 'session is closed';
            ok $ctrl.sharedobj<logrobj>.chainobj.table_drop, 'clear log';
        }
        else {
            skip-rest('ethereum node is not available');
        }
    }
    else {
        skip-rest('database object is not available');
    }
}, 'Check auth, validation, extending and exit';

# Check session validation
subtest {
    plan 10;

    my $addon = Pheix::Addons::Embedded::Admin.new(:$ctrl, :$jsonobj, :txwaititers(txwaititers));
    my $dbobj = Pheix::Model::Database::Access.new(
        :table(localtab),
        :fields([]),
        :test(True),
        :jsonobj($addon.jsonobj),
        :txwaititers(txwaititers)
    );

    if $dbobj {
        if $dbobj.dbswitch {
            my $tries   = 9;
            my $success = 0;

            my %details = stats => {valid => 0, invalid => 0};

            ok $dbobj.dbswitch, 'blockchain tab connected';

            my @accounts = $dbobj.chainobj.ethobj.eth_accounts;

            ok @accounts.elems, 'ethereum accounts';

            %details<auth> = $addon.auth_api(
                :match({}),
                :tick(1),
                :sharedobj($ctrl.sharedobj),
                :credentials({login => @accounts.head, password => $dbobj.chainobj.ethobj.unlockpwd}),
            );

            ok %details<auth><tparams><pkey> ~~ m:i/^ 0x<xdigit>**64 $/ , 'pkey found';
            ok %details<auth><tparams><tx> ~~ m:i/^ 0x<xdigit>**64 $/ , 'tx found';
            ok %details<auth><tparams><status>, 'session is authenticated';

            $ctrl.sharedobj<fastcgi>.set_env(
                :key('HTTP_COOKIE'),
                :value(sprintf("pheixauth=%s; pheixsender=%s", %details<auth><tparams><tx>, @accounts.head)),
            );

            react {
                whenever Supply.interval(10) -> $step {
                    %details<validate>.push: $addon.manage_session_api(
                        :match({}),
                        :route('/api/session/validate'),
                        :tick(1),
                        :sharedobj($ctrl.sharedobj),
                    );

                    my $session_status = (%details<validate>.tail)<sesstatus>;

                    diag(sprintf("validate session at step %d: %s (expires in %s sec)", $step, $session_status, (%details<validate>.tail)<tparams><session>))
                        if tstobj.debug;

                    $success++ if $session_status;

                    $ctrl.sharedobj<fastcgi>.set_env(
                        :key('HTTP_COOKIE'),
                        :value(sprintf("pheixauth=%s; pheixsender=%s", (%details<validate>.tail)<tparams><tx>, @accounts.head)),
                    ) if $session_status;

                    done() if $step == 8;
                }
            }

            for @(%details<validate>) -> $data {
                $data<sesstatus> ??
                    %details<stats><valid>++ !!
                        %details<stats><invalid>++;
            }

            %details<close> = $addon.manage_session_api(
                :match({}),
                :route('/api/session/close'),
                :tick(1),
                :sharedobj($ctrl.sharedobj),
            );

            ok %details<close>.keys.elems, 'exit response';
            is %details<stats><valid>, $success, sprintf("session is valid for %s", $addon.sesstimeout);
            is %details<stats><invalid>, ($tries - $success), sprintf("session is lost after %s", $addon.sesstimeout);
            ok $ctrl.sharedobj<logrobj>.chainobj.table_drop, 'clear log';

            todo 'possible blockchain latency';
            ok $success == 6 && ($tries - $success == 3), 'correct attempts';
        }
        else {
            skip-rest('ethereum node is not available');
        }
    }
    else {
        skip-rest('database object is not available');
    }
}, 'Check session validation';

# Check catch contract feature
subtest {
    plan 14;

    my $addon = Pheix::Addons::Embedded::Admin.new(:$ctrl, :$jsonobj, :txwaititers(txwaititers));
    my $dbobj = Pheix::Model::Database::Access.new(
        :table(localtab),
        :fields([]),
        :test(True),
        :jsonobj($addon.jsonobj),
        :txwaititers(txwaititers)
    );

    if $dbobj {
        if $dbobj.dbswitch {
            my %details = stats => {valid => 0, invalid => 0};

            ok $dbobj.dbswitch, 'blockchain tab connected';

            my @accounts = $dbobj.chainobj.ethobj.eth_accounts;

            ok @accounts.elems, 'ethereum accounts';

            %details<auth>.push: $addon.auth_api(
                :match({}),
                :tick(1),
                :sharedobj($ctrl.sharedobj),
                :credentials({login => @accounts.head, password => $dbobj.chainobj.ethobj.unlockpwd}),
            );

            ok %details<auth>.tail<tparams><pkey> ~~ m:i/^ 0x<xdigit>**64 $/ , 'pkey found';
            ok %details<auth>.tail<tparams><tx> ~~ m:i/^ 0x<xdigit>**64 $/ , 'tx found';
            ok %details<auth>.tail<tparams><status>, 'session is authenticated';

            $ctrl.sharedobj<fastcgi>.set_env(
                :key('HTTP_COOKIE'),
                :value(sprintf("pheixauth=%s; pheixsender=%s", %details<auth>.tail<tparams><tx>, @accounts.head)),
            );

            %details<auth>.pop;

            react {
                whenever Supply.interval(10) -> $step {
                    %details<auth>.push: $addon.auth_api(
                        :match({}),
                        :tick(1),
                        :sharedobj($ctrl.sharedobj),
                        :credentials({login => @accounts.head, password => $dbobj.chainobj.ethobj.unlockpwd}),
                    );

                    diag(sprintf("catch contract at step %d for %d", $step, (%details<auth>.tail<tparams><session>)))
                        if tstobj.debug;

                    done() if $step == 5;
                }
            }

            for @(%details<auth>).kv -> $index, $session {
                 ok $session<tparams><status> && $session<tparams><session> > 0,
                    sprintf("catch %s seconds for session no.%d", $session<tparams><session>, $index);
            }

            %details<close> = $addon.manage_session_api(
                :match({}),
                :route('/api/session/close'),
                :tick(1),
                :sharedobj($ctrl.sharedobj),
            );

            ok %details<close>.keys.elems, 'exit response';

            todo 'possible blockchain latency';
            is %details<auth>.elems, 6, sprintf("catch %d sessions", %details<auth>.elems);

            ok $ctrl.sharedobj<logrobj>.chainobj.table_drop, 'clear log';
        }
        else {
            skip-rest('ethereum node is not available');
        }
    }
    else {
        skip-rest('database object is not available');
    }
}, 'Check catch contract feature';

done-testing;
