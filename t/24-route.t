use v6.d;
use Test;
use lib 'lib';

use Router::Right;

use Pheix::Addons;
use Pheix::Model::Route;
use Pheix::Test::Helpers;

my $r = Pheix::Model::Route.new;

plan 8;

use-ok 'Pheix::Model::Route';
throws-like
    {
        $r.basic_routes_with_handler(
            :handler_class('Foo::Bar'),
            :extrouter(Router::Right.new)
        )
    },
    Exception,
    message => /'No prefix for external router is given'/,
    'dies on undefined prefix'
;

# Check static API routes
subtest {
    plan 3;

    my $router = $r.basic_routes_with_handler(:handler_class('A::B'));

    ok $router.route('api-debug')<route>:exists, 'api-debug route';
    ok $router.route('api-error')<route>:exists, 'api-error route';
    ok $router.route('api')<route>:exists, 'api route';
}, 'Check static API routes';

# Check static API routes with blank external router
subtest {
    plan 9;

    my $extr = Router::Right.new;

    my $router = $r.basic_routes_with_handler(
        :handler_class('A::B'),
        :prefix('/ext'),
        :extrouter($extr),
    );

    ok $router.route('api-debug')<route>:exists, 'ext api-debug route';
    is $router.route('api-debug')<path>, '/api-debug', 'ext api-debug path';
    is $router.route('api-debug')<methods>, ['GET'], 'ext api-debug methods';
    ok $router.route('api-error')<route>:exists, 'ext api-error route';
    is $router.route('api-error')<path>, '/api-error/{code:<[0..9]>**3..3}', 'ext api-error path';
    is $router.route('api-error')<methods>, ['GET', 'POST'], 'ext api-error methods';
    ok $router.route('api')<route>:exists, 'ext api route';
    is $router.route('api')<path>, '/api', 'ext api path';
    is $router.route('api')<methods>, ['POST'], 'ext api methods';
}, 'Check static API routes with blank external router';

# Check static API routes with predefined external router
subtest {
    plan 10;

    my $extr = Router::Right.new;

    $extr.add(:name('rt'), :path('GET /rt'), :payload('A::B#rt'));
    $extr.add(:name('api-debug'), :path('PUT /a'), :payload('A::B#a'));
    $extr.add(:name('api-error'), :path('PATCH /ae'), :payload('A::B#ae'));
    $extr.add(:name('api'), :path('DELETE /b'), :payload('A::B#b'));

    my $router = $r.basic_routes_with_handler(
        :handler_class('A::B'),
        :prefix('/ext'),
        :extrouter($extr),
    );

    ok $router.route('rt')<route>:exists, 'ext predefined route';
    ok $router.route('api-debug')<route>:exists, 'ext api-debug route';
    is $router.route('api-debug')<path>, '/a', 'ext predefined api-debug path';
    is $router.route('api-debug')<methods>, ['PUT'], 'ext predefined api-debug methods';
    ok $router.route('api-error')<route>:exists, 'ext api-error route';
    is $router.route('api-error')<path>, '/ae', 'ext predefined api-error path';
    is $router.route('api-error')<methods>, ['PATCH'], 'ext predefined api-error methods';
    ok $router.route('api')<route>:exists, 'ext api route';
    is $router.route('api')<path>, '/b', 'ext predefined api path';
    is $router.route('api')<methods>, ['DELETE'], 'ext predefined api methods';
}, 'Check static API routes with predefined external router';

# Check generic dynamic routes
subtest {
    plan 1;

    my Str $ctrl = 'A::B';
    my $router   = $r.basic_routes_with_handler(:handler_class($ctrl));

    ok checkdynaroutes(
        :router($router),
        :ctrl($ctrl),
    ), 'generic dynamic routes';
}, 'Check generic dynamic routes';

# Check dynamic routes for external router
subtest {
    plan 2;

    my Str $ctrl = 'A::B';
    my Str $pfx  = '/ext';
    my $extr     = Router::Right.new;

    $extr.add(:name('rt'), :path('GET /rt'), :payload($ctrl ~ '#rt'));

    my $router = $r.basic_routes_with_handler(
        :handler_class($ctrl),
        :prefix($pfx),
        :extrouter($extr),
    );

    ok $router.route('rt')<route>:exists, 'predefined route';
    ok checkdynaroutes(
        :router($router),
        :ctrl($ctrl),
        :prefix($pfx),
    ), 'external router dynamic routes';
}, 'Check dynamic routes for external router';

if !Pheix::Test::Helpers.new.check_submodules {
    skip-rest('No Pheix submodules are pulled');
    exit;
}

# Check addons routes import
subtest {
    plan 4;

    my $router = Router::Right.new;
    my %addons = Pheix::Addons.new.get_addons;

    ok $r.import_addon_routes(
        :prefix(Str),
        :router($router),
        :addons(%addons)
    ) ~~ Router::Right, 'return type with blank prefix';

    ok $r.import_addon_routes(
        :prefix('/api'),
        :router($router),
        :addons(%addons)
    ) ~~ Router::Right, 'return type with api prefix';

    my @etalon = [
        {
            name => 'EmbeddedUser-browseindex-default',
            path => '/embedded'
        },
        {
            name => 'EmbeddedUser-browseseo-default',
            path => '/embedded/{seouri:<[a..z0..9\\-\\_]>+(\\.html?)?}'
        },
        {
            name => 'EmbeddedUser-browsepage-default',
            path => '/embedded/{page:\'page\'}/{seouri:<[a..z0..9\\-\\_]>+(\\.html?)?}'
        },
        {
            name => 'EmbeddedUser-browseindex-api',
            path => '/api/embedded'
        },
        {
            name => 'EmbeddedUser-browseseo-api',
            path => '/api/embedded/{seouri:<[a..z0..9\\-\\_]>+(\\.html?)?}'
        },
        {
            name => 'EmbeddedAdmin-login-default',
            path => '/admin'
        },
        {
            name => 'EmbeddedAdmin-login-api',
            path => '/api/admin'
        },
        {
            name => 'EmbeddedAdmin-authentication-api',
            path => '/api/admin/auth'
        },
        {
            name => 'EmbeddedAdmin-validate-api',
            path => '/api/admin/session/validate'
        },
        {
            name => 'EmbeddedAdmin-extend-api',
            path => '/api/admin/session/extend'
        },
        {
            name => 'EmbeddedAdmin-refresh-api',
            path => '/api/admin/session/refresh'
        },
        {
            name => 'EmbeddedAdmin-close-api',
            path => '/api/admin/session/close'
        },
    ];

    is $router.routes.elems, @etalon.elems, 'routes count';
    ok checkdynaroutes(
        :router($router),
        :ctrl('Pheix::Addons::Embedded::User'),
        :etaroutes(@etalon)
    ), 'embedded addons routes';
}, 'Check addons routes import';

done-testing;

sub checkdynaroutes(
    Router::Right :$router!,
    Str :$ctrl!,
    Str :$prefix,
    :@etaroutes = List.new
) returns Bool {
    my Bool $ret = True;
    my Str $pfx  = $prefix // q{};

    my @etalon = @etaroutes.elems ?? @etaroutes !! [
        {
            name => 'index',
            path => sprintf("%s%s", $pfx, '/{index:(\'index\')?}')
        },
        {
            name => 'error',
            path => sprintf("%s%s", $pfx, '/error/{code:<[0..9]>**3..3}')
        },
        {
            name => 'bigbro',
            path => sprintf("%s%s", $pfx, '/bigbrother/{query:.*}')
        },
        {
            name => 'sitemap',
            path => sprintf("%s%s", $pfx, '/sitemap{.format:\'xml\'}')
        },
        {
            name => 'redirect',
            path => sprintf("%s%s", $pfx, '/redirect/{query:.*}')
        },
        {
            name => 'captcha',
            path => sprintf("%s%s", $pfx, '/captcha/{query:.*}')
        },
        {
            name => 'presentation',
            path => sprintf("%s%s", $pfx, '/presentation')
        },
        {
            name => 'tpc20cic',
            path => sprintf("%s%s", $pfx, '/tpc20cic')
        }
    ];

    for @etalon -> $er {
        my Bool $found = False;
        my Str  $name  = $er<name>;

        for $router.routes -> $route {
            for @$route -> $r {
                if ($r<name>:exists) &&
                   ($r<name> ~~ /:r \d+ '.' \d+ '-' $name/) &&
                   ($r<path>) eq $er<path>
                {
                    $found = True;
                    last;
                }
            }
            last if $found;
        }

        if !$found {
            "Route $er<name> not found".say;
            $ret = False;
            last;
        }
    }

    $ret;
}
