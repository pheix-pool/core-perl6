use v6.d;
use Test;
use lib 'lib';

use JSON::Fast;
use P5quotemeta;

use Pheix::Test::Helpers;
use Pheix::Test::Content;
use Pheix::Test::FastCGI;
use Pheix::Model::Route;
use Pheix::Controller::Basic;
use Pheix::View::Web::Cookie;

plan 22;

use-ok 'Pheix::Controller::Basic';

if !Pheix::Test::Helpers.new.check_submodules {
    skip-rest('No Pheix submodules are pulled');
    exit;
}

my $test = Pheix::Test::Content.new;
my $fcgi = Pheix::Test::FastCGI.new;
my $ctrl = Pheix::Controller::Basic.new(
    :apirobj(Nil),
    :test(True),
);

my $rapi = Pheix::Model::Route.new(:test(True));
my $capi = Pheix::Controller::Basic.new(
    :apirobj(
        $rapi.basic_routes_with_handler(
            :handler_class('Pheix::Controller::API')
        )
    ),
    :test(True),
);

my %res =
    0 => {
        status  => 0,
        msg     => 'bad API request',
        content => '<p>Bad API request</p>',
        request => '{}'
    },
    1 => {
        status  => 0,
        msg     => 'bad API request',
        content => '<p>Bad API request</p>',
        request =>
            sprintf(
                "\{credentials => \{token => %s\}, httpstat => 200, method => GET, route => /index\}",
                $ctrl.sharedobj<fastcgi>.uniq
            )
    },
    2 => {
        status  => 0,
        msg     => 'bad API request',
        content => '<p>Bad API request</p>',
        request =>
            sprintf(
                "\{credentials => \{token => %s\}, httpstat => 404, method => GET, route => /%s/foo/bar\}",
                $ctrl.sharedobj<fastcgi>.uniq,
                $ctrl.sharedobj<fastcgi>.uniq
            )

    },
    3 => {
        status => 1,
        msg    => 'data is logged'
    },
    31 => {
        status => 0,
        msg    => 'bad request'
    },
    32 => {
        status => 0,
        msg    => 'payload too large'
    }

;

# Check JSON request validation
subtest {
    plan 3;

    ok $ctrl.sharedobj<validatorobj>, 'validator object';

    my $json_ok = '{
        "credentials": {
            "token": "0x29666d374a4e81e895998b8070c7d589cf97126bd1a12fda7bda698814366a0316f48fb7a98411cbf5917c432fb8d2"
        },
        "method": "GET",
        "route": "%2Fcaptcha%2F%3F%2596%25f7%25d5%253d%25d3%253d%25e5%256b%25ef%2563%2516%254f%2599%25a6%256c%25dd",
        "httpstat": "200",
        "message": ""
    }';

    my $json_failure = '{
        "credentials": {},
        "method": "GET",
        "route": "%2F",
        "httpstat": "200",
        "message": ""
    }';

    my $request = $ctrl.validate_request(:r($json_ok));

    ok $request ~~ Hash, 'request validated';

    throws-like {
        $ctrl.validate_request(:r($json_failure));
    },
    Exception,
    message => /'Validation failed'/,
    'exception on not valid request';
}, 'Check JSON request validation';

# Check FastCGI mocked object
subtest {
    plan 7;

    ok $ctrl.sharedobj<fastcgi>.env.elems == 37, 'environment';
    ok $ctrl.sharedobj<fastcgi>.accept, 'accept method';
    is $ctrl.sharedobj<fastcgi>.reset, 0, 'reset method';
    is $ctrl.sharedobj<fastcgi>.header(Hash.new), 1, 'header method';
    is $ctrl.sharedobj<fastcgi>.Print('data').tail, 'data', 'Print method';
    ok $ctrl.sharedobj<fastcgi>.fetch ~~ /'data'/, 'fetch method';
    is $ctrl.sharedobj<fastcgi>.Read(0), '{}', 'Read method';
}, 'Check FastCGI mocked object';

# Check userdefined method with non-existed database
subtest {
    plan 4;

    my %match =
        details    => %(path => 'fakepath'),
        params     => %(db_name => 'test', db_flds => <id data comp>),
        controller => 'A',
        action     => 'B',
    ;

    is $ctrl.sharedobj<fastcgi>.reset, 0, 'reset content array';

    $ctrl.userdefined(:tick(1), :match(%match));
    my ($hdr, $cnt) = $ctrl.sharedobj<fastcgi>.fetch.split("\n\n", 2);

    nok $cnt ~~ / '<TMPL_VAR tmpl_' <[a..z_]>+ '>' /, 'no tmpl vars';
    ok $test.checkheader(:header($hdr), :status(404)), 'HTTP header';
    ok $test.checkcontent(
        :cnt($cnt),
        :params(
            %(
                $ctrl.sharedobj<pageobj>.get_pparams,
                $ctrl.sharedobj<pageobj>.get_tparams
            )
        )
    ), 'check content: userdefined() -> 404';

}, 'Check userdefined method with non-existed database';

# Check userdefined method with presentation database
subtest {
    plan 4;

    my %db = tb => 'presentation', cl => <id data compression[rand:bzip2]>;

    my %match =
        details    => %(path => '/presentation'),
        params     => %(db_name => %db<tb>, db_flds => %db<cl>),
        controller => 'A',
        action     => 'B',
    ;

    is $ctrl.sharedobj<fastcgi>.reset, 0, 'reset content array';

    $ctrl.userdefined(:tick(1), :match(%match));
    my ($hdr, $cnt) = $ctrl.sharedobj<fastcgi>.fetch.split("\n\n", 2);

    nok $cnt ~~ / '<TMPL_VAR tmpl_' <[a..z_]>+ '>' /, 'no tmpl vars';
    ok $test.checkheader(:header($hdr)), 'HTTP header';
    is(
        $cnt,
        $ctrl.sharedobj<pageobj>.raw_pg(
            :table(%db<tb>),
            :fields(%db<cl>),
        ),
        'check content: userdefined() -> presentation',
    );

}, 'Check userdefined method with presentation database';

# Check userdefined method with tpc20cic database
subtest {
    plan 4;

    my %db = tb => 'tpc20cic_presentation', cl => <id data compression[rand:bzip2]>;

    my %match =
        details    => %(path => '/tpc20cic'),
        params     => %(db_name => %db<tb>, db_flds => %db<cl>),
        controller => 'A',
        action     => 'B',
    ;

    is $ctrl.sharedobj<fastcgi>.reset, 0, 'reset content array';

    $ctrl.userdefined(:tick(1), :match(%match));
    my ($hdr, $cnt) = $ctrl.sharedobj<fastcgi>.fetch.split("\n\n", 2);

    nok $cnt ~~ / '<TMPL_VAR tmpl_' <[a..z_]>+ '>' /, 'no tmpl vars';
    ok $test.checkheader(:header($hdr)), 'HTTP header';
    is(
        $cnt,
        $ctrl.sharedobj<pageobj>.raw_pg(
            :table(%db<tb>),
            :fields(%db<cl>),
        ),
        'check content: userdefined() -> tpc20cic',
    );

}, 'Check userdefined method with tpc20cic database';

# Check index method
subtest {
    plan 4;

    my %match =
        details    => %(path => '/index'),
        controller => 'A',
        action     => 'B',
    ;

    is $ctrl.sharedobj<fastcgi>.reset, 0, 'reset content array';

    $ctrl.index(
        :tick(1),
        :match(%match),
        :header({ETag=>$ctrl.sharedobj<fastcgi>.uniq, Max-Forwards=>10}),
        :cookies([Pheix::View::Web::Cookie.new(
            :name('token'),
            :value($ctrl.sharedobj<fastcgi>.uniq)).cookie({Secure=>True}
        )]),
    );

    my ($hdr, $cnt) = $ctrl.sharedobj<fastcgi>.fetch.split("\n\n", 2);

    nok $cnt ~~ / '<TMPL_VAR tmpl_' <[a..z_]>+ '>' /, 'no tmpl vars';

    ok $test.checkheader(:header($hdr)), 'HTTP header';
    ok $test.checkcontent(
        :cnt($cnt),
        :params(
            %(
                $ctrl.sharedobj<pageobj>.get_pparams,
                $ctrl.sharedobj<pageobj>.get_tparams
            )
        )
    ), 'check content: index()';
}, 'Check index method';

# Check error method - direct
subtest {
    plan 32;

    for (400...405, 413, 429) {
        my %match =
            code       => ~$_,
            details    => %(path => '/error/' ~ now.Rat ~ q{/} ~ $_),
            controller => 'A',
            action     => 'B',
        ;

        is $ctrl.sharedobj<fastcgi>.reset, 0, 'reset content array';

        $ctrl.error(
            :tick(1),
            :%match
        );
        my ($hdr, $cnt) =
            $ctrl.sharedobj<fastcgi>.fetch.split("\n\n", 2);

        nok $cnt ~~ / '<TMPL_VAR tmpl_' <[a..z_]>+ '>' /, 'no tmpl vars';
        ok $test.checkheader(
            :header($hdr),
            :status($_)
        ), 'HTTP header';
        ok $test.checkcontent(
            :cnt($cnt),
            :params(
                %(
                    $ctrl.sharedobj<pageobj>.get_pparams,
                    $ctrl.sharedobj<pageobj>.get_tparams
                )
            )
        ), 'check content: error() -> ' ~ $_;
    }
}, 'Check error method - direct';

# Check error method - redirect
subtest {
    plan 32;

    for (400...405, 413, 429) {
        my %match =
            details    => %(path => '/error/' ~ now.Rat ~ q{/} ~ $_),
            controller => 'A',
            action     => 'B',
        ;

        is $ctrl.sharedobj<fastcgi>.reset, 0, 'reset content array';

        $ctrl.error(
            :tick(1),
            :code($_),
            :req(%match<details><path>),
            :msg('comment on ' ~ $_ ~ ' error')
        );
        my ($hdr, $cnt) =
            $ctrl.sharedobj<fastcgi>.fetch.split("\n\n", 2);

        nok $cnt ~~ / '<TMPL_VAR tmpl_' <[a..z_]>+ '>' /, 'no tmpl vars';
        ok $test.checkheader(
            :header($hdr),
            :status($_)
        ), 'HTTP header';
        ok $test.checkcontent(
            :cnt($cnt),
            :params(
                %(
                    $ctrl.sharedobj<pageobj>.get_pparams,
                    $ctrl.sharedobj<pageobj>.get_tparams
                )
            )
        ), 'check content: error() -> ' ~ $_;
    }
}, 'Check error method - redirect';

# Check redirect method - successful scenario
subtest {
    plan 3;

    my Str $url = 'https://pheix.org';
    my %match   =
        details    => %(path => '/redirect'),
        controller => 'A',
        action     => 'B',
        query      => '?redirectto=' ~ $url,
    ;

    my Str $value = ~(%match<query>);
    $value ~~ s:g/^ '?' // if $value ~~ /^ \? /;

    %*ENV<QUERY_STRING> = $value;

    is $ctrl.sharedobj<fastcgi>.reset, 0, 'reset content array';

    $ctrl.redirect(:tick(1), :match(%match));
    my $hdr = $ctrl.sharedobj<fastcgi>.fetch;

    ok (
        $test.checkheader(:header($hdr), :status(302)) &&
        $hdr ~~ / 'Location:' \s?: { quotemeta($url) }/
    ), 'correct HTTP redirect header';
    is %*ENV<QUERY_STRING>:delete, $value, 'purge environment';

}, 'Check redirect method - successful scenario';

# Check redirect method - failure scenario
subtest {
    plan 4;

    my %match   =
        details    => %(path => '/redirect'),
        controller => 'A',
        action     => 'B',
    ;

    is $ctrl.sharedobj<fastcgi>.reset, 0, 'reset content array';

    $ctrl.redirect(:tick(1), :match(%match));
    my ($hdr, $cnt) = $ctrl.sharedobj<fastcgi>.fetch.split("\n\n", 2);

    nok $cnt ~~ / '<TMPL_VAR tmpl_' <[a..z_]>+ '>' /, 'no tmpl vars';
    ok $test.checkheader(:header($hdr), :status(404)), 'HTTP header';
    ok $test.checkcontent(
        :cnt($cnt),
        :params(
            %(
                $ctrl.sharedobj<pageobj>.get_pparams,
                $ctrl.sharedobj<pageobj>.get_tparams
            )
        )
    ), 'check content: redirect() -> 404';
}, 'Check redirect method - failure scenario';

# Check bigbrother method
subtest {
    plan 5;

    my %match   =
        details    => %(path => '/bigbrother'),
        controller => 'A',
        action     => 'B',
        query      =>
            '?page=/test/' ~ now.Rat ~ '/route/&amp;' ~
            'resolut=1280*800&amp;ref=undef&amp;rnd=0.8240235702801',
    ;

    my Str $value = ~(%match<query>);
    $value ~~ s:g/^ '?' // if $value ~~ /^ \? /;

    %*ENV<QUERY_STRING> = $value;

    is $ctrl.sharedobj<fastcgi>.reset, 0, 'reset content array';

    $ctrl.bigbrother(:tick(1), :match(%match));
    my $hdr = $ctrl.sharedobj<fastcgi>.fetch;

    ok $test.checkheader(:header($hdr)), 'HTTP header';
    ok $ctrl.sharedobj<statobj>.dbpath.IO.e, 'bigbro stats is saved';
    ok $ctrl.sharedobj<statobj>.dbpath.IO.unlink, 'purge bigbro stats';
    is %*ENV<QUERY_STRING>:delete, $value, 'purge environment';
}, 'Check bigbrother method';

# Check logger method - positive scenario
subtest {
    plan 8;

    my %match =
        details    => %(path => '/logger'),
        controller => 'A',
        action     => 'B',
    ;

    my @responses;

    for (4,6) -> $req {
        is $ctrl.sharedobj<fastcgi>.reset, 0, 'reset content array req=' ~ $req;

        $ctrl.sharedobj<fastcgi>.clen = $req;
        $ctrl.logger(:tick(1), :match(%match));

        @responses.push($ctrl.sharedobj<fastcgi>.fetch.split("\n\n", 2));
    }

    my ($hdr, $cnt) = @responses.tail;

    my @rows = $ctrl.sharedobj<logrobj>.chainobj.row_get(:clause({}));

    ok $test.checkheader(:header($hdr)), 'HTTP header';
    is-deeply %res{3}, from-json($cnt), %res{3}<msg>;
    ok $ctrl.sharedobj<logrobj>.chainobj.get_path.IO.e, 'logfile is ok';
    ok @rows.elems >= 1, 'found records in log';
    is(
        $ctrl.sharedobj<mb64obj>.decode-str(@rows.tail<log>),
        from-json($ctrl.sharedobj<fastcgi>.Read(6))<log>,
        'valid data in log'
    );
    ok $ctrl.sharedobj<logrobj>.chainobj.table_drop, 'clear log';
}, 'Check logger method - positive scenario';

# Check logger method - 400 error
subtest {
    plan 3;

    my %match =
        details    => %(path => '/logger'),
        controller => 'A',
        action     => 'B',
    ;

    is $ctrl.sharedobj<fastcgi>.reset, 0, 'reset content array';

    $ctrl.sharedobj<fastcgi>.clen = 65535;
    $ctrl.logger(:tick(1), :match(%match));

    my ($hdr, $cnt) = $ctrl.sharedobj<fastcgi>.fetch.split("\n\n", 2);

    my @rows = $ctrl.sharedobj<logrobj>.chainobj.row_get(:clause({}));

    ok $test.checkheader(:header($hdr), :status(400)), 'HTTP header';
    is-deeply %res{31}, from-json($cnt), %res{31}<msg>;
}, 'Check logger method - 400 error';

# Check logger method - 413 error
subtest {
    plan 3;

    my %match =
        details    => %(path => '/logger'),
        controller => 'A',
        action     => 'B',
    ;

    is $ctrl.sharedobj<fastcgi>.reset, 0, 'reset content array';

    $ctrl.sharedobj<fastcgi>.clen = 65536;
    $ctrl.logger(:tick(1), :match(%match));

    my ($hdr, $cnt) = $ctrl.sharedobj<fastcgi>.fetch.split("\n\n", 2);

    my @rows = $ctrl.sharedobj<logrobj>.chainobj.row_get(:clause({}));

    ok $test.checkheader(:header($hdr), :status(413)), 'HTTP header';
    is-deeply %res{32}, from-json($cnt), %res{32}<msg>;
}, 'Check logger method - 413 error';

# Check api_debug method
subtest {
    plan 3;

    my %match   =
        details    => %(path => '/api-debug'),
        controller => 'A',
        action     => 'B',
    ;

    is $ctrl.sharedobj<fastcgi>.reset, 0, 'reset content array';

    $ctrl.api_debug(:tick(1), :match(%match));
    my ($hdr, $cnt) = $ctrl.sharedobj<fastcgi>.fetch.split("\n\n", 2);

    ok $test.checkheader(:header($hdr)), 'HTTP header';
    is $cnt, $ctrl.sharedobj<dbugobj>.api_debug_form(:url('/api')),
        'check content: api_debug()';
}, 'Check api_debug method';

# Check sitemap method with dynamic sitemap
subtest {
    plan 4;

    my $pobj = $capi.sharedobj<pageobj>;

    my $d = $test.checkdate($pobj.fchnobj.get_path(:tab($pobj.indxcnt)).IO.modified);
    my $r = $ctrl.sharedobj<headobj>.proto_sn;

    my %match   =
        details    => %(path => '/sitemap'),
        controller => 'A',
        action     => 'B',
        format     => 'xml'
    ;

    is $ctrl.sharedobj<fastcgi>.reset, 0, 'reset content array';

    $ctrl.sitemap(:tick(1), :match(%match));
    my ($hdr, $cnt) = $ctrl.sharedobj<fastcgi>.fetch.split("\n\n", 2);
    ok $test.checkheader(
        :header($hdr),
        :ctype('text/xml; charset=UTF-8')
    ), 'HTTP header';
    ok $cnt ~~ / '<lastmod>' $d '</lastmod>' /, 'lastmod is ' ~ $d;
    ok $cnt ~~ / '<loc>' $r '</loc>' /, 'loc is ' ~ $r,
}, 'Check sitemap method with dynamic sitemap';

# Check sitemap method with static sitemap
subtest {
    plan 3;

    my $d = $test.checkdate(now);
    my $r = $ctrl.sharedobj<headobj>.proto_sn;

    my %match   =
        details    => %(path => '/sitemap'),
        controller => 'A',
        action     => 'B',
        format     => 'xml'
    ;

    is $ctrl.sharedobj<fastcgi>.reset, 0, 'reset content array';

    $ctrl.sitemap(:tick(1), :match(%match), :forcestatic(True));
    my ($hdr, $cnt) = $ctrl.sharedobj<fastcgi>.fetch.split("\n\n", 2);

    ok $test.checkheader(
        :header($hdr),
        :ctype('text/xml; charset=UTF-8'),
        :f($ctrl.sharedobj<utilobj>.smfn)
    ), 'HTTP header';

    my Str $sm = $ctrl.sharedobj<utilobj>.smfn.IO.slurp;
    $sm ~~ s:g:i/ :r \'/'\\\''/;

    ok $cnt ~~ / $sm /, 'found static sitemap content';

}, 'Check sitemap method with static sitemap';

# Check sitemap forces 404 error on fake sitemap filename
subtest {
    plan 5;

    my $d = $test.checkdate(now);
    my $r = $ctrl.sharedobj<headobj>.proto_sn;

    my %match   =
        details    => %(path => '/sitemap'),
        controller => 'A',
        action     => 'B',
        format     => 'xml'
    ;

    ok $ctrl.sharedobj<utilobj>.set_smfn(
        :fname('foo/bar.xml'),
        :force(True)
    ), 'set fake sitemap';

    is $ctrl.sharedobj<fastcgi>.reset, 0, 'reset content array';

    $ctrl.sitemap(:tick(1), :match(%match), :forcestatic(True));

    my ($hdr, $cnt) = $ctrl.sharedobj<fastcgi>.fetch.split("\n\n", 2);

    nok $cnt ~~ / '<TMPL_VAR tmpl_' <[a..z_]>+ '>' /, 'no tmpl vars';
    ok $test.checkheader(:header($hdr), :status(404)), 'HTTP header';
    ok $test.checkcontent(
        :cnt($cnt),
        :params(
            %(
                $ctrl.sharedobj<pageobj>.get_pparams,
                $ctrl.sharedobj<pageobj>.get_tparams
            )
        )
    ), 'check content: sitemap() -> 404';

}, 'Check sitemap forces 404 error on fake sitemap filename';

# Check api method with null apirobj
subtest {
    plan 12;

    my %match =
        details    => %(path => 'fakepath'),
        controller => 'A',
        action     => 'B',
    ;

    for ^3 {
        is $ctrl.sharedobj<fastcgi>.reset, 0, 'reset content array';

        $ctrl.sharedobj<fastcgi>.clen = $_;
        $ctrl.api(:tick(1), :match(%match));
        my ($hdr, $cnt) =
            $ctrl.sharedobj<fastcgi>.fetch.split("\n\n", 2);

        ok $test.checkheader(:header($hdr)), 'HTTP header';

#        is-deeply
#            %res{$_},
#            from-json($cnt),
#            'api bad request ' ~ %res{$_}<request>.gist
#        ;

        my $response = from-json($cnt);

        is $response<msg>, 'bad API request', 'error message';
        nok $response<status>, 'error status';
    }
}, 'Check api method with null apirobj';

# Check index and its exception at api method with default apirobj
subtest {
    plan 6;

    my Pheix::Controller::Basic $c;

    for ^2 {
        if !$_ {
            $c = Pheix::Controller::Basic.new(
                :apirobj(
                    $rapi.basic_routes_with_handler(
                        :handler_class('Pheix::Controller::Fake')
                    )
                ),
                :mockedfcgi($fcgi),
                :test(True),
            );
        }
        else {
            $c = $capi;
        }

        is $c.sharedobj<fastcgi>.reset, 0, 'reset content array';

        $c.sharedobj<fastcgi>.clen = 1;
        $c.api(:tick(1), :match(Hash.new));

        my ($hdr, $cnt) = $c.sharedobj<fastcgi>.fetch.split("\n\n", 2);

        my %res = from-json($cnt);

        ok $test.checkheader(:header($hdr)), 'HTTP header';

        $_ ??
            ok %res<status>, 'got no exception on index api handler' !!
                nok %res<status>, 'got exception: ' ~ %res<content><tparams><tmpl_exception_msg>;
    }
}, 'Check index and its exception at api method with default apirobj';

# Check error and its exception at api method with default apirobj
subtest {
    plan 6;

    for [3, 2].kv -> $step, $req {
        is $capi.sharedobj<fastcgi>.reset, 0, 'reset content array';

        $capi.sharedobj<fastcgi>.clen = $req;
        $capi.api(:tick(1), :match(Hash.new));

        my ($hdr, $cnt) =
            $capi.sharedobj<fastcgi>.fetch.split("\n\n", 2);

        my %res = from-json($cnt);

        ok $test.checkheader(:header($hdr)), 'HTTP header';

        $step ??
            ok %res<status>, 'got error on api handler' !!
                nok %res<status>, 'got exception: ' ~ %res<content>;;
    }
}, 'Check error and its exception at api method with default apirobj';

done-testing;
