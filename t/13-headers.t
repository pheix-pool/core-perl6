use v6.d;
use Test;
use lib 'lib';

plan 12;

use Pheix::Test::Helpers;
use Pheix::View::Web::Headers;
use Pheix::View::Web::Cookie;

my $obj = Pheix::View::Web::Headers.new;
my $dp  = Pheix::Datepack.new(date => DateTime.now, unixtime => time);

my $httpdate = $dp.get_http_response_date;

my $c_1 = Pheix::View::Web::Cookie.new(name => 'c1', value => 'c1-value');
my $c_2 = Pheix::View::Web::Cookie.new(name => 'c2', value => 'c2-value');
my $c_3 = Pheix::View::Web::Cookie.new(name => 'c3', value => 'c3-value');
my $c_4 = Pheix::View::Web::Cookie.new(name => 'c4', value => 'c4-value');

$httpdate ~~ s/(\d\d)\:(\d\d)\:(\d\d)/\\d\\d\\:\\d\\d\\:\\d\\d/;
$httpdate ~~ s:g/\,/\\\,/;
$httpdate ~~ s:g/\s/\\s/;

my $policy = 'P3P:policyref="/w3c/p3p.xml", CP="NON DSP ADM ' ~
    'DEV PSD IVDo OUR IND STP PHY PRE NAV UNI"';

# Check default header
subtest {
    plan 7;
    my Str $hdr =
        $obj.header(Hash.new, List.new, True)<Composed-Header>;
    my $expr1   = 'Last\-Modified\:' ~ $httpdate;
    my $expr2   = 'Expires\:' ~ $httpdate;
    ok(
        ( $hdr ~~ /<$expr1>/ ),
        'found Last-Modified with pattern: ' ~ $expr1,
    );
    ok(
        ( $hdr ~~ /<$expr2>/ ),
        'found Expires with pattern: ' ~ $expr2,
    );
    ok(
        $hdr ~~ /
            'Cache-Control:no-cache,no-store,max-age=0,must-revalidate'
        /,
        'found Cache-Control',
    );
    ok(
        $hdr ~~ /
            'Content-Type:text/html; charset=UTF-8'
        /,
        'found Content-Type',
    );
    ok(
        $hdr ~~ /
            'P3P:CP="Pheix does not have a P3P policy."'
        /,
        'found P3P',
    );
    ok( (  $hdr ~~ /'Status:200'/ ), 'found Status 200' );
    nok( ( $hdr ~~ /'Set-Cookie'/ ), 'not found Set-Cookie' );
}, 'Check default header';

# Check default header with cookies
subtest {
    plan 4;

    my Str $hdr = $obj.header(
        Hash.new,
        [
            $c_1.cookie(%(Max-Age => -1)),
            $c_2.cookie(%(Max-Age => 10)),
            $c_3.cookie(%(Max-Age => 100)),
            $c_4.cookie(%(Max-Age => 1000)),
        ],
        True,
    )<Composed-Header>;

    ok(
        $hdr ~~ /'Set-Cookie:c1=c1-value;' / && $hdr ~~ /'Max-Age=-1;'/,
        'cookie 1',
    );

    ok(
        $hdr ~~ /'Set-Cookie:c2=c2-value;'/ && $hdr ~~ /'Max-Age=10;'/,
        'cookie 2',
    );

    ok(
        $hdr ~~ /'Set-Cookie:c3=c3-value;'/ && $hdr ~~ /'Max-Age=100;'/,
        'cookie 3',
    );

    ok(
        $hdr ~~ /'Set-Cookie:c4=c4-value;'/ && $hdr ~~ /'Max-Age=1000;'/,
        'cookie 4',
    );
}, 'Check default header with cookies';

# Check user header
subtest {
    plan 10;
    my $d1 = Pheix::Datepack.new(
        date => Date.new('1985-09-15').DateTime,
        unixtime => 1474372872,
    );
    my $d2 = Pheix::Datepack.new(
        date => Date.new('1982-08-07').DateTime,
        unixtime => 1474372301,
    );
    my $h1 = $d1.get_http_response_date;
    my $h2 = $d2.get_http_response_date;
    my Str $hdr = $obj.header(
        %(
            Expires       => $h1,
            Content-Type  => 'utf8',
            Cache-Control => 'private, max-age=60',
            Last-Modified => $h2,
            P3P => 'policyref="/w3c/p3p.xml", CP="NON DSP ADM ' ~
                   'DEV PSD IVDo OUR IND STP PHY PRE NAV UNI"',
        ),
        @(
            $c_1.cookie(%(Max-Age => -1)),
            $c_2.cookie(%(Max-Age => 10)),
            $c_3.cookie(%(Max-Age => 100)),
            $c_4.cookie(%(Max-Age => 1000)),
        ),
        True,
    )<Composed-Header>;
    ok(
        $hdr ~~ / 'Last-Modified:' $h2 /,
        'found Last-Modified:' ~ $h2,
    );
    ok(
        $hdr ~~ / 'Expires:' $h1 /,
        'found Expires:' ~ $h1,
    );
    ok(
        $hdr ~~ / 'Cache-Control:private, max-age=60' /,
        'found Cache-Control',
    );
    ok(
        $hdr ~~ / 'Content-Type:utf8' /,
        'found Content-Type',
    );
    ok(
        $hdr ~~ / $policy /,
        'found P3P'
    );

    ok $hdr ~~ /'Status:200'/, 'found Status 200';

    ok $hdr ~~ /'Set-Cookie:c1=c1-value;'/ && $hdr ~~ /'Max-Age=-1;'/, 'cookie 1';
    ok $hdr ~~ /'Set-Cookie:c2=c2-value;'/ && $hdr ~~ /'Max-Age=10;'/, 'cookie 2';
    ok $hdr ~~ /'Set-Cookie:c3=c3-value;'/ && $hdr ~~ /'Max-Age=100;'/, 'cookie 3';
    ok $hdr ~~ /'Set-Cookie:c3=c3-value;'/ && $hdr ~~ /'Max-Age=1000;'/, 'cookie 4';
}, 'Check user header';

# Check setup Content-Type in user header
subtest {
    plan 6;
    my $ud = 'user-defined-value';
    my Str $hdr = $obj.header(
        %(Content-Type => $ud ),
        List.new,
        True,
    )<Composed-Header>;
    my $expr1 = 'Last\-Modified\:' ~ $httpdate;
    my $expr2 = 'Expires\:' ~ $httpdate;
    ok(
        ( $hdr ~~ /<$expr1>/ ),
        'found Last-Modified with pattern: ' ~ $expr1,
    );
    ok(
        ( $hdr ~~ /<$expr2>/ ),
        'found Expires with pattern: ' ~ $expr2,
    );
    ok(
        $hdr ~~ /
            'Cache-Control:no-cache,no-store,max-age=0,must-revalidate'
        /,
        'found Cache-Control',
    );
    ok(
        $hdr ~~ /
            'P3P:CP="Pheix does not have a P3P policy."'
        /,
        'found P3P',
    );
    ok( ( $hdr ~~ / 'Status:200' / ), 'found Status 200' );
    ok(
        $hdr ~~ / 'Content-Type:' $ud /,
        'found USER DEFINED Content-Type',
    );
}, 'Check setup Content-Type in user header';

# Check setup Status in user header
subtest {
    plan 6;
    my $ud = 'user-defined-value';
    my Str $hdr = $obj.header(
        %(Status => $ud ),
        List.new,
        True,
    )<Composed-Header>;
    my $expr1 = 'Last\-Modified\:' ~ $httpdate;
    my $expr2 = 'Expires\:' ~ $httpdate;
    ok(
        ( $hdr ~~ /<$expr1>/ ),
        'found Last-Modified with pattern: ' ~ $expr1,
    );
    ok(
        ( $hdr ~~ /<$expr2>/ ),
        'found Expires with pattern: ' ~ $expr2,
    );
    ok(
        $hdr ~~ /
            'Cache-Control:no-cache,no-store,max-age=0,must-revalidate'
        /,
        'found Cache-Control',
    );
    ok(
        $hdr ~~ /
            'P3P:CP="Pheix does not have a P3P policy."'
        /,
        'found P3P',
    );
    ok(
        $hdr ~~ / 'Content-Type:text/html; charset=UTF-8' /,
        'found Content-Type',
    );
    ok( ( $hdr ~~ / 'Status:' $ud / ), 'found USER DEFINED Status' );
}, 'Check setup Status in user header';

# Check setup Last-Modified in user header
subtest {
    plan 6;
    my $ud = 'user-defined-value';
    my Str $hdr = $obj.header(
        %(Last-Modified => $ud),
        List.new,
        True,
    )<Composed-Header>;
    my $expr = 'Expires\:' ~ $httpdate;
    ok(
        ( $hdr ~~ /<$expr>/ ),
        'found Expires with pattern: ' ~ $expr,
    );
    ok(
        $hdr ~~ /
            'Cache-Control:no-cache,no-store,max-age=0,must-revalidate'
        /,
        'found Cache-Control',
    );
    ok(
        $hdr ~~ /
            'P3P:CP="Pheix does not have a P3P policy."'
        /,
        'found P3P',
    );
    ok(
        $hdr ~~ / 'Content-Type:text/html; charset=UTF-8' /,
        'found Content-Type',
    );
    ok(
        $hdr ~~ / 'Last-Modified:' $ud /,
        'found USER DEFINED Last-Modified',
    );
    ok( ( $hdr ~~ / 'Status:200' / ), 'found Status 200' );
}, 'Check setup Last-Modified in user header';

# Check setup Expires in user header
subtest {
    plan 6;
    my $ud = 'user-defined-value';
    my Str $hdr = $obj.header(
        %(Expires => $ud),
        List.new,
        True,
    )<Composed-Header>;
    my $expr = 'Last\-Modified\:' ~ $httpdate;
    ok(
        ( $hdr ~~ /<$expr>/ ),
        'found Last-Modified with pattern: ' ~ $expr,
    );
    ok(
        $hdr ~~ /
            'Cache-Control:no-cache,no-store,max-age=0,must-revalidate'
        /,
        'found Cache-Control',
    );
    ok(
        $hdr ~~ /
            'P3P:CP="Pheix does not have a P3P policy."'
        /,
        'found P3P',
    );
    ok(
        $hdr ~~ / 'Content-Type:text/html; charset=UTF-8' /,
        'found Content-Type',
    );
    ok( ( $hdr ~~ / 'Expires:' $ud / ), 'found USER DEFINED Expires' );
    ok( ( $hdr ~~ / 'Status:200' / ), 'found Status 200' );
}, 'Check setup Expires in user header';

# Check setup Cache-Control in user header
subtest {
    plan 6;
    my $ud = 'user-defined-value';
    my Str $hdr = $obj.header(
        %(Cache-Control => $ud),
        List.new,
        True,
    )<Composed-Header>;
    my $expr1 = 'Last\-Modified\:' ~ $httpdate;
    my $expr2 = 'Expires\:' ~ $httpdate;
    ok(
        ( $hdr ~~ /<$expr1>/ ),
        'found Last-Modified with pattern: ' ~ $expr1,
    );
    ok(
        ( $hdr ~~ /<$expr2>/ ),
        'found Expires with pattern: ' ~ $expr2,
    );
    ok(
        $hdr ~~ /
            'P3P:CP="Pheix does not have a P3P policy."'
        /,
        'found P3P',
    );
    ok(
        $hdr ~~ / 'Content-Type:text/html; charset=UTF-8' /,
        'found Content-Type',
    );
    ok(
        $hdr ~~ / 'Cache-Control:' $ud /,
        'found USER DEFINED Cache-Control',
    );
    ok( ( $hdr ~~ / 'Status:200' / ), 'found Status 200' );
}, 'Check setup Cache-Control in user header';

# Check setup P3P in user header
subtest {
    plan 6;
    my $ud = 'user-defined-value';
    my Str $hdr = $obj.header(
        %(P3P => $ud),
        List.new,
        True,
    )<Composed-Header>;
    my $expr1 = 'Last\-Modified\:' ~ $httpdate;
    my $expr2 = 'Expires\:' ~ $httpdate;
    ok(
        ( $hdr ~~ /<$expr1>/ ),
        'found Last-Modified with pattern: ' ~ $expr1,
    );
    ok(
        ( $hdr ~~ /<$expr2>/ ),
        'found Expires with pattern: ' ~ $expr2,
    );
    ok(
        $hdr ~~ /
            'Cache-Control:no-cache,no-store,max-age=0,must-revalidate'
        /,
        'found Cache-Control',
    );
    ok(
        $hdr ~~ / 'Content-Type:text/html; charset=UTF-8' /,
        'found Content-Type',
    );
    ok( ( $hdr ~~ / 'Status:200' / ), 'found Status 200' );
    ok( ( $hdr ~~ / 'P3P:' $ud / ), 'found USER DEFINED P3P' );
}, 'Check setup P3P in user header';

# Check setup not default field in user header
subtest {
    plan 7;
    my $udf  = 'X-Request-ID';
    my $udv  = 'user-defined-value';
    my Str $hdr = $obj.header(
        %($udf => $udv),
        List.new,
        True,
    )<Composed-Header>;
    my $expr1 = 'Last\-Modified\:' ~ $httpdate;
    my $expr2 = 'Expires\:' ~ $httpdate;
    ok(
        ( $hdr ~~ /<$expr1>/ ),
        'found Last-Modified with pattern: ' ~ $expr1,
    );
    ok(
        ( $hdr ~~ /<$expr2>/ ),
        'found Expires with pattern: ' ~ $expr2,
    );
    ok(
        $hdr ~~ /
            'Cache-Control:no-cache,no-store,max-age=0,must-revalidate'
        /,
        'found Cache-Control',
    );
    ok(
        $hdr ~~ / 'Content-Type:text/html; charset=UTF-8' /,
        'found Content-Type',
    );
    ok( ( $hdr ~~ / 'Status:200' / ), 'found Status 200' );
    ok( ( $hdr ~~ / 'P3P:CP="Pheix does not have a P3P policy."' / ), 'found P3P' );
    ok( ( $hdr ~~ / $udf\:$udv / ), 'found USER DEFINED ' ~ $udf );
}, 'Check setup not default X-Request-ID in user header';

# Check setup misc shuffle values in user header
subtest {
    plan 6;
    my $ud = 'user-defined-value';
    my Str $hdr = $obj.header(
        %(
            P3P => $ud,
            Status => $ud,
            Expires => $ud,
        ),
        List.new,
        True,
    )<Composed-Header>;
    my $expr = 'Last\-Modified\:' ~ $httpdate;
    ok(
        ( $hdr ~~ /<$expr>/ ),
        'found Last-Modified with pattern: ' ~ $expr,
    );
    ok(
        $hdr ~~ /
            'Cache-Control:no-cache,no-store,max-age=0,must-revalidate'
        /,
        'found Cache-Control',
    );
    ok(
        $hdr ~~ / 'Content-Type:text/html; charset=UTF-8' /,
        'found Content-Type',
    );
    ok( ( $hdr ~~ / 'Expires:' $ud / ), 'found USER DEFINED Expires' );
    ok( ( $hdr ~~ / 'Status:' $ud / ), 'found USER DEFINED Status' );
    ok( ( $hdr ~~ / 'P3P:' $ud / ), 'found USER DEFINED P3P' );
}, 'Check setup misc shuffle values in user header';

if !Pheix::Test::Helpers.new.check_submodules {
    skip-rest('No Pheix submodules are pulled');
    exit;
}

# Check proto_sn method
subtest {
    plan 2;
    is(
        $obj.proto_sn,
        'https://pheix.org',
        'get proto and servername from config',
    );

    $obj = Pheix::View::Web::Headers.new(addon => 'FooBar');
    is(
        $obj.proto_sn,
        'http://undef',
        'get proto and servername from config of unknown addon',
    );
}, 'Check proto_sn method';

done-testing;
