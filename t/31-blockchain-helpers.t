use v6.d;
use Test;
use Test::Mock;
use lib 'lib';

use Net::Ethereum;
use Pheix::Test::Blockchain;
use Pheix::Test::BlockchainComp::Helpers;

plan 2;

constant blank  = '0x0000000000000000000000000000000000000000000000000000000000000000';
constant excptn = '0xffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff';
constant trx    = '0x97e92abe8de919f6e7f79105b534690f88704d8dc7c81b805cc3410baad30e1a';
constant sc     = '0xe6df2905589db3c09091462f771ed0a3b820017c';
constant acc    = '0x9dc4c65c12d81f741ae57ac7abc3a02a342625a4';
constant sgnlog = [
    '2023-06-18 14:17:40.2202|000016|458|111844|0xc070479207f314ef5ba5364740aebe5c09f79d14da25ed4a67fa7933f0d578b5|newTable',
    '2023-06-18 14:17:45.3716|000017|1034|463803|0xcd05d7a2ddc8e28d6d0a96e90cc9b29e9024207a2d4852228abed3c2ff8e114b|insert',
    '2023-06-18 14:17:50.5415|000018|842|374289|0x3fe4d9209869d34c5923fd7eb2d13cabe34e129802fdb094151f63aad2b526be|insert',
    '2023-06-18 14:17:55.7030|000019|906|380208|0xa2ae87fd4d04fce890a83a86eafa74269e2f25ca696f665b0992be59fe26c15a|insert',
    '2023-06-18 14:18:00.0000|000020|522|134172|0x470f603365caa857c3088ed3e454875a1832ce624796d9a492cb15dbd2547070|set',
    '2023-06-18 14:18:06.0363|000021|266|112254|0xe4f5bbd9081b3c7a63fa5e76a8d5138c1cc5e9bbc8c2bed8c3569ee1334bb41b|remove',
    '2023-06-18 14:18:10.7235|000022|266|168925|0x690ad996ea48f34fdb0557fa66f4fcf36d1bacd282b4eee12bdcd376120f7aa4|remove',
    '2023-06-18 14:18:11.2477|000023|266|150174|0xc0e1f9f2e67421b0275281013f851acae9d30b86f7b75a3829ba6a101cea8853|remove'
];

constant sgnlogfn = sprintf("%s-sgn-%s.log", $*PROGRAM.basename, DateTime.now(formatter => {
    sprintf(
        "%04d%02d%02d-%02d:%02d:%02u",
        .year,
        .month,
        .day,
        .hour,
        .minute,
        .second.Int
    )
}));

class MockedBlockchain {
    has Net::Ethereum $.ethobj;
    has $.sgnlog is rw;
}

class MockedDatabase {
    has MockedBlockchain $.chainobj;
}

my $ne = mocked(
    Net::Ethereum,
    returning => {
        contract_method_call_estimate_gas => 3_000_000,
        sendTransaction => trx,
        retrieve_contract => sc,
        eth_accounts => [acc],
        eth_getTransactionByHash => {blockNumber => 1},
        eth_getTransactionReceipt => {status => 1},
        debug_traceTransaction => {failure => False}
    },
    overriding => {
        debug_traceTransaction => -> :$dbobj, :$trx, :$diag = True {
            die 'emulate exception for ' ~ $trx if $trx eq excptn;

            $trx eq blank ?? Any !! {failure => False};
        }
    }
);

my MockedDatabase $dbobj =
    MockedDatabase.new(:chainobj(MockedBlockchain.new(:ethobj($ne), :sgnlog(sgnlog))));

my Pheix::Test::BlockchainComp::Helpers $helpobj =
    Pheix::Test::BlockchainComp::Helpers.new(
        :testnet(Str),
        :localtab(Str),
        :debug(False),
        :tstobj(Pheix::Test::Blockchain.new)
    );

subtest {
    plan 5;

    my $trace = $helpobj.trace_transaction(:dbobj($dbobj), :trx(trx), :diag(True));

    ok ($trace<trx>:exists) && ($trace<receipt>:exists) && ($trace<trace>:exists), 'basic keys in trace';

    is ($helpobj.trace_transaction(:dbobj($dbobj), :trx(Str), :diag(False)))<error>,
        'invalid trx hash: ',
            'error on null transaction hash';

    is ($helpobj.trace_transaction(:dbobj($dbobj), :trx('not hex trx hash'), :diag(False)))<error>,
        'invalid trx hash: not hex trx hash',
            'error on invalid transaction hash';

    is ($helpobj.trace_transaction(:dbobj($dbobj), :trx(blank), :diag(False)))<trace><status>,
        'null trace',
            'status null trace';

    is ($helpobj.trace_transaction(:dbobj($dbobj), :trx(excptn), :diag(False)))<trace><error>,
        'emulate exception for ' ~ excptn,
            'exception message in trace';
}, 'Check trace transaction method';

subtest {
    my $log = $dbobj.chainobj.sgnlog.join("\n");

    plan 8;

    ok !sgnlogfn.IO.e, 'no log file at initial step';

    ok $helpobj.flush_signing_session(:sgnlog($dbobj.chainobj.sgnlog), :filename(sgnlogfn)), sprintf("log save to %s", sgnlogfn);

    ok sgnlogfn.IO.e && sgnlogfn.IO.f, 'log file is found';
    is sgnlogfn.IO.slurp, sprintf("%s\n", $log), 'valid content';

    ok $helpobj.flush_signing_session(:sgnlog($dbobj.chainobj.sgnlog), :filename(sgnlogfn)), sprintf("log append to %s", sgnlogfn);
    is sgnlogfn.IO.slurp, sprintf("%s\n%s\n", $log, $log), 'valid content';

    ok unlink(sgnlogfn), 'delete log file';

    $dbobj.chainobj.sgnlog = [];

    ok !$helpobj.flush_signing_session(:sgnlog($dbobj.chainobj.sgnlog)), 'log failure if empty';
}, 'Flush signing log';

done-testing;
