use v6.d;
use Test;
use lib 'lib';

plan 17;

use Pheix::Datepack;
my Any $obj = Nil;

my %timestamps =
    2017 => 1484989202,
    1970 => 2710802,
    1982 => 394963202,
    2011 => 1299834002,
    1985 => 495619202,
    2018 => 1524128402,
    2100 => 4115350802,
    2019 => 1561194002,
    2021 => 1627722002,
    2010 => 1288252802,
    2210 => 7600467602,
    2000 => 975661202
;

subtest {
    my $testplan = 1_000;

    plan $testplan;

    my @unixtimes = (1..$testplan).map({(now - 1_000_000_000.rand).UInt});

    for @unixtimes -> $unixtime {
        my $dateobj = Pheix::Datepack.new(
            :date(DateTime.new($unixtime)),
            :unixtime($unixtime)
        );

        my $date      = $dateobj.get_http_response_date;
        my $converted = $dateobj.convert_http_response_date_to_unixtime(:$date);

        is $unixtime, $converted, sprintf("convert gives correct unixtime %d from date %s", $unixtime, $date);
    }
}, 'check http date convert';

subtest {
    plan 4;
    $obj = Pheix::Datepack.new(
        date => Date.new('2017-01-21').DateTime,
        unixtime => %timestamps<2017>,
    );
    is(
        $obj.format_update,
        '21 January, 2017',
        'format_update() on 2017-01-21',
    );
    is(
        $obj.year_update,
        '2017',
        'year_update() on 2017',
    );
    is(
        $obj.hex_unixtime,
        '%31%34%38%34%39%38%39%32%30%32',
        'hex_unixtime() on ' ~ %timestamps<2017>,
    );
    is(
        $obj.get_http_response_date,
        'Sat, 21 Jan 2017 09:00:02 GMT',
        'get_http_response_date() with ' ~ %timestamps<2017>,
    );
}, 'Date subtest for 21 January, 2017';

subtest {
    plan 4;
    $obj = Pheix::Datepack.new(
        date => Date.new('1970-02-01').DateTime,
        unixtime => %timestamps<1970>,
    );
    is(
        $obj.format_update,
        '1 February, 1970',
        'format_update() on 1970-02-01',
    );
    is(
        $obj.year_update,
        '1970',
        'year_update() on 1970',
    );
    is(
        $obj.hex_unixtime,
        '%32%37%31%30%38%30%32',
        'hex_unixtime() on ' ~ %timestamps<1970>,
    );
    is(
        $obj.get_http_response_date,
        'Sun, 01 Feb 1970 09:00:02 GMT',
        'get_http_response_date() with ' ~ %timestamps<1970>,
    );
}, 'Date subtest for 1 February, 1970';

subtest {
    plan 4;
    $obj = Pheix::Datepack.new(
        date => Date.new('1982-08-07').DateTime,
        unixtime => %timestamps<1982>,
    );
    is(
        $obj.format_update,
        '7 August, 1982',
        'format_update() on 1982-07-08',
    );
    is(
        $obj.year_update,
        '1982',
        'year_update() on 1982',
    );
    is(
        $obj.hex_unixtime,
        '%33%39%34%39%36%33%32%30%32',
        'hex_unixtime() on ' ~ %timestamps<1982>,
    );
    is(
        $obj.get_http_response_date,
        'Sat, 07 Aug 1982 08:00:02 GMT',
        'get_http_response_date() with ' ~ %timestamps<1982>,
    );
}, 'Date subtest for 7 August, 1982';

subtest {
    plan 4;
    $obj = Pheix::Datepack.new(
        date => Date.new('2011-03-11').DateTime,
        unixtime => %timestamps<2011>,
    );
    is(
        $obj.format_update,
        '11 March, 2011',
        'format_update() on 2011-03-11',
    );
    is(
        $obj.year_update,
        '2011',
        'year_update() on 2011',
    );
    is(
        $obj.hex_unixtime,
        '%31%32%39%39%38%33%34%30%30%32',
        'hex_unixtime() on ' ~ %timestamps<2011>,
    );
    is(
        $obj.get_http_response_date,
        'Fri, 11 Mar 2011 09:00:02 GMT',
        'get_http_response_date() with ' ~ %timestamps<2011>,
    );
}, 'Date subtest for 11 March, 2011';

subtest {
    plan 4;
    $obj = Pheix::Datepack.new(
        date => Date.new('1985-09-15').DateTime,
        unixtime => %timestamps<1985>,
    );
    is(
        $obj.format_update,
        '15 September, 1985',
        'format_update() on 1985-09-15',
    );
    is( $obj.year_update, '1985', 'year_update() on 1985' );
    is(
        $obj.hex_unixtime,
        '%34%39%35%36%31%39%32%30%32',
        'hex_unixtime() on ' ~ %timestamps<1985>,
    );
    is(
        $obj.get_http_response_date,
        'Sun, 15 Sep 1985 08:00:02 GMT',
        'get_http_response_date() with ' ~ %timestamps<1985>,
    );
}, 'Date subtest for 15 September, 1985';

subtest {
    plan 4;
    $obj = Pheix::Datepack.new(
        date => Date.new('2018-04-19').DateTime,
        unixtime => %timestamps<2018>,
    );
    is(
        $obj.format_update,
        '19 April, 2018',
        'format_update() on 2018-04-25',
    );
    is( $obj.year_update, '2018', 'year_update() on 2018' );
    is(
        $obj.hex_unixtime,
        '%31%35%32%34%31%32%38%34%30%32',
        'hex_unixtime() on ' ~ %timestamps<2018>,
    );
    is(
        $obj.get_http_response_date,
        'Thu, 19 Apr 2018 09:00:02 GMT',
        'get_http_response_date() with ' ~ %timestamps<2018>,
    );
}, 'Date subtest for 19 April, 2018';

subtest {
    plan 4;
    $obj = Pheix::Datepack.new(
        date => Date.new('2100-05-30').DateTime,
        unixtime => %timestamps<2100>,
    );
    is(
        $obj.format_update,
        '30 May, 2100',
        'format_update() on 2100-05-30',
    );
    is( $obj.year_update, '2100', 'year_update() on 2100' );
    is(
        $obj.hex_unixtime,
        '%34%31%31%35%33%35%30%38%30%32',
        'hex_unixtime() on ' ~ %timestamps<2100>,
    );
    is(
        $obj.get_http_response_date,
        'Sun, 30 May 2100 09:00:02 GMT',
        'get_http_response_date() with ' ~ %timestamps<2100>,
    );
}, 'Date subtest for 30 May, 2100';

subtest {
    plan 4;
    $obj = Pheix::Datepack.new(
        date => Date.new('2019-06-22').DateTime,
        unixtime => %timestamps<2019>,
    );
    is(
        $obj.format_update,
        '22 June, 2019',
        'format_update() on 2019-06-22',
    );
    is( $obj.year_update, '2019', 'year_update() on 2019' );
    is(
        $obj.hex_unixtime,
        '%31%35%36%31%31%39%34%30%30%32',
        'hex_unixtime() on ' ~ %timestamps<2019>,
    );
    is(
        $obj.get_http_response_date,
        'Sat, 22 Jun 2019 09:00:02 GMT',
        'get_http_response_date() with ' ~ %timestamps<2019>,
    );
}, 'Date subtest for 22 June, 2019';

subtest {
    plan 4;
    $obj = Pheix::Datepack.new(
        date => Date.new('2021-07-31').DateTime,
        unixtime => %timestamps<2021>,
    );
    is(
        $obj.format_update,
        '31 July, 2021',
        'format_update() on 2021-07-31',
    );
    is( $obj.year_update, '2021', 'year_update() on 2021' );
    is(
        $obj.hex_unixtime,
        '%31%36%32%37%37%32%32%30%30%32',
        'hex_unixtime() on ' ~ %timestamps<2021>,
    );
    is(
        $obj.get_http_response_date,
        'Sat, 31 Jul 2021 09:00:02 GMT',
        'get_http_response_date() with ' ~ %timestamps<2021>,
    );
}, 'Date subtest for 31 July, 2021';

subtest {
    plan 4;
    $obj = Pheix::Datepack.new(
        date => Date.new('2010-10-28').DateTime,
        unixtime => %timestamps<2010>,
    );
    is(
        $obj.format_update,
        '28 October, 2010',
        'format_update() on 2010-10-28',
    );
    is( $obj.year_update, '2010', 'year_update() on 2010' );
    is(
        $obj.hex_unixtime,
        '%31%32%38%38%32%35%32%38%30%32',
        'hex_unixtime() on ' ~ %timestamps<2010>,
    );
    is(
        $obj.get_http_response_date,
        'Thu, 28 Oct 2010 08:00:02 GMT',
        'get_http_response_date() with ' ~ %timestamps<2010>,
    );
}, 'Date subtest for 28 October, 2010';

subtest {
    plan 4;
    $obj = Pheix::Datepack.new(
        date => Date.new('2210-11-07').DateTime,
        unixtime => %timestamps<2210>,
    );
    is(
        $obj.format_update,
        '7 November, 2210',
        'format_update() on 2210-11-07',
    );
    is(
        $obj.year_update,
        '2210',
        'year_update() on 2210',
    );
    is(
        $obj.hex_unixtime,
        '%37%36%30%30%34%36%37%36%30%32',
        'hex_unixtime() on ' ~ %timestamps<2210>,
    );
    is(
        $obj.get_http_response_date,
        'Wed, 07 Nov 2210 09:00:02 GMT',
        'get_http_response_date() with ' ~ %timestamps<2210>,
    );
}, 'Date subtest for 7 November, 2210';

subtest {
    plan 4;
    $obj = Pheix::Datepack.new(
        date => Date.new('1946-12-01').DateTime,
        unixtime => %timestamps<2000>,
    );
    is(
        $obj.format_update,
        '1 December, 1946',
        'format_update() on 1946-12-01'
    );
    is( $obj.year_update, '1946', 'year_update() on 1946' );
    is(
        $obj.hex_unixtime,
        '%39%37%35%36%36%31%32%30%32',
        'hex_unixtime() on ' ~ %timestamps<2000>,
    );
    is(
        $obj.get_http_response_date,
        'Sun, 01 Dec 1946 09:00:02 GMT',
        'get_http_response_date() with ' ~ %timestamps<2000>,
    );
}, 'Date subtest for 1 December, 1946';

subtest {
    plan 3;
    $obj = Pheix::Datepack.new(
        date => Date.new('0000-01-01').DateTime,
        unixtime => 0,
    );
    is(
        $obj.format_update,
        '1 January, 0',
        'format_update() on 0000-01-01',
    );
    is( $obj.year_update, '0', 'year_update() on 1' );
    is( $obj.hex_unixtime, Str, 'hex_unixtime() on Nil' );
}, 'Date subtest for 0000-01-01';

subtest {
    plan 5;
    $obj = Pheix::Datepack.new(
        date => Date.new('0000-01-01').DateTime,
        unixtime => UInt,
    );
    is( $obj.hex_unixtime, Str, 'hex_unixtime() on Any' );

    $obj = Pheix::Datepack.new(
        date => Date.new('0000-01-01').DateTime,
        unixtime => UInt,
    );
    is( $obj.hex_unixtime, Str, 'hex_unixtime() on -1' );

    $obj = Pheix::Datepack.new(
        date => Date.new('0000-01-01').DateTime,
        unixtime => 0,
    );
    is( $obj.hex_unixtime, Str, 'hex_unixtime() on 0' );

    $obj = Pheix::Datepack.new;
    is(
        $obj.year_update,
        Str,
        'year_update() on not initialized date',
    );
    is(
        $obj.format_update,
        Str,
        'format_update() on not initialized date',
    );
}, 'Date subtest for failures and wrong unexpected values';

subtest {
    $obj = Pheix::Datepack.new(:unixtime(%timestamps<2021>));

    is $obj.get_date_filename, 'dump-Sat-31-Jul-2021-09-00-02.log', 'default file name is ok';
    is $obj.get_date_filename(:extension('bin')), 'dump-Sat-31-Jul-2021-09-00-02.bin', 'binary file name is ok';
    is $obj.get_date_filename(:prefix('file'), :extension('txt')), 'file-Sat-31-Jul-2021-09-00-02.txt', 'text custom file name is ok';

    my DateTime $d = DateTime.now;
    my Str      $f = sprintf("%04d\-%02d\-%02d\-%02d\.log", $d.year, $d.hour, $d.minute, $d.second);

    $obj = Pheix::Datepack.new(:date($d));

    my Str $date_fname = $obj.get_date_filename;

    ok $date_fname ~~ / $f /, sprintf("file name %s contains %s", $date_fname, $f);
    ok $date_fname ~~ /^^ 'dump' /, sprintf("file name %s has prefix <dump>", $date_fname);
}, 'Get filename with date';

subtest {
    my $logdate = Pheix::Datepack.new.get_date_logging;

    ok $logdate ~~ /^ <[0..9]> ** 4 '-' <[0..9]> ** 2 '-' <[0..9]> ** 2 \s <[0..9]> ** 2 ':' <[0..9]> ** 2 ':' <[0..9]> ** 2 '.' <[0..9]> ** 4 $/, 'logging date';
}, 'Check logger date';

done-testing;
