use v6.d;
use Test;
use lib 'lib';

use Pheix::Model::Database::Access;
use Pheix::Controller::Blockchain::Signer;
use Pheix::Test::Blockchain;

my Pheix::Model::Database::Access $poa =
    Pheix::Model::Database::Access.new(
        :table('blockchain'),
        :fields(<id data compression>),
        :test(True),
        :txwaititers(120)
    );

my Pheix::Controller::Blockchain::Signer $signobj =
    Pheix::Controller::Blockchain::Signer.new(
        :signtab('default-local-signer'),
        :test(True),
        :target($poa)
    );

constant tstobj = Pheix::Test::Blockchain.new;

plan 6;

if !tstobj.pte {
    diag('PHEIXTESTENGINE was not set');
    skip-rest('Blockchain test should be run via Pheix test engine');
    exit;
}

if $signobj.selfcheck {
    is $signobj.targetobj.dbswitch, 1, 'target object is ok';
    is $signobj.signerobj.dbswitch, 1, 'signer object is ok';

    my $data = $signobj.signerobj.chainobj.ethobj.marshal('init', {});
    my $raw  = $signobj.sign(:marshalledtx($data));

    ok $raw, 'data is signed ok';

    my $txhash = $signobj.send(:rawtx($raw));

    ok $txhash, 'signed tx is sent ' ~ $txhash;
    ok $signobj.latestgas > 0, 'gas used ' ~ $signobj.latestgas;

    ok $poa.chainobj.wait_for_transactions(:hashes([$txhash])), 'tx is mined';
}
else {
    skip-rest('signer node is not available');
}

done-testing;
