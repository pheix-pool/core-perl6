use v6.d;
use Test;
use lib 'lib';

use experimental :pack;
use OpenSSL::Digest;
use Pheix::Model::JSON;
use Pheix::Test::Blockchain;
use Pheix::Test::Helpers;

constant tstobj = Pheix::Test::Blockchain.new;

plan 14;

use-ok 'Pheix::Model::JSON';

if !Pheix::Test::Helpers.new.check_submodules {
    skip-rest('No Pheix submodules are pulled');
    exit;
}

my Str  $arg    = @*ARGS[0] || ( "0x" ~ q{0} x 40 );
my Any  $obj    = Pheix::Model::JSON.new.set_entire_config(:addon('Pheix'));
my Bool $pubnet = tstobj.testnet ?? True !! False;

# Check set with temparary specifier
subtest {
    plan 4;

    my $jo = Pheix::Model::JSON.new.set_entire_config(:addon('Pheix'));

    my %initial_setup = $jo.get_entire_config(:addon('Pheix'), :nocache(True));

    $jo.set_group_setting('Pheix', 'storage', 'install', 'port', '8808', :temporary(True));
    $jo.set_setting('Pheix', 'servername', 'value', 'https://pheix.org.ru', :temporary(True));
    $jo.set_conf_value('Pheix', 'systemtype', '8808', :temporary(True));

    is(
        $jo.get_group_setting('Pheix', 'storage', 'install', 'port'),
        8808,
        'Temporary group setting',
    );

    is(
        $jo.get_setting('Pheix', 'servername', 'value'),
        'https://pheix.org.ru',
        'Temporary setting',
    );

    is(
        $jo.get_conf_value('Pheix', 'systemtype'),
        8808,
        'Temporary config value',
    );

    #%initial_setup<module><systemtype>.gist.say;
    #%initial_setup<module><configuration><settings><servername>.gist.say;
    #%initial_setup<module><configuration><settings><storage><group><install>.gist.say;

    is-deeply %initial_setup,
        $jo.get_entire_config(:addon('Pheix'), :nocache(True)),
            'Initial and not cached setups';
}, 'Check set with temparary specifier';

# Check get_json_fn method
subtest {
    plan 4;

    is(
        $obj.get_json_fn('Pheix'),
        'conf/config.json',
        'Pheix config file',
    );

    is(
        $obj.get_json_fn('EmbeddedUser'),
        'conf/addons/EmbeddedUser/config.json',
        'Addon config file',
    );

    is(
        $obj.get_json_fn('FooBar'),
        Str,
        'Unknown addon config file',
    );

    is(
        $obj.get_json_fn(q{}),
        Str,
        'Blank addon config file',
    );
}, 'Check get_json_fn method';

# Check blank and populated settings
subtest {
    plan 20;

    my Str $apiurl;

    my Str $add  = 'Pheix';
    my Str $strg = 'storage';
    my Str @tabs = <settings-aggregator-blank settings-aggregator-populated>;

    my @settings_keys = <mock type path strg extn prtl host port qstr hash user pass data sign depl>;

    for @tabs -> $tab {
        my %h = $obj.get_all_settings_for_group_member($add, $strg, $tab);

        if $tab ~~ /blank/ {
            for @settings_keys -> $key {
                ok %h{$key}:exists && %h{$key} eq q{},
                    sprintf("tab %s key %s exists and blank", $tab, $key);
            }
        }
        else {
            my %h_original =
                mock => 'sa',
                type => ~1,
                path => 'conf/system/eth',
                strg => 'PheixDatabase',
                extn => 'abi',
                prtl => 'https://',
                host => 'netname.ghost.pm',
                port => ~18088,
                qstr => '/test/query/string?request',
                hash => '0xf9a13e90d3af48306e32cec1317e9fb162b1b3b06da52dda585edce7ff6ff5aa',
                user => 'usr',
                pass => 'pwd',
                data => 'undefined',
                sign => 'undefined',
                depl => ~1;

            is-deeply %h, %h_original,
                sprintf("tab %s all keys valid", $tab);
        }

        $apiurl = get_apiurl(:sets(%h));

        if $tab ~~ /blank/ {
            is $apiurl, q{:}, 'apiurl for ' ~ $tab;
        }
        else {
            is $apiurl,
                'https://netname.ghost.pm:18088/test/query/string?request',
                    'apiurl for ' ~ $tab;
        }

        %h<qstr>:delete;

        $apiurl = get_apiurl(:sets(%h));;

        if $tab ~~ /blank/ {
            is $apiurl, q{:}, 'apiurl for ' ~ $tab ~ ' and qstr is deleted';
        }
        else {
            is $apiurl,
                'https://netname.ghost.pm:18088',
                    'apiurl for populated ' ~ $tab ~ ' and qstr is deleted';
        }
    }

}, 'Check blank and populated settings';

# Check get_all_settings_for_group_member for addons
subtest {
    plan 5;

    my Str $add  = 'Pheix';
    my Str $strg = 'addons';
    my Str $tab  = 'installed';

    my $h = {
        embedadm => {
            addon => 'Pheix::Addons::Embedded::Admin',
            config => 'conf/addons/custom_path'
        },
        embedusr => {
            addon => 'Pheix::Addons::Embedded::User'
        }
    };

    my %addons =
        $obj.get_all_settings_for_group_member($add, $strg, $tab);

    is-deeply(%addons, $h, 'check installed addons in settings');

    for %addons.kv -> $k, $v {
        next unless $v && $v<addon>;

        my $module = $v<addon>;

        use-ok $module;

        {
            require ::($module);
            is ::($module).new(:confpath($v<config> // 'conf/addons')).get_class, $module, 'call ' ~ $module;
        }
    }
}, 'check get_all_settings_for_group_member for addons';

# Check set_group_setting method
subtest {
    plan 24;
    my Str $currbackup = backup_conf;
    my Str $add        = 'Pheix';
    my Str $strg       = 'storage';
    my Str $tab;
    my %h =
        type => (0 .. 200).rand.floor,
        path => 'conf/config/system',
        strg => 'my-sample-storage',
        extn => 'tnk|txt',
        prtl => 'wss://',
        host => '192.168.1.1',
        port => 8080,
        hash => '0x235ae7b706c35cc2979da3307607c7c6f7a488984e9257ab413db55cab535d56',
        user => 'pheixuser',
        pass => 'mysecretpass',
        data => '0x87322e5f2a74,0x7ed68770e660',
    ;
    $tab  = 'install';
    ok(
        $obj.set_group_setting($add, $strg, $tab, 'type', ~(%h<type>)),
        'set <' ~ %h<type> ~ '> to ' ~ "$add/$strg/$tab/type",
    );
    ok(
        $obj.set_group_setting( $add, $strg, $tab, 'path', %h<path> ),
        'set <' ~ %h<path> ~ '> to ' ~ "$add/$strg/$tab/path",
    );
    ok(
        $obj.set_group_setting( $add, $strg, $tab, 'strg', %h<strg> ),
        'set <' ~ %h<strg> ~ '> to ' ~ "$add/$strg/$tab/strg",
    );
    ok(
        $obj.set_group_setting( $add, $strg, $tab, 'extn', %h<extn> ),
        'set <' ~ %h<extn> ~ '> to ' ~ "$add/$strg/$tab/extn",
    );
    ok(
        $obj.set_group_setting( $add, $strg, $tab, 'prtl', ~(%h<prtl>) ),
        'set <' ~ %h<prtl> ~ '> to ' ~ "$add/$strg/$tab/prtl",
    );
    ok(
        $obj.set_group_setting( $add, $strg, $tab, 'host', %h<host> ),
        'set <' ~ %h<host> ~ '> to ' ~ "$add/$strg/$tab/host",
    );
    ok(
        $obj.set_group_setting( $add, $strg, $tab, 'port', %h<port> ),
        'set <' ~ %h<port> ~ '> to ' ~ "$add/$strg/$tab/port",
    );
    ok(
        $obj.set_group_setting( $add, $strg, $tab, 'hash', %h<hash> ),
        'set <' ~ %h<hash> ~ '> to ' ~ "$add/$strg/$tab/hash",
    );
    ok(
        $obj.set_group_setting( $add, $strg, $tab, 'user', %h<user> ),
        'set <' ~ %h<user> ~ '> to ' ~ "$add/$strg/$tab/user",
    );
    ok(
        $obj.set_group_setting( $add, $strg, $tab, 'pass', %h<pass> ),
        'set <' ~ %h<pass> ~ '> to ' ~ "$add/$strg/$tab/pass",
    );
    ok(
        $obj.set_group_setting( $add, $strg, $tab, 'data', %h<data> ),
        'set <' ~ %h<data> ~ '> to ' ~ "$add/$strg/$tab/data",
    );
    is-deeply(
        $obj.get_all_settings_for_group_member( $add, $strg, $tab ), %h,
        'check ' ~ $tab ~ ' storage'
    );
    $tab = 'bigbro';
    %h =
        type => (0 .. 200).rand.floor,
        path => '/smart-contracts/t/PheixDatabase/',
        strg => 'docker',
        extn => 'dock',
        prtl => 'socket|ipc',
        host => 'localhost@localmachine',
        port => 2790,
        hash => '0x5d2ef6960f2e75ce6f6e2e38b5084da675791f857cd44dd0acc3af4ba49e859c',
        user => 'pheixuser',
        pass => 'qwerty',
        data => '0xa0c4f65d2809e36e37b9dd997405dc27e7ceb66645aca867f6d92838ee8bec6f',
    ;
    ok(
        $obj.set_group_setting( $add, $strg, $tab, 'type', ~(%h<type>) ),
        'set <' ~ %h<type> ~ '> to ' ~ "$add/$strg/$tab/type",
    );
    ok(
        $obj.set_group_setting( $add, $strg, $tab, 'path', %h<path> ),
        'set <' ~ %h<path> ~ '> to ' ~ "$add/$strg/$tab/path",
    );
    ok(
        $obj.set_group_setting( $add, $strg, $tab, 'strg', %h<strg> ),
        'set <' ~ %h<strg> ~ '> to ' ~ "$add/$strg/$tab/strg",
    );
    ok(
        $obj.set_group_setting( $add, $strg, $tab, 'extn', %h<extn> ),
        'set <' ~ %h<extn> ~ '> to ' ~ "$add/$strg/$tab/extn",
    );
    ok(
        $obj.set_group_setting( $add, $strg, $tab, 'prtl', ~(%h<prtl>) ),
        'set <' ~ %h<prtl> ~ '> to ' ~ "$add/$strg/$tab/prtl",
    );
    ok(
        $obj.set_group_setting( $add, $strg, $tab, 'host', %h<host> ),
        'set <' ~ %h<host> ~ '> to ' ~ "$add/$strg/$tab/host",
    );
    ok(
        $obj.set_group_setting( $add, $strg, $tab, 'port', %h<port> ),
        'set <' ~ %h<port> ~ '> to ' ~ "$add/$strg/$tab/port",
    );
    ok(
        $obj.set_group_setting( $add, $strg, $tab, 'hash', %h<hash> ),
        'set <' ~ %h<hash> ~ '> to ' ~ "$add/$strg/$tab/hash",
    );
    ok(
        $obj.set_group_setting( $add, $strg, $tab, 'user', %h<user> ),
        'set <' ~ %h<user> ~ '> to ' ~ "$add/$strg/$tab/user",
    );
    ok(
        $obj.set_group_setting( $add, $strg, $tab, 'pass', %h<pass> ),
        'set <' ~ %h<pass> ~ '> to ' ~ "$add/$strg/$tab/pass",
    );
    ok(
        $obj.set_group_setting( $add, $strg, $tab, 'data', %h<data> ),
        'set <' ~ %h<data> ~ '> to ' ~ "$add/$strg/$tab/data",
    );
    is-deeply(
        $obj.get_all_settings_for_group_member( $add, $strg, $tab ), %h,
        'check ' ~ $tab ~ ' storage'
    );
    backup_conf(restoreconf => $currbackup);
}, 'Check set_group_setting method';

# Check set_setting method
subtest {
    plan 40;
    my Str $currbackup = backup_conf;
    my Str $add        = 'Pheix';
    my @settings = [
        servername   => <label value>,
        workviaproto => <toggle togglelabels label value>,
        locale       => <label value>,
        dynasitemap  => <toggle label value>,
        apopheoznews => <toggle label value>,
        bigbro_user  => <toggle label value>,
        bigbro_admin => <toggle label value>
    ];
    for @settings -> %h {
        my Str $key = ~((%h<>:k)[0]);
        for (%h<>:v)[0] -> $member {
            my $wvalue = md5( (0..1000).rand.Str.encode('UTF8') ).unpack("H*");
            ok(
                $obj.set_setting( $add, $key, $member, $wvalue ),
                'set <' ~ $wvalue ~ '> to ' ~ "$add/$key/$member",
            );
            my $rvalue = $obj.get_setting($add, $key, $member);
            ok(
                ( $rvalue eq $wvalue ),
                'get <' ~ $rvalue ~ '> from ' ~ "$add/$key/$member",
            );
        }
    }
    backup_conf( restoreconf => $currbackup );
}, 'Check set_setting method';

# Check set_conf_value method
subtest {
    plan 18;
    my Str $currbackup = backup_conf;
    my Str $add        = 'Pheix';
    my @confvalues = <
        systemtype name menuitem description version modulepath
        entryscript entryroute staticsitemap>;
    for @confvalues -> $member {
        my $wvalue = md5( (0..1000).rand.Str.encode('UTF8') ).unpack("H*");
        ok(
            $obj.set_conf_value( $add, $member, $wvalue ),
            'set <' ~ $wvalue ~ '> to ' ~ "$add/$member",
        );
        my $rvalue = $obj.get_conf_value($add, $member);
        ok(
            ( $rvalue eq $wvalue ),
            'get <' ~ $rvalue ~ '> from ' ~ "$add/$member",
        );
    }
    backup_conf( restoreconf => $currbackup );
}, 'Check set_conf_value method';

# Check get_conf_value method
subtest {
    plan 7;

    $obj = $obj.set_entire_config(:addon('Pheix'));

    is(
        $obj.get_conf_value('Pheix', 'version'),
        '4.0',
        'Get version from config file',
    );
    is(
        Pheix::Model::JSON.new
            .set_entire_config(:addon('FooBar'))
            .get_conf_value('FooBar', 'version', :nocache(True)),
        Cool,
        'Get version from config file of unknown addon',
    );

    is(
        $obj.get_conf_value('Pheix', 'version_unknown'),
        Cool,
        'Get unknown setting from config file',
    );
    is(
        Pheix::Model::JSON.new
            .set_entire_config(:addon('FooBar'))
            .get_conf_value('FooBar', 'version_unknown', :nocache(True)),
        Cool,
        'Get unknown setting from config file of unknown addon',
    );
    is(
        $obj.get_conf_value('Pheix', q{}),
        Cool,
        'Get blank setting from config file',
    );
    is(
        $obj.get_conf_value('Pheix', 'name'),
        'Setup',
        'Get name from config file',
    );
    is(
        $obj.get_conf_value('Pheix', 'staticsitemap'),
        'sitemap_static.xml',
        'Get staticsitemap from config file',
    );
}, 'Check get_conf_value method';

# Check get_setting method
subtest {
    plan 9;
    is(
        $obj.get_setting('Pheix', 'servername', 'value'),
        'https://pheix.org',
        'Get servername setting value from config file of addon',
    );
    is(
        $obj.get_setting('Pheix', 'servername', 'label'),
        'Pheix works on host',
        'Get servername setting label from config file',
    );
    is(
        Pheix::Model::JSON.new
            .set_entire_config(:addon('FooBar'))
            .get_setting('FooBar', 'servername', 'label', :nocache(True)),
        Cool,
        'Get servername setting label from config ' ~
            'file of unknown addon',
    );
    is(
        Pheix::Model::JSON.new
            .set_entire_config(:addon(q{}))
            .get_setting(q{}, 'servername', 'label'),
        Cool,
        'Get servername setting label from config file of blank addon',
    );
    is(
        $obj.get_setting('Pheix', 'serverpath', 'label'),
        Cool,
        'Get unknown setting label from config file',
    );
    is(
        $obj.get_setting('Pheix', 'serverpath', 'lebal'),
        Cool,
        'Get unknown setting unknown member from config file',
    );
    is(
        $obj.get_setting('Pheix', 'servername', q{}),
        Cool,
        'Get servername setting blank member from config file',
    );
    is(
        $obj.get_setting('Pheix', q{}, 'lebel'),
        Cool,
        'Get blank setting label member from config file',
    );
    is(
        $obj.get_setting('Pheix', q{}, q{}),
        Cool,
        'Get blank setting blank member from config file',
    );
}, 'Check get_setting method';

# Check is_setting_group method
subtest {
    plan 2;
    ok(
        ( $obj.is_setting_group('Pheix', 'workviaproto') == 0 ),
        'Setting is not a group',
    );
    ok(
        ( $obj.is_setting_group('Pheix', 'indexseotags') == 5) ,
        'Get children of indexseotags setting from config file',
    );
}, 'Check is_setting_group method';

# Check get_group_setting method
subtest {
    plan 17;

    # 1
    is(
        $obj.get_group_setting(
            'Pheix',
            'indexseotags',
            'title',
            'value',
        ),
        'Hello, world! I\'m working ;-)',
        'Get indexseotags group title setting value from config file',
    );

    # 2
    is(
        $obj.get_group_setting(
            'Pheix',
            'indexseotags',
            'title',
            'label',
        ),
        'title',
        'Get indexseotags group title setting ' ~
            'label member from config file',
    );

    # 3
    is(
        $obj.get_group_setting(
            'Pheix',
            'indexseotags',
            'metadescr',
            'value',
        ),
        'Welcome to index page',
        'Get indexseotags group metadescr setting ' ~
            'value from config file',
    );

    # 4
    is(
        $obj.get_group_setting(
            'Pheix',
            'indexseotags',
            'metadescr',
            'label',
        ),
        'meta.description',
        'Get indexseotags group metadescr setting '~
            'label member from config file',
    );

    # 5
    is(
        $obj.get_group_setting(
            'Pheix',
            '404seotags',
            'metakeywords',
            'value',
        ),
        '404, page not found, missed page, error 404, error',
        'Get 404seotags group metakeywords setting '~
            'value from config file',
    );

    # 6
    is(
        $obj.get_group_setting(
            'Pheix',
            '404seotags',
            'metakeywords',
            'label',
        ),
        'meta.keywords',
        'Get 404seotags group metakeywords setting '~
            'label member from config file',
    );

    # 7
    is(
        $obj.get_group_setting(
            'EmbeddedUser',
            'filters',
            'filter-03',
            q{},
        ),
        Cool,
        'Get filters group filter-03 setting blank ' ~
            'member from config file of addon',
    );

    # 8
    is(
        $obj.get_group_setting( 'Embedded', 'filters', q{}, q{} ),
        Cool,
        'Get filters group blank setting blank ' ~
            'member from config file of addon',
    );

    # 9
    is(
        $obj.get_group_setting( 'Embedded', q{}, q{}, q{} ),
        Cool,
        'Get blank group blank setting blank ' ~
            'member from config file of addon',
    );

    # 10
    is(
        Pheix::Model::JSON.new
            .set_entire_config(:addon('FooBar'))
            .get_group_setting( 'FooBar', q{}, q{}, q{} ),
        Cool,
        'Get blank group blank setting blank ' ~
            'member from config file of unknown addon',
    );

    # 11
    is(
        $obj.get_group_setting( 'Pheix', q{}, q{}, 'label' ),
        Cool,
        'Get blank group blank setting label ' ~
            'member from config file',
    );

    # 12
    is(
        $obj.get_group_setting( 'Pheix', q{}, q{}, q{} ),
        Cool,
        'Get blank group blank setting blank ' ~
            'member from config file',
    );

    # 13
    is(
        $obj.get_group_setting( 'Pheix', 'indexseotags', q{}, q{} ),
        Cool,
        'Get indexseotags group blank setting blank ' ~
            'member from config file',
    );

    # 14
    is(
        $obj.get_group_setting( 'Pheix', q{}, 'title', q{} ),
        Cool,
        'Get blank group title setting blank ' ~
            'member from config file',
    );

    # 15
    is(
        $obj.get_group_setting(
            'Pheix',
            'indexseotags',
            'title',
            'foobar',
        ),
        Cool,
        'Get indexseotags group title setting unknown ' ~
            'member from config file',
    );

    # 16
    is(
        $obj.get_group_setting(
            'Pheix',
            'indexseotags',
            'foobar',
            'value',
        ),
        Cool,
        'Get indexseotags group unknown setting ' ~
            'value member from config file',
    );

    # 17
    is(
        $obj.get_group_setting(
            'Pheix',
            'foobar',
            'title',
            'value',
        ),
        Cool,
        'Get unknown group title setting ' ~
            'value member from config file',
    );
}, 'Check get_group_setting method';

# Check get_all_settings_for_group_member method
subtest {
    plan 8;
    my Str $add  = 'Pheix';
    my Str $strg = 'storage';
    my Str $tab;
    my %h;
    my %t_h1 =
        type => '0',
        path => 'conf/system',
        strg => q{},
        extn => 'tnk',
        prtl => 'https://',
        host => '127.0.0.1',
        port => '3306',
        hash => '0x0',
        user => q{},
        pass => q{},
        data => 'undefined',
    ;
    my %t_h2 =
        type => '0',
        path => 'conf/system',
        strg => q{},
        extn => 'tnk',
        prtl => 'socket',
        host => '127.0.0.1',
        port => '3306',
        hash => '0x0',
        user => q{},
        pass => q{},
        data => 'undefined',
    ;
    my %t_h3 =
        type => '1',
        path => 'conf/system/eth',
        strg => 'PheixDatabase',
        extn => 'abi',
        prtl => 'http://',
        host => '127.0.0.1',
        port => '8541',
        hash => q{},
        user => q{},
        pass => 'node1',
        data => 'undefined',
        sign => q{},
        conf => {
            legacy => False,
            signatures => True,
            privatekey => "ffb710adfca1342e24e65766603dc0a176f4f3e003d035628b0318858b1e69a8",
            compression => 'bzip2',
            explorer => {
                abi => "conf/system/eth/txexplorer/PheixDatabase.abi"
            }
        }
    ;
    my %t_h4 =
        type => '1',
        path => 'conf/system/eth',
        strg => 'PheixDatabase',
        extn => 'abi',
        prtl => 'http://',
        host => '127.0.0.1',
        port => '8541',
        hash => q{},
        user => q{},
        pass => 'node1',
        data => 'undefined',
        sign => 'default-local-signer',
    ;
    my %t_h5 =
        type => '1',
        path => 'conf/system/eth',
        strg => 'PheixDatabase',
        extn => 'abi',
        prtl => 'https://',
        host => '127.0.0.1',
        port => '40881',
        hash => '0x62483fb6abd4a13be7dedc196115ef132f5d42eda0611070745bc1e909c6da47',
        user => '0xcdf4e0481e796afae76a9e4c537d4b895925b0cc',
        pass => 'node-goerli',
        data => 'undefined',
        sign => q{},
    ;
    my %t_h6 =
        type => '1982',
        path => 'conf/system',
        strg => q{},
        extn => 'tnk',
        prtl => 'http://',
        host => '78.47.192.226',
        port => '80',
        hash => '0x0956d2fbd5d5c29844a4d21ed2f76e0c',
        user => q{},
        pass => q{},
        data => 'Lorem ipsum',
    ;
    $tab = 'install';
    is-deeply(
        $obj.get_all_settings_for_group_member($add, $strg, $tab, :nocache(True)), %t_h1,
        'check ' ~ $tab ~ ' storage'
    );
    $tab = 'bigbro';
    is-deeply(
        $obj.get_all_settings_for_group_member($add, $strg, $tab, :nocache(True)), %t_h2,
        'check ' ~ $tab ~ ' storage'
    );

    $tab     = 'blockchain';
    %h       = $obj.get_all_settings_for_group_member($add, $strg, $tab);
    %h<hash> = q{};

    is-deeply %h, %t_h4, 'check ' ~ $tab ~ ' storage';

    $tab     = 'tst_table';
    %h       = $obj.get_all_settings_for_group_member($add, $strg, $tab);
    %h<hash> = q{};

    is-deeply %h, %t_h3, 'check ' ~ $tab ~ ' storage';

    $tab = 'goerli_local_storage';
    is-deeply(
        $obj.get_all_settings_for_group_member( $add, $strg, $tab ), %t_h5,
        'check ' ~ $tab ~ ' storage'
    );
    $tab = 'teststorage';
    is-deeply(
        $obj.get_all_settings_for_group_member( $add, $strg, $tab ), %t_h6,
        'check ' ~ $tab ~ ' storage'
    );
    $tab = 'fake';
    is(
        $obj.get_all_settings_for_group_member( $add, $strg, $tab ), Empty,
        'check ' ~ $tab ~ ' storage'
    );
    $tab = q{};
    is(
        $obj.get_all_settings_for_group_member( $add, $strg, $tab ), Empty,
        'check empty storage param'
    );
}, 'Check get_all_settings_for_group_member method';

# Update smart contract addr for tst_table and blockchain storages
subtest {
    plan 4 - (2 * $pubnet.Int);

    my     @tab  = <tst_table blockchain>;
    my Str $add  = 'Pheix';
    my Str $strg = 'storage';
    my Str $scaddr = $arg unless ( $arg ~~ m:i/^ 0x<[0]>**40 $/ );
    my %h =
        type => '1',
        path => 'conf/system/eth',
        strg => 'PheixDatabase',
        extn => 'abi',
        prtl => 'http://',
        host => '127.0.0.1',
        port => '8541',
        hash => $scaddr // q{},
        user => q{},
        pass => 'node1',
        data => 'undefined',
        sign => q{}
    ;

    for @tab -> $t {
        %h<conf>:delete if %h<conf>;

        if !$pubnet {
            if $scaddr {
                ok(
                    $obj.set_group_setting(
                        $add,
                        $strg,
                        $t,
                        'hash',
                        $scaddr
                    ),
                    'set <' ~ $scaddr ~ '> to ' ~ "$add/$strg/$t/hash",
                );
            }
            else {
                skip 'smart contract addr is missed', 1;
            }
        }

        my %s    = $obj.get_all_settings_for_group_member($add, $strg, $t);
        %s<hash> = q{} if !$scaddr;

        %h<sign> = 'default-local-signer' if $t eq 'blockchain';
        %h<conf> = {
            legacy => False,
            signatures  => True,
            privatekey  => "ffb710adfca1342e24e65766603dc0a176f4f3e003d035628b0318858b1e69a8",
            compression => 'bzip2',
            explorer => {
                abi => "conf/system/eth/txexplorer/PheixDatabase.abi"
            }
        } if $t eq 'tst_table';

        is-deeply %s, %h, 'check blockchain storage <' ~ $t ~ '> after write';
    }
}, 'Update smart contract addr for tst_table and blockchain storages';

done-testing;

sub backup_conf( Str :$restoreconf? ) returns Str {
    my Str $ret;
    if $restoreconf.defined && ( $restoreconf ne q{} ) {
        $obj.get_json_fn( 'Pheix' ).IO.spurt( $restoreconf );
    }
    else {
        $ret = $obj.get_json_fn( 'Pheix' ).IO.slurp;
    }
    $ret;
}

sub get_apiurl(:%sets) returns Str {
    my Str $qstr = (%sets<qstr>:exists) ?? %sets<qstr> !! q{};

    return sprintf("%s%s:%s%s", %sets<prtl>, %sets<host>, %sets<port>, $qstr);
}
