<TABLE WIDTH="100%" BORDER=0 CELLPADDING=7 CELLSPACING=0>
	<TR>
		<TD  class=standart_text>
			<h3>Перекрытия из ферм</h3>

<p>Ниже перечислены типы строений и конструкций, в покупке информации о которых мы <u>ЗАИНТЕРЕСОВАНЫ</u>. Если Вы владеете таковой информацией <a href="contact.html" class=main_menu>свяжитесь с нами</a>!</p>

<p align=center><IMG SRC="images/prometal/buynow/otherfactories/photo00.jpg" width="450" border=0 alt=""></p>

<p>&nbsp;&nbsp;&nbsp;&nbsp;Сварные металлические фермы имеют преимущества перед массивными <a href="library_1331730353.html" class=main_menu>стальными балками и швеллерами</a>. В частности, меньший объем металла и, следовательно, меньший вес.</p> 

<table border="0" cellpadding="0" cellspacing="10" width="400" align=center>
  <tr>
    <td><a rel="lightbox" href="images/prometal/buynow/otherfactories/photo01.jpg" target=_blank><IMG SRC="images/prometal/buynow/small/photo46.jpg" border=0 WIDTH=200 ALT="Нажмите на фотографию для увеличения"></a></td>
    <td><a rel="lightbox" href="images/prometal/buynow/otherfactories/photo02.jpg" target=_blank><IMG SRC="images/prometal/buynow/small/photo47.jpg" border=0 WIDTH=200 ALT="Нажмите на фотографию для увеличения"></a></td>
  </tr>
  <tr>
    <td><a rel="lightbox" href="images/prometal/buynow/otherfactories/photo03.jpg" target=_blank><IMG SRC="images/prometal/buynow/small/photo48.jpg" border=0 WIDTH=200 ALT="Нажмите на фотографию для увеличения"></a></td>
    <td><a rel="lightbox" href="images/prometal/buynow/otherfactories/photo04.jpg" target=_blank><IMG SRC="images/prometal/buynow/small/photo49.jpg" border=0 WIDTH=200 ALT="Нажмите на фотографию для увеличения"></a></td>
  </tr>
  <tr>
    <td><a rel="lightbox" href="images/prometal/buynow/otherfactories/photo06.jpg" target=_blank><IMG SRC="images/prometal/buynow/small/photo51.jpg" border=0 WIDTH=200 ALT="Нажмите на фотографию для увеличения"></a></td>
    <td><a rel="lightbox" href="images/prometal/buynow/otherfactories/photo08.jpg" target=_blank><IMG SRC="images/prometal/buynow/small/photo53.jpg" border=0 WIDTH=200 ALT="Нажмите на фотографию для увеличения"></a></td>
  </tr>
  <tr>
    <td><a rel="lightbox" href="images/prometal/buynow/otherfactories/photo09.jpg" target=_blank><IMG SRC="images/prometal/buynow/small/photo54.jpg" border=0 WIDTH=200 ALT="Нажмите на фотографию для увеличения"></a></td>
    <td><a rel="lightbox" href="images/prometal/buynow/otherfactories/photo10.jpg" target=_blank><IMG SRC="images/prometal/buynow/small/photo55.jpg" border=0 WIDTH=200 ALT="Нажмите на фотографию для увеличения"></a></td>
  </tr>
</table>

<p>&nbsp;&nbsp;&nbsp;&nbsp;Сортовой металлопрокат (уголок, швеллер, балка) пластичен и склонен к деформациям, в то время как <a href="library_1331664443.html" class=main_menu>стальные фермы</a>, выполненные из легкого металлопроката (например, уголка), значительно прочнее при меньшем весе и не подвержены изгибу и деформациям в сравнении с цельной металлической балкой.</p>

<table border="0" cellpadding="0" cellspacing="10" width="400" align=center>
  <tr>
    <td><a rel="lightbox" href="images/prometal/buynow/otherfactories/photo11.jpg" target=_blank><IMG SRC="images/prometal/buynow/small/photo56.jpg" border=0 WIDTH=200 ALT="Нажмите на фотографию для увеличения"></a></td>
    <td><a rel="lightbox" href="images/prometal/buynow/otherfactories/photo12.jpg" target=_blank><IMG SRC="images/prometal/buynow/small/photo57.jpg" border=0 WIDTH=200 ALT="Нажмите на фотографию для увеличения"></a></td>
  </tr>
  <tr>
    <td><a rel="lightbox" href="images/prometal/buynow/otherfactories/photo13.jpg" target=_blank><IMG SRC="images/prometal/buynow/small/photo58.jpg" border=0 WIDTH=200 ALT="Нажмите на фотографию для увеличения"></a></td>
    <td><a rel="lightbox" href="images/prometal/buynow/otherfactories/photo14.jpg" target=_blank><IMG SRC="images/prometal/buynow/small/photo59.jpg" border=0 WIDTH=200 ALT="Нажмите на фотографию для увеличения"></a></td>
  </tr>
  <tr>
    <td align=right><a rel="lightbox" href="images/prometal/buynow/otherfactories/photo05.jpg" target=_blank><IMG SRC="images/prometal/buynow/small/photo50.jpg" border=0 WIDTH=150 ALT="Нажмите на фотографию для увеличения"></a></td>
    <td><a rel="lightbox" href="images/prometal/buynow/otherfactories/photo07.jpg" target=_blank><IMG SRC="images/prometal/buynow/small/photo52.jpg" border=0 WIDTH=150 ALT="Нажмите на фотографию для увеличения"></a></td>
  </tr>
</table>

<p>&nbsp;&nbsp;&nbsp;&nbsp;Некоторые фотографии, представленные выше сделаны в Саратовской области на заброшенном заводе ЖБИ в Пугачеве:</p>
<center><iframe width="450" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.ru/maps/ms?msa=0&amp;msid=217915909716040642474.0004be90b4c9c72a0b8d3&amp;hl=ru&amp;ie=UTF8&amp;t=h&amp;ll=52.088789,48.812256&amp;spn=0.147666,0.291824&amp;z=11&amp;iwloc=0004bf1efc56b70f9bb02&amp;output=embed"></iframe><br /><small>Просмотреть <a href="http://maps.google.ru/maps/ms?msa=0&amp;msid=217915909716040642474.0004be90b4c9c72a0b8d3&amp;hl=ru&amp;ie=UTF8&amp;t=h&amp;ll=52.088789,48.812256&amp;spn=0.147666,0.291824&amp;z=11&amp;iwloc=0004bf1efc56b70f9bb02&amp;source=embed" class=main_menu>Фермы и металлоконструкции</a> на карте большего размера</small></center>

<p align=center>Если Вы владеете информацией о перечисленных типах<br>строений и конструкций - <a href="contact.html" class=main_menu>свяжитесь с нами</a>!</p>
<br><br>			
		</TD>
	</TR>
</TABLE>