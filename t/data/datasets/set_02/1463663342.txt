<div class="slider">  
    <div class="camera_wrap">
        <div data-src="images/castiglion/picture1.jpg"><div class="camera-caption fadeIn"><p class="title" style="margin-top:90px;">Выполнение</p><p class="description">государственных заказов</p><a href="object-3.html" class="btn-default btn1">подробнее</a></div></div>
        <div data-src="images/castiglion/picture2.jpg"><div class="camera-caption fadeIn"><p class="title" style="margin-top:90px;">Строительство</p><p class="description">объектов федерального значения</p><a href="object-1.html" class="btn-default btn1">подробнее</a></div></div>
        <div data-src="images/castiglion/picture3.jpg"><div class="camera-caption fadeIn"><p class="title" style="margin-top:90px;">Стройплощадки</p><p class="description">в крупнейших регионах РФ</p><a href="object-4.html" class="btn-default btn1">подробнее</a></div></div>
    </div>
</div>
<!--content-->
<div class="content"> 
    <div class="thumb-box1">
        <div class="container">
            <h3 class="wow fadeIn">На строящихся объектах</h3>
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 wow fadeInUp">
                    <div class="thumb-pad1">
                        <div class="thumbnail">
                            <figure><img src="images/castiglion/page1_pic1.jpg" alt=""></figure>
                            <div class="caption">
                                <p class="title">Загорская АЭС</p>
                                <p>Загорская ГАЭС конструктивно разделяется на две очереди — собственно Загорскую ГАЭС (первая очередь) и строящуюся Загорскую ГАЭС-2 (вторая очередь).</p>
                                <a href="object-0.html" class="btn-default btn2">Далее</a>
                            </div>  
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 wow fadeInUp" data-wow-delay="0.1s">
                    <div class="thumb-pad1">
                        <div class="thumbnail">
                            <figure><img src="images/castiglion/page1_pic2.jpg" alt=""></figure>
                            <div class="caption">
                                <p class="title">ЦПК</p>
                                <p>В гидролаборатории ЦПК проводится отработка действий в условиях невесомости открытого космоса на полноразмерном макете орбитальной станции МКС.</p>
                                <a href="object-1.html" class="btn-default btn2">Далее</a>
                            </div>  
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 wow fadeInUp" data-wow-delay="0.2s">
                    <div class="thumb-pad1">
                        <div class="thumbnail">
                            <figure><img src="images/castiglion/page1_pic3.jpg" alt=""></figure>
                            <div class="caption">
                                <p class="title">Щепкина, 42</p>
                                <p>Выполнение строительных работ на территории Федерального космического агентства Роскосмос - ул. Щепкина, д. 42. </p>
                                <a href="object-2.html" class="btn-default btn2">Далее</a>
                            </div>  
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 wow fadeInUp" data-wow-delay="0.3s">
                    <div class="thumb-pad1">
                        <div class="thumbnail">
                            <figure><img src="images/castiglion/page1_pic4.jpg" alt=""></figure>
                            <div class="caption">
                                <p class="title">Красная площадь</p>
                                <p>Красная площадь дом 5 - Комплекс зданий музеев московского Кремля</p>
                                <a href="object-3.html" class="btn-default btn2">Далее</a>
                            </div>  
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="thumb-box2">
        <div class="container">
            <p class="title wow fadeInUp"><span>Почему выбирают нас</span></p>
            <div class="row">
                <div class="col-lg-8 col-md-10 col-lg-offset-2 col-md-offset-1 wow fadeInUp">
                    <div class="list_carousel1 responsive clearfix ">
                        <ul id="foo1">
                            <li>
                                <figure><img src="images/castiglion/page1_pic5.jpg" alt=""></figure>
                                <p>Практический опыт, подкрепленный знанием европейских стандартов и культуры строительства, успешно применяется нами в России. Высокое качество обеспечено несколькими факторами: квалификацией специалистов и современным оборудованием, используемым на стройплощадке.</p>
                            </li>
                            <li>
                                <figure><img src="images/castiglion/page1_pic6.jpg" alt=""></figure>
                                <p>Качество строительного материала – ключевой фактор, определяющий прочность, долговечность и комфортность объекта. Реализовывая проекты, мы используем передовые архитектурные концепции и опыт ведущих стран Стандарты качества компании соответствуют мировым.</p>
                            </li>
                        </ul>
                    </div>
                    <div class="foo-btn clearfix">
                        <div class="pagination" id="foo2_pag"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="thumb-box3">
        <div class="container">
            <p class="title wow fadeIn"><span>Миссия</span></p>
            <div class="row">
                <div class="col-lg-10 col-md-10 col-lg-offset-1 col-md-offset-1">
                    <figure class="wow fadeInUp"><img src="images/castiglion/quote_icon.png" alt=""></figure>
                    <p class="wow fadeInUp" data-wow-delay="0.1s">Наша миссия – предоставление услуг в области строительства с целью удовлетворения самого взыскательного Заказчика. Поэтому в своей деятельности компания ориентирована на использование современных материалов и оборудования. Построенные нами здания отвечают необходимым техническим требованиям, отличаются надежностью и быстрыми сроками возведения.</p>
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-lg-offset-4 col-md-offset-4 wow fadeInUp" data-wow-delay="0.2s">
                            <strong><span>А. Гамм, директор по строительству</span></strong>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
     </div>
                </div>
                <div class="col-lg-2 col-md-3 col-sm-4 col-xs-6 wow fadeInUp" data-wow-delay="0.3s">
                    <figure><img src="images/castiglion/icon6.png" alt=""></figure>
                    <div class="extra-wrap">
                        <p class="title">doreel</p>
                        <p>Excepteur sint</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<section class="content_map">
      <div class="google-map-api"> 
        <div id="map-canvas" class="gmap"></div> 
      </div> 
</section>