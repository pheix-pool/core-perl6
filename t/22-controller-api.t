use v6.d;
use Test;
use Test::Mock;
use lib 'lib';

use MIME::Base64;

use Pheix::Utils;
use Pheix::Test::Helpers;
use Pheix::Test::FastCGI;
use Pheix::Controller::API;
use Pheix::Controller::Basic;

plan 8;

use-ok 'Pheix::Controller::API';

if !Pheix::Test::Helpers.new.check_submodules {
    skip-rest('No Pheix submodules are pulled');
    exit;
}

my $fcgi = Pheix::Test::FastCGI.new;
my $ctrl = Pheix::Controller::Basic.new(
    :apirobj(Nil),
    :mockedfcgi($fcgi),
    :test(True),
);
my $capi = $ctrl.sharedobj<ctrlapi>;

# Check error method - direct
subtest {
    my @codes  = <400 401 402 403 404 405 413 429>;
    my @routes = <error api-error>;

    plan @routes.elems * @codes.elems;

    for @codes -> $code {
        for @routes -> $r {
            my @tokens;

            my Str $route = sprintf("/%s/%d", $r, $code);

            $ctrl.sharedobj<pageobj>.fill_seodata($code, 'Pheix');

            my %match =
                code       => $code,
                details    => %(path => $route),
                controller => 'A',
                action     => 'B',
                index      => Match.new,
            ;

            my %cntn = $capi.error(
                :%match,
                :$route,
                :tick(1),
                :sharedobj($ctrl.sharedobj),
                :message(sprintf("error %s", $code)),
            );

            my Str $data = './conf/_pages/http-error.txt'.IO.slurp;

            $data ~~ m:g/ 'props.' (<[a..z_]>+) { @tokens.push($0) if $0 }/;

            # update content as: ./conf/_pages/convert-bytes-to-filechain.raku#119
            $data ~~ s:g/^^ <[\s]>+ //;
            $data ~~ s:g/^^ <[\s]>+ $$//;
            $data ~~ s:g/ <[\r\n]>+ //;

            subtest {
                plan 5 + @tokens.elems;

                ok (%cntn<component>:exists) && (%cntn<component> ne q{}), 'component';
                ok (%cntn<tparams>:exists) && (%cntn<tparams> ~~ Hash), 'tparams';

                is(
                    $ctrl.sharedobj<mb64obj>.decode-str(%cntn<component>),
                    $data,
                    'component content'
                );

                for @tokens -> $t {
                    ok (%cntn<tparams>{$t}:exists) && (%cntn<tparams>{$t}), $t;
                }

                is %cntn<tparams><tmpl_httperr_code>, $code, sprintf("error %s handled", $code);
                is %cntn<tparams><tmpl_httperr_text>, $ctrl.sharedobj<rsrcobj>.http_codes{$code}, sprintf("error message for %s", $code);
            }, sprintf("route %s", $route);
        }
    }
}, 'Check error method - direct';

# Check error method - redirect
subtest {
    my @tokens;

    my Str $code  = '404';
    my Str $route = sprintf("/error/%d", $code);

    $ctrl.sharedobj<pageobj>.fill_seodata('404', 'Pheix');

    my %cntn = $capi.error(
        :$route,
        :$code,
        :message('Page is not found'),
        :tick(1),
        :sharedobj($ctrl.sharedobj)
    );

    my Str $data = './conf/_pages/http-error.txt'.IO.slurp;

    $data ~~ m:g/ 'props.' (<[a..z_]>+) { @tokens.push($0) if $0 }/;

    # update content as: ./conf/_pages/convert-bytes-to-filechain.raku#119
    $data ~~ s:g/^^ <[\s]>+ //;
    $data ~~ s:g/^^ <[\s]>+ $$//;
    $data ~~ s:g/ <[\r\n]>+ //;

    plan 5 + @tokens.elems;

    ok (%cntn<component>:exists) && (%cntn<component> ne q{}), 'component';
    ok (%cntn<tparams>:exists) && (%cntn<tparams> ~~ Hash), 'tparams';

    is(
        $ctrl.sharedobj<mb64obj>.decode-str(%cntn<component>),
        $data,
        'component content'
    );

    for @tokens -> $t {
        ok (%cntn<tparams>{$t}:exists) && (%cntn<tparams>{$t}), $t;
    }

    is %cntn<tparams><tmpl_httperr_code>, $code, sprintf("error %s handled", $code);
    is %cntn<tparams><tmpl_httperr_text>, $ctrl.sharedobj<rsrcobj>.http_codes{$code}, sprintf("error message for %s", $code);
}, 'Check error method - redirect';

# Check index method
subtest {
    my @tokens;

    $ctrl.sharedobj<pageobj>.fill_seodata('index', 'Pheix');

    my %cntn = $capi.index(
        :route(q{/} ~ now.Rat ~ '/index'),
        :tick(1),
        :sharedobj($ctrl.sharedobj),
        :match(Hash.new)
    );

    my Str $data = './conf/_pages/index.txt'.IO.slurp;

    $data ~~ m:g/ 'props.' (<[a..z_]>+) { @tokens.push($0) if $0 }/;

    # update content as: ./conf/_pages/convert-to-filechain.bash#23
    $data ~~ s:g/^^ <[\s]>+ //;
    $data ~~ s:g/^^ <[\s]>+ $$//;
    $data ~~ s:g/ <[\r\n]>+ //;

    plan 4 + @tokens.elems;

    ok (%cntn<component>:exists) && (%cntn<component> ne q{}), 'component';
    ok (%cntn<component_render>:exists) && (%cntn<component> ne q{}), 'component_render';
    ok (%cntn<tparams>:exists) && (%cntn<tparams> ~~ Hash), 'tparams';
    is(
        $ctrl.sharedobj<mb64obj>.decode-str(%cntn<component>),
        $data,
        'component content'
    );

    for @tokens -> $t {
        ok (%cntn<tparams>{$t}:exists) && (%cntn<tparams>{$t}), $t;
    }
}, 'Check index method';

# Check captcha method - successful run
subtest {
    plan 4;

    my Str  $encap;
    my UInt $md = 64;

    my $cqry = $ctrl.sharedobj<utilobj>.do_encrypt('0123456789');

    todo 'possibly unpatched MagickWand';
    lives-ok {
        $encap = $capi.captcha(
            :route('/captcha/?' ~ $cqry),
            :tick(1),
            :sharedobj($ctrl.sharedobj),
            :match({query => q{?} ~ $cqry}),
            :credentials({token => $ctrl.sharedobj<pageobj>.sesstoken}),
        );
    }, 'captcha method lives ok';

    if $encap {
        $encap ~~ s/'data:image/png;base64,'//;

        my $captchabuf = MIME::Base64.decode($encap);

        ok $captchabuf.bytes, 'captcha ' ~ $captchabuf.bytes ~ ' bytes';

        my $fh = './t/captchas/test-captcha-2.png'.IO.open;
        my $etalonbuf = $fh.read;
        $fh.close;

        my $delta =
            max($captchabuf.bytes, $etalonbuf.bytes) -
            min($captchabuf.bytes, $etalonbuf.bytes);

        todo 'requires ImageMagick 6.9.9-51 installed';
        ok $delta < $md, 'captcha delta ' ~ $delta ~ ' bytes';

        my Str $fpath = './t/captchas/' ~ time ~ '.png';

        ok $fpath.IO.spurt($captchabuf), 'save to ' ~ $fpath;
    }
    else {
        skip-rest 'captcha method throws exception';
    }
}, 'Check captcha method - successful run';

# Check captcha method - exceptions
subtest {
    plan 4;

    nok $capi.captcha(
        :route('/captcha/dies'),
        :tick(1),
        :sharedobj($ctrl.sharedobj),
        :match({})
    ), 'undef with no token';

    dies-ok {
        $capi.captcha(
            :route('/captcha/dies'),
            :tick(1),
            :sharedobj(Nil),
            :match({})
        )
    }, 'dies with null sharedobj';

    dies-ok {
        $capi.captcha(
            :route('/captcha/dies'),
            :tick(1),
            :sharedobj($ctrl.sharedobj),
            :match({}),
            :credentials({token => $ctrl.sharedobj<pageobj>.sesstoken}),
        )
    }, 'dies with null match';

    dies-ok {
        $capi.captcha(
            :route('/captcha/dies'),
            :tick(1),
            :sharedobj($ctrl.sharedobj),
            :match({query => '?foobar123'}),
            :credentials({token => $ctrl.sharedobj<pageobj>.sesstoken}),
        )
    }, 'dies with wrong query';
}, 'Check captcha method - exceptions';

# Check captcha method - exception on blank payload
subtest {
    plan 1;

    my $cqry     = $ctrl.sharedobj<utilobj>.do_encrypt('0123456789');
    my $mockeduo = mocked(
        Pheix::Utils,
        returning => {
            test => True,
            jsonobj => $ctrl.sharedobj<jsonobj>,
            lzwsz => 97000,
            show_captcha => (Str),
            get_token_by_now => $ctrl.sharedobj<pageobj>.sesstoken,
        }
    ).new;

    my $c = Pheix::Controller::Basic.new(
        :apirobj(Nil),
        :mockedfcgi($fcgi),
        :mockedutils($mockeduo),
        :test(True),
    );

    throws-like {
        $capi.captcha(
            :route('/captcha/?' ~ $cqry),
            :tick(1),
            :sharedobj($c.sharedobj),
            :match({query => q{?} ~ $cqry}),
            :credentials({token => $ctrl.sharedobj<pageobj>.sesstoken}),
        )
    },
    Exception,
    message => /'Captcha generation error'/,
    'captcha method throws exception on blank payload';
}, 'Check captcha method - exception on blank payload';

subtest {
    plan 1;

    ok $ctrl.sharedobj<logrobj>.remove_all, 'drop log database';
}, 'Clear logs';

done-testing;
