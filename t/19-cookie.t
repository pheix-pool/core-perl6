use v6.d;
use Test;
use lib 'lib';

plan 5;

use Pheix::View::Web::Cookie;

my Str $cname = 'cookie-1';
my Str $cval  = 'cookie-1-value' ~ rand;

my $qkie = Pheix::View::Web::Cookie.new(:name($cname), :value($cval));
my $hdat =
    Pheix::Datepack
        .new(:date(DateTime.new($qkie.expires_t)),:unixtime($qkie.expires_t))
        .get_http_response_date;

$hdat ~~ s/(\d\d)\:(\d\d)\:(\d\d)/\\d\\d\\:\\d\\d\\:\\d\\d/;
$hdat ~~ s:g/\,/\\\,/;
$hdat ~~ s:g/\s/\\s/;

# Check cookie fields
subtest {
    plan 8;

    ok $qkie.field(:key('Expires')) ~~ /^<{$hdat}>$/, 'cookie Expires field ok';
    is $qkie.field(:key('Max-Age')), $qkie.expire_calc($qkie.expires_s), 'cookie Max-Age field ok';
    is $qkie.field(:key('Domain')), %*ENV<HTTP_HOST> // Str, 'cookie Domain field ok';
    is $qkie.field(:key('Path')), q{/}, 'cookie Path field ok';
    is $qkie.field(:key('SameSite')), 'Strict', 'cookie SameSite field ok';
    is $qkie.field(:key('Secure')), False, 'cookie SameSite field ok';
    is $qkie.field(:key('HttpOnly')), True, 'cookie HttpOnly field ok';
    is $qkie.field(:key('Fake')), Cool, 'cookie missed field ok';
}, 'Check cookie fields';

# Check default cookie
subtest {
    plan 6;
    my Str $expr = 'Expires\=' ~ $hdat;
    my $hdr_qkie = $qkie.cookie;

    ok $hdr_qkie ~~ /$cname\=$cval\;/, 'cookie: ' ~ $cname ~ "=" ~ $cval;
    ok $hdr_qkie ~~ /<$expr>/, 'found Expires with pattern: ' ~ $expr;
    ok $hdr_qkie ~~ /'Max-Age=3600;'/, 'found Max-Age';
    ok $hdr_qkie ~~ /'Path=/;'/, 'found Path';
    ok $hdr_qkie ~~ /'SameSite=Strict;'/, 'found SameSite';
    ok $hdr_qkie ~~ /'HttpOnly;'/, 'found HttpOnly';
}, 'Check default cookie';

# Check corrupted user cookie
subtest {
    plan 1;
    is(
        $qkie.cookie(
            {
                Expires => q{},
                Max-Age => q{},
                Domain => q{},
                Path => q{},
                SameSite => q{},
                HttpOnly => False,
                Secure => False,
            },
        ),
        $cname ~ q{=} ~ $cval ~ q{;},
        'corrupted cookie is <' ~
            $cname ~ q{=} ~ $cval ~ q{>},
    );
}, 'Check corrupted user cookie';

# Check expire_calc() method
subtest {
    plan 26;
    ok(
        ( $qkie.expire_calc( '' )   == 0 ),
        'Empty string as arg is 0',
    );
    ok( ( $qkie.expire_calc('+1m')  == 60 ), '+1m is 60' );
    ok( ( $qkie.expire_calc('+10m') == 600 ), '+10m is 600' );
    ok( ( $qkie.expire_calc('+30m') == 1800 ), '+30m is 1800' );
    ok( ( $qkie.expire_calc('+1h')  == 3600 ), '+1h is 3600' );
    ok( ( $qkie.expire_calc('+24h') == 86400 ), '+24h is 86400' );
    ok( ( $qkie.expire_calc('+1d')  == 86400 ), '+1d is 86400' );
    ok( ( $qkie.expire_calc('+1M')  == 2592000 ), '+1M is 2592000' );
    ok( ( $qkie.expire_calc('+1y')  == 31536000 ), '+1y is 31536000' );
    ok( ( $qkie.expire_calc('-1m')  == -60 ), '-1m is -60' );
    ok( ( $qkie.expire_calc('-10m') == -600 ), '-10m is -600' );
    ok( ( $qkie.expire_calc('-30m') == -1800 ), '-30m is -1800' );
    ok( ( $qkie.expire_calc('-1h')  == -3600 ), '-1h is -3600' );
    ok( ( $qkie.expire_calc('-24h') == -86400 ), '-24h is -86400' );
    ok( ( $qkie.expire_calc('-1d')  == -86400 ), '-1d is -86400' );
    ok( ( $qkie.expire_calc('-1M')  == -2592000 ), '-1M is -2592000' );
    ok(
        ( $qkie.expire_calc('-1y')  == -31536000 ),
        '-1y is -31536000',
    );
    ok( ( $qkie.expire_calc('now')  == 0 ), 'now is 0' );
    ok( ( $qkie.expire_calc('5m')   == 300 ), '5m is 300' );
    ok( ( $qkie.expire_calc('1d')   == 86400 ), '1d is 86400' );
    ok( ( $qkie.expire_calc('24h')  == 86400 ), '24h is 86400' );
    ok( ( $qkie.expire_calc('1y')   == 31536000 ), '1y is 31536000' );
    ok( ( $qkie.expire_calc('-1')   == -1 ), '-1 is -1' );
    ok(
        ( $qkie.expire_calc('239934')    == 239934 ),
        '239934 is 239934',
    );
    ok(
        ( $qkie.expire_calc('-3993841')  == -3993841 ),
        '-3993841 is -3993841',
    );
    ok ( $qkie.expire_calc('test11212k') == 0 ), '"test11212k" is 0';

}, 'Check expire_calc() method';

# Check user cookie
subtest {
    plan 8;
    my $d = Pheix::Datepack.new(
        date     => Date.new('1985-09-15').DateTime,
        unixtime => 1474372872,
    );
    my $hdat = $d.get_http_response_date;
    my $hdr_qkie = $qkie.cookie(
        %(
            Expires => $hdat,
            Max-Age => '+7d',
            Domain => 'foo.bar.org',
            Path => '/usr/local/bin',
            SameSite => 'Strict',
            HttpOnly => 'True',
            Secure => 'True',
        ),
    );
    ok(
        ( $hdr_qkie ~~ /$cname\=$cval\;/ ),
        'found cookie: ' ~ $cname ~ q{=} ~ $cval,
    );
    ok(
        ( $hdr_qkie ~~ /Expires\=$hdat\;/ ),
        'found Expires=' ~ $hdat ~ q{;},
    );
    ok ( $hdr_qkie ~~ / 'Max-Age=604800;' / ), 'found Max-Age=604800';
    ok(
        ( $hdr_qkie ~~ / 'Domain=foo.bar.org;' / ),
        'found Domain=foo.bar.org',
    );
    ok(
        ( $hdr_qkie ~~ / 'Path=/usr/local/bin;' / ),
        'found Path=/usr/local/bin',
    );
    ok(
        ( $hdr_qkie ~~ / 'SameSite=Strict;' / ),
        'found SameSite=Strict',
    );
    ok( ( $hdr_qkie ~~ / 'HttpOnly;' / ), 'found HttpOnly' );
    ok( ( $hdr_qkie ~~ / 'Secure;' / ), 'found Secure' );
}, 'Check user cookie';

done-testing;
