use v6.d;
use Test;
use lib 'lib';

use Net::Ethereum;
use Pheix::Model::Database::Access;
use Pheix::Test::Blockchain;
use Pheix::Test::BlockchainComp::Helpers;

plan 1;

constant localtab   = 'tst_table';
constant tstobj     = Pheix::Test::Blockchain.new(:locstorage(localtab), :genwords(300));
constant debug      = tstobj.debug;
constant debuglevel = tstobj.debuglevel;

constant testnet =
    ((@*ARGS[0].defined && @*ARGS[0] (elem) <ropsten rinkeby goerli sepolia holesky>) || tstobj.testnet) ??
        @*ARGS[0] // tstobj.testnet !!
            localtab;

constant helpobj = Pheix::Test::BlockchainComp::Helpers.new(
    :testnet(testnet),
    :localtab(localtab),
    :tstobj(tstobj)
);

srand((now.to-posix[0]*100000 % 1000000).Int);

my UInt $tab_size = 15;
my UInt $iter_max = 5;
my UInt $tabs_max = testnet eq localtab ?? 10 !! 5;
my UInt $iter_num = testnet eq localtab ?? (tstobj.statictest ?? $iter_max !! ($iter_max.rand.Int || $iter_max)) !! 1;
my Str  $diagnet  = testnet eq localtab ?? 'local PoA net' !! testnet ~ ' net';
my Str  $tab      = testnet eq localtab ?? localtab !! testnet ~ '_storage';
my      @farr     = <domains ip_addrs browsers resolut pages countries payload>;

my Any $dbobj = Pheix::Model::Database::Access.new(
    :table($tab),
    :fields(@farr),
    :test(True),
    :debug(debug)
);

my @tabsnums = tabnums($iter_num);

if !tstobj.pte {
    diag('PHEIXTESTENGINE was not set');
    skip-rest('Blockchain test should be run via Pheix test engine');
    exit;
}

if $dbobj && $dbobj.dbswitch == 1 {
    $dbobj = helpobj.dbobj_tweak(:$dbobj, :testnet(testnet));

    helpobj.diag(
        :l(tstobj.logwrn),
        :m(sprintf("Run %s tests on %s", (tstobj.statictest ?? 'static' !! 'dynamic'), $diagnet))
    );
}

# Blockchain heavy test
my Bool $subtest = subtest(
    'Blockchain heavy test on ' ~ $diagnet,
    sub {
        my @bctab;

        my $t_per_tab  = 7;
        my $t_per_iter = 3;

        plan @tabsnums.sum*$t_per_tab + $t_per_iter*$iter_num + 2;

        if $dbobj {
            if $dbobj.dbswitch == 1 {
                if $dbobj.chainobj.ethobj.contract_id {
                    is (@tabsnums.sum > 0 && $iter_num > 0), True, tstobj.bm(
                        :ts(sprintf("ht: itrs=%d, tabs=%d", $iter_num, @tabsnums.sum)),
                        :return(True)
                    );

                    for (1..$iter_num) {
                        is $dbobj.chainobj.unlock_account, True,
                            tstobj.bm(:ts('ht: account unlock'), :return(True));

                        my $tabs_on_blockchain = $dbobj.chainobj.count_tables;

                        if $tabs_on_blockchain {
                            is tstobj.drop_all(:dbobj($dbobj)), True,
                                tstobj.bm(
                                    :ts('ht: init drop'),
                                    :return(True)
                                );
                        }
                        else {
                            is $tabs_on_blockchain, 0, tstobj.bm(
                                :ts('ht: empty blockchain'),
                                :return(True)
                            );
                        }

                        my @tables =
                            (1..@tabsnums[($_ - 1)]).map({ 'tst_tab_' ~ $_ });

                        my %database = helpobj.create_database(
                            :dbobj($dbobj),
                            :tabnames(@tables),
                            :fixedrows($tab_size),
                            #:comptype('plain')
                        );

                        is %database<status>, True, tstobj.bm(
                            :ts('ht: create db with ' ~ %database<db>.elems ~ ' tabs'),
                            :return(True)
                        );

                        if !@(%database<db>).elems {
                            skip-rest('empty database at iter ' ~ $_);

                            next;
                        }

                        for @(%database<db>).kv -> $index, $table {
                            is(
                                $dbobj.chainobj.unlock_account, True,
                                tstobj.bm(
                                    :ts('ht: account unlock'),
                                    :return(True)
                                )
                            );

                            @bctab = helpobj.validate_database(:dbobj($dbobj), :t(@tables[$index]));

                            is-deeply $table, @bctab[0], tstobj.bm(
                                :ts('ht: validate'),
                                :t(@tables[$index]),
                                :r($table.elems),
                                :return(True)
                            );

                            my List $tab = tstobj.fast_select_all(
                                :dbobj($dbobj),
                                :t(@tables[$index]),
                                :withcomp(True)
                            );

                            is-deeply $tab, $table, tstobj.bm(
                                :ts('ht: select all memory check'),
                                :t(@tables[$index]),
                                :r($table.elems),
                                :return(True)
                            );

                            my @list = indexes_list(
                                :len(tstobj.statictest ?? ($table.elems/3).Int !! ($table.elems.rand.Int || 1)),
                                :dbsz($table.elems),
                                :debug(debug),
                            );

                            my @upd_tab = remove_from_memtab(
                                :table($table),
                                :indexes(@list),
                            );

                            is(
                                remove_table(
                                    :t(@tables[$index]),
                                    :indexes(@list),
                                    :debug(debug)
                                ), True,
                                tstobj.bm(
                                    :ts('ht: remove rows'),
                                    :t(@tables[$index]),
                                    :r(@list.elems),
                                    :return(True)
                                )
                            );

                            @bctab = helpobj.validate_database(:dbobj($dbobj), :t(@tables[$index]));

                            is-deeply @upd_tab.sort, @bctab[0].sort, tstobj.bm(
                                :ts('ht: post remove'),
                                :t(@tables[$index]),
                                :r(@upd_tab.elems),
                                :return(True)
                            );

                            @upd_tab = helpobj.generate_table(:tsz(@upd_tab.elems));

                            is(
                                set_table(
                                    :t(@tables[$index]),
                                    :table(@upd_tab),
                                    :debug(debug),
                                ), True,
                                tstobj.bm(
                                    :ts('ht: set rows'),
                                    :t(@tables[$index]),
                                    :r(@upd_tab.elems),
                                    :return(True)
                                )
                            );

                            @bctab = helpobj.validate_database(:dbobj($dbobj), :t(@tables[$index]));

                            is-deeply @upd_tab.sort, @bctab[0].sort, tstobj.bm(
                                :ts('ht: post set'),
                                :t(@tables[$index]),
                                :r(@upd_tab.elems),
                                :return(True)
                            );

                            @(%database<db>)[$index] = @bctab[0];
                        }
                    }

                    todo 'possible non-signer mode', 1;
                    ok helpobj.flush_signing_session(:sgnlog($dbobj.chainobj.sgnlog)), 'save signing log';
                }
                else {
                    skip-rest('no target smart contract address');
                }
            }
            else {
                skip-rest('ethereum node is not available');
            }
        }
        else {
           skip-rest('database object is not available');
        }
    }
).Bool;

done-testing;

sub tabnums(UInt $len) returns List {
    my @rc;

    for (1..$len) {
        my $size = tstobj.statictest ??
            $tabs_max !!
                (1..($tabs_max + 1)).rand.Int;

        @rc.push($size);
    };

    return @rc;
}

sub print_tab(:@table) {
    for @table.kv -> $i, $rec {
        (sprintf("%05d", $i) ~ ': ' ~ $rec.substr(0..72) ~ ' ...').say;
    };
}

sub rowids_list(Str :$t, :@indexes, Bool :$debug = False) {
    my @ret;

    for @indexes -> $index {
        my Int $rowid = $dbobj.chainobj.get_id_byindex(
            :t($t),
            :index($index),
        );
        if $rowid > 0 {
            @ret.push( $rowid )
        }
    }

    helpobj.diag(
        :l(tstobj.logdbg),
        :m('remove plan for <' ~ $t ~ '>: ' ~ @ret.elems ~ ' rows, ids=[' ~ @ret.join(q{,}) ~ q{]})
    );

    return @ret;
}

sub indexes_list(UInt :$len, UInt :$dbsz; Bool :$debug) returns List {
    my @ret;

    for (1..$len) -> $i {
        my Int $index = -1;
        my Bool $flag = True;

        while $flag {
            $flag  = False;
            $index = tstobj.statictest ?? $i !! $dbsz.rand.Int;
            for @ret -> $i {
                if $i == $index {
                    $flag = True;
                    last;
                }
            }
        }

        if $index > -1 {
            @ret.push($index);
        }
        else {
            helpobj.diag(:l(tstobj.logerr), :m('while generating index for i=' ~ $i));
        }
    }

    helpobj.diag(
        :l(tstobj.logdbg),
        :m('prepare to remove: ' ~ $len ~ ' elements, indexes=[' ~ @ret.join(q{,}) ~ q{]})
    );

    return @ret;
}

sub remove_from_memtab(:@table, :@indexes) {
    my @updated_db;

    for @table.kv -> $i, $record {
        my Bool $flag = True;

        for @indexes -> $index {
            if ( $index > -1 && $index < @table.elems && $index == $i) {
                $flag = False;
                last;
            }
        }

        if $flag {
            @updated_db.push($record);
        }
    }

    return @updated_db;
}

sub remove_table(Str :$t, :@indexes, Bool :$debug) {
    my @hashes;
    my Bool $ret     = True;
    my UInt $rm_rows = @indexes.elems;

    my @rowids_to_rm = rowids_list(
        :t($t),
        :indexes(@indexes),
        :debug($debug),
    );

    my UInt $rows_before_rm = $dbobj.chainobj.count_rows(:t($t));

    for @rowids_to_rm -> $rowid {
        @hashes.push(
            (
                $dbobj.chainobj.delete(
                    :t($t),
                    :id($rowid),
                    :waittx(False),
                )
            )<txhash>
        );
    }

    my Bool $mined = $dbobj.chainobj.wait_for_transactions(:hashes(@hashes), :attempts(tstobj.waittxiters));
    my Int  $actual_rows = $dbobj.chainobj.count_rows(:t($t));

    if ($actual_rows == ($rows_before_rm - $rm_rows) && $mined) {
        my Bool $chk = True;

        for @rowids_to_rm -> $rowid {
            if $dbobj.chainobj.id_exists(:t($t), :id($rowid)) {
                $chk = False;

                helpobj.diag(:l(tstobj.logerr), :m('remove from <' ~ $t ~ '> fails, found removed id=' ~ $rowid));
            }
        }

        if $chk {
            helpobj.diag(:l(tstobj.logdbg), :m('remove from table <' ~ $t ~ '> ok!'));
        }
        else {
            $ret = False;
        }
    }
    else {
        helpobj.diag(:l(tstobj.logerr), :m('failure while remove from table <' ~ $t ~ '>, count_rows=' ~ $actual_rows ~ ', mined txs=' ~ @hashes.elems) );

        $ret = False;
    }

    return $ret
}

sub set_table(Str :$t!, :@table!, Bool :$debug) {
    my @hashes;
    my Bool $ret = True;
    my UInt $storage_tab_len = $dbobj.chainobj.count_rows(:t($t));

    if $storage_tab_len == @table.elems {
        $dbobj.chainobj.unlock_account;

        for @table.kv -> $i, $record {
            my Int $rowid =
                $dbobj.chainobj.get_id_byindex(:t($t), :index($i));

            if $rowid > 0 {
                @hashes.push(
                    (
                        $dbobj.chainobj.update(
                            :t($t),
                            :id($rowid),
                            :data(helpobj.make_etalon_rec(:array($record.chomp.split(q{|})))),
                            :waittx(False))
                    )<txhash>
                );
            }
        }

        my Bool $mined = $dbobj.chainobj.wait_for_transactions(:hashes(@hashes), :attempts(tstobj.waittxiters));
        my Int  $actual_rows = $dbobj.chainobj.count_rows(:t($t));

        if ($actual_rows != @table.elems || !$mined) {
            helpobj.diag(:l(tstobj.logerr), :m('could not insert to table <' ~ $t ~ '>, count_rows=' ~ $actual_rows ~ q{/} ~ @table.elems) );

            $ret = False;
        }
    }
    else {
        helpobj.diag(:l(tstobj.logerr), :m('tables have diff lengths blockchain_tab=' ~ $storage_tab_len ~ ', memory_tab=' ~ @table.elems));

        $ret = False;
    }

    return $ret;
}

if $dbobj {
    if $dbobj.dbswitch == 1 {
        tstobj.print_statistics if $subtest;
    }
}
