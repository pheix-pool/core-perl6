use v6.d;
use Test;
use lib 'lib';

use URI;
use Crypt::LibGcrypt;
use Crypt::LibGcrypt::Random;
use JSON::Fast;
use Net::Ethereum;
use Pheix::Model::Database::Access;
use Pheix::Model::Database::Blockchain;
use Pheix::Model::Database::Blockchain::SendTx;
use Pheix::Test::Blockchain;

constant iterations   = 10;
#constant storagename = 'alchemy/holesky/v0.6.19';
constant keystorepath = './t/data/keystore/geth';
constant tstobj   = Pheix::Test::Blockchain.new;
constant neteth   = Net::Ethereum.new;
constant testnet  =
    ((@*ARGS[0].defined && @*ARGS[0] (elem) <sepolia holesky>) || tstobj.testnet) ??
        @*ARGS[0] // tstobj.testnet !! Str;

plan 3;

use-ok 'Pheix::Model::Database::Blockchain::SendTx';

subtest {
    plan 7;

    if testnet && testnet.chars {
        my $dbobj = Pheix::Model::Database::Access.new(
            :table(sprintf("%s_storage", testnet)),
#           :table(storagename),
            :fields(List.new),
            :test(True),
            :debug(tstobj.debug),
        );

        if $dbobj {
            if $dbobj.dbswitch == 1 {
                my $captchafile = './t/captchas/test-captcha.png';

                X::AdHoc.new(:payload(sprintf("***ERR: invalid blob captcha %s", $captchafile))).throw unless $captchafile.IO.f;
                X::AdHoc.new(:payload('***ERR: can not continue without signer')).throw unless
                    $dbobj.chainobj.sgnobj && $dbobj.chainobj.sgnobj ~~ Pheix::Model::Database::Blockchain;

                (my $signeracc = $dbobj.chainobj.sgnobj.ethacc) ~~ s:i/^0x//;

                ok $signeracc ~~ m:i/^ <xdigit>**40 $/, sprintf("signer account 0x%s", $signeracc);
                ok keystorepath.IO.d, 'keystore path';

                my $keystore = keystorepath.IO.dir.grep({$_.f && $_.basename ~~ /<{$signeracc}>/}).head;

                ok $keystore, sprintf("keystore %s", $keystore // 'file');

                X::AdHoc.new(:payload(sprintf("***ERR: can not get keystore for %s address from %s", $signeracc, keystorepath))).throw unless $keystore;

                $dbobj.chainobj.sgnobj.config = { keystore => ~$keystore };

                my $uri   = URI.new($dbobj.chainobj.apiurl);
                my $blobs = [$captchafile.IO.slurp(:bin)];

                $dbobj.chainobj.ethobj.keepalive = False;

                my %ret = $dbobj.chainobj.set_blob_data(
                    :$blobs,
                    :data({endpoint => sprintf("%s://%s:%d", $uri.scheme, $uri.host, $uri.port)})
                );

                ok %ret<status>, sprintf("send blob transaction %s", %ret<txhash>);
                is sleep(60), Nil, 'wait 1 min to sync blockchain data';

                my $blob = $dbobj.chainobj.get_blob_data(:trxhash(%ret<txhash>), :hashindex(0));

                ok $blob && $blob.bytes, 'fetch blob data from transaction';
                todo 'possible wrong blob at index 0', 1;
                is-deeply $blob, $blobs.head, 'validate blob data';
            }
            else {
                skip-rest('ethereum node is not available');
            }
        }
        else {
           skip-rest('database object is not available');
        }
    }
    else {
        skip-rest('no testnet given');
    }
}, 'Check blob transaction on public test network';

subtest {
    plan 2 * iterations + (iterations == 1 ?? 3 !! 6);

    my %captchadata;

    my $sendtx = Pheix::Model::Database::Blockchain::SendTx.new;
    my $range  = ($sendtx.eip4844.kzg.constants<BYTES_PER_FIELD_ELEMENT> - 1) * ($sendtx.eip4844.kzg.constants<FIELD_ELEMENTS_PER_BLOB> - $sendtx.eip4844.kzg.constants<BYTES_PER_FIELD_ELEMENT>);

    for ^iterations -> $iter {
        my %kzgdata;
        my $dataset   = sprintf("./t/data/datasets/set_%02d", (1..8).rand.Int);
        my $blobsizes = [0, 0, 0, 0];

        X::AdHoc.new(:payload(sprintf("***ERR: invalid dataset %s", $dataset))).throw unless $dataset.IO.d;

        my @files    = $dataset.IO.dir.map({$_ if $_.f && $_.s <= $range}).sort;
        my $fileblob = @files[(@files.elems - 1).rand.Int];

        X::AdHoc.new(:payload(sprintf("***ERR: invalid blob dataset %s/%s", $dataset, $fileblob.Str))).throw unless $fileblob && $fileblob.f;

        my $captchafile = sprintf("./t/captchas/test-captcha%s.png", $iter % 2 ?? q{} !! '-2');

        X::AdHoc.new(:payload(sprintf("***ERR: invalid blob captcha %s", $captchafile))).throw unless $captchafile.IO.f;

        @$blobsizes.map({
            my $randsize = $range.rand.Int;
            $_ = $randsize <= $range ?? $randsize !! $range;
        });

        my $blobs  = [
            random($blobsizes[0]),
            random($blobsizes[1]),
            random($blobsizes[2]),
            random($blobsizes[3]),
            $captchafile.IO.slurp(:bin),
            $fileblob.slurp(:bin),
        ];

        diag(sprintf("max: %d, captchafile: %s, textfile: %s", $range, $captchafile, $fileblob.Str));

        for $blobs.keys -> $index {
            diag(sprintf("%06d/%06d", $blobs[$index].bytes, $blobsizes[$index] // 0));
        }

        lives-ok { %kzgdata = $sendtx.eip4844.kzg_commitment_data(:$blobs, :fees({baseFeePerBlobGas => 0x1, maxFeePerBlobGas => 0x2})) }, 'retrieve KZG data';
        ok %kzgdata.keys, 'found keys in KZG data';

        %captchadata{$captchafile}<kzgversionedhash>.push(neteth.buf2hex(%kzgdata<kzgversionedhashes>[4]).lc);
        %captchadata{$captchafile}<kzgcommitment>.push(neteth.buf2hex(%kzgdata<kzgcommitments>[4]).lc);
        %captchadata{$captchafile}<kzgproof>.push(neteth.buf2hex(%kzgdata<kzgproofs>[4]).lc);

        diag(serialize_kzg_data(:%kzgdata));
    }

    for %captchadata.kv -> $filename, $data {
        my $versioned_hash = $data<kzgversionedhash>.head;
        my $commitment     = $data<kzgcommitment>.head;
        my $proof          = $data<kzgproof>.head;

        is-deeply $data<kzgversionedhash>.List, $versioned_hash xx $data<kzgversionedhash>.elems, sprintf("versioned hashes for %s", $filename);
        is-deeply $data<kzgcommitment>.List, $commitment xx $data<kzgcommitment>.elems, sprintf("commitments for %s", $filename);
        is-deeply $data<kzgproof>.List, $proof xx $data<kzgproof>.elems, sprintf("proofs for %s", $filename);
    }
}, 'Check KZG data';

done-testing;

sub serialize_kzg_data(:%kzgdata!) returns Str {
    my $serialized;

    for %kzgdata.keys -> $key {
        my $data = %kzgdata{$key};

        if $data ~~ Array {
            for $data.values -> $value {
                if $value ~~ Buf {
                    if $value.bytes == 4096 * 32 {
                        my $blob = neteth.buf2hex($value).lc;
                        $serialized{$key}.push(sprintf("%s...%s", $blob.substr(0, 32), $blob.substr(*-32)));
                    }
                    else {
                        $serialized{$key}.push(neteth.buf2hex($value).lc);
                    }
                }
            }
        }
        else {
            $serialized{$key} = $data;
        }
    }

    return to-json($serialized);
}
