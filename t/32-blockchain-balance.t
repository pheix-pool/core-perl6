use v6.d;
use Test;
use lib 'lib';

use Pheix::Addons::Embedded::Admin;
use Pheix::Addons::Embedded::Admin::Blockchain::Balance;
use Pheix::Controller::Basic;
use Pheix::Model::Database::Access;
use Pheix::Test::FastCGI;
use Pheix::Test::Helpers;

constant tstobj   = Pheix::Test::Blockchain.new(:locstorage('auth-node'), :genwords(200));
constant testport = (@*ARGS[0].defined && @*ARGS[0].Int ~~ 2049..9999) ?? @*ARGS[0] !! 8541;

plan 4;

use-ok 'Pheix::Addons::Embedded::Admin::Blockchain::Balance';

subtest {
    plan 5;

    my $balance = Pheix::Addons::Embedded::Admin::Blockchain::Balance.new;
    my $trxhash = sprintf("0x%s", ('f' xx 64).join);

    nok $balance.transactonlog.elems, 'log is empty';
    ok $balance.push2log(:transaction($trxhash)), 'add a transacton to log';
    is $balance.transactonlog.elems, 1, 'found transacton in log';

    my @logrecord = $balance.transactonlog[0].split(q{|}, :skip-empty);

    (my $created = @logrecord[0]) ~~ s/\s/T/;
    my $hash     = @logrecord[4];

    ok DateTime.now.Instant.UInt - DateTime.new($created).Instant.UInt <= 1, 'timestamp';
    is $hash, $trxhash, 'correct trxhash in log';
}, 'internal logging';

if !tstobj.pte {
    diag('PHEIXTESTENGINE was not set');
    skip-rest('Blockchain test should be run via Pheix test engine');
    exit;
}

my $jsonobj;

my $thlp = Pheix::Test::Helpers.new(:storages(<auth-smart-contract auth-node>));
my $fcgi = Pheix::Test::FastCGI.new;
my $ctrl = Pheix::Controller::Basic.new(
    :apirobj(Nil),
    :mockedfcgi($fcgi),
    :test(True),
);

lives-ok {
    $jsonobj = $thlp.patch_storage_config(
        :addon('EmbeddedAdmin'),
        :addonpath('conf/addons/custom_path'),
        :pairs({port => testport})
    );
}, 'patch ports config for auth gateways';

subtest {
    plan 6;

    my $addon = Pheix::Addons::Embedded::Admin.new(:$ctrl, :$jsonobj);
    my $agw   = $addon.get_authnode.get_authgateway;

    if $agw {
        if $agw.dbswitch {
            my @tokens;

            my $balance = Pheix::Addons::Embedded::Admin::Blockchain::Balance.new;

            my %hdr = $ctrl.sharedobj<headobj>.header(
                %(Status => 200, X-Request-ID => 1),
                List.new
            );

            my @accounts     = $agw.chainobj.ethobj.eth_accounts;
            my %auth_details = $addon.auth_api(
                :match({}),
                :tick(1),
                :sharedobj($ctrl.sharedobj),
                :credentials({login => @accounts.head, password => $agw.chainobj.ethobj.unlockpwd}),
                :header(%hdr),
            );

            ok %auth_details<tparams><pkey> && %auth_details<tparams><pkey> ~~ m:i/^ 0x<xdigit>**64 $/ , 'pkey';
            ok %auth_details<tparams><tx> && %auth_details<tparams><tx> ~~ m:i/^ 0x<xdigit>**64 $/ , 'tx';

            @tokens.push(%auth_details<tparams><tx> // q{});
            @tokens.push($balance.update(:agw($addon.get_authnode.get_authgateway(:token(@tokens.tail))), :token(@tokens.tail)));

            is @tokens.elems, 2, 'found 2 tokens';
            ok @tokens.tail ~~ m:i/^ 0x<xdigit>**64 $/, 'latest token';

            # is $agw.chainobj.ethobj.wait_for_transaction(:hashes([@tokens.tail])).elems, 1, sprintf("transaction %s is mined", @tokens.tail);
            ok $balance.validate(:agw($addon.get_authnode.get_authgateway(:token(@tokens.tail))), :token(@tokens.tail)), 'valid token balance';
            is $balance.transactonlog.elems, 1, 'records in log';
        }
        else {
            skip-rest('ethereum node is not available');
        }
    }
    else {
        skip-rest('database object is not available');
    }
}, 'Check trace transaction method';

done-testing;
