use v6.d;
use Test;
use lib 'lib';

plan 9;

use Pheix::Model::Database::Access;

my  @legend = <id date block data>;
my  $obj    = Pheix::Model::Database::Access.new(
    table   => 'test_container',
    fields  => @legend,
    test    => True
);

my @s = (
    %(
        id    => ~(time),
        date  => Date.today.yyyy-mm-dd,
        block => ~(0),
        data  => 'sample data chain no.1'
    ),
    %(
        id    => ~(time - 10),
        date  => Date.today.earlier(:7days).yyyy-mm-dd,
        block => ~(1),
        data  => 'sample data chain no.2'
    ),
    %(
        id    => ~(time - 20),
        date  => Date.today.earlier(:21days).yyyy-mm-dd,
        block => ~(2),
        data  => 'sample data chain common no.3 + no.4'
    ),
    %(
        id    => ~(time - 30),
        date  => Date.today.earlier(:21days).yyyy-mm-dd,
        block => ~(3),
        data  => 'sample data chain common no.3 + no.4'
    ),
);

# Check constructor
subtest {
    plan 16;
    my $tab     = 'install';
    my @farr    = <id date name config power status>;
    my $dbobj   = Pheix::Model::Database::Access.new(
        table   => $tab,
        fields  => @farr,
        test    => True
    );
    ok $dbobj.table eq $tab, 'table prop for ' ~ $tab ~ ' tab';
    ok $dbobj.dbswitch == 0, 'dbswitch prop for ' ~ $tab ~ ' tab';
    ok $dbobj.test == True, 'test prop for ' ~ $tab ~ ' tab';
    is-deeply $dbobj.fields, @farr, 'fields prop for ' ~ $tab ~ ' tab';
    $tab     = 'bigbro';
    @farr    = <id referer ip useragent resolution page country>;
    $dbobj   = Pheix::Model::Database::Access.new(
        table   => $tab,
        fields  => @farr,
        test    => False
    );
    ok $dbobj.table eq $tab, 'table prop for ' ~ $tab ~ ' tab';
    ok $dbobj.dbswitch == 0, 'dbswitch prop for ' ~ $tab ~ ' tab';
    ok $dbobj.test == False, 'test prop for ' ~ $tab ~ ' tab';
    is-deeply $dbobj.fields, @farr, 'fields prop for ' ~ $tab ~ ' tab';
    $tab     = 'blockchain';
    @farr    = <id data>;
    $dbobj   = Pheix::Model::Database::Access.new(
        table   => $tab,
        fields  => @farr,
        test    => True
    );
    ok $dbobj.table eq $tab, 'table prop for ' ~ $tab ~ ' tab';
    ok(
        $dbobj.dbswitch == 0 || $dbobj.dbswitch == 1,
        'prop dbswitch=' ~ $dbobj.dbswitch ~ ' for ' ~ $tab ~ ' tab'
    );
    ok $dbobj.test == True, 'test prop for ' ~ $tab ~ ' tab';
    is-deeply $dbobj.fields, @farr, 'fields prop for ' ~ $tab ~ ' tab';
    $tab     = 'teststorage';
    @farr    = <lorem ipsum dolor amet consectetur adipisicing eiusmod tempor>;
    $dbobj   = Pheix::Model::Database::Access.new(
        table   => $tab,
        fields  => @farr,
        test    => True
    );
    ok $dbobj.table eq $tab, 'table prop for ' ~ $tab ~ ' tab';
    ok $dbobj.dbswitch == 0, 'dbswitch prop for ' ~ $tab ~ ' tab fallback to 0';
    ok $dbobj.test == True, 'test prop for ' ~ $tab ~ ' tab';
    is-deeply $dbobj.fields, @farr, 'fields prop for ' ~ $tab ~ ' tab';
}, 'Check constructor';

# Check insert method
subtest {
    plan 6;
    insert_values(@s);
    my $_vc = check_file(@s);
    ok($_vc, 'validate content after inserts: insert method');
    ok($obj.remove_all, 'clean up after insert: remove_all');
}, 'Check insert method';

# Check get, get_count and get_all methods
subtest {
    plan 17;
    my @_rows;
    my Int $count;

    insert_values(@s);
    my $_vc = check_file(@s);
    ok(
        $_vc,
        'validate content after inserts: get, get_count, get_all',
    );

    # get method
    @_rows = $obj.get(%(date => @s[2]<date>, data => @s[3]<data>));
    ok(
        (
            @_rows &&
            @_rows[0]<id> eq @s[2]<id>.Str &&
            @_rows[1]<id> eq @s[3]<id>.Str
        ),
        'get valid multiple rows',
    );

    @_rows = $obj.get(
        %(
            id => @s[0]<id>,
            date => @s[0]<date>
        )
    );
    ok(
        (@_rows && @_rows[0]<id> eq @s[0]<id>.Str),
        'get valid Hash no.1',
    );

    @_rows = $obj.get(
        %(
            data => @s[1]<data>,
            date => @s[1]<date>,
            block => @s[1]<block>
        )
    );
    ok(
        (@_rows && @_rows[0]<id> eq @s[1]<id>.Str),
        'get valid Hash no.2',
    );

    @_rows = $obj.get(
        %(
            id => @s[2]<id>,
            data => @s[2]<data>,
            date => @s[2]<date>,
        )
    );
    ok(
        (@_rows && @_rows[0]<id> eq @s[2]<id>.Str),
        'get valid Hash no.3',
    );

    @_rows = $obj.get(%(foo => 'bar'));
    nok(@_rows, 'get invalid Hash no.1');

    @_rows =  $obj.get(%(id => @s[0]<id>, data => @s[1]<data>));
    nok(@_rows, 'get invalid Hash no.2');

    # get_count method
    $count = $obj.get_count(
        %(
            date => @s[2]<date>,
            data => @s[3]<data>,
        )
    );
    ok $count == 2, 'get_count method test no.1';

    $count = $obj.get_count(
        %(
            id => @s[0]<id>,
            data => @s[0]<data>,
        )
    );
    ok $count == 1, 'get_count method test no.2';

    $count = $obj.get_count(%(id => @s[0]<id>, foo => 'bar'));
    ok $count == 0, 'get_count method test no.3';

    # get_all method
    @_rows = $obj.get_all;
    ok @_rows && @_rows.elems == 4, 'get_all method test no.1';
    ok(
        (
            @_rows &&
            @_rows[0]<id> eq @s[0]<id>.Str &&
            @_rows[1]<id> eq @s[1]<id>.Str &&
            @_rows[2]<id> eq @s[2]<id>.Str &&
            @_rows[3]<id> eq @s[3]<id>.Str
        ),
        'get_all method test no.2',
    );

    ok $obj.remove_all, 'clean up after insert: remove_all';
}, 'Check get, get_count and get_all methods';

# Check set method
subtest {
    plan 18;

    my $data;
    my %_h;

    insert_values(@s);
    my $_vc = check_file(@s);
    ok $_vc, 'validate content after inserts: set method';

    # set method
    my @_s;
    for @s {
        my %_h = $_.Hash;
        @_s.push(%_h);
    }

    @_s[0]<block> = 10.Str;
    @_s[0]<data>  = @s[0]<data> ~ q{+} ~ @s[1]<data>;

    $data = {
        block => @_s[0]<block>,
        data  => @_s[0]<data>,
    };

    ok(
        $obj.set(
            $data,
            %(
                id    => @_s[0]<id>,
                date  => @_s[0]<date>,
            ),
        ),
        'set block and data to row with id, date: test no.1',
    );

    $_vc = check_file(@_s);
    ok $_vc, 'validate content after set test no.1';

    @_s[1]<date>  = Date.today.earlier(:30days).yyyy-mm-dd;
    @_s[1]<block> = 20.Str;
    @_s[1]<data>  = @s[1]<data>.flip;

    $data = {
        date  => @_s[1]<date>,
        block => @_s[1]<block>,
        data  => @_s[1]<data>,
    };

    ok(
        $obj.set(
            $data,
            %(
                id    => @_s[1]<id>,
            ),
        ),
        'set block and data to row with id, date: test no.2',
    );

    $_vc = check_file(@_s);
    ok $_vc, 'validate content after set test no.2';

    @_s[2]<date>  =
        @_s[3]<date> = Date.today.earlier(:365days).yyyy-mm-dd;
    @_s[2]<block> = @_s[3]<block> = 666.Str;
    @_s[2]<data>  =
        @_s[3]<data> = (@s[1]<data> ~ q{+} ~ @s[1]<data>).flip;

    $data = {
        date  => @_s[2]<date>,
        block => @_s[2]<block>,
        data  => @_s[2]<data>,
    };

    ok(
        $obj.set(
            $data,
            %(
                data  => @s[3]<data>,
            ),
        ),
        'set block and data to row with id, date: test no.3',
    );

    $_vc = check_file(@_s);
    ok $_vc, 'validate content after set test no.3';
    ok $obj.remove_all, 'clean up after insert: remove_all';

    @_s[0]<date>  = @_s[2]<date>.flip;
    @_s[0]<block> = @_s[2]<block> ~ @_s[0]<block>;
    @_s[0]<data>  = @_s[2]<data>;

    $data = {
        id    => @_s[0]<id>,
        date  => @_s[0]<date>,
        block => @_s[0]<block>,
        data  => @_s[0]<data>,
    };

    ok(
        $obj.set(
            $data,
            %(
                data  => @_s[1]<data>.flip
            ),
        ),
        'set -> insert block with multiple data',
    );

    $data = {
        id    => @_s[1]<id>,
        date  => @_s[1]<date>,
        block => @_s[1]<block>,
        data  => @_s[1]<data>,
    };

    ok $obj.insert($data), 'insert row with id, date, block and data: test no.1';

    $data = {
        id    => @_s[2]<id>,
        date  => @_s[2]<date>,
        block => @_s[2]<block>,
        data  => @_s[2]<data>,
    };

    ok $obj.insert($data), 'insert row with id, date, block and data: test no.2';

    $data = {
        id    => @_s[3]<id>,
        date  => @_s[3]<date>,
        block => @_s[3]<block>,
        data  => @_s[3]<data>,
    };

    ok $obj.insert($data), 'insert row with id, date, block and data: test no.3';

    @_s[0]<data> = (@_s[0]<data>, @_s[1]<data>.flip).Str;
    $_vc = check_file(@_s);
    ok $_vc, 'validate content after set and inserts';
    ok $obj.remove_all, 'clean up after insert: remove_all';
}, 'Check set method';

# Check remove method
subtest {
    plan 20;
    insert_values(@s);
    my $_vc = check_file(@s);
    ok $_vc, 'validate content after inserts: remove method pt.1';

    # remove method
    my @_s;
    for @s {
        @_s.push($_.Hash);
    }

    for ^4 {
        my $i = $_;
        ok(
            $obj.remove(%(id => @s[$i]<id>)),
            'remove row with id: test no.' ~ ($i + 1),
        );
        my @_s1;
        for @_s -> $h {
            my %_h = $h.Hash;
            if %_h<id> ne @s[$i]<id> {
                @_s1.push(%_h);
            }
        }
        @_s = @_s1;
        $_vc = check_file(@_s);
        ok $_vc, 'validate content after remove: test no.' ~ ($i + 1);
    }
    my $_d = 'not unique data';
    @_s = ();
    for @s {
        my %_h = $_.Hash;
        %_h<data> = $_d;
        @_s.push(%_h);
    }
    insert_values(@_s);
    $_vc = check_file(@_s);
    ok $_vc, 'validate content after inserts: remove method pt.2';
    ok $obj.remove(%(data => $_d)), 'remove multiple rows';

    $_vc = check_file;
    ok $_vc, 'validate content after inserts: remove multipe rows';
}, 'Check remove method';

# Check get_fields method
subtest {
    plan 9;
    is-deeply(
        $obj.chainobj.get_fields,
        @legend,
        'fetch fields attr'
    );
    insert_values(@s);
    my $objclone = Pheix::Model::Database::Access.new(
        table   => 'test_container',
        fields  => Nil,
        test    => True
    );
    is-deeply(
        $objclone.chainobj.get_fields,
        @legend,
        'fetch fields from table file'
    );
    ok $obj.remove_all, 'clean up after insert: remove_all';

    $objclone = Pheix::Model::Database::Access.new(
        table   => 'test_container',
        fields  => Nil,
        test    => True
    );

    is $objclone.chainobj.get_fields.elems, 1, 'fetch anon list len';
    is $objclone.chainobj.get_fields[0], 'anonymous', 'fetch anon val';
}, 'Check get_fields method';

# Check anonymous fields
subtest {
    plan 4;
    my Hash $sample = {anonymous => @s[2]<data>};
    my $obj = Pheix::Model::Database::Access.new(
        table   => 'test_container',
        fields  => Nil,
        test    => True
    );
    is-deeply $obj.chainobj.get_fields, ["anonymous"], 'fetch field';
    ok $obj.insert($sample), 'insert value';
    is-deeply $obj.get_all, [$sample], 'get value';
    ok $obj.remove_all, 'clean up';
}, 'Check work with not defined fields';

# Check anonymous access to non-anonymous fields
subtest {
    plan 7;
    insert_values(@s);

    my $cloneobj = Pheix::Model::Database::Access.new(
        table   => 'test_container',
        fields  => Nil,
        test    => True
    );

    is-deeply(
        $cloneobj.chainobj.get_fields,
        @legend,
        'fetch fields with anonymous obj'
    );
    is-deeply $cloneobj.get_all, @s, 'get non-anonymous values';
    ok $obj.remove_all, 'clean up';
}, 'Check work with not defined fields';

# Check table_raw method
subtest {
    plan 8;

    my $obj = Pheix::Model::Database::Access.new(
        table   => 'tst',
        fields  => @legend,
        test    => True
    );

    insert_values(@s, $obj);

    can-ok $obj.chainobj, 'table_raw', 'table_raw method is accessible';
    is-deeply $obj.get_all(:fast(True)), @s, 'fast get_all()';
    is-deeply $obj.chainobj.table_raw, @s, 'chainobj.table_raw()';
    ok $obj.remove_all, 'clean up';

}, 'Check table_raw method';

done-testing;

sub insert_values(
    @array,
    Pheix::Model::Database::Access $db?
) returns Int {
    my Pheix::Model::Database::Access $dbobj = $db // $obj;

    for @array.kv -> $i, $v {
        my $data = {
            id    => $v<id>,
            date  => $v<date>,
            block => $v<block>,
            data  => $v<data>,
        };

        ok $dbobj.insert($data), 'insert id, date, block, data: test no.' ~ ($i + 1);
    }

    return 0;
}

sub check_file returns Int {
    my (@_s) = @_;
    my Str $content  = q{};
    my Str $_content = q{};
    my Int $ret      = 0;
    my $path         = $obj.get_chain_obj.get_path;

    if @_s.elems {
        $content = '# ' ~ @legend.join(q{;}) ~ "\n";
        for @_s {
            $content ~=
                join(q{|}, $_<id>, $_<date>, $_<block>, $_<data>) ~
                    "\n";
        }
    }

    if $path.IO.e {
        $_content = $path.IO.slurp;
    }

    if $_content eq $content {
        $ret = 1;
    }

    $ret;
}
