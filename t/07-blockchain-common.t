use v6.d;
use Test;
use lib 'lib';

use Net::Ethereum;
use Pheix::Model::Database::Access;
use Pheix::Test::Blockchain;
use Pheix::Test::BlockchainComp::Helpers;

plan 8;

constant localtab   = 'tst_table';
constant tstobj     = Pheix::Test::Blockchain.new(:locstorage(localtab), :genwords(200));
constant debug      = tstobj.debug;
constant debuglevel = tstobj.debuglevel;

constant testnet =
    ((@*ARGS[0].defined && @*ARGS[0] (elem) <ropsten rinkeby goerli sepolia holesky>) || tstobj.testnet) ??
        @*ARGS[0] // tstobj.testnet !!
            localtab;

constant helpobj = Pheix::Test::BlockchainComp::Helpers.new(
    :testnet(testnet),
    :localtab(localtab),
    :tstobj(tstobj)
);

constant sgnlogfn = sprintf("%s-sgn-%s.log", $*PROGRAM.basename, DateTime.now(formatter => {
    sprintf(
        "%04d%02d%02d-%02d:%02d:%02u",
        .year,
        .month,
        .day,
        .hour,
        .minute,
        .second.Int
    )
}));

my UInt $startblk = 0;
my UInt $tab_size = 10;
my Str  $diagnet  = testnet eq localtab ?? 'local PoA net' !! testnet ~ ' net';
my Str  $tab      = testnet eq localtab ?? localtab !! testnet ~ '_storage';
my      @farr     = <domains ip_addrs browsers resolut pages countries payload>;

my Any $dbobj = Pheix::Model::Database::Access.new(
    :table($tab),
    :fields(@farr),
    :test(True),
    :debug(debug)
);

if !tstobj.pte {
    diag('PHEIXTESTENGINE was not set');
    skip-rest('Blockchain test should be run via Pheix test engine');
    exit;
}

if $dbobj && $dbobj.dbswitch == 1 {
    $dbobj    = helpobj.dbobj_tweak(:$dbobj, :testnet(testnet));
    $startblk = :16(($dbobj.chainobj.ethobj.eth_getBlockByNumber('latest'))<number>);

    helpobj.diag(
        :l(tstobj.logwrn),
        :m(sprintf("Run %s tests on %s from block %d", (tstobj.statictest ?? 'static' !! 'dynamic'), $diagnet, $startblk))
    );
}

# Drop/Create/Select blockchain test
my Bool $subtest_1 = subtest(
    'Drop/Create/Select blockchain test on ' ~ $diagnet,
    sub {
        plan 10;
        if $dbobj {
            if $dbobj.dbswitch == 1 {
                my %resdb;
                my @tables = <tab01 tab02 tab03 tab04 tab05>;

                is $dbobj.chainobj.get_data_byindex(:t('faketab'),:index(0)), Str,
                    tstobj.bm(:ts('s1: get data from unexisted tab'), :return(True));

                my %thash = helpobj.make_etalon_tab(
                    :t('t'),
                    :dbobj($dbobj),
                    :tsz(1),
                );

                is %thash<status>, True,
                    tstobj.bm(:ts('s1: create sample tab'), :return(True));

                is tstobj.drop_all(:dbobj($dbobj)), True,
                    tstobj.bm(:ts('s1: inital drop'), :return(True));

                %resdb = helpobj.create_database(
                    :dbobj($dbobj),
                    :tabnames(@tables),
                    :fixedrows($tab_size),
                    :comptype('plain')
                );

                is %resdb<status>, True, tstobj.bm(
                    :ts('s1: create db with ' ~ %resdb<db>.elems ~ ' tabs'),
                    :return(True)
                );

                for @tables -> $tabname {
                    my Bool $exst = $dbobj.chainobj.table_exists(:t($tabname));
                    my Int $trows = $dbobj.chainobj.count_rows(:t($tabname));

                    is $exst, True, tstobj.bm(
                        :ts('s1: check'), :t($tabname), :return(True));
                }

                is-deeply
                    helpobj.validate_database(:dbobj($dbobj)),
                        @(%resdb<db>),
                            tstobj.bm(:ts('s1: db checking'), :return(True));
            }
            else {
                skip-rest('ethereum node is not available');
            }
        }
        else {
           skip-rest('database object is not available');
        }
    }
).Bool;

# More than naive blockchain test
my Bool $subtest_2 = subtest(
    'More than naive blockchain test on ' ~ $diagnet,
    sub {
        plan 24;
        if $dbobj {
            if $dbobj.dbswitch == 1 {
                ok(
                    (
                        $dbobj.chainobj.fabi &&
                        $dbobj.chainobj.fabi ne q{} &&
                        $dbobj.chainobj.fabi eq $dbobj.chainobj.get_path
                    ),
                    tstobj.bm(:ts('s2: get_path method'), :return(True))
                );
                my %thash = helpobj.make_etalon_tab(
                    :dbobj($dbobj),
                    :tsz(4)
                );

                is %thash<status>, True, tstobj.bm(
                    :ts('s2: make etalon'),
                    :t($tab),
                    :r(4),
                    :return(True)
                );

                is $dbobj.chainobj.table_exists, True, tstobj.bm(
                    :ts('s2: existance'),
                    :t($tab),
                    :return(True)
                );

                ok $dbobj.chainobj.table_index > -1, tstobj.bm(
                    :ts('s2: index'),
                    :t($tab),
                    :return(True)
                );

                my UInt $k = $dbobj.chainobj.count_tables;

                ok $k == 6, tstobj.bm(
                    :ts('s2: found ' ~ $k ~ ' tabs'),
                    :return(True)
                );

                $k = $dbobj.chainobj.count_rows;

                ok $k > 0, tstobj.bm(
                    :ts('s2: count_rows'),
                    :t($tab),
                    :r($k),
                    :return(True)
                );

                $k = $dbobj.chainobj.get_max_id;

                ok $k > 1, tstobj.bm(
                    :ts('s2: maxid=' ~ $k),
                    :t($tab),
                    :return(True)
                );

                my UInt $rowid = $dbobj.chainobj.get_id_byindex(
                    :index($dbobj.chainobj.count_rows - 1)
                );

                ok ($rowid == ($k - 1) && $rowid > -1), tstobj.bm(
                    :ts('s2: get_id_byindex(count_rows-1)')
                    :t($tab),
                    :r($rowid),
                    :return(True)
                );

                $k = $dbobj.chainobj.count_rows;

                my Bool $comp  = $dbobj.chainobj.is_row_compressed(:id($k));

                my %_row = $dbobj.chainobj.select(:id($k), :comp($comp));

                %_row<payload> = %_row<payload>.encode.bytes;

                ok %_row.pairs == $dbobj.fields.elems, tstobj.bm(
                    :ts('s2: select no.1'),
                    :t($tab),
                    :b(%_row<payload>),
                    :return(True)
                );

                is tstobj.drop_table(:dbobj($dbobj), :t($tab)), True,
                    tstobj.bm(:ts('s2: drop'), :t($tab), :return(True));

                is $dbobj.chainobj.table_exists, False, tstobj.bm(
                    :ts('s2: existance after drop'),
                    :t($tab),
                    :return(True)
                );

                ok $dbobj.chainobj.table_index == -1, tstobj.bm(
                    :ts('s2: index after drop'),
                    :t($tab),
                    :return(True)
                );

                is ($dbobj.chainobj.table_create)<status>, True,
                    tstobj.bm(:ts('s2: create'), :t($tab), :return(True));

                is $dbobj.chainobj.table_exists, True, tstobj.bm(
                    :ts('s2: existance after create'),
                    :t($tab),
                    :return(True)
                );

                $k = $dbobj.chainobj.table_index;

                ok $k > -1, tstobj.bm(
                    :ts('s2: index after create ' ~ $k),
                    :t($tab),
                    :return(True)
                );

                is $dbobj.chainobj.count_tables, 6, tstobj.bm(
                    :ts('s2: count_tables after create'),
                    :return(True)
                );

                my %h = $dbobj.chainobj.row_insert(:data(helpobj.make_etalon_rec));

                is %h<status>, True, tstobj.bm(
                    :ts('s2: insert data into blank tab'),
                    :t($tab),
                    :return(True)
                );

                $k = $dbobj.chainobj.count_rows;

                is $k, 1, tstobj.bm(
                    :ts('s2: rows after insert'),
                    :t($tab),
                    :r($k),
                    :return(True)
                );

                is $dbobj.chainobj.get_max_id, 2, tstobj.bm(
                    :ts('s2: max id after insert'),
                    :t($tab),
                    :return(True)
                );

                %_row = $dbobj.chainobj.select(:id($k));

                %_row<payload> = %_row<payload>.encode.bytes;

                ok %_row.pairs == $dbobj.fields.elems, tstobj.bm(
                    :ts('s2: select no.2'),
                    :t($tab),
                    :b(%_row<payload>),
                    :return(True)
                );

                my %sethash = helpobj.make_etalon_rec;

                is(
                    ($dbobj.chainobj.update(
                        :id($dbobj.chainobj.count_rows),
                        :data(%sethash),
                    ))<status>,
                    True,
                    tstobj.bm(:ts('s2: set data'), :t($tab), :return(True)),
                );

                %_row =
                    $dbobj.chainobj.select(:id($dbobj.chainobj.count_rows));

                is-deeply %sethash, %_row,
                    tstobj.bm(:ts('s2: checking set/get hashes'), :return(True));

                is(
                    ($dbobj.chainobj.delete(
                        :id($dbobj.chainobj.count_rows)
                    ))<status>,
                    True,
                    tstobj.bm(
                        :ts('s2: remove'),
                        :t($tab),
                        :return(True)
                    )
                );

                $k = $dbobj.chainobj.count_rows;

                is $k, 0, tstobj.bm(
                    :ts('s2: rows after remove'),
                    :t($tab),
                    :r($k),
                    :return(True)
                );
            }
            else {
                skip-rest('ethereum node is not available');
            }
        }
        else {
           skip-rest('database object  is not available');
        }
    }
).Bool;

# Init/Drop blockchain test
my Bool $subtest_3 = subtest(
    'Init/Drop blockchain test on ' ~ $diagnet,
    sub {
        plan 10;
        if $dbobj {
            if $dbobj.dbswitch == 1 {
                my %tab = name => 't', rows => 1;

                is tstobj.drop_all(:dbobj($dbobj)), True,
                    tstobj.bm(:ts('s3: initial drop'), :return(True));

                is $dbobj.chainobj.count_tables, 0,
                    tstobj.bm(:ts('s3: drop proved'), :return(True));

                is $dbobj.chainobj.unlock_account, True,
                    tstobj.bm(:ts('s3: account unlock'), :return(True));

                is ($dbobj.chainobj.table_debug)<status>, True,
                    tstobj.bm(:ts('s3: table_debug'), :return(True));

                is tstobj.drop_table(:dbobj($dbobj), :t('table_1')), True,
                    tstobj.bm(:ts('s3: drop'), :t('table_1'), :return(True));

                ok(
                    (
                        $dbobj.chainobj.table_index(:t('table_2')) == 1 &&
                        $dbobj.chainobj.table_index(:t('table_3')) == 0 &&
                        $dbobj.chainobj.count_tables == 2 &&
                        !( $dbobj.chainobj.table_exists(:t('table_1')) )
                    ),
                    tstobj.bm(
                        :ts('s3: forward drop check no.1'),
                        :t('table_1'),
                        :return(True)
                    ),
                );

                is tstobj.drop_table(:dbobj($dbobj), :t('table_2')), True,
                    tstobj.bm(:ts('s3: drop'), :t('table_2'), :return(True));

                ok(
                    (
                        $dbobj.chainobj.table_index(:t('table_3')) == 0 &&
                        $dbobj.chainobj.count_tables == 1 &&
                        !( $dbobj.chainobj.table_exists(:t('table_2')) )
                    ),
                    tstobj.bm(
                        :ts('s3: forward drop check no.2'),
                        :t('table_2'),
                        :return(True)
                    ),
                );

                is tstobj.drop_table(:dbobj($dbobj), :t('table_3')), True,
                    tstobj.bm(:ts('s3: drop'), :t('table_3'), :return(True));

                ok(
                    (
                        $dbobj.chainobj.count_tables == 0 &&
                        !( $dbobj.chainobj.table_exists(:t('table_3')) )
                    ),
                    tstobj.bm(
                        :ts('s3: forward drop check no.3'),
                        :t('table_3'),
                        :return(True)
                    ),
                );
            }
            else {
                skip-rest('ethereum node is not available');
            }
        }
        else {
           skip-rest('database object  is not available');
        }
    }
).Bool;

# Check get_logs and get_modify_time methods
my Bool $subtest_4 = subtest(
    'Check get_logs and get_modify_time methods on ' ~ $diagnet,
    sub {
        plan testnet eq localtab ?? 7 !! 4;
        if $dbobj {
            if $dbobj.dbswitch == 1 {
                is sleep-until(now + 5), True,
                    tstobj.bm(:ts('s4: consume 5 seconds'), :return(True));

                if testnet eq localtab {
                    my $tlog = $dbobj.chainobj.get_logs(:from($startblk));
                    tstobj.bm(:ts('s4: get test logs'));

                    my $flog = $dbobj.chainobj.get_logs;
                    tstobj.bm(:ts('s4: get full logs'));

                    (my $method = $dbobj.chainobj.ethobj.string2hex('*')) ~~ s/^ '0x' //;

                    my $tfunc = sprintf("0x%s%s", $method, q{0} x (64 - $method.chars));
                    my $tcode = sprintf("0x%064x", 8);

                    my $topicslog = $dbobj.chainobj.get_logs(
                        :from($startblk),
                        :address($dbobj.chainobj.scaddr),
                        :topics([$tfunc, $tcode])
                    );

                    tstobj.bm(:ts('s4: get topics logs'));

                    ok $flog.elems, 'full logs are fetched ok';
                    ok $tlog.elems, 'test logs are fetched ok';
                    ok $topicslog.elems, 'topics logs are fetched ok';
                    ok $tlog.elems < $flog.elems, 'full logs are heavier';
                    ok $topicslog.elems < $tlog.elems, 'test logs are heavier than topics logs';
                }
                else {
                    my $cblck = :16(($dbobj.chainobj.ethobj.eth_getBlockByNumber('latest'))<number>);
                    tstobj.bm(:ts('s4: get curr block'));

                    ok $cblck > $startblk, sprintf("current block %d, start block %d", $cblck, $startblk);

                    my $logsz = $dbobj.chainobj.get_logs(:from($startblk)).elems;
                    tstobj.bm(:ts('s4: get test logs'));

                    ok $logsz > 0, 'get test logs, size=' ~ $logsz;
                }

                $dbobj.chainobj.evsign = 'BigBro_access(bytes32,uint256)';

                nok $dbobj.chainobj.get_logs(:from($startblk)).elems,
                    tstobj.bm(:ts('s4: get logs for missed'), :return(True));
            }
            else {
                skip-rest('ethereum node is not available');
            }
        }
        else {
           skip-rest('database object  is not available');
        }
    }
).Bool;

# Methods is_tab/row_compressed() blockchain test
my Bool $subtest_5 = subtest(
    'Methods is_tab/row_compressed() blockchain test on ' ~ $diagnet,
    sub {
        plan 39;
        my %resdb;
        my $frows = 5;
        my @tnms  = <tab_0 tab_1 tab_2>;
        if $dbobj {
            if $dbobj.dbswitch == 1 {
                $dbobj.chainobj.comp = True;

                %resdb = helpobj.create_database(
                    :dbobj($dbobj),
                    :tabnames(@tnms),
                    :fixedrows($frows),
                    :comptype('compressed')
                );

                is %resdb<status>, True, tstobj.bm(
                    :ts('s5: create db with ' ~ %resdb<db>.elems),
                    :return(True)
                );

                for ^3 -> $tinx {
                    my Str $tabname = 'tab_' ~ $tinx;

                    is $dbobj.chainobj.is_tab_compressed(:t($tabname)), True,
                        tstobj.bm(
                            :ts('s5: check comp'),
                            :t($tabname),
                            :return(True)
                        );

                    for ^$frows -> $rinx {
                        my $rowid = $rinx + 1;
                        is(
                            $dbobj.chainobj.is_row_compressed(
                                :t($tabname),
                                :id($rowid)
                            ),
                            True,
                            tstobj.bm(
                                :ts('s5: check row comp'),
                                :t($tabname),
                                :r($rowid),
                                :return(True)
                            ),
                        );
                    }
                }

                is tstobj.drop_all(:dbobj($dbobj)), True,
                    tstobj.bm(:ts('s5: initial drop'), :return(True));

                $dbobj.chainobj.comp = False;

                %resdb = helpobj.create_database(
                    :dbobj($dbobj),
                    :tabnames(@tnms),
                    :fixedrows($frows),
                    :comptype('plain')
                );

                is %resdb<status>, True, tstobj.bm(
                    :ts('s5: create db with ' ~ %resdb<db>.elems ~ ' tabs'),
                    :r(@tnms.elems * $frows),
                    :return(True)
                );

                for ^3 -> $tinx {
                    my Str $tabname = 'tab_' ~ $tinx;
                    is $dbobj.chainobj.is_tab_compressed(:t($tabname)), False,
                        tstobj.bm(:ts('s5: check comp'), :t($tabname), :return(True));

                    for ^$frows -> $rinx {
                        my $rowid = $rinx + 1;
                        is(
                            $dbobj.chainobj.is_row_compressed(
                                :t($tabname),
                                :id($rowid)
                            ),
                            False,
                            tstobj.bm(
                                :ts('s5: check row comp'),
                                :t($tabname),
                                :r($rowid),
                                :return(True)
                            ),
                        );
                    }
                }
            }
            else {
                skip-rest('ethereum node is not available');
            }
        }
        else {
           skip-rest('database object is not available');
        }
    }
).Bool;

# Methods get_table_field() and set_table_field() blockchain test
my Bool $subtest_6 = subtest(
    'Methods get/set_table_field() blockchain test on ' ~ $diagnet,
    sub {
        plan 13;
        my %resdb;
        my $frows = 5;
        my @tnms  = <tab_0 tab_1 tab_2>;
        if $dbobj {
            if $dbobj.dbswitch == 1 {
                is tstobj.drop_all(:dbobj($dbobj)), True,
                    tstobj.bm(:ts('s6: initial drop'), :return(True));

                $dbobj.chainobj.comp = False;

                %resdb = helpobj.create_database(
                    :dbobj($dbobj),
                    :tabnames(@tnms),
                    :fixedrows($frows),
                    :comptype('plain')
                );

                is %resdb<status>, True, tstobj.bm(
                    :ts('s6: create db with ' ~ %resdb<db>.elems ~ ' tabs'),
                    :r(@tnms.elems * $frows),
                    :return(True)
                );

                for @tnms -> $tabname {
                    my @updf = <domain ipv4 useragent screen url location>;
                    is(
                        $dbobj.chainobj.get_table_fields(:t($tabname)),
                        '# ' ~ @farr.join(q{;}),
                        tstobj.bm(
                            :ts('s6: check fields no.1'),
                            :t($tabname),
                            :r($frows),
                            :return(True)
                        )
                    );

                    is(
                        ($dbobj.chainobj.set_table_fields(
                            :t($tabname),
                            :fields(@updf),
                            :waittx(True)
                        ))<status>,
                        True,
                        tstobj.bm(
                            :ts('s6: set fields'),
                            :t($tabname),
                            :r($frows),
                            :return(True)
                        )
                    );

                    is(
                        $dbobj.chainobj.get_table_fields(:t($tabname)),
                        '# ' ~ @updf.join(q{;}),
                        tstobj.bm(
                            :ts('s6: check fields no.2'),
                            :t($tabname),
                            :r($frows),
                            :return(True)
                        ),
                    );
                }

                my $dbobj_ = Pheix::Model::Database::Access.new(
                    :table($tab),
                    :fields(),
                    :test(True),
                    :debug(debug)
                );

                $dbobj_ = helpobj.dbobj_tweak(:dbobj($dbobj_), :testnet(testnet));

                %resdb = helpobj.create_database(
                    :dbobj($dbobj_),
                    :tabnames(@($tab)),
                    :fixedrows($frows),
                    :comptype('plain')
                );

                is %resdb<status>, True, tstobj.bm(
                    :ts('s6: create tab with empty fields'),
                    :t($tab),
                    :r($frows),
                    :return(True)
                );

                is(
                    $dbobj_.chainobj.get_table_fields(:t($tab)),
                    '# id;data;compression',
                    tstobj.bm(
                        :ts('s6: check tab default fields'),
                        :t($tab),
                        :r($frows),
                        :return(True)
                    ),
                );

                $dbobj.chainobj.sgnlog.push($dbobj_.chainobj.sgnlog.Slip);
                $dbobj.chainobj.nonce = $dbobj_.chainobj.nonce;
            }
            else {
                skip-rest('ethereum node is not available');
            }
        }
        else {
           skip-rest('database object is not available');
        }
    }
).Bool;

# Modtime and version blockchain test
my Bool $subtest_7 = subtest(
    'Modtime and version blockchain test on ' ~ $diagnet,
    sub {
        plan 14;
        if $dbobj {
            if $dbobj.dbswitch == 1 {
                my @tabs = <t1 t2>;

                is $dbobj.chainobj.unlock_account, True,
                    tstobj.bm(:ts('s7: account unlock'), :return(True));

                my $tabs_on_blockchain = $dbobj.chainobj.count_tables;
                if $tabs_on_blockchain {
                    is tstobj.drop_all(:dbobj($dbobj)), True,
                        tstobj.bm(:ts('s7: initial drop'), :return(True));
                }
                else {
                    ok $tabs_on_blockchain == 0, 'blockchain is empty';
                }

                my $initmtime = $dbobj.chainobj.get_modify_time.UInt;

                tstobj.bm(:ts('s7: init glob mtime'));

                for @tabs -> $t {
                    helpobj.make_etalon_tab(
                        :t($t),
                        :dbobj($dbobj),
                        :tsz(1)
                    ) && tstobj.bm(:ts('s7: create tab'), :t($t), :r(1));
                }

                ok $dbobj.chainobj.get_modify_time.UInt > $initmtime,
                    tstobj.bm(:ts('s7: last glob mtime'), :return(True));

                my $gmtime = $dbobj.chainobj.get_modify_time.UInt;

                my @mtimes;
                my @umtimes;

                for @tabs -> $t {
                    @mtimes.push($dbobj.chainobj.get_modify_time(:t($t)).UInt);
                }

                is(
                    $dbobj.chainobj.get_modify_time(:t('fake')).UInt, 0,
                    'zero mtime' ~ tstobj.bm(:ts('s7: zero mtime'))
                );

                ok @mtimes.tail > @mtimes.head, @tabs.join(q{,}) ~ ' mtimes';
                ok $gmtime > @mtimes.head, @tabs.head ~ ' mtime < glob mtime';
                is $gmtime, @mtimes.tail, @tabs.tail ~ ' mtime == glob mtime';

                for @tabs -> $t {
                    $dbobj.chainobj.update_tab_mtime(:t($t));
                    tstobj.bm(:ts('s7: update mtime'), :t($t));
                }

                for @tabs -> $t {
                    @umtimes.push($dbobj.chainobj.get_modify_time(:t($t)).UInt);
                }

                $gmtime = $dbobj.chainobj.get_modify_time.UInt;

                ok @umtimes.tail > @umtimes.head, @tabs.join(q{,}) ~ ' updated mtimes';
                ok $gmtime > @umtimes.head, @tabs.head ~ ' updated mtime < glob mtime';
                is $gmtime, @umtimes.tail, @tabs.tail ~ ' updated mtime == glob mtime';

                ok @umtimes.tail > @mtimes.tail, @tabs.tail ~ ' compare mtimes';
                ok @umtimes.head > @mtimes.head, @tabs.head ~ ' compare mtimes';

                my $smver = $dbobj.chainobj.get_smartcontract_ver;

                ok(
                    $smver ~~ /^^ <[0 .. 9]>+ \. <[0 .. 9]>+ \. <[0 .. 9]>+ $$/,
                    'smart contract version ' ~ $smver
                );

                todo 'possible non-signer mode', 1;
                ok helpobj.flush_signing_session(:sgnlog($dbobj.chainobj.sgnlog)), 'save signing log';
            }
            else {
                skip-rest('ethereum node is not available');
            }
        }
        else {
           skip-rest('database object is not available');
        }
    }
).Bool;

# Close keep-alive connection at finalization
my Bool $subtest_8 = subtest(
    'Close keep-alive connection at finalization on ' ~ $diagnet,
    sub {
        plan 1;
        if $dbobj {
            if $dbobj.dbswitch == 1 {
                if (
                    $dbobj.chainobj.ethobj.check_ua_keepalive &&
                    $dbobj.chainobj.ethobj.keepalive
                ) {
                    todo 'connections may be closed by peer already', 1;
                    is $dbobj.chainobj.ethobj.finalize_request, True,
                        tstobj.bm(:ts('fin: close connection'), :return(True));
                } else {
                    skip 'close connection', 1;
                }
            }
            else {
                skip-rest('ethereum node is not available');
            }
        }
        else {
           skip-rest('database object  is not available');
        }
    }
).Bool;

done-testing;

if $dbobj {
    if $dbobj.dbswitch == 1 {
        if  $subtest_1 &&
            $subtest_2 &&
            $subtest_3 &&
            $subtest_4 &&
            $subtest_5 &&
            $subtest_6 &&
            $subtest_7 &&
            $subtest_8
        {
            tstobj.print_statistics;
        }
    }
}
