use v6.d;
use Test;
use lib 'lib';

use Net::Ethereum;
use Pheix::Model::Database::Blockchain::Signatures;

constant privatekey = 'ffb710adfca1342e24e65766603dc0a176f4f3e003d035628b0318858b1e69a8';

plan 2;

use-ok 'Pheix::Model::Database::Blockchain::Signatures';

subtest {
    plan 5;

    my $fixtures = {
        1 => {
            data => 53680828557,
            keccak => 'C6722756599CA35AAF5BF301AF2503C224DAA22B2C0F179B88201DB844F63FAD'
        },
        2 => {
            data => 78395619050,
            keccak => 'F90B4B1F96AE47A5E65F0B4C12FE8EF19AAEDCF632422BD7FE551E74E0BF953E'
        }
    };

    my $sigfactory = Pheix::Model::Database::Blockchain::Signatures.new(
        :privatekey(privatekey),
        :netethobj(Net::Ethereum.new)
    );

    ok $sigfactory, 'signature factory';

    my $msg_1 = sprintf("%d", $fixtures<1><data>);
    my $msg_2 = sprintf("%d", $fixtures<2><data>);

    my $signature_1 = $sigfactory.sign(:message($msg_1));
    my $signature_2 = $sigfactory.sign(:message($msg_2));

    # diag($signature_1);
    # diag($signature_2);

    ok $sigfactory.validate(:message($msg_1),  :signature($signature_1)), sprintf("check message %s and signature %s", $msg_1, $signature_1);
    ok $sigfactory.validate(:message($msg_2),  :signature($signature_2)), sprintf("check message %s and signature %s", $msg_2, $signature_2);
    ok !$sigfactory.validate(:message(~0),     :signature($signature_1)), sprintf("check invalid message %s and signature %s", 0, $signature_1);
    ok !$sigfactory.validate(:message($msg_1), :signature($signature_2)), sprintf("check invalid message %s and signature %s", $msg_1, $signature_2);
}, 'Check signatures';

done-testing;
