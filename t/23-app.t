use v6.d;
use Test;
use lib 'lib';

use JSON::Fast;

use Pheix::App;
use Pheix::Test::Content;
use Pheix::Test::Helpers;
use Pheix::Test::FastCGI;
use Pheix::Model::Route;
use Pheix::Controller::Basic;

my $test = Pheix::Test::Content.new;
my $fcgi = Pheix::Test::FastCGI.new;
my $rapi = Pheix::Model::Route.new(:test(True));

plan 5;

use-ok 'Pheix::App';

if !Pheix::Test::Helpers.new.check_submodules {
    skip-rest('No Pheix submodules are pulled');
    exit;
}

my $ctrl = Pheix::Controller::Basic.new(
    :apirobj(Nil),
    :mockedfcgi($fcgi),
    :test(True),
);

# Check default api route
subtest {
    plan 3;

    my $p = Pheix::App.new(:test(True), :ctrl($ctrl));

    my %r =
        status  => 0,
        msg     => 'bad API request',
        content => "Validation failed for root: Missing required properties: 'credentials', 'method', 'route'",
        request => '{}'
    ;

    is $p.ctrl.sharedobj<fastcgi>.reset, 0, 'reset content array';

    $p.start;
    my ($hdr, $cnt) = $p.ctrl.sharedobj<fastcgi>.fetch.split("\n\n", 2);

    ok $test.checkheader(:header($hdr)), 'HTTP header';
    is-deeply %r, from-json($cnt), 'api default request';
}, 'Check default api route';

# Check exception
subtest {
    plan 3;

    my $p = Pheix::App.new(
        :test(True),
        :ctrl($ctrl),
        :route(
            $rapi.basic_routes_with_handler(
                :handler_class('Pheix::Addons::Embedded::Admin')
            )
        )
    );

    $p.ctrl.sharedobj<fastcgi>.ruri = '/index';
    $p.ctrl.sharedobj<fastcgi>.rmth = 'GET';

    is $p.ctrl.sharedobj<fastcgi>.reset, 0, 'reset content array';

    $p.start;
    my ($hdr, $cnt) = $p.ctrl.sharedobj<fastcgi>.fetch.split("\n\n", 2);

    ok $test.checkheader(:header($hdr), :status(400)), 'HTTP header';
    ok $test.checkcontent(
        :cnt($cnt),
        :params(
            %(
                $p.ctrl.sharedobj<pageobj>.get_pparams,
                $p.ctrl.sharedobj<pageobj>.get_tparams
            )
        )
    ), 'check content: error 400';
}, 'Check exception';

# Check unknown route
subtest {
    plan 3;

    my $p = Pheix::App.new(:test(True), :ctrl($ctrl));

    $p.ctrl.sharedobj<fastcgi>.ruri = '/unknown/route';
    $p.ctrl.sharedobj<fastcgi>.rmth = 'GET';

    is $p.ctrl.sharedobj<fastcgi>.reset, 0, 'reset content array';

    $p.start;
    my ($hdr, $cnt) = $p.ctrl.sharedobj<fastcgi>.fetch.split("\n\n", 2);

    ok $test.checkheader(:header($hdr), :status(404)), 'HTTP header';
    ok $test.checkcontent(
        :cnt($cnt),
        :params(
            %(
                $p.ctrl.sharedobj<pageobj>.get_pparams,
                $p.ctrl.sharedobj<pageobj>.get_tparams
            )
        )
    ), 'check content: error 404';
}, 'Check unknown route';

# Check default route
subtest {
    plan 3;

    my $p = Pheix::App.new(:test(True), :ctrl($ctrl));

    $p.ctrl.sharedobj<fastcgi>.ruri = Str;
    $p.ctrl.sharedobj<fastcgi>.rmth = Str;

    is $p.ctrl.sharedobj<fastcgi>.reset, 0, 'reset content array';

    $p.start;
    my ($hdr, $cnt) = $p.ctrl.sharedobj<fastcgi>.fetch.split("\n\n", 2);

    ok $test.checkheader(:header($hdr)), 'HTTP header';
    ok $test.checkcontent(
        :cnt($cnt),
        :params(
            %(
                $p.ctrl.sharedobj<pageobj>.get_pparams,
                $p.ctrl.sharedobj<pageobj>.get_tparams
            )
        )
    ), 'check content: index';
}, 'Check default route';

done-testing;
