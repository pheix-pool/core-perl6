use v6.d;
use Test;
use lib 'lib';

use Pheix::Addons;
use Pheix::Test::Blockchain;
use Pheix::Test::Content;
use Pheix::Test::Helpers;
use Pheix::Model::Database::Access;
use Pheix::Model::Database::Compression;
use Pheix::View::Template;
use Pheix::View::Pages;
use Pheix::Model::Database::Blockchain;
use Pheix::Test::BlockchainComp::Helpers;

use P5quotemeta;
use MIME::Base64;
use XML;
use Net::Ethereum;

plan 12;

use-ok 'Pheix::View::Pages';

my $nobj = Net::Ethereum.new;
my $bobj = Pheix::Test::BlockchainComp::Helpers.new(:tstobj(Pheix::Test::Blockchain.new));
my $mb64 = MIME::Base64.new;
my $tcnt = Pheix::Test::Content.new;
my $thlp = Pheix::Test::Helpers.new;
my $hobj = Pheix::View::Web::Headers.new;
my $jobj = Pheix::Model::JSON.new;
my $uobj = Pheix::Utils.new(:test(True), :jsonobj($jobj));
my $tobj = Pheix::View::Template.new;
my $robj = Pheix::Model::Resources.new.init;

if !$thlp.check_submodules {
    skip-rest('No Pheix submodules are pulled');
    exit;
}

my $pobj = Pheix::View::Pages.new(
    :test(True),
    :tobj($tobj),
    :utilobj($uobj),
    :headobj($hobj),
    :jsonobj($jobj),
    :rsrcobj($robj),
    :addons(Pheix::Addons.new.get_addons),
);
my $dobj = Pheix::Model::Database::Access.new(
    :table('install'),
    :fields(<id date name config power status>),
    :test(True)
);
my $cobj =
    Pheix::Model::Database::Compression.new(:dictsize($uobj.lzwsz));


my Int $record_len  = 256;
my Str $prerender   = '<h1><TMPL_VAR rndr_var_1></h1><p><TMPL_VAR rndr_var_2></p><div><TMPL_VAR rndr_text></div>';
my Str $sample_data = qq~
Sed ut perspiciatis, unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam eaque ipsa, qua
e ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt, explicabo. Nemo enim ipsam voluptatem, quia voluptas sit,
aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos, qui ratione voluptatem sequi nesciunt, neque porro quisq uam
est, qui dolorem ipsum, quia dolor sit, amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt, ut labor
e et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laborio
sam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit, qui in ea voluptate velit esse, quam nihil mo
lestiae consequatur, vel illum, qui dolorem eum fugiat, quo voluptas nulla pariatur? At vero eos et accusamus et iusto odio dignissi
mos ducimus, qui blanditiis praesentium voluptatum deleniti atque corrupti, quos dolores et quas molestias excepturi sint, obcaecati
cupiditate non provident, similique sunt in culpa, qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum qui
dem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio, cumque nihil impedit, quo min
us id, quod maxime placeat, facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et au
t officiis debitis aut rerum necessitatibus saepe eveniet, ut et voluptates repudiandae sint et molestiae non recusandae. Itaque ear
um rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperio
res repellat.
~;

$sample_data ~~ s:g/<[\n]>*//;

# Check captcha attributes
subtest {
    plan 3;

    my %tparams  = $pobj.get_tparams(:dofiltering(False));
    my $decypted = $uobj.do_decrypt(%tparams<tmpl_encaptcha>);

    ok $decypted ~~ m/^<{%tparams<tmpl_decaptcha>}>/, 'captcha value decrypted';

    my ($code, $timestamp) = $decypted.split(q{:}, :skip-empty);

    is $code, %tparams<tmpl_decaptcha>, 'codes are equal';
    ok (time - $timestamp < 60), 'captcha is generated less than minute ago';
}, 'Check captcha attributes';

# Filechain content fetch scenarios for raw_pg method
subtest {
    plan 3;

    my $tab = 'page_content';

    subtest {
        plan 4;

        my @farr   = <id data compression[rand:bzip2]>;
        my $dbobj  = Pheix::Model::Database::Access.new(
            table  => $tab,
            fields => @farr,
            test   => True
        );

        if $dbobj.dbpath.IO.e {
            ok $dbobj.remove_all, 'remove previous session database';
        }
        else {
            is $dbobj.get_count(Hash.new), 0, 'no database from previous session';
        }

        my Str $content = get_sample_text(:text($sample_data));
        my @data_frames = $bobj.text_to_frames(:text($content));
        my @skipindxs   = $bobj.get_skip_indexes(:mode('rand'), :blocks(@data_frames.elems));

        for @data_frames.kv -> $index, $frame {
            my $data = $frame;
            my $comp = False;

            if @skipindxs.map({$_ if $index == $_}).elems == 0 {
                $data = compress(:data($data), :legend(@farr), :$tab, :algo('bzip2'));
                $comp = True;
            }

            my $insert_params = {
                id         => $index + 1,
                data       => $data,
                @farr.tail => $comp ?? q{1} !! q{0}
            };

            $dbobj.insert($insert_params);
        }

        is $dbobj.get_count(Hash.new), @data_frames.elems, 'save content';

        is $pobj.raw_pg(
            :table($tab),
            :fields(@farr),
            :test(True)
        ), $content, 'validate content';

        ok $dbobj.remove_all, 'drop table';
    }, 'random table uncompressed content';

    subtest {
        plan 3;

        my @farr   = <id data compression[skip:bzip2]>;
        my $dbobj  = Pheix::Model::Database::Access.new(
            table  => $tab,
            fields => @farr,
            test   => True
        );

        my Str $content = get_sample_text(:text($sample_data));

        my $insert_params = {
            id         => 1,
            data       => $content,
            @farr.tail => q{0}
        };

        ok $dbobj.insert($insert_params), 'save content';

        is $pobj.raw_pg(
            :table($tab),
            :fields(@farr),
            :test(True)
        ), $content, 'validate content';

        ok $dbobj.remove_all, 'drop table';
    }, 'full table uncompressed content';

    subtest {
        plan 6;

        for <lzw bzip2> -> $algo {
            my @farr   = ('id', 'data', sprintf("compression[full:%s]", $algo));
            my $dbobj  = Pheix::Model::Database::Access.new(
                table  => $tab,
                fields => @farr,
                test   => True
            );

            my Str $content   = get_sample_text(:text($sample_data));
            my $insert_params = {
                id         => 1,
                data       => compress(:data($content), :legend(@farr), :$tab, :algo($algo)),
                @farr.tail => q{1}
            };

            ok $dbobj.insert($insert_params), sprintf("save %s content", $algo);

            is $pobj.raw_pg(
                :table($tab),
                :fields(@farr),
                :test(True)
            ), $content, 'validate content';

            ok $dbobj.remove_all, 'drop table';
        }
    }, 'full table compressed content';
}, 'Filechain content fetch scenarios for raw_pg method';

# Check show_pg method for debug mode, index and 40X pages
subtest {
    my @cases = <debug index 400 401 402 403 404 405>;

    plan @cases.elems * 2;

    my Pheix::View::Pages $p = $pobj.clone;

    for @cases -> $case {
        my $cnt = $p.show_pg(
            :pg_type($case),
            :pg_route('/' ~ $case),
            :pg_content($case eq 'debug' ?? $sample_data !! Str),
        );

        nok $cnt ~~ / '<TMPL_VAR tmpl_' <[a..z_]>+ '>' /, 'no vars';

        given $case {
            when 'debug' {
                is $cnt, $sample_data, 'check template for debug';
            }
            default {
                ok $tcnt.checkcontent(
                    :cnt($cnt),
                    :params(
                        %(
                            $p.get_cparams,
                            $p.get_pparams,
                            $p.get_tparams
                        )
                    )
                ), 'check template for ' ~ $case ~ ' page';
            }
        }
    }

}, 'Check show_pg method for debug mode, index and 40X pages';

# Check show_sm method without addons
subtest {
    plan 3;

    my $pobj_noaddons = Pheix::View::Pages.new(
        :test(True),
        :tobj($tobj),
        :utilobj($uobj),
        :headobj($hobj),
        :jsonobj($jobj),
        :rsrcobj($robj),
        :addons([]),
    );

    my Str $r = $hobj.proto_sn;
    my Str $s = $pobj_noaddons.show_sm;

    my @urls = from-xml($s).elements(:TAG<url>);

    is @urls.elems, 1, 'single url in urlset';

    my $loc = @urls[0].firstChild;
    my $mod = @urls[0].lastChild;

    is $loc.contents.join, $r, 'location ' ~ $r;

    my $d = DateTime.new($pobj_noaddons.fchnobj.get_path(:tab($pobj_noaddons.indxcnt)).IO.modified || now );
    my $update = sprintf("%04d-%02d-%02d", $d.year, $d.month, $d.day);

    is $mod.contents.join, $update, 'last update ' ~ $update;

}, 'Check show_sm method without addons';

# Check show_sm method with Pheix::Addons::Embedded::User
subtest {
    plan 2;

    my Str $a  = 'Pheix::Addons::Embedded::User';
    my Str $r  = $hobj.proto_sn;
    my Str $s  = $pobj.show_sm;
    my @smtest = $thlp.get_addon_sitemap_from_fs(:addon($pobj.addons{$a}<objct>));

    my @sitemap;
    my Bool $indexrecord = False;

    for from-xml($s).elements(:TAG<url>) -> $url {
        my $loc = $url.firstChild;
        my $mod = $url.lastChild;

        my $u = {
            $loc.name => $loc.contents.join,
            $mod.name => $mod.contents.join,
        };

        $u{$loc.name} ~~ s/ $r '/' //;

        if $u{$loc.name} eq $r {
            $indexrecord = True;

            next;
        }

        @sitemap.push($u);
    }

    ok $indexrecord, 'url for ' ~ $r;
    is-deeply(@sitemap.sort, @smtest.sort, 'embedded sitemap');

}, 'Check show_sm method with Pheix::Addons::Embedded';

# Check lazy_load method
subtest {
    plan 1;

    my %cfg =
        instance    => 'test',
        credentials => {token => '098f6bcd4621d373cade4e832627b4f6'},
        method      => 'GET',
        route       => 'test/lazy',
        httpstat    => 200,
        message     => "this is msg to be 'quoted'"
    ;

    my Str $expected = sprintf(
        "loadAPI_v2('%s', %s, '%s', '%s', '%s', '%s', null, false, null)",
        %cfg<instance>,
        $jobj.as-json(:data(%cfg<credentials>)),
        %cfg<method>,
        %cfg<route>,
        %cfg<httpstat>,
        quotemeta(%cfg<message>)
    );


    is $expected, $pobj.lazy_load(:conf(%cfg)), 'lazy_load method';
}, 'Check lazy_load method';

# Check show_rtm method
subtest {
    plan 2;

    ok(
        my $o = $pobj.show_rtm(:s(True)) ~~ m:i/
            '<script type="text/javascript">'
            '$("#pheix_render_time").text("'
            <[\d\.]>+
            '");</script>'
        /,
        'method show_rtm with script arg',
    );
    ok(
        $o = $pobj.show_rtm ~~ m:i/
            '<p class="_phx-cntr _phx-ccc _phx-fnt10">'
            'render time: '
            <[\d\.]>+
            ' seconds</p>'
        /,
        'method show_rtm w/o args',
    );
}, 'Check show_rtm method';

# Check fill_seodata, get_tparams and get_pparams methods
subtest {
    plan 8;

    my %s =
        m => 'Pheix',
        p => <indexseotags 404seotags>,
        s => <title header metadescr metakeywords>,
        t => 'value'
    ;

    my $ptype = 'index';

    my $pt = $jobj.get_group_setting(%s<m>, %s<p>[0], %s<s>[0], %s<t>);
    my $ph = $jobj.get_group_setting(%s<m>, %s<p>[0], %s<s>[1], %s<t>);
    my $md = $jobj.get_group_setting(%s<m>, %s<p>[0], %s<s>[2], %s<t>);
    my $mk = $jobj.get_group_setting(%s<m>, %s<p>[0], %s<s>[3], %s<t>);

    $pobj.fill_seodata($ptype, %s<m>);

    my %h = $pobj.get_pparams, $pobj.get_tparams;

    is %h<tmpl_pageheader>, $ph, 'set tmpl_pageheader at ' ~ $ptype;
    is %h<tmpl_pagetitle>, $pt, 'set tmpl_pagetitle at ' ~ $ptype;
    is %h<tmpl_metadesc>, $md, 'set tmpl_metadesc at ' ~ $ptype;
    is %h<tmpl_metakeys>, $mk, 'set tmpl_metakeys at ' ~ $ptype;

    $ptype = '404';

    $pt = $jobj.get_group_setting(%s<m>, %s<p>[1], %s<s>[0], %s<t>);
    $ph = $jobj.get_group_setting(%s<m>, %s<p>[1], %s<s>[1], %s<t>);
    $md = $jobj.get_group_setting(%s<m>, %s<p>[1], %s<s>[2], %s<t>);
    $mk = $jobj.get_group_setting(%s<m>, %s<p>[1], %s<s>[3], %s<t>);

    $pobj.fill_seodata($ptype, %s<m>);

    %h = $pobj.get_pparams, $pobj.get_tparams;

    is %h<tmpl_pageheader>, $ph, 'set tmpl_pageheader at ' ~ $ptype;
    is %h<tmpl_pagetitle>, $pt, 'set tmpl_pagetitle at ' ~ $ptype;
    is %h<tmpl_metadesc>, $md, 'set tmpl_metadesc at ' ~ $ptype;
    is %h<tmpl_metakeys>, $mk, 'set tmpl_metakeys at ' ~ $ptype;
}, 'Check fill_seodata, get_tparams and get_pparams methods';

# Check raw_pg method
subtest {
    my Hash $insert_params;
    my Int  $iteration;
    my Str  $rawpage;
    my Int  $dbase_plan = 1;
    my Int  $pg_plan    = 1;
    my Int  $cmp_plan   = 25;
    my Int  $index      = 0;
    my Str  $pg_sample  = get_sample_text(:text($sample_data));

    my @et_recs = get_etalon_records(:text($pg_sample), :len($record_len));
    my $tab     = 'rawpage_content';
    my @farr    = <id data compression[rand:bzip2]>;
    my $dbobj   = Pheix::Model::Database::Access.new(
        table   => $tab,
        fields  => @farr,
        test    => True
    );
    plan 1 + $dbase_plan + $pg_plan + $cmp_plan;

    ok $pg_sample.chars > 0, 'page sample is not empty';
    is(
        $pobj.raw_pg(
            :table('unexisted_tab'),
            :fields(List.new),
            :test(True)
        ),
        Empty,
        'raw_pg method with unexisted tab'
    );

    my Str $pg_etalon = '<body><p>This is etalon!</p></body>';
    $dbobj.remove_all;

    $insert_params = {
        id          => 1,
        data        => $pg_etalon,
        compression => ~(0),
    };

    $dbobj.insert($insert_params);

    is(
        $pobj.raw_pg(
            :table($tab),
            :fields(@farr),
            :test(True)
        ),
        $pg_etalon,
        'raw_pg w/o compression is validated'
    );

    while $dbobj && $cmp_plan {
        $dbobj.remove_all;
        $iteration++;

        for @et_recs -> $rec {
            next unless $rec;

            my $cmp = (0..1).rand.round;

            my $data = $cmp ??
                compress(:data($rec), :legend(@farr), :$tab, :algo('bzip2')) !!
                    $rec;

            $insert_params = {
                id         => $index,
                data       => $data,
                @farr.tail => ~($cmp),
            };

            $dbobj.insert($insert_params);

            $index++;
        }

        $rawpage = $pobj.raw_pg(
            :table($tab),
            :fields(@farr),
            :test(True)
        );

        ok(
            $rawpage eq $pg_sample,
            'raw page is validated at step no.' ~ $iteration,
        );
        $dbobj.remove_all;
        $cmp_plan--;
    }
}, 'Check raw_pg method';

# Check get_cparams and set_cparams methods
subtest {
    plan 1;

    my @words;
    my @w = $sample_data.split(/<[\s\.\,\?]>+/, :skip-empty);
    my @consume = @w.clone;

    while @consume {
        for $pobj.get_cparams.keys -> $key {
            $pobj.set_cparams($key, @consume.pop // q{});
        }

        for $pobj.get_cparams.kv -> $key, $val {
            @words.push($val) if $val ne q{};
        }
    }

    is-deeply
        @w,
        @words.reverse.Array,
        'setter/getter on ' ~ @words.elems ~ ' steps'
    ;
}, 'Check get_cparams and set_cparams methods';

# Check cookie_dependent method
subtest {
    plan 15;

    my Str $mval = now.Rat.Str;
    my Str $mdat = 'abcdefghijklmnopqrstuvwxyz';

    nok $pobj.cookie_dependent, 'no cookie (undefined)';
    nok $pobj.cookie_dependent(:env(Hash.new)), 'no cookie (empty)';

    is $pobj.get_cparams<tmpl_modeclass>, 'daymode', 'default cparam';
    ok $pobj.set_cparams('tmpl_modeclass', q{}), 'cparam reset';

    nok $pobj.cookie_dependent(:env(%(C => 'a=b;'))), 'no cookie (key)';

    is $pobj.get_cparams<tmpl_modeclass>, 'daymode', 'default cparam';
    ok $pobj.set_cparams('tmpl_modeclass', q{}), 'cparam reset';

    nok
        $pobj.cookie_dependent(:env(%(HTTP_COOKIE => 'a=b;'))),
        'no cookie (correct cookie key, invalid name)'
    ;

    is $pobj.get_cparams<tmpl_modeclass>, 'daymode', 'default cparam';
    ok $pobj.set_cparams('tmpl_modeclass', q{}), 'cparam reset';

    nok
        $pobj.cookie_dependent(
            :env(
                %(HTTP_COOKIE => 'tmpl_modeclass=' ~ $mval ~ q{;})
            )
        ),
        'no cookie (correct cookie key, correct name, invalid value)'
    ;

    is $pobj.get_cparams<tmpl_modeclass>, 'daymode', 'default cparam';
    ok $pobj.set_cparams('tmpl_modeclass', q{}), 'cparam reset';

    ok
        $pobj.cookie_dependent(
            :env(
                %(HTTP_COOKIE => 'tmpl_modeclass=' ~ $mdat ~ q{;})
            )
        ), 'cookie'
    ;

    is $pobj.get_cparams<tmpl_modeclass>, $mdat, 'user cparam';
}, 'Check cookie_dependent method';

done-testing;

sub get_etalon_records(:$text, :$len) returns Array {
    my @records;
    my Int $index = 0;

    while True {
        my Str $s = substr($text, ($index*$len), $len);
        @records.push($s);
        $index++;
        if $s.chars < $len {
            last;
        }
    }

    @records;
}

sub get_sample_text(:$text) returns Str {
    my Str $str;
    my Int $len = (1..10).rand.Int;

    for ^$len {
        if (0..1).rand.round {
            if (0..1).rand.round {
                $str ~= $text.flip;
            }
            else {
                $str ~= $text.flip.uc;
            }
        }
        else {
            $str ~= $text;
        }
    }

    $str;
}

sub compress(Str :$data!, :@legend!, Str :$tab!, Str :$algo) returns Str {
    my $bcobj = Pheix::Model::Database::Blockchain.new(
        :abi('{}'),
        :apiurl('http://localhost'),
        :sctx('0x0'),
        :fields(@legend),
        :table($tab),
        :config({compression => $algo})
    );

    return $nobj.buf2hex($bcobj.cmpobj.compress(:data($data)));
}
