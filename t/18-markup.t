use v6.d;
use Test;
use lib 'lib';

plan 1;

my Str $content =
    'Sed ut perspiciatis, unde omnis iste natus error sit ' ~
    'voluptatem accusantium doloremque laudantium, totam rem ' ~
    'aperiam eaque ipsa, qua';
my Str $class   =
    'pheix-specific-class1 ' ~
    'pheix-specific-class2 ' ~
    'pheix-specific-class3';

use Pheix::View::HTML::Markup;
my Any $obj = Pheix::View::HTML::Markup.new;

# Check br attribute and uni_tag method
subtest {
    plan 4;
    is  $obj.br, '<br>', 'br returns <br>';
    is(
        $obj.uni_tag('p', $content),
        '<p>' ~ $content ~ '</p>',
        'uni_tag with p tag and Nil class',
    );
    is(
        $obj.uni_tag('p', $content, :attrs({class => ""})),
        '<p>' ~ $content ~ '</p>',
        'uni_tag with p tag and empty [""] class',
    );
    is(
        $obj.uni_tag('p', $content, :attrs({class => $class})),
        '<p class="' ~ $class ~ '">' ~ $content ~ '</p>',
        'uni_tag with p tag and non-empty class',
    );
}, 'Check br attribute and uni_tag method';

done-testing;
