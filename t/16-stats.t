use v6.d;
use Test;
use lib 'lib';

plan 6;

use Pheix::Addons::November::CGI;
use Pheix::Datepack;
use Pheix::Controller::Stats;
use Pheix::Test::Helpers;
use Pheix::Test::Blockchain;
use Pheix::Test::BlockchainComp::Helpers;

constant iters  = 50;
constant tstobj = Pheix::Test::Blockchain.new;

use-ok 'Pheix::Controller::Stats';

my $thlp  = Pheix::Test::Helpers.new;
my $sobj  = Pheix::Controller::Stats.new(:test(True));
my $bhelp = Pheix::Test::BlockchainComp::Helpers.new(:tstobj(tstobj), :payload(False));

if !$thlp.check_submodules {
    skip-rest('No Pheix submodules are pulled');
    exit;
}

my $dbobj = Pheix::Model::Database::Access.new(
    :table('bigbro'),
    :fields(<id referer ip useragent resolution page country>),
    :test(True)
);

my UInt $globt = time - 60;

# Check get_traffic method
subtest {
    plan 6 * iters + 1;

    nok $dbobj.chainobj.get_path.IO.e, 'no database existed';

    if $dbobj.chainobj.get_path.IO.e {
        diag(sprintf("database %s is modified %s", $dbobj.chainobj.get_path, $dbobj.chainobj.get_modify_time.DateTime));
        diag($dbobj.chainobj.get_path.IO.slurp);
    }

    for ^iters {
        my $iter    = $_;
        my $records = 10;
        my $dayweek = 7;
        my $daysec  = 86_400;
        my $today   = time;

        my @synthdb =
            $bhelp.generate_table(:tsz($dayweek * $records))
                .map({$_.split(q{|}, :skip-empty)});

        my @hosts;
        my @dates;
        my @countries;

        for (0..$dayweek - 1) -> $day {
            my @hosts_per_day;
            my @countries_per_day;

            my $datets  = $today + $daysec * $day;
            my $dateobj = DateTime.new($datets);

            for ^$records -> $r {
                my $dbrec = @synthdb[$day*10 + $r];

                my $data = {
                    id         => ($datets + $r),
                    referer    => $dbrec[0],
                    ip         => $dbrec[1],
                    useragent  => $dbrec[2],
                    resolution => $dbrec[3],
                    page       => $dbrec[4],
                    country    => $dbrec[5],
                };

                $dbobj.insert($data);

                @hosts_per_day.push($dbrec[1]);
                @countries_per_day.push($dbrec[5]);
            }

            @countries.push(@countries_per_day.unique.sort.Array);
            @hosts.push(@hosts_per_day.unique.elems);
            @dates.push(sprintf("%01d-%01d", $dateobj.day, $dateobj.month));
        }

        my %traffic = $sobj.get_traffic;

        is %traffic<dates>.elems, $dayweek, 'num of dates in stats';
        is-deeply %traffic<dates>, @dates, 'dates in stats';
        is-deeply %traffic<visits>, [$records xx $dayweek], 'visitors in stats';
        is-deeply %traffic<hosts>, @hosts, 'hosts in stats';
        is-deeply %traffic<countries>, @countries, 'countries in stats';

        ok $dbobj.remove_all, 'clean database';
    }
}, 'Check get_traffic method';

# Check do_log method
subtest {
    plan 125;
    $thlp.get_code_time(
        :descr('do_log'),
        :coderef(
            {
                my Any $cgi  = Pheix::Addons::November::CGI.new;
                for (1..25) {
                    my Str $qstr =
                        'action=bigbrother&page=/index.html&' ~
                        'resolut=1920*1200&ref=https://pheix.org/foobar.html&' ~
                        'rnd=0.8973966537898391';

                    $cgi.parse_params( $qstr );

                    lives-ok {
                        ok $sobj.do_log(Nil), 'default do_log no.' ~ $_;
                        ok check_log(False), 'check default log no.' ~ $_;
                        ok $sobj.do_log($cgi), 'do_log with data no.' ~ $_;
                        ok check_log(True), 'check log with CGI no.' ~ $_;
                    }, 'stats subtests no.1 survive';

                    $cgi.params = %();
                }
            }
        ),
    );
}, 'Check do_log method';

# Check get_country method
subtest {
    plan 27;
    if $sobj.get_mmdbpath.defined {
        my @_data = (
            %( ip => '95.153.133.169', country_code => 'RU' ),
            %( ip => '89.176.43.76',   country_code => 'CZ' ),
            %( ip => '188.32.131.93',  country_code => 'RU' ),
            %( ip => '37.204.52.249',  country_code => 'RU' ),
            %( ip => '178.140.107.6',  country_code => 'RU' ),
            %( ip => '78.20.206.6',    country_code => 'BE' ),
            %( ip => '37.144.156.142', country_code => 'RU' ),
            %( ip => '94.180.220.227', country_code => 'RU' ),
            %( ip => '181.115.140.34', country_code => 'BO' ),
            %( ip => '95.108.129.200', country_code => 'RU' ),
            %( ip => '78.47.192.226',  country_code => 'DE' ),
            %( ip => '92.63.91.212',   country_code => 'LV' ),
            %( ip => '46.42.131.34',   country_code => 'RU' ),
            %( ip => '90.9.255.181',   country_code => 'FR' ),
            %( ip => '37.9.118.24',    country_code => 'RU' ),
            %( ip => '217.86.62.229',  country_code => 'DE' ),
            %( ip => '188.32.226.86',  country_code => 'RU' ),
            %( ip => '87.250.20.140',  country_code => 'RU' ),
            %( ip => '46.42.175.246',  country_code => 'RU' ),
            %( ip => '37.170.35.88',   country_code => 'FR' ),
            %( ip => '194.228.13.149', country_code => 'CZ' ),
            %( ip => '65.55.210.203',  country_code => 'US' ),
            %( ip => '188.32.128.51',  country_code => 'RU' ),
            %( ip => '176.181.81.198', country_code => 'FR' ),
            %( ip => '128.72.121.20',  country_code => 'RU' ),
            %( ip => '65.55.210.151',  country_code => 'US' ),
            %( ip => '94.141.52.4',    country_code => 'RU' ),
        );

        $thlp.get_code_time(
            :descr('get_country'),
            :coderef(
                {
                    for @_data {
                        is(
                            $sobj.get_country($_<ip>),
                            $_<country_code>,
                            'country_code for ' ~ $_<ip> ~ ' is proved',
                        );
                    }
                }
            )
        );
    }
    else {
        skip-rest('geoip database is not available');
    }

}, 'Check get_country method';

# Check crop_log method
subtest {
    plan 850;

    $thlp.get_code_time(
        :descr('crop_log'),
        :coderef(
            {
                for (0..24) {
                    insert_values;
                    my UInt $_crd = $_ + 7;
                    ok(
                        $sobj.crop_log( $_crd ),
                        'cropping logs for ' ~ $_crd ~ ' days',
                    );
                    my @_r = $dbobj.get_all;
                    is(
                        @_r.elems,
                        $_crd,
                        'test cropped logs for ' ~ $_crd ~ ' days',
                    );
                    ok( $dbobj.remove_all, 'clean up after cropping' );
                }
            }
        )
    );
}, 'Check crop_log method';

# Check crop_log method on currupted table
subtest {
    plan 11;

    my Str $inval = sprintf("%s|undef|127.0.0.1|\nMozilla/5.0 Firefox/84.0|1280\n*800|/|undef", time);
    my Str $valid = time ~ '|undef|127.0.0.1|Mozilla/5.0 Firefox/84.0|1280*800|/|undef';
    my $logs = [
        '1578787200|undef|127.0.0.1|Firefox/84.0|1280*800|/|undef',
        '1578787201|undef|127.0.0.1|Firefox/84.0|1280*800|/|undef',
        '1578787202|undef|127.0.0.1|Mozilla/5.0|1280*800|/|undef',
        $inval,
        '1578787203|undef|127.0.0.1|Mozilla/5.0 Firefox/84.0|1280*800|/|undef',
        '1578787204|undef|127.0.0.1|Mozilla/5.0 Firefox/84.0|1280*800|/|undef',
        $valid
    ];

    for @$logs -> $d {
        my @col = $d.split(q{|}, :skip-empty);

        my $data = {
            id  => @col[0],
            referer    => @col[1],
            ip         => @col[2],
            useragent  => @col[3],
            resolution => @col[4],
            page       => @col[5],
            country    => @col[6],
        };

        ok $dbobj.insert($data), 'insert to bigbro tab';
    }

    ok $sobj.crop_log(7), 'crop logs';

    # filter invalid records
    my @data = $dbobj.get_all.map({
        $_ if ($_.keys.elems == $dbobj.fields.elems)
    });

    is @data.elems, 1, 'one record after crop';

    is-deeply(
        @data[0].keys.map({ @data[0]{$_} }).sort,
        $valid.split(q{|}, :skip-empty).sort,
        'record is valid'
    );

    ok $dbobj.remove_all, 'clean database';

}, 'Check crop_log method on currupted table';

done-testing;

sub check_log( Bool $withcgi ) returns Bool {
    my Bool $ret = False;
    my $f = $dbobj.dbpath;
    if $f.IO.e {
        my @db = $f.IO.slurp.lines;
        if @db.elems == 2 {
            if @db.head ~~
                /^
                    '# id;referer;ip;useragent;resolution;page;country'
                / {
                if !$withcgi {
                    if @db.tail ~~ m/^
                            (<[\d]>+)
                            ('|undef|*.*.*.*|undef|undef|undef|undef')
                        / {
                        $ret = True;
                    }
                }
                else {
                    if @db.tail ~~ m/^
                            (<[\d]>+)
                            (
                                '|/foobar.html|*.*.*.*|undef'
                                '|1920*1200|/index.html|undef'
                            )
                        / {
                        $ret = True;
                    }
                }
            }
        }
        unlink $f;
    }
    $ret;
}

sub insert_values returns Bool {
    for (0..30) -> $index {
        my $data = {
            id  =>
                DateTime
                    .new( $globt )
                    .earlier( days => $index )
                    .posix,
            referer    => 'undef',
            ip         => '*.*.*.*',
            useragent  => 'Perl6 TestBot on: ' ~
                DateTime.new( time ).earlier( days => $index ),
            resolution => '1024*768',
            page       => 'index.html',
            country    => 'RU',
        };

        ok(
            $dbobj.insert($data),
            'insert <id referer ip useragent resolution ' ~
            'page country>: test no.' ~ ($index + 1),
        );
    }
    True;
}

$thlp.time_stats(:module('Pheix::Controller::Stats'));
