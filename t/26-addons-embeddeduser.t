use v6.d;
use Test;
use lib 'lib';

use Pheix::Model::JSON;
use Pheix::Test::Content;
use Pheix::Test::FastCGI;
use Pheix::Test::Helpers;
use Pheix::Model::Route;
use Pheix::Controller::Basic;
use Pheix::Addons::Embedded::User;

use JSON::Fast;

my $tcnt = Pheix::Test::Content.new;
my $fcgi = Pheix::Test::FastCGI.new;
my $thlp = Pheix::Test::Helpers.new;

my %etalon =
    1607363760 => {
        id => '1607363760',
        timestamp => '1607363760',
        seouri => 'table-of-contents',
        title => 'Table of contents',
        metadescr => 'This page is performing table of contents for Embedded module',
        metakeywords => 'table, contents, index',
        header => 'Table of contents',
        footer => 'Default footer for 1607363760',
        content => q{},
        allowpage => '0'
    },
    1515622353 => {
        id => '1515622353',
        timestamp => '1515622353',
        seouri => 'pheix-beta-release',
        title => 'Pheix β-release',
        metadescr => 'One step to β-release',
        metakeywords => 'pheix, beta, release, cms, raku',
        header => 'One step to β-release',
        footer => 'Default footer for 1515622353',
        content => 'To be announced.',
        allowpage => '0'
    },
    default => {
        header => 'Embedded module header',
        metadescr => 'Welcome to embedded module index page',
        metakeywords => 'embedded module, index page, home page, information about site, welcome',
        title => 'Embedded module default route'
    }
;

my UInt $content_page = 1611521427;
my Str  $err_response = $tcnt.cntpath ~ '/http-error.txt';
my @stats;

plan 14;

use-ok 'Pheix::Addons::Embedded::User';

if !$thlp.check_submodules {
    skip-rest('No Pheix submodules are pulled');
    exit;
}

my $ctrl = Pheix::Controller::Basic.new(
    :apirobj(Nil),
    :mockedfcgi($fcgi),
    :test(True),
);

# Check sitemap, get_defpid and get_name methods
subtest {
    plan 3;

    my @smaddon;
    my $addon    = Pheix::Addons::Embedded::User.new;

    $thlp.get_code_time(:descr('get_sm method'), :coderef({ @smaddon = $addon.get_sm; }));

    my @smtest   = $thlp.get_addon_sitemap_from_fs(:addon($addon));

    ok $addon.get_defpid ~~ UInt, 'get_defpid method';
    is $addon.get_name, 'EmbeddedUser', 'get_name method';
    is-deeply(@smaddon.sort, @smtest.sort, 'sitemap method');
}, 'Check get_defpid and get_name methods';

# Check allow_as_page method
subtest {
    plan 8;

    my @cache = [ False, True ];
    my @ids = [
        {id => 999, val => False},
        {id => 1607363760, val => False},
        {id => $content_page, val => True}
    ];

    for @cache -> $c {
        my $a = Pheix::Addons::Embedded::User.new(:nocache($c));

        for @ids -> $item {
            is(
                $a.allow_as_page(:match({id => $item<id>})),
                $item<val>,
                'allowance for record ' ~ $item<id> ~ ', cache: ' ~ $c
            );
        }

        is $a.allow_as_page(:match(Hash.new)), False, 'null hash' ~ ', cache: ' ~ $c;
    }
}, 'Check allow_as_page method';

# Check get_record method
subtest {
    plan 2;

    my @ids    = [1607363760, 1515622353];
    my $addon  = Pheix::Addons::Embedded::User.new;

    for @ids -> $id {
        my %h = $addon.get_record(:id($id));
        is-deeply %h, %etalon{$id}, 'data for record ' ~ $id;
    }
}, 'Check get_record method';

# Check check_page method
subtest {
    plan 7;

    my @ids   = [1607363760, 1515622353];
    my @fake  = [7789279932, 9992388823, 9992389993];
    my $addon = Pheix::Addons::Embedded::User.new;

    for @ids -> $id {
        ok $addon.check_page(:match({id => $id})), 'actual id ' ~ $id;
    }

    for @fake -> $id {
        nok $addon.check_page(:match({id => $id})), 'fake id ' ~ $id;
    }

    nok $addon.check_page(:match(Hash)), 'empty match no.1';
    nok $addon.check_page(:match(Hash.new)), 'empty match no.2';
}, 'Check check_page method';

# Check fill_seodata and get_content methods
subtest {
    plan 23;

    my @ids    = [1607363760, 1515622353, 100, 0, 999];
    my $addon  = Pheix::Addons::Embedded::User.new;
    my $jo     = Pheix::Model::JSON.new;

    for @ids -> $id {
        my %match = %(id => $id, rmatch => %(id => $id));

        is-deeply(
            $addon.fill_seodata(:match(%match), :jobj($jo)),
            $id > 1000 ?? %etalon{$id} !! %etalon<default>,
            'SEO data for record ' ~ $id
        );

        is(
            $addon.get_content(:match(%match)),
            $id > 1000 ?? %etalon{$id}<content> !! %etalon{$addon.get_defpid}<content>,
            'SEO content for record ' ~ $id
        );

        is(
            $addon.get_content(:match(%match), :hdr(True)),
            $id > 1000 ?? %etalon{$id}<header> !! %etalon{$addon.get_defpid}<header>,
            'SEO header for record ' ~ $id
        );

        is(
            $addon.get_content(:match(%match), :ftr(True)),
            $id > 1000 ?? %etalon{$id}<footer> !! %etalon{$addon.get_defpid}<footer>,
            'SEO footer for record ' ~ $id
        );
    }

    is-deeply(
        $addon.fill_seodata(:match(Hash), :jobj($jo)),
        %etalon<default>,
        'default SEO data'
    );

    is(
        $addon.get_content(:match(Hash), :hdr(True)),
        %etalon{$addon.get_defpid}<header>,
        'default SEO header'
    );

    is(
        $addon.get_content(:match(Hash), :ftr(True)),
        %etalon{$addon.get_defpid}<footer>,
        'default SEO footer'
    );
}, 'Check fill_seodata and get_content methods';

# Check error method
subtest {
    my %match =
        details    => %(path => '/error/' ~ now.Rat),
        controller => 'A',
        action     => 'B',
    ;

    my %cntn = Pheix::Addons::Embedded::User.new.error(
        :tick(1),
        :match(%match),
        :sharedobj($ctrl.sharedobj)
    );

    my %details = get_page_details(:fp('./conf/_pages/http-error.txt'));

    plan 3 + %details<tokens>.elems;

    ok (%cntn<component>:exists) && (%cntn<component> ne q{}), 'component';
    ok (%cntn<tparams>:exists) && (%cntn<tparams> ~~ Hash), 'tparams';
    is(
        $ctrl.sharedobj<mb64obj>.decode-str(%cntn<component>),
        %details<data>,
        'component content'
    );

    # tmpl_exception_msg is set to blank at User.rakumod#377
    %cntn<tparams><tmpl_exception_msg> = 'dummy value';

    for %details<tokens>.List -> $t {
        ok (%cntn<tparams>{$t}:exists) && (%cntn<tparams>{$t}), $t;
    }
}, 'Check error method';

# Check browse_api method
subtest {
    my $addon = Pheix::Addons::Embedded::User.new;
    my $pid   = $addon.get_defpid;
    my %match =
        id         => $pid,
        details    => %(path => sprintf("/embedded/%s", $pid)),
        controller => 'Pheix::Addons::Embedded::User',
        action     => 'browse_api',
    ;

    my %cntn = $addon.browse_api(
        :tick(1),
        :match(%match),
        :sharedobj($ctrl.sharedobj)
    );

    my %details = get_page_details(
        :fp('./conf/_pages/embeddeduser/' ~ $addon.get_defpid ~ '.txt')
    );

    plan 3 + %details<tokens>.elems;

    ok (%cntn<component>:exists) && (%cntn<component> ne q{}), 'component';
    ok (%cntn<tparams>:exists) && (%cntn<tparams> ~~ Hash), 'tparams';

    # todo "need data to be prepared cause of compression records";
    is(
        $ctrl.sharedobj<mb64obj>.decode-str(%cntn<component>),
        %details<data>,
        'component content'
    );

    for %details<tokens>.List -> $t {
        ok (%cntn<tparams>{$t}:exists) && (%cntn<tparams>{$t}), $t;
    }
}, 'Check browse_api method';

# Check browse_api method for not allowed page
subtest {
    my $addon = Pheix::Addons::Embedded::User.new;
    my %match =
        id         => $content_page,
        details    => %(path => sprintf("/embedded/%s", $content_page)),
        controller => 'Pheix::Addons::Embedded::User',
        action     => 'browse_api',
    ;

    my %cntn = $addon.browse_api(
        :tick(1),
        :match(%match),
        :sharedobj($ctrl.sharedobj)
    );

    my %details = get_page_details(:fp('./conf/_pages/http-error.txt'));

    plan 6 + %details<tokens>.elems;

    ok (%cntn<component>:exists) && (%cntn<component> ne q{}), 'component';
    ok (%cntn<tparams>:exists) && (%cntn<tparams> ~~ Hash), 'tparams';

    ok(
        (
            (%cntn<tparams><tmpl_httperr_code>:exists) &&
            (%cntn<tparams><tmpl_httperr_code>.UInt == 403)
        ),
        'tmpl_httperr_code content'
    );

    ok(
        (
            (%cntn<tparams><tmpl_httperr_text>:exists) &&
            (%cntn<tparams><tmpl_httperr_text> eq 'Forbidden')
        ),
        'tmpl_httperr_text content'
    );

    ok(
        (
            (%cntn<tparams><tmpl_exception_msg>:exists) &&
            (%cntn<tparams><tmpl_exception_msg> ~~ /'<' $content_page '>'/)
        ),
        'tmpl_exception_msg content'
    );

    is(
        $ctrl.sharedobj<mb64obj>.decode-str(%cntn<component>),
        %details<data>,
        'component content'
    );

    for %details<tokens>.List -> $t {
        ok (%cntn<tparams>{$t}:exists) && (%cntn<tparams>{$t}), $t;
    }
}, 'Check browse_api method for not allowed page';

# Check browse method for index
subtest {
    plan 12;

    my @routes = ['/embedded', '/embedded/1607363760', '/embedded/1515622353'];
    my $addon  = Pheix::Addons::Embedded::User.new(:ctrl($ctrl));

    for @routes -> $r {
        my UInt $id = 0;

        $r ~~ m:g/ 'embedded/' (<[\d]>+) { $id = $0.UInt if $0 }/;

        my %match =
            details    => %(path => $r),
            controller => $addon.get_class,
            action     => 'browse',
        ;

        %match<id> = $id if $id > 0;

        is $ctrl.sharedobj<fastcgi>.reset, 0, 'reset content array';

        $addon.browse(:tick(1), :match(%match));

        my ($hdr, $cnt) = $ctrl.sharedobj<fastcgi>.fetch.split("\n\n", 2);

        nok $cnt ~~ / '<TMPL_VAR tmpl_' <[a..z_]>+ '>' /, 'no tmpl vars';
        ok $tcnt.checkheader(:header($hdr)), 'HTTP header';
        ok $tcnt.checkcontent(
            :cnt($cnt),
            :params(
                %(
                    $ctrl.sharedobj<pageobj>.get_pparams,
                    $ctrl.sharedobj<pageobj>.get_tparams
                )
            )
        ), 'check content: ' ~ $r;
    }
}, 'Check browse method for index';

# Check browse method for error
subtest {
    plan 12;

    my @routes = ['/embedded/10', '/embedded/100', '/embedded/9987237'];
    my $addon  = Pheix::Addons::Embedded::User.new(:ctrl($ctrl));

    for @routes -> $r {
        my UInt $id = 0;

        $r ~~ m:g/ 'embedded/' (<[\d]>+) { $id = $0.UInt if $0 }/;

        my %match =
            details    => %(path => $r),
            id         => $id,
            controller => $addon.get_class,
            action     => 'browse',
        ;

        is $ctrl.sharedobj<fastcgi>.reset, 0, 'reset content array';

        $addon.browse(:tick(1), :match(%match));

        my ($hdr, $cnt) = $ctrl.sharedobj<fastcgi>.fetch.split("\n\n", 2);

        nok $cnt ~~ / '<TMPL_VAR tmpl_' <[a..z_]>+ '>' /, 'no tmpl vars';
        ok $tcnt.checkheader(:header($hdr), :status(404)), 'HTTP header';
        ok $tcnt.checkcontent(
            :cnt($cnt),
            :params(
                %(
                    $ctrl.sharedobj<pageobj>.get_pparams,
                    $ctrl.sharedobj<pageobj>.get_tparams
                )
            )
        ), 'check content: ' ~ $r;
    }
}, 'Check browse method for error';

# Check embedded content via API
subtest {
    plan 54;

    my $a_default = Pheix::Addons::Embedded::User.new;
    my $a_allow   = Pheix::Addons::Embedded::User.new(:allowid(True));
    my %setup     = $a_default.jsonobj.get_entire_config;

    $thlp.get_code_time(
        :descr('content api'),
        :coderef(
            {
                for %setup<database>.keys -> $record {
                    my %content;

                    my $pid    = %setup<database>{$record}<id>;
                    my $seouri = %setup<database>{$record}<seouri>;

                    next if $pid.UInt ∈ $thlp.ethereumpids ||
                        %setup<database>{$record}<allowpage> == 1;

                    my %details = get_page_details(
                        :fp(
                            sprintf(
                                "%s/%s/%s.txt",
                                $tcnt.cntpath,
                                $a_default.get_name.lc,
                                $pid,
                            )
                        )
                    );

                    %content = get_addon_content(
                        :addon($a_default),
                        :seouri($seouri),
                        :page(False)
                    );

                    is(
                        $ctrl.sharedobj<mb64obj>.decode-str(%content<component>),
                        %details<data>,
                        'component content for ' ~ $seouri
                    );

                    %content = get_addon_content(
                        :addon($a_allow),
                        :seouri($pid),
                        :page(False)
                    );

                    is(
                        $ctrl.sharedobj<mb64obj>.decode-str(%content<component>),
                        %details<data>,
                        'component content for ' ~ $pid
                    );

                    %content = get_addon_content(
                        :addon($a_default),
                        :seouri($pid),
                        :page(False)
                    );

                    %details = get_page_details(:fp($$err_response));

                    is(
                        $ctrl.sharedobj<mb64obj>.decode-str(%content<component>),
                        %details<data>,
                        'component content for ' ~ $pid ~ ' if not allowed (error content)'
                    );
                }
            }
        ),
    );
}, 'Check embedded content via API';

# Check embedded content via stand alone pages
subtest {
    plan 28;

    my $a_default = Pheix::Addons::Embedded::User.new(:ctrl($ctrl));
    my $a_allow   = Pheix::Addons::Embedded::User.new(:ctrl($ctrl), :allowid(True));
    my %setup     = $a_default.jsonobj.get_entire_config;

    $thlp.get_code_time(
        :descr('content pages'),
        :coderef(
            {
                for %setup<database>.keys.sort -> $record {
                    my @fcgidata;
                    my $pid    = %setup<database>{$record}<id>;
                    my $seouri = %setup<database>{$record}<seouri>;

                    next if %setup<database>{$record}<allowpage> == 0;

                    my %details = get_page_details(
                        :fp(
                            sprintf(
                                "%s/%s/%s.txt",
                                $tcnt.cntpath,
                                $a_default.get_name.lc,
                                $pid
                            )
                        ),
                        :modify(($pid.UInt ∈ $thlp.nomodpids) ?? False !! True)
                    );

                    $ctrl.sharedobj<fastcgi>.reset;
                    get_addon_content(:addon($a_default), :seouri($seouri));

                    @fcgidata = $ctrl.sharedobj<fastcgi>.fetch.split("\n\n", 2);

                    is(
                        @fcgidata.tail,
                        %details<data>,
                        'page content for ' ~ $seouri
                    );

                    $ctrl.sharedobj<fastcgi>.reset;
                    get_addon_content(:addon($a_allow), :seouri($pid));

                    @fcgidata = $ctrl.sharedobj<fastcgi>.fetch.split("\n\n", 2);

                    ok $tcnt.checkheader(
                        :header(@fcgidata.head),
                        :status(200)
                    ), 'page HTTP status for ' ~ $pid ~ ' OK';

                    is(
                        @fcgidata.tail,
                        %details<data>,
                        'page content for ' ~ $pid
                    );

                    $ctrl.sharedobj<fastcgi>.reset;
                    get_addon_content(:addon($a_default), :seouri($pid));

                    @fcgidata = $ctrl.sharedobj<fastcgi>.fetch.split("\n\n", 2);

                    ok $tcnt.checkheader(
                        :header(@fcgidata.head),
                        :status(404)
                    ), 'page content for ' ~ $pid ~ ' if not allowed (404)';
                }
            }
        )
    );
}, 'Check embedded content via stand alone pages';

subtest {
    plan 1;

    ok $ctrl.sharedobj<logrobj>.remove_all, 'drop log database';
}, 'Clear logs';

done-testing;

sub get_addon_content(
    Pheix::Addons::Embedded::User :$addon,
    Str  :$seouri!,
    Bool :$page = True
) returns Hash {
    my %content;

    my Str $pg = $page ?? 'page/' !! q{};
    my %match  =
        seouri     => $seouri,
        details    => %(path => sprintf("/embedded/%s%s", $pg, $seouri)),
        controller => 'Pheix::Addons::Embedded::User',
        action     => $page ?? 'browse_api' !! 'browse_seo_api',
    ;

    %match<page> = 'page' if $page;

    if $page {
        $addon.browse(:tick(1), :match(%match));
    }
    else {
        %content = $addon.browse_seo_api(
            :tick(1),
            :match(%match),
            :sharedobj($ctrl.sharedobj)
        );
    }

    return %content
}

sub get_page_details(Str :$fp, Bool :$modify = True) returns Hash {
    my %ret = data => q{}, tokens => [];

    my %subst =
        nl => q{},
        vl => '&VerticalLine;'
    ;

    if $fp.IO.e {
        %ret<data> =  $fp.IO.slurp;
        %ret<data> ~~ m:g/ 'props.' (<[a..z_]>+) { %ret<tokens>.push($0) if $0 }/;
        if $modify {
            %ret<data> ~~ s:g/^^<[\s]>+//;
            %ret<data> ~~ s:g/^^<[\s]>+$$//;
            %ret<data> ~~ s:g/<[\r\n]>+//;
            %ret<data> ~~ s:g/<[|]>+/%subst<vl>/;
        }
    }

    %ret;
}

$thlp.time_stats(:module('Pheix::Addons::Embedded::User'));
