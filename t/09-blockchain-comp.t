# Specific dataset deployment
# PHEIXTESTENGINE=1 PHEIXDEBUG=1 PHEIXDEBUGLEVEL=0 raku ./t/09-blockchain-comp.t local deploy ./conf/_pages/embeddeduser/1725901939.txt embeddeduser/ true true

use v6.d;
use Test;
use lib 'lib';

use Net::Ethereum;
use Pheix::Model::Database::Access;
use Pheix::Model::JSON;
use Pheix::Test::Blockchain;
use Pheix::Test::BlockchainComp::Helpers;
use Pheix::Utils;
use Pheix::View::Pages;

use Pheix::Model::Database::Blockchain::Explorer;

plan 5;

constant localtab   = 'tst_table';
constant tstobj     = Pheix::Test::Blockchain.new(:locstorage(localtab));
constant debug      = tstobj.debug;
constant debuglevel = tstobj.debuglevel;
constant fulltest   = tstobj.fulltest;
constant pgdataset  = 7;

constant testnet  =
    ((@*ARGS[0].defined && @*ARGS[0] (elem) <ropsten rinkeby goerli sepolia holesky>) || tstobj.testnet) ??
        @*ARGS[0] // tstobj.testnet !!
            localtab;

constant dsdeploy = ((@*ARGS[1] // q{}) eq 'deploy') ?? True !! False;
constant ds       = (@*ARGS[2] && @*ARGS[2].IO.e) ?? @*ARGS[2] !! Str;
constant tabprefx = ((@*ARGS[3] // q{}) ne q{}) ?? @*ARGS[3] !! Str;
constant skipurge = ((@*ARGS[4] // q{}) ~~ m:i/true/) ?? True !! False;
constant target   = ((@*ARGS[5] // q{}) ~~ m:i/true/) ?? True !! False;
constant dictsz   = ((@*ARGS[6] // q{}) ~~ m:i/^<[\d]>+$/) ?? @*ARGS[6].Int !! 200000;
constant helpobj  = Pheix::Test::BlockchainComp::Helpers.new(
    :testnet(testnet),
    :localtab(localtab),
    :tstobj(tstobj)
);

my @latest_dataset_transactions;
my $latest_dataset_content;

my Str  $targettb = dsdeploy ?? target_tab_name(:name(ds), :pfx(tabprefx)) !! localtab;

my UInt $frlen    = (testnet ne localtab && !dsdeploy) ?? 2048 !! (dsdeploy ?? 16384 !! 4096);
my UInt $tab_size = 10;
my Int  $tbot_inx = fulltest ?? -1 !! 0;
my Int  $ttop_inx = fulltest ?? -1 !! (testnet eq localtab ?? 10 !! 2);
my Str  $dspath   = './t/data/datasets';
my Str  $utf8smpl = './t/data/utf8demo.txt';
my Str  $diagnet  = testnet eq localtab ?? 'local PoA net' !! testnet ~ ' net';
my Str  $tab      = testnet eq localtab ?? localtab !! testnet ~ '_storage';
my      @farr     = <domains ip_addrs browsers resolut pages countries payload>;
my      @tabs     = <table_1 table_2 table_3>;

my Any  $dbobj = Pheix::Model::Database::Access.new(
    :table($tab),
    :fields(@farr),
    :test(True),
    :debug(debug),
    :utils(Pheix::Utils.new(:test(True), :lzwsz(dictsz))),
);

if !tstobj.pte {
    diag('PHEIXTESTENGINE was not set');
    skip-rest('Blockchain test should be run via Pheix test engine');
    exit;
}

if $dbobj && $dbobj.dbswitch == 1 {
    $dbobj = helpobj.dbobj_tweak(:$dbobj, :testnet(testnet));

    helpobj.diag(:l(tstobj.logwrn), :m(sprintf("Run %s tests on %s", (tstobj.statictest ?? 'static' !! 'dynamic'), $diagnet)));
    helpobj.diag(:l(tstobj.logwrn), :m(sprintf("Compression dict size %d", $dbobj.chainobj.cmpobj.dictsize)));
}

# Blockchain content fetch scenarios for raw_pg method
my $subtest_0 = subtest(
    sprintf("Blockchain content fetch scenarios for raw_pg method %s, tab: %s", $diagnet, $tab),
    sub {
        plan 6;

        my Str $dataset = get_datasets.sort[pgdataset];
        my Str $content = $dataset.IO.slurp;

        my $pobj   = Pheix::View::Pages.new(:test(True), :jsonobj(Pheix::Model::JSON.new), :utilobj(Pheix::Utils.new));
        my $dbobj_ = Pheix::Model::Database::Access.new(
            :table($tab),
            :fields(['data']),
            :debug(debug),
#           :utils(Pheix::Utils.new(:test(True), :lzwsz(dictsz))),
        );

        $dbobj_ = helpobj.dbobj_tweak(:dbobj($dbobj_), :testnet(testnet))
            if $dbobj_ && $dbobj_.dbswitch == 1;

        subtest {
            plan 14;

            if $dbobj_ && !dsdeploy {
                if $dbobj_.dbswitch == 1 {
                    for <uncompressed encoded> -> $type {
                        is $dbobj.chainobj.unlock_account, True, tstobj.bm(
                            :ts('s0: account unlock'),
                            :return(True)
                        );

                        if check_existance(:dbobj($dbobj_), :t($tab), :skip) {
                            ok tstobj.drop_table(:dbobj($dbobj_), :t($tab)), tstobj.bm(
                                :t($tab),
                                :ts('s0: drop table'),
                                :return(True)
                            );
                        }
                        else {
                            my $tabindex = $dbobj_.chainobj.table_index(:t($tab));

                            is $tabindex, -1, tstobj.bm(
                                :t($tab),
                                :ts(sprintf("s0: %s table index %d", $tab, $tabindex)),
                                :return(True)
                            );
                        }

                        ok $dbobj_.chainobj.table_create(:t($tab),:comp(False)).keys, tstobj.bm(
                            :t($tab),
                            :ts('s0: create table'),
                            :return(True)
                        );

                        ok check_existance(:dbobj($dbobj_), :t($tab)), tstobj.bm(
                            :t($tab),
                            :ts('s0: table exists'),
                            :return(True)
                        );

                        my Str $content = get_datasets[pgdataset].IO.slurp;

                        if ($type ~~ /encoded/) {
                            $content = $dbobj_.chainobj.cmpobj.compress(:data($content)).decode("iso-8859-1");;
                        }

                        my @data_frames = helpobj.text_to_frames(:text($content));

                        my @txs;

                        # my $insert_params = {id => $index + 1, data => $frame};
                        # $dbobj_.insert($insert_params);
                        for @data_frames.kv -> $index, $frame {
                            my %hash = $dbobj_.chainobj.row_insert(
                                :t($tab),
                                :data({id => $index + 1, data => $frame}),
                                :comp(False),
                                :waittx(False),
                            );

                            @txs.push(%hash<txhash>) if %hash<status>;
                        }

                        ok @txs && @txs.elems == @data_frames.elems && $dbobj_.chainobj.wait_for_transactions(:hashes(@txs)),
                            tstobj.bm(
                                :ts(sprintf("s0: save %d records", @txs.elems)),
                                :return(True)
                            );

                        is $pobj.raw_pg(
                            :table($tab),
                            :fields(['data']),
                            :test(True)
                        ), $content, tstobj.bm(:ts(sprintf("s0: validate %s data", $type)), :return(True));

                        ok tstobj.drop_table(:dbobj($dbobj_), :t($tab)),
                            tstobj.bm(:t($tab), :ts('s0: drop table'), :return(True));
                    }
                }
                else {
                    skip-rest('ethereum node is not available');
                }
            }
            else {
               skip-rest(
                dsdeploy ??
                    'cmp flag setter/getter test on deploy' !!
                        'database object is not available'
               );
            }
        }, sprintf("full table uncompressed and encoded content from %s", $dataset);

        for <lzw bzip> -> $algo {
            subtest {
                plan 7;

                if $dbobj_ && !dsdeploy {
                    if $dbobj_.dbswitch == 1 {
                        $dbobj_.chainobj.config<compression>        = $algo;
                        $dbobj_.chainobj.cmpobj.config<compression> = $algo;

                        helpobj.diag(
                            :l(tstobj.loginf),
                            :m(sprintf("testing compression via %s", $dbobj_.chainobj.config<compression>))
                        );

                        is $dbobj.chainobj.unlock_account, True, tstobj.bm(
                            :ts('s0: account unlock'),
                            :return(True)
                        );

                        if check_existance(:dbobj($dbobj_), :t($tab), :skip) {
                            ok tstobj.drop_table(:dbobj($dbobj_), :t($tab)), tstobj.bm(
                                :t($tab),
                                :ts('s0: drop table'),
                                :return(True)
                            );
                        }
                        else {
                            my $tabindex = $dbobj_.chainobj.table_index(:t($tab));

                            is $tabindex, -1, tstobj.bm(
                                :t($tab),
                                :ts(sprintf("s0: %s table index %d", $tab, $tabindex)),
                                :return(True)
                            );
                        }

                        ok $dbobj_.chainobj.table_create(:t($tab),:comp(True)).keys, tstobj.bm(
                            :t($tab),
                            :ts('s0: create table'),
                            :return(True)
                        );

                        ok check_existance(:dbobj($dbobj_), :t($tab)), tstobj.bm(
                            :t($tab),
                            :ts('s0: table exists'),
                            :return(True)
                        );

                        my buf8 $contentbuf = $dbobj_.chainobj.cmpobj.compress(:data($content));

                        my @data_frames;

                        if $dbobj_.chainobj.cmpobj.get_algo eq 'bzip2' {
                            @data_frames = helpobj.buffer_to_frames(:buf($contentbuf));
                        }
                        else {
                            @data_frames = helpobj.text_to_frames(:text($contentbuf.decode));
                        }

                        my @txs;

                        for @data_frames.kv -> $index, $frame {
                            my $rawdata;

                            if $dbobj_.chainobj.cmpobj.get_algo eq 'bzip2' {
                                $rawdata = $dbobj_.chainobj.ethobj.buf2hex($frame);
                            }
                            else {
                                $rawdata = $frame;
                            }

                            # my $insert_params = {id => $index + 1, data => $rawdata};
                            # $dbobj_.insert($insert_params);
                            my %hash = $dbobj_.chainobj.row_insert(
                                :t($tab),
                                :data({id => $index + 1, data => $rawdata}),
                                :comp(False),
                                :waittx(False),
                            );

                            @txs.push(%hash<txhash>) if %hash<status>;
                        }

                        ok @txs && @txs.elems == @data_frames.elems && $dbobj_.chainobj.wait_for_transactions(:hashes(@txs)),
                            tstobj.bm(
                                :ts(sprintf("s0: save %d records", @txs.elems)),
                                :return(True)
                            );

                        is $pobj.raw_pg(
                            :table($tab),
                            :fields(['data']),
                            :database($dbobj_),
                            :test(True)
                        ), $content, tstobj.bm(:ts('s0: validate data'), :return(True));

                        ok tstobj.drop_table(:dbobj($dbobj_), :t($tab)),
                            tstobj.bm(:t($tab), :ts('s0: drop table'), :return(True));
                    }
                    else {
                        skip-rest('ethereum node is not available');
                    }
                }
                else {
                   skip-rest(
                    dsdeploy ??
                        'cmp flag setter/getter test on deploy' !!
                            'database object is not available'
                   );
                }
            }, sprintf("full table compressed content from %s", $dataset);

            subtest {
                plan 8;

                if $dbobj_ && !dsdeploy {
                    if $dbobj_.dbswitch == 1 {
                        $dbobj_.chainobj.config<compression>        = $algo;
                        $dbobj_.chainobj.cmpobj.config<compression> = $algo;

                        helpobj.diag(
                            :l(tstobj.loginf),
                            :m(sprintf("testing compression via %s", $dbobj_.chainobj.config<compression>))
                        );

                        is $dbobj.chainobj.unlock_account, True, tstobj.bm(
                            :ts('s0: account unlock'),
                            :return(True)
                        );

                        if check_existance(:dbobj($dbobj_), :t($tab), :skip) {
                            ok tstobj.drop_table(:dbobj($dbobj_), :t($tab)), tstobj.bm(
                                :t($tab),
                                :ts('s0: drop table'),
                                :return(True)
                            );
                        }
                        else {
                            my $tabindex = $dbobj_.chainobj.table_index(:t($tab));

                            is $tabindex, -1, tstobj.bm(
                                :t($tab),
                                :ts(sprintf("s0: %s table index %d", $tab, $tabindex)),
                                :return(True)
                            );
                        }

                        ok $dbobj_.chainobj.table_create(:t($tab),:comp(False)).keys, tstobj.bm(
                            :t($tab),
                            :ts('s0: create table'),
                            :return(True)
                        );

                        ok check_existance(:dbobj($dbobj_), :t($tab)), tstobj.bm(
                            :t($tab),
                            :ts('s0: table exists'),
                            :return(True)
                        );

                        my @data_frames = helpobj.text_to_frames(:text($content));
                        my @skipindxs   = helpobj.get_skip_indexes(:mode('rand'), :blocks(@data_frames.elems));

                        @skipindxs.pop if @data_frames.elems == @skipindxs.elems;

                        ok @skipindxs.elems,
                            tstobj.bm(
                                :ts(sprintf("s0: skip %d rows %s", @skipindxs.elems, @skipindxs.gist)),
                                :return(True)
                            );

                        my @txs;

                        for @data_frames.kv -> $index, $frame {
                            my $comp = False;

                            if @skipindxs.map({$_ if $index == $_}).elems == 0 {
                                $comp = True;
                            }

                            # my $insert_params = {id => $index + 1, data => $frame, comp => $comp};
                            # $dbobj_.insert($insert_params);
                            my %hash = $dbobj_.chainobj.row_insert(
                                :t($tab),
                                :data({id => $index + 1, data => $frame}),
                                :comp($comp),
                                :waittx(False),
                            );

                            @txs.push(%hash<txhash>) if %hash<status>;
                        }

                        ok @txs && @txs.elems == @data_frames.elems && $dbobj_.chainobj.wait_for_transactions(:hashes(@txs)),
                            tstobj.bm(
                                :ts(sprintf("s0: save %d records", @txs.elems)),
                                :return(True)
                            );

                        is $pobj.raw_pg(
                            :table($tab),
                            :fields(['data']),
                            :database($dbobj_),
                            :test(True)
                        ), $content, tstobj.bm(:ts('s0: validate content'), :return(True));

                        ok tstobj.drop_table(:dbobj($dbobj_), :t($tab)),
                            tstobj.bm(:t($tab), :ts('s0: drop table'), :return(True));
                    }
                    else {
                        skip-rest('ethereum node is not available');
                    }
                }
                else {
                   skip-rest(
                    dsdeploy ??
                        'cmp flag setter/getter test on deploy' !!
                            'database object is not available'
                   );
                }
            }, sprintf("randomly compressed content from %s", $dataset);
        }

        if $dbobj_ && $dbobj_.dbswitch == 1 {
            $dbobj.chainobj.sgnlog.push($dbobj_.chainobj.sgnlog.Slip);
            $dbobj.chainobj.nonce = $dbobj_.chainobj.nonce;

            todo 'possible non-signer mode', 1;
            ok helpobj.flush_signing_session(:sgnlog($dbobj.chainobj.sgnlog)),
                tstobj.bm(:ts('s0: save signing log'), :return(True));
        }
        else {
            skip-rest('no active blockchain database');
        }
    }
).Bool;

# Compression flag setter and getter loop test
my $subtest_1 = subtest(
    'Compression flag setter and getter loop test on ' ~ $diagnet ~ ', tab: ' ~ $tab,
    sub {
        plan 4;
        if $dbobj && !dsdeploy {
            if $dbobj.dbswitch == 1 {
                is $dbobj.chainobj.unlock_account, True, tstobj.bm(
                    :ts('s1: account unlock'),
                    :return(True)
                );

                my $tabs_on_blockchain = $dbobj.chainobj.count_tables;

                if $tabs_on_blockchain {
                    is tstobj.drop_all(:dbobj($dbobj)), True, tstobj.bm(
                        :ts('s1: drop ' ~ $tabs_on_blockchain ~ ' tabs'),
                        :return(True)
                    );
                }
                else {
                    ok $tabs_on_blockchain == 0,
                        tstobj.bm(:ts('s1: empty blockchain'), :return(True));
                }

                is ($dbobj.chainobj.table_debug(:waittx(True)))<status>,
                    True,
                        tstobj.bm(:ts('s1: init internal db'), :return(True));

                is(
                    random_set_get_cmp_test(
                        :dbobj($dbobj),
                        :iters(5),
                        :debug(debug)
                    ),
                    True,
                    tstobj.bm(:ts('s1: random cmp set/get'), :return(True)),
                );
            }
            else {
                skip-rest('ethereum node is not available');
            }
        }
        else {
           skip-rest(
            dsdeploy ??
                'cmp flag setter/getter test on deploy' !!
                    'database object is not available'
           );
        }
    }
).Bool;

# Tables with independent records test
my $subtest_2 = subtest(
    'Tables with independent records test on ' ~ $diagnet ~ ', tab: ' ~ $tab,
    sub {
        plan 24;
        if $dbobj && !dsdeploy {
            if $dbobj.dbswitch == 1 {
                is $dbobj.chainobj.unlock_account, True,
                    tstobj.bm(:ts('s2: account unlock'), :return(True));

                my $tabs_on_blockchain = $dbobj.chainobj.count_tables;

                if $tabs_on_blockchain {
                    is tstobj.drop_all(:dbobj($dbobj)), True, tstobj.bm(
                        :ts('s1: drop ' ~ $tabs_on_blockchain ~ ' tabs'),
                        :return(True)
                    );
                }
                else {
                    ok $tabs_on_blockchain == 0,
                        tstobj.bm(:ts('s1: empty blockchain'), :return(True));
                }

                for ^10 {
                    my Str $f = helpobj.generate_content(:words(tstobj.genwords));

                    ok $f.chars > 100, tstobj.bm(
                        :ts('s2: generate utf8 data ' ~ $_),
                        :return(True)
                    );

                    $dbobj.chainobj.cmpobj.compress(:data($f));

                    my int $r = $dbobj.chainobj.cmpobj.get_ratio;

                    ok $r >= 0, tstobj.bm(
                        :ts('s2: utf8 data ' ~ $_ ~ ' cmp ratio ' ~  $r ~ q{%}),
                        :return(True)
                    );
                }

                my %database  = helpobj.create_database(
                    :dbobj($dbobj),
                    :tabnames(@tabs),
                    :fixedrows($tab_size)
                );

                helpobj.debug_database(:database(%database));

                is %database<status>, True, tstobj.bm(
                    :ts('s2: create database (' ~ %database<db>.elems ~ ' tabs)'),
                    :return(True)
                );

                my @blockchaindb = helpobj.validate_database(:dbobj($dbobj));

                is-deeply @(%database<db>), @blockchaindb, tstobj.bm(
                    :ts('s2: validate database'),
                    :return(True)
                );
            }
            else {
                skip-rest('ethereum node is not available');
            }
        }
        else {
            skip-rest(
             dsdeploy ??
                 'tabs with independent records test on deploy' !!
                     'database object is not available'
            );
        }
    }
).Bool;

# Tables with content datasets test (LZW/BZIP2)
my $subtest_3 = subtest(
    sprintf("Tables with content datasets test on %s, tab: %s (LZW/BZIP2)", $diagnet, dsdeploy ?? $targettb !! $tab),
    sub {
        plan dsdeploy ?? 5 !! 10;

        my @algos = <lzw bzip2>;

        #@algos.pop if dsdeploy;
        @algos.shift if dsdeploy;

        for @algos -> $compression_algo {
            my Any $dbobj_ = Pheix::Model::Database::Access.new(
                :table(dsdeploy ?? $targettb !! $tab),
                :fields(@('rowdata')),
                :test(True),
                :debug(debug),
                :utils(Pheix::Utils.new(:test(True), :lzwsz(dictsz)))
            );

            $dbobj_ = helpobj.dbobj_tweak(:dbobj($dbobj_), :testnet(testnet));

            if $dbobj_ {
                if $dbobj_.dbswitch == 1 {
                    $dbobj_.chainobj.config<compression> = $compression_algo;
                    $dbobj_.chainobj.cmpobj.config<compression> = $compression_algo;

                    if dsdeploy {
                        skip 'do not unlock account on dataset deployment', 1;
                    }
                    else {
                        is $dbobj_.chainobj.unlock_account, True, tstobj.bm(
                            :ts('s3: account unlock'),
                            :return(True)
                        );
                    }

                    my $tabs_on_blockchain = $dbobj_.chainobj.count_tables;

                    if $tabs_on_blockchain {
                        if skipurge {
                            ok skipurge, 'do not clean blockchain';
                        }
                        else {
                            is tstobj.drop_all(:dbobj($dbobj_)), True, tstobj.bm(
                                :ts('s3: drop ' ~ $tabs_on_blockchain ~ ' tabs'),
                                :return(True)
                            );
                        }
                    }
                    else {
                        ok $tabs_on_blockchain == 0,
                            tstobj.bm(:ts('s3: empty blockchain'), :return(True));
                    }

                    my $cmp_ds_details = compress_datasets(
                        :dbobj($dbobj_),
                        :comp(True),
                    );

                    ok $cmp_ds_details<status>,
                        tstobj.bm(:ts(sprintf("s3: store %s comp datasets", $compression_algo)), :return(True));

                    if skipurge {
                        ok skipurge, 'do not validate datasets';
                    }
                    else {
                        is(
                            decompress_datasets(
                                :dbobj($dbobj_),
                                :datasets($cmp_ds_details<processed_data>)
                            ), True,
                            tstobj.bm(:ts('s3: validate datasets'), :return(True)),
                        );
                    }

                    if !dsdeploy {
                        $dbobj.chainobj.sgnlog.push($dbobj_.chainobj.sgnlog.Slip);
                        $dbobj.chainobj.nonce = $dbobj_.chainobj.nonce;

                        todo 'possible non-signer mode', 1;
                        ok helpobj.flush_signing_session(:sgnlog($dbobj.chainobj.sgnlog)),
                            tstobj.bm(:ts('s3: save signing log'), :return(True)),
                    }
                    else {
                        skip 'do not save signing log on dataset deployment', 1;
                    }
                }
                else {
                    skip-rest('ethereum node is not available');
                }
            }
            else {
               skip-rest('database object is not available');
            }
        }
    }
).Bool;

# Blockchain explorer
my $subtest_4 = subtest(
    sprintf("Blockchain explorer on %s", $diagnet),
    sub {
        plan 6;

        if $dbobj && !dsdeploy && @latest_dataset_transactions.elems {
            if $dbobj.dbswitch == 1 {
                is $dbobj.chainobj.unlock_account, True,
                    tstobj.bm(:ts('s4: account unlock'), :return(True));

                my $explorer = Pheix::Model::Database::Blockchain::Explorer.new(:blockchainobj($dbobj.chainobj));

                my $data = $explorer.unmarshal_trx_input(:fname('insert'), :txhash(@latest_dataset_transactions.head));

                ok $data ~~ Hash && $data.keys, tstobj.bm(:ts('s4: unmarshalled data'), :return(True));
                ok $data<rowdata> ~~ Buf, tstobj.bm(:ts('s4: row data as Buf'), :return(True));

                my @rows = $explorer.distributed_select_all(:fname('insert'), :hashes(@latest_dataset_transactions));

                ok @rows && @rows.elems, tstobj.bm(:ts('s4: select distributed data'), :return(True));

                my $datasetbuffer = $dbobj.chainobj.ethobj.hex2buf(@rows.join(q{}));

                ok $datasetbuffer.List.elems, tstobj.bm(:ts('s4: latest dataset buffer'), :return(True));

                my $dataset = $dbobj.chainobj.cmpobj.decompress(:data($datasetbuffer));

                is $latest_dataset_content, $dataset, tstobj.bm(:ts('s4: latest dataset validation'), :return(True));
            }
            else {
                skip-rest('ethereum node is not available');
            }
        }
        else {
           skip-rest(
            dsdeploy ??
                'cmp flag setter/getter test on deploy' !!
                    (!$dbobj ?? 'database object is not available' !! 'no latest transactions')
           );
        }
    }
).Bool;

done-testing;

# Datasets test subroutines
sub compress_datasets(
         :$dbobj!,
    Bool :$comp!,
) returns Hash {
    my @ds = (!ds && $ttop_inx >= 0 && $tbot_inx >= 0 && $tbot_inx <= $ttop_inx) ??
                 get_datasets[$tbot_inx..$ttop_inx] !!
                     get_datasets(:ds(ds));

    my @actual_processed_data;

    for @ds.kv -> $inx, $ds {
        my Str $cnt = $ds.IO.slurp if $ds.IO.f;
        if $cnt {
            my Str $data;

            my Str $tnam = $ds.IO.basename;

            if (tabprefx && dsdeploy && ds) {
                (my $truefname = $ds.IO.basename) ~~ s:g:i/ \. <{$ds.IO.extension}> //;
                $tnam = tabprefx ~ $truefname;
            }
            else {
                $tnam ~~ s:g:i/\./_/;
            }

            my Bool $actcmp = False;

            if $comp {
                my $buff  = $dbobj.chainobj.cmpobj.compress(:data($cnt));
                my int $r = $dbobj.chainobj.cmpobj.get_ratio;
                my str $a = $dbobj.chainobj.cmpobj.get_algo;

                my Str $dcmp = $dbobj.chainobj.cmpobj.decompress(:data($buff));

                if $dcmp ne $cnt {
                    helpobj.diag(:l(tstobj.logerr), :m('comp<->decomp failure ' ~ $ds));

                    return {status => False};
                }
                else {
                    helpobj.diag(:l(tstobj.loginf), :m('comp<->decomp ok ' ~ $ds));
                }

                if $r < 0 {
                    $data = $cnt;
                }
                else {
                    $actcmp = True;

                    if $dbobj.chainobj.config<compression> eq 'bzip2' {
                        #$data = $buff.decode("iso-8859-1");
                        $data = $dbobj.chainobj.ethobj.buf2hex($buff);
                    }
                    else {
                        $data = $buff.decode;
                    }

                    helpobj.diag(:l(tstobj.loginf), :m(sprintf("'\{%d\} %s comp ratio %d%% (%s)", $inx, $tnam, $r, $a)))
                }
            }
            else {
                $data = $cnt;
            }

            my @frms = dataset_to_frames(:dataset($data));

            if @frms {
                $dbobj.chainobj.unlock_account unless dsdeploy;

                my %newtab_ret     = $dbobj.chainobj.table_create(:t($tnam),:comp($actcmp));
                my Bool $tabexists = check_existance(:$dbobj, :t($tnam));

                if $tabexists {
                    my @processed;
                    my @skipped;
                    my @txs;

                    helpobj.diag(:l(tstobj.loginf), :m('tab ' ~ $tnam ~ ' is created'));

                    for @frms.kv -> $finx, $f {
                        my %data = rowdata => $f;

                        my %rhash = $dbobj.chainobj.row_insert(
                            :t($tnam),
                            :data(%data),
                            :comp(False),
                            :waittx(False),
                        );

                        if %rhash<status> && %rhash<error> eq q{} {
                            @txs.push(%rhash<txhash>);
                            @processed.push($f);
                        }
                        else {
                            @skipped.push(sprintf("%d/%d: %s", $finx + 1, @frms.elems, %rhash<error>));
                        }
                    }

                    if @frms.elems != @txs.elems && debug {
                        helpobj.diag(:l(tstobj.logwrn), :m(sprintf("frames for %s \{%d, %s\} were skipped:", $tnam, $inx, $ds)));

                        for @skipped -> $skipped_dataset {
                            helpobj.diag(:l(tstobj.logmsg), :m(sprintf("\t%s", $skipped_dataset)));
                        }
                    }

                    @actual_processed_data.push({
                        file    => $ds,
                        frames  => @processed,
                        skipped => @frms.elems != @txs.elems ?? 1 !! 0
                    });

                    if !$dbobj.chainobj.wait_for_transactions(:hashes(@txs)) && @txs {
                        for @txs.kv -> $index, $txhash {
                            my %h =
                                $dbobj.chainobj.ethobj.
                                    eth_getTransactionReceipt($txhash);

                            if %h<status>:!exists || %h<status> == 0 {
                                helpobj.diag(:l(tstobj.logerr), :m(sprintf("%s trx %s failure", &?ROUTINE.name, $txhash)));

                                helpobj.trace_transaction(:dbobj($dbobj), :trx($txhash));
                            }
                        }

                        return {status => False};
                    }
                    else {
                        @latest_dataset_transactions = @txs;
                        $latest_dataset_content      = $cnt;

                        if tabprefx && dsdeploy && ds {
                            helpobj.diag(:l(tstobj.logdpl), :m(@txs.join(q{,})));
                        }
                    }
                }
                else {
                    helpobj.diag(
                        :l(tstobj.logerr),
                        :m(sprintf("%s table existence %s : %s", $tnam, ~$tabexists, %newtab_ret.gist))
                    );

                    if (%newtab_ret<txhash>:exists) && %newtab_ret<txhash> !~~ m:i/^ 0x<[0]>**64 $/ {
                        helpobj.trace_transaction(:dbobj($dbobj), :trx(%newtab_ret<txhash>));
                    }

                    return {status => False};
                }
            }
            else {
                helpobj.diag(:l(tstobj.logerr), :m('split to frames is failed!'));

                return {status => False};
            }
        }
        else {
            helpobj.diag(:l(tstobj.logerr), :m('dataset is empty!'));

            return {status => False};
        }
    }

    return {status => True, processed_data => @actual_processed_data};
}

# Datasets test subroutines
sub decompress_datasets(
         :$dbobj!,
         :$datasets!
) returns Bool {
    for @$datasets -> $dataset_details {
        my Int $r   = 0;
        my $file    = $dataset_details<file>;
        my $frames  = $dataset_details<frames>;
        my $skipped = $dataset_details<skipped>;
        my Str $cnt = $frames.join(q{});

        my Str $tnam = $file.IO.basename;

        if (tabprefx && dsdeploy && ds) {
            (my $truefname = $file.IO.basename) ~~ s:g:i/ \. <{$file.IO.extension}> //;
            $tnam = tabprefx ~ $truefname;
        }
        else {
            $tnam ~~ s:g:i/\./_/;
        }

        $dbobj.chainobj.unlock_account;

        if check_existance(:$dbobj, :t($tnam)) {
            my @data;
            my UInt $rows = $dbobj.chainobj.count_rows(:t($tnam));

            for ^$rows -> $rinx {
                my $rowid =
                    $dbobj.chainobj.get_id_byindex(:t($tnam),:index($rinx));
                if $rowid > 0 {
                    my %row = $dbobj.chainobj.select(
                        :t($tnam),
                        :id($rowid),
                        :comp(False)
                    );
                    my $ts = $dbobj.fields.map({ %row{$_} }) if %row;

                    @data.push($ts);
                }
            }

            if @data {
                my Str  $chck;
                my Bool $ic = $dbobj.chainobj.is_tab_compressed(:t($tnam));

                if $ic && !$skipped {
                    my Str $data = @data.join(q{});

                    my $cmpdata;
                    my $cmpcnt;

                    if $dbobj.chainobj.config<compression> eq 'bzip2' {
                        #$cmpdata = $data.encode("iso-8859-1");
                        #$cmpcnt  = $cnt.encode("iso-8859-1");
                        $cmpdata = $dbobj.chainobj.ethobj.hex2buf($data);
                        $cmpcnt  = $dbobj.chainobj.ethobj.hex2buf($cnt);
                    }
                    else {
                        $cmpdata = Buf[uint8].new($data.encode);
                        $cmpcnt  = Buf[uint8].new($cnt.encode);
                    }

                    $chck = $dbobj.chainobj.cmpobj.decompress(:data($cmpdata));
                    $cnt  = $dbobj.chainobj.cmpobj.decompress(:data($cmpcnt));
                }
                else {
                    $chck = @data.join(q{});
                }

                if $chck eq $cnt {
                    helpobj.diag(:l(tstobj.loginf), :m('{' ~ $file ~ '} ' ~ ($ic ?? 'compressed' !! 'native') ~ ' tab ' ~ $tnam ~ ' content looks good'));
                }
                else {
                    helpobj.diag(:l(tstobj.logerr), :m($tnam  ~ ' content failure'));

                    return False;
                }
            }
            else {
                if $cnt {
                    helpobj.diag(:l(tstobj.logerr), :m('table is empty!'));

                    return False;
                }
            }
        }
        else {
            helpobj.diag(:l(tstobj.logerr), :m($tnam ~ ' is not existed!'));

            return False;
        }
    }

    return True;
}

sub dataset_to_frames(Str :$dataset) returns List {
    my @frames;
    if $dataset.defined && $dataset ne q{} {
        my $ff  = ($dataset.chars / $frlen).ceiling;
        my $hf  = $dataset.chars % $frlen;

        for ^$ff -> $index {
            if $index < $ff {
                @frames.push($dataset.substr($index * $frlen, $frlen));
            } else {
                @frames.push($dataset.substr($index * $frlen, $hf));
            }
        }
    }

    return @frames;
}

sub get_datasets(Str :$ds) returns List {
    my @files;

    return [$ds] if target && $ds && $ds.IO.e && $ds.IO.f;

    my $dir  = $dspath;
    my @todo = $dir.IO;

    while @todo {
        for @todo.pop.dir -> $path {
            if $ds {
                if $path.f && $path.Str eq $ds {
                    @files.push($path.Str);
                }
            }
            else {
                @files.push($path.Str) if $path.f;
            }

            @todo.push: $path if $path.d;
        }
    }

    return @files;
}

sub random_set_get_cmp_test(
         :$dbobj!,
    UInt :$iters!,
    Bool :$debug
) returns Bool {
    my Bool $rc   = True;
    my Bool $cmp  = True;
    my UInt $tabs = $dbobj.chainobj.count_tables;

    for ^$iters -> $iter {
        helpobj.diag(:l(tstobj.logdbg), :m('random_set_get_cmp_test: ' ~ ($iter + 1) ~ q{/} ~ $iters));

        my @states = (
            {
                t0 => False,
                r0 => False,
                r1 => False
            },
            {
                t1 => False,
                r0 => False,
                r1 => False
            },
            {
                t2 => False,
                r0 => False,
            }
        );

        my $rti = tstobj.statictest ?? $tabs !! $tabs.rand.Int;

        for ^$tabs -> $tinx {
            $dbobj.chainobj.unlock_account;
            my Str $tnam = $dbobj.chainobj.get_tabname_byindex(:index($tinx));

            if $tinx == $rti {
                if $dbobj.chainobj.set_tab_cmp(
                   :t($tnam),
                   :comp($cmp),
                   :waittx(True)
                ) {
                    my Str $tkey = 't' ~ $tinx;
                    @states[$tinx]{$tkey} = True;
                }
                else {
                    return False;
                }
            }

            my UInt $rows = $dbobj.chainobj.count_rows(:t($tnam));
            my $rtri      = tstobj.statictest ?? $rows !! $rows.rand.Int;

            for ^$rows -> $rinx {
                my $rowid =
                    $dbobj.chainobj.get_id_byindex(:t($tnam), :index($rinx));

                if $rowid > 0 && $rinx == $rtri {
                    if $dbobj.chainobj.set_row_cmp(
                       :t($tnam),
                       :id($rowid),
                       :comp($cmp),
                       :waittx(True)
                    ) {
                        my Str $rkey = 'r' ~ $rinx;
                        @states[$tinx]{$rkey} = True;
                    }
                    else {
                        return False;
                    }
                }
            }
        }

        @states.gist.say if debug;

        for @states.kv -> $index, $state {
            my Str  $tkey = 't' ~ $index;
            my Bool $scmp = $state{$tkey};
            my Str  $tnam = $dbobj.chainobj.get_tabname_byindex(:index($index));
            my Bool $tcmp = $dbobj.chainobj.is_tab_compressed(:t($tnam));

            if $tcmp != $scmp {
                return False;
            }
            else {
                my UInt $rows = $dbobj.chainobj.count_rows(:t($tnam));

                for ^$rows -> $rinx {
                    my $rowid =
                        $dbobj.chainobj.get_id_byindex(:t($tnam),:index($rinx));

                    if $rowid > 0 {
                        my Str  $rkey = 'r' ~ $rinx;

                        my Bool $rcmp = $dbobj.chainobj.is_row_compressed(
                            :t($tnam),
                            :id($rowid)
                        );
                        my Bool $srcmp = $state{$rkey};

                        if $rcmp != $srcmp {
                            return False;
                        }
                    }
                }
            }
        }

        if !reset_compression(:cmp(False),:debug($debug)) {
            return False;
        }
    }
    $rc;
}

sub reset_compression(Bool :$cmp, Bool :$debug) returns Bool {
    helpobj.diag(:l(tstobj.logdbg), :m('reset compression to ' ~ $cmp ~ ': application level'));

    for @tabs.kv -> $index, $tnam {
        if check_existance(:$dbobj, :t($tnam)) {
            if $dbobj.chainobj.is_tab_compressed(:t($tnam)) {
                $dbobj.chainobj.unlock_account;

                if !$dbobj.chainobj.set_tab_cmp(
                   :t($tnam),
                   :comp($cmp),
                   :waittx(True)
                ) {
                    return False;
                }
                else {
                    helpobj.diag(:l(tstobj.logdbg), :m('reset cmp ' ~ $tnam));
                }
            }

            my UInt $rows = $dbobj.chainobj.count_rows(:t($tnam));

            for ^$rows -> $rinx {
                my $rowid =
                    $dbobj.chainobj.get_id_byindex(:t($tnam),:index($rinx));

                if $rowid > 0 {
                    if $dbobj.chainobj.is_row_compressed(
                        :t($tnam),
                        :id($rowid)
                    ) {
                        if !$dbobj.chainobj.set_row_cmp(
                           :t($tnam),
                           :id($rowid),
                           :comp($cmp),
                           :waittx(True)
                        ) {
                            return False;
                        }
                        else {
                            helpobj.diag(:l(tstobj.logdbg), :m('reset cmp ' ~ $tnam ~ ' row ' ~ $rowid));
                        }
                    }
                }
            }
        }
    }

    return True;
}

sub target_tab_name(Str :$name, Str :$pfx) returns Str {
    my Str $rs;

    if ($name ~~ m:i/ '/' (<[\da..z\_\-]>+) '.' <[a .. z]>/) {
        $rs = $pfx ~ $0;
        helpobj.diag(:l(tstobj.logdpl), :m('Target table name to deploy: ' ~ $rs));
    }
    else {
        die 'Target table name construct failed';
    }

    $rs;
}

sub check_existance(Str :$t!, :$dbobj!, Bool :$skip = False) {
    return $dbobj.chainobj.table_exists(:$t) if $skip;

    for ^5 -> $iteration {
        return True if $dbobj.chainobj.table_exists(:$t);

        sleep(1);

        helpobj.diag(
            :l(tstobj.logwrn),
            :m(sprintf("wait %d sec(s) for table %s to be available",
                $iteration, $t))
        );
    }

    return False;
}

if $dbobj {
    if $dbobj.dbswitch == 1 {
        if $subtest_0 && $subtest_1 && $subtest_2 && $subtest_3 && $subtest_4 {
            tstobj.print_statistics;
        }
    }
}
