use v6.d;
use Test;
use lib 'lib';

use Pheix::Model::Database::Compression;
use Compress::Bzip2;

my int $ts = 397556573;
my int $ds = 256000;
my Str $ph = (
    'Тексты - это не энциклопедические и не лингвистические ' ~
    ' системы. Тексты сужают бесконечные или неопределенные ' ~
    'возможности систем и создают закрытый универсум. Системы ' ~
    'редельны, но бесконечны. Тексты - предельны и конечны, хотя ' ~
    'интерпретаций может быть очень много. Источник - Умберто Эко ' ~
    'От интернета к Гуттенбергу: текст и гипертекст - URL: ' ~
    'http://kiev.philosophy.ru/library/eco/internet.html Точность ' ~
    'цитирования - почти дословно ♥ ♥ ♥'
) x 32;

plan 2;

# Basic compression checks
subtest {
    plan 5;

    my $cmpobj = Pheix::Model::Database::Compression.new(:dictsize($ds));

    is $cmpobj.lzw.dictsize, $ds, 'dictsize is set to ' ~ $ds;
    is $cmpobj.compress(:data(Str)), Buf[uint8].new, 'dummy compress';
    is $cmpobj.decompress(:data(Buf[uint8].new)), q{}, 'dummy decompress';

    is(
        $cmpobj.decompress(:data($cmpobj.compress(:data($ph)))),
        $ph,
        'phrase compress/decompress',
    );

    my $r = $cmpobj.get_ratio;

    ok $r > 0, 'compress ratio is set: ' ~ $r ~ '%';
}, 'Basic compression checks';

# Exception on compress
subtest {
    plan 4;

    my $cmpobj = Pheix::Model::Database::Compression.new(:test(True));

    throws-like { $cmpobj.compress(:data($ph)) },
        Exception,
        message => /'compress failure, dumps at'/,
        'throws exception on test attr';

    #dies-ok { $cmpobj.compress(:data($ph)) }, 'died with test';

    my $log = $cmpobj.dumper(:unixtime($ts), :dry);
    my $bin = $cmpobj.dumper(:unixtime($ts), :bin, :dry);

    ok $log.IO.e, 'log file is ok';
    ok $bin.IO.e, 'bin file is ok';

    my $compressed = read_file(:fname($bin), :bin);
    my $plaintext  = read_file(:fname($log));

    is decompressToBlob($compressed).decode, $plaintext.decode, 'binary data is ok';

}, 'Exception on compress';

done-testing;

sub read_file(Str :$fname, Bool :$bin = False) returns Blob {
    my $fhdl = $fname.IO.open(:r, :bin($bin));
    my $file = $fhdl.read;
    $fhdl.close;

    $file;
}
