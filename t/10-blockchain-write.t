use v6.d;
use Test;
use Test::Mock;
use lib 'lib';

use Net::Ethereum;
use Pheix::Model::Database::Blockchain;

my     @farr = <domains ip_addrs browsers resolut pages countries>;
my Str $tab  = 'tst_table';
my Str $tx   = '0x97e92abe8de919f6e7f79105b534690f88704d8dc7c81b805cc3410baad30e1a';
my Str $sc   = '0xe6df2905589db3c09091462f771ed0a3b820017c';
my Str $acc  = '0x9dc4c65c12d81f741ae57ac7abc3a02a342625a4';

my Any $chainobj = Pheix::Model::Database::Blockchain.new(
    :abi(Str),
    :apiurl('http://127.0.0.1:9999'),
    :sctx($tx)
    :fields(@farr),
    :table($tab),
    :account($acc),
    :debug(False),
);

plan 3;

# Check write_blockchain method (undefined transaction gas)
subtest {
    plan 1;

    my $ne = mocked(
        Net::Ethereum,
        returning => {
            contract_method_call_estimate_gas => UInt,
            sendTransaction => $tx,
            retrieve_contract => $sc,
            eth_gasPrice => 0,
            eth_getTransactionCount => 1,
            marshal => '0x0',
        },
        overriding => {
            sendTransaction => -> :$account, :$scid, :$fname, :$fparams, :$gas {
                die 'gas value is invalid ' if $gas == 0 || !$gas.defined;
                die 'gas has no default value' if $gas != $chainobj.gasqty;
                $tx;
            }
        }
    );

    $chainobj.ethobj = $ne;
    $chainobj.set_contract_addr;

    lives-ok {
        my $retcode = $chainobj.write_blockchain(
            :method('remove'),
            :data({
                rowid => 98,
                tabname => 'tst_table'
            }),
            :waittx(False)
        );
    }, 'undefined transaction gas value lives ok and has right value';

}, 'Check write_blockchain method (undefined transaction gas)';

# Check write_blockchain method (null transaction gas)
subtest {
    plan 1;

    my $ne = mocked(
        Net::Ethereum,
        returning => {
            contract_method_call_estimate_gas => UInt,
            sendTransaction => $tx,
            retrieve_contract => $sc,
            eth_gasPrice => 0,
            eth_getTransactionCount => 1,
            marshal => '0x0',
        },
        overriding => {
            sendTransaction => -> :$account, :$scid, :$fname, :$fparams, :$gas {
                die 'gas value is invalid ' if $gas == 0 || !$gas.defined;
                die 'gas has no default value' if $gas != $chainobj.gasqty;
                $tx;
            }
        }
    );

    $chainobj.ethobj = $ne;
    $chainobj.set_contract_addr;

    my $retcode;

    lives-ok {
        $retcode = $chainobj.write_blockchain(
            :method('remove'),
            :data({
                rowid => 98,
                tabname => 'tst_table'
            }),
            :waittx(False)
        );
    }, 'null transaction gas value lives ok and has right value';

}, 'Check write_blockchain method (null transaction gas)';

# Check write_blockchain method (generic behaviour)
subtest {
    my $iter = 100;

    plan $iter;

    if $chainobj {

        while ($iter > 0) {
            my $ne = mocked(
                Net::Ethereum,
                returning => {
                    contract_method_call_estimate_gas => (0..7_000_000).rand.Int, # 117990, # 137745, 109945
                    wait_for_transaction => (1),
                    sendTransaction => $tx,
                    retrieve_contract => $sc,
                    eth_gasPrice => 0,
                    eth_getTransactionCount => 1,
                    marshal => '0x0',
                }
            );

            $chainobj.gaslim = 10_000_000;
            $chainobj.ethobj = $ne;
            $chainobj.set_contract_addr;

            my $retcode = $chainobj.write_blockchain(
                :method('remove'),
                :data({
                    rowid => 98,
                    tabname => 'tst_table'
                }),
                :waittx(False)
            );

            is $retcode<status>, True,
                'write_blockchain is, iter ' ~ sprintf("%04d", $iter);

            $iter--;
        }
    }
    else {
       skip-rest('blockchain object is not available');
    }
}, 'Check write_blockchain method (generic behaviour)';

done-testing;
