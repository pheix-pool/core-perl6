use v6.d;
use Test;
use Test::Mock;
use lib 'lib';

use Pheix::Model::Database::Access;
use Pheix::Test::Blockchain;
use MIME::Base64;
use Pheix::Test::BlockchainComp::Helpers;

constant txwaititers = 120;
constant localtab    = 'blockchain';
constant mb64        = MIME::Base64.new;
constant tstobj      = Pheix::Test::Blockchain.new(:locstorage(localtab));

constant testnet  =
    ((@*ARGS[0].defined && @*ARGS[0] (elem) <ropsten rinkeby goerli sepolia holesky>) || tstobj.testnet) ??
        @*ARGS[0] // tstobj.testnet !!
            localtab;

constant helpobj = Pheix::Test::BlockchainComp::Helpers.new(
    :testnet(testnet),
    :localtab(localtab),
    :tstobj(tstobj)
);

my UInt $dbsz    = 3;
my UInt $unipl   = 20;
my Str  $bstorg  = testnet eq localtab ?? localtab !! testnet ~ '_storage';
my Str  $diagnet = testnet eq localtab ?? 'local PoA net' !! testnet ~ ' net';
my Str  $fstorg  = 'bigbro';
my Str  $etdata  = 'pheix_sample_data';
my      @farr    = <id data compression>;
my      @frow    = <domains ip_addrs browsers resolut pages countries>;

plan 2;

diag('PHEIXTESTENGINE was not set') if !tstobj.pte;

# Check unified access to filechain
subtest {
    my $dbobj = Pheix::Model::Database::Access.new(
        :table($fstorg),
        :fields(@farr),
        :test(True),
        :txwaititers(txwaititers)
    );

    plan $unipl + 1;

    if $dbobj {
        if $dbobj.dbswitch == 0 {
            ok ($dbobj.exists ?? $dbobj.remove_all !! True), 'initial clean up';

            build_chain(:records(generate_filedb), :dbobj($dbobj));
            unified_test(:dbobj($dbobj));
        }
        else {
            skip-rest('file chain is not available');
        }
    }
    else {
       skip-rest('database object is not available');
    }
}, 'Check unified access to filechain';

# Check unified access to blockchain
subtest {
    my $dbobj = Pheix::Model::Database::Access.new(
        :table($bstorg),
        :fields(@farr),
        :test(True),
        :debug(tstobj.debug),
        :txwaititers(txwaititers)
    );

    plan $unipl + 4;

    if $dbobj {
        if $dbobj.dbswitch == 1 {
            $dbobj.chainobj.ethobj.tx_wait_sec =
                tstobj.is_local(:storage(testnet)) ??
                    tstobj.local_wait !!
                        tstobj.public_wait;

            $dbobj.chainobj.nounlk = True if tstobj.is_public(:storage(testnet));
            $dbobj.chainobj.diag   = sub _diag(Str:$m){helpobj.diag(:l(tstobj.logdbg),:m($m))};

            helpobj.diag(:m(sprintf("Smart contract %s v%s", $dbobj.chainobj.scaddr, $dbobj.chainobj.get_smartcontract_ver)));

            if tstobj.pte {
                ok (
                    $dbobj.exists ??
                        $dbobj.chainobj.unlock_account &&
                            $dbobj.remove_all !! $dbobj.chainobj.unlock_account
                ), 'initial clean up';

                todo 'create should be added to Pheix::Model::Database::Access';
                is ($dbobj.chainobj.table_create)<status>, True, 'create table';

                my @records = generate_filedb;

                build_chain(:records(@records), :dbobj($dbobj));
                unified_test(:dbobj($dbobj));

                todo 'possible non-signer mode', 1;
                dies-ok {
                    $dbobj.chainobj.sgnobj.config = { keystore => './t/data/keystore/92745E7e310d18e23384511b50FAd184cB7CF826.keystore' };
                    $dbobj.insert(@records[0]);
                },
                'thrown on local signing with wrong keystore';

                $dbobj.chainobj.sgnlog.map({helpobj.diag(:m($_))}) if tstobj.debug;

                todo 'possible non-signer mode', 1;
                ok helpobj.flush_signing_session(:sgnlog($dbobj.chainobj.sgnlog)), 'save signing log';
            }
            else {
                skip-rest('test should be run via Pheix test engine');
            }
        }
        else {
            skip-rest('block chain is not available');
        }
    }
    else {
       skip-rest('database object is not available');
    }
}, 'Check unified access to blockchain on ' ~ $diagnet;

done-testing;

sub unified_test(Pheix::Model::Database::Access :$dbobj!) returns Int {
    my UInt $db = $dbobj.dbswitch;

    ok $dbobj.modified, 'got modified time';
    ok $dbobj.dbpath, 'got path';
    ok $dbobj.exists, 'table exists';

    is
        $dbobj.get_count(Hash.new),
        $dbsz,
        sprintf("found %d records", $dbsz);

    my @db = $dbobj.get_all;

    is @db.elems, $dbsz, sprintf("database has %d records", $dbsz);

    my UInt $rowid =
        @db[0]{@farr[0]}:exists ??
            @db[0]{@farr[0]}.UInt !!
                ($db == 1 && $dbobj.chainobj.get_max_id ??
                    ($dbobj.chainobj.get_max_id - 1) !! 0);
    my $set_data = {
        @farr[0] => $rowid,
        @farr[1] => $etdata,
        @farr[2] => ~(1),
    };

    ok (($db == 1 ?? $dbobj.chainobj.unlock_account !! True) && $dbobj.set(
        $set_data,
        %(@farr[0] => $rowid)
    )), 'data is set, id=' ~ $rowid;

    if $dbobj.dbswitch == 1 {
        ok $set_data<txhash> ne q{} && $set_data<txhash> ~~ m:i/^ 0x<xdigit>**64 $/, sprintf("transaction hash for set %s", $set_data<txhash>);
    }
    else {
        skip sprintf("no transaction hash for chain type %d", $dbobj.dbswitch), 1;
    }

    @db = $dbobj.get(%(@farr[0] => $rowid));

    is @db[0]{@farr[1]}, $etdata, 'data is get: ' ~ @db.gist;

    skip-rest('database is inconsistent') if not @db[0]{@farr[1]}.defined;

    ok (($db == 1 ?? $dbobj.chainobj.unlock_account !! True) && $dbobj.remove(
        %(@farr[0] => $rowid)
    )), 'data is removed';

    is
        $dbobj.get_all.elems,
        $dbsz - 1,
        sprintf("database has %d records", $dbsz - 1);

    ok ($db == 1 ?? $dbobj.chainobj.unlock_account !! True) && $dbobj.remove_all, 'clean up';

    return 1;
}

sub build_chain(
    :@records!,
    Pheix::Model::Database::Access :$dbobj!
) returns Int {
    my UInt $i = 0;

    for @records -> %r {
        my $row;

        @farr.map({$row{$_} = %r{$_}});

        is
            $row.keys.elems,
            @farr.elems,
            sprintf("record %02d consistency ok", $i + 1);

        $dbobj.chainobj.unlock_account if $dbobj.dbswitch == 1;

        ok $dbobj.insert($row), sprintf("record %02d is inserted", $i + 1);

        if $dbobj.dbswitch == 1 {
            ok $row<txhash> ne q{} && $row<txhash> ~~ m:i/^ 0x<xdigit>**64 $/, sprintf("transaction hash for insert %s", $row<txhash>);
        }
        else {
            skip sprintf("no transaction hash for chain type %d", $dbobj.dbswitch), 1;
        }

        $i++;
    }

    return 1;
}

sub generate_filedb returns List {
    my @db_in_mem;

    for ^$dbsz {
        my %indexes;
        my %row;

        @frow.map({%indexes{$_} = tstobj."$_"().elems.rand.Int});
        @frow.map({%row{$_} = (tstobj."$_"())[%indexes{$_}]});

        helpobj.diag(:m(sprintf("%d: %s", $_, %indexes.gist)));

        @db_in_mem.push(%(
            id          => ($_ + 1),
            data        => mb64.encode-str(%row.keys.sort.map({%row{$_}}).join(q{|}), :oneline),
            compression => ~(0)
        ));
    }

    @db_in_mem;
}
