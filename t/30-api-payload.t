use v6.d;
use Test;
use Test::Mock;
use lib 'lib';

use JSON::Fast;
use MIME::Base64;

use Pheix::Utils;
use Pheix::Test::FastCGI;
use Pheix::Test::Helpers;
use Pheix::Controller::API;
use Pheix::Controller::Basic;

class A {
    has Hash $.c = {};
    has Hash $.p = {};

    method get(:$ctrl) {
        self;
    }

    method B (
        :%match!,
        Str  :$route,
        UInt :$tick!,
        Hash :$sharedobj!,
        Hash :$credentials = {},
        Hash :$payload = {},
    ){
        $!c = $credentials;
        $!p = $payload;

        return {
            payload => $!p,
            credentials => $!c
        };
    }
}

plan 2;

my $apiobj = mocked(
    Pheix::Controller::API,
    returning => {
        match => {
            details => {path => '/api-debug'},
            controller => 'A',
            action     => 'B'
        },
    });

my A $mocked_addom = A.new;

my $fcgi = Pheix::Test::FastCGI.new;

if !Pheix::Test::Helpers.new.check_submodules {
    skip-rest('No Pheix submodules are pulled');
    exit;
}

my $ctrl = Pheix::Controller::Basic.new(
    :apirobj($apiobj),
    :mockedfcgi($fcgi),
    :test(True),
    :addons({A => {objct => $mocked_addom}})
);

subtest {
    plan 3;

    my %match   =
        details    => %(path => '/api-debug'),
        controller => 'A',
        action     => 'B',
    ;

    is $ctrl.sharedobj<fastcgi>.reset, 0, 'reset content array';

    $ctrl.sharedobj<fastcgi>.clen = 5;
    $ctrl.api(:tick(1), :match(%match));
    my ($hdr, $cnt) = $ctrl.sharedobj<fastcgi>.fetch.split("\n\n", 2);

    my %parsed_cnt = from-json($cnt);

    is-deeply $mocked_addom.c, %parsed_cnt<content><credentials>, 'valid populated credentials';
    is-deeply $mocked_addom.p, %parsed_cnt<content><payload>, 'valid populated payload';
}, 'Check populated payload params';

subtest {
    plan 3;

    my %match   =
        details    => %(path => '/api-debug'),
        controller => 'A',
        action     => 'B',
    ;

    is $ctrl.sharedobj<fastcgi>.reset, 0, 'reset content array';

    $ctrl.sharedobj<fastcgi>.clen = 1;
    $ctrl.api(:tick(1), :match(%match));
    my ($hdr, $cnt) = $ctrl.sharedobj<fastcgi>.fetch.split("\n\n", 2);

    my %parsed_cnt = from-json($cnt);

    is-deeply $mocked_addom.c, %parsed_cnt<content><credentials>, 'valid token as credentials';
    is-deeply $mocked_addom.p, {}, 'valid blank payload';
}, 'Check populated blank params';

done-testing;
