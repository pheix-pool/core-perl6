use v6.d;
use Test;
use lib 'lib';

plan 1;

use Pheix::View::Debug;

my Pheix::View::Debug $obj = Pheix::View::Debug.new;

# Check debug
subtest {
    plan 4;

    my Str $path = q{};
    my Str $env  = $obj.show_env_vars;

    ok $env, 'show_env_variables() returns non-empty value';

    ok $env ~~ m/ \%\*ENV\{PATH\} /, '%*ENV{PATH} is found';

    if $env ~~ m/ '%*ENV{PATH} = ' ( . ** 1..* ) '<br>' / {
        $path = $0.Str;
    }

    ok $path, 'PATH is not empty';

    nok $obj.log(:entry('hello')), 'log method with no shared obj'
}, 'Check debug';

done-testing;
